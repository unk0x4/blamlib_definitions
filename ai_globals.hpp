/* ---------- enums */


/* ---------- structures */

struct s_ai_globals_datum_gravemind_property_block;
struct s_tag_reference_block;
struct s_ai_globals_datum_squad_template;
struct s_ai_globals_datum;
struct s_ai_globals;

struct s_ai_globals
{
    c_tag_block<s_ai_globals_datum> data;
};
static_assert(sizeof(s_ai_globals) == 0xC);

struct s_ai_globals_datum
{
    float infantry_on_ai_weapon_damage_scale;
    float vehicle_on_ai_weapon_damage_scale;
    float player_on_ai_weapon_damage_scale;
    float danger_broadly_facing;
    float danger_shooting_near;
    float danger_shooting_at;
    float danger_extremely_close;
    float danger_shield_damage;
    float danger_extended_shield_damage;
    float danger_body_damage;
    float danger_extended_body_damage;
    s_tag_reference global_dialogue;
    string_id default_mission_dialogue_sound_effect;
    float jump_down;
    float jump_step;
    float jump_crouch;
    float jump_stand;
    float jump_storey;
    float jump_tower;
    float max_jump_down_height_down;
    float max_jump_down_height_step;
    float max_jump_down_height_crouch;
    float max_jump_down_height_stand;
    float max_jump_down_height_storey;
    float max_jump_down_height_tower;
    real_bounds hoist_step;
    real_bounds hoist_crouch;
    real_bounds hoist_stand;
    real_bounds vault_step;
    real_bounds vault_crouch;
    float search_range_infantry;
    float search_range_flying;
    float search_range_vehicle;
    float search_range_giant;
    c_tag_block<s_ai_globals_datum_gravemind_property_block> gravemind_properties;
    float scary_target_threshold;
    float scary_weapon_threshold;
    float player_scariness;
    float berserking_actor_scariness;
    float kamikazeing_actor_scariness;
    float invincible_actor_scariness;
    float minimum_death_time;
    float projectile_distance;
    float idle_clump_distance;
    float dangerous_clump_distance;
    float conver_search_duration;
    float task_search_duration;
    float unknown7;
    c_tag_block<s_tag_reference_block> styles;
    c_tag_block<s_tag_reference_block> formations;
    c_tag_block<s_ai_globals_datum_squad_template> squad_templates;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
};
static_assert(sizeof(s_ai_globals_datum) == 0x144);

struct s_ai_globals_datum_squad_template
{
    s_tag_reference template;
};
static_assert(sizeof(s_ai_globals_datum_squad_template) == 0x10);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

struct s_ai_globals_datum_gravemind_property_block
{
    float minimum_retreat_time;
    float ideal_retreat_time;
    float maximum_retreat_time;
};
static_assert(sizeof(s_ai_globals_datum_gravemind_property_block) == 0xC);

