/* ---------- enums */

enum e_shield_impact_flags
{
    _shield_impact_flags_render_first_person_bit = 0,
    _shield_impact_flags_dont_render_third_person_bit = 1,
    _shield_impact_flags_unused_bit = 2,
    _shield_impact_flags_dont_render_as_effect_bit = 3
};


/* ---------- structures */

struct s_shield_impact_shield_intensity_block;
struct s_tag_function;
struct s_shield_impact_function;
struct s_shield_impact_shield_edge_block;
struct s_shield_impact_plasma_block;
struct s_shield_impact_extrusion_oscillation_block;
struct s_shield_impact_hit_response_block;
struct s_shield_impact;

struct s_shield_impact
{
    c_flags<e_shield_impact_flags, ushort> flags;
    short version;
    s_shield_impact_shield_intensity_block shield_intensity;
    s_shield_impact_shield_edge_block shield_edge;
    s_shield_impact_plasma_block plasma;
    s_shield_impact_extrusion_oscillation_block extrusion_oscillation;
    s_shield_impact_hit_response_block hit_response;
    real_quaternion edge_scales;
    real_quaternion edge_offsets;
    real_quaternion plasma_scales;
    real_quaternion depth_fade_parameters;
};
static_assert(sizeof(s_shield_impact) == 0x1E4);

struct s_shield_impact_hit_response_block
{
    float hit_time;
    s_shield_impact_function hit_color;
    s_shield_impact_function hit_intensity;
};
static_assert(sizeof(s_shield_impact_hit_response_block) == 0x3C);

struct s_shield_impact_extrusion_oscillation_block
{
    s_tag_reference oscillation_bitmap1;
    s_tag_reference oscillation_bitmap2;
    float oscillation_tiling_scale;
    float oscillation_scroll_speed;
    s_shield_impact_function extrusion_amount;
    s_shield_impact_function oscillation_amplitude;
};
static_assert(sizeof(s_shield_impact_extrusion_oscillation_block) == 0x60);

struct s_shield_impact_plasma_block
{
    float plasma_depth_fade_range;
    s_tag_reference plasma_noise_bitmap1;
    s_tag_reference plasma_noise_bitmap2;
    float tiling_scale;
    float scroll_speed;
    float edge_sharpness;
    float center_sharpness;
    float plasma_outer_fade_radius;
    float plasma_center_radius;
    float plasma_inner_fade_radius;
    s_shield_impact_function plasma_center_color;
    s_shield_impact_function plasma_center_intensity;
    s_shield_impact_function plasma_edge_color;
    s_shield_impact_function plasma_edge_intensity;
};
static_assert(sizeof(s_shield_impact_plasma_block) == 0xB0);

struct s_shield_impact_shield_edge_block
{
    float one_if_overshield;
    float depth_fade_range;
    float outer_fade_radius;
    float center_radius;
    float inner_fade_radius;
    s_shield_impact_function edge_glow_color;
    s_shield_impact_function edge_glow_intensity;
};
static_assert(sizeof(s_shield_impact_shield_edge_block) == 0x4C);

struct s_shield_impact_function
{
    string_id input_variable;
    string_id range_variable;
    s_tag_function function;
};
static_assert(sizeof(s_shield_impact_function) == 0x1C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_shield_impact_shield_intensity_block
{
    float recent_damage_intensity;
    float current_damage_intensity;
};
static_assert(sizeof(s_shield_impact_shield_intensity_block) == 0x8);

