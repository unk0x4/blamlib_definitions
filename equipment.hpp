/* ---------- enums */

enum e_equipment_equipment_flag_bits
{
    _equipment_equipment_flag_bits_pathfinding_obstacle_bit = 0,
    _equipment_equipment_flag_bits_equipment_is_dangerous_to_ai_bit = 1,
    _equipment_equipment_flag_bits_never_dropped_by_ai_bit = 2,
    _equipment_equipment_flag_bits_protects_parent_from_aoe_bit = 3,
    _equipment_equipment_flag_bits_third_person_camera_always_bit = 4,
    _equipment_equipment_flag_bits_use_forced_primary_change_color_bit = 5,
    _equipment_equipment_flag_bits_use_forced_secondary_change_color_bit = 6,
    _equipment_equipment_flag_bits_can_not_be_picked_up_by_player_bit = 7,
    _equipment_equipment_flag_bits_is_removed_from_world_on_deactivation_bit = 8,
    _equipment_equipment_flag_bits_not_dropped_by_player_bit = 9,
    _equipment_equipment_flag_bits_is_dropped_by_ai_bit = 10
};

enum e_equipment_optional_unit_camera_block_flag_bits
{
    _equipment_optional_unit_camera_block_flag_bits_pitch_bounds_absolute_space_bit = 0,
    _equipment_optional_unit_camera_block_flag_bits_only_collides_with_environment_bit = 1,
    _equipment_optional_unit_camera_block_flag_bits_hides_player_unit_from_camera_bit = 2,
    _equipment_optional_unit_camera_block_flag_bits_use_aiming_vector_instead_of_marker_forward_bit = 3
};

enum e_equipment_multiplayer_powerup_block_flavor
{
    _equipment_multiplayer_powerup_block_flavor_red_powerup = 0,
    _equipment_multiplayer_powerup_block_flavor_blue_powerup = 1,
    _equipment_multiplayer_powerup_block_flavor_yellow_powerup = 2,
    _equipment_multiplayer_powerup_block_flavor_custom_powerup = 3
};

enum e_equipment_spawner_block_type
{
    _equipment_spawner_block_type_along_aiming_vector = 0,
    _equipment_spawner_block_type_camera_pos_z_plane = 1,
    _equipment_spawner_block_type_foot_pos_z_plane = 2
};


/* ---------- structures */

struct s_equipment_optional_unit_camera_block_camera_track;
struct s_equipment_optional_unit_camera_block_camera_acceleration_block;
struct s_equipment_optional_unit_camera_block;
struct s_equipment_health_pack_block;
struct s_equipment_multiplayer_powerup_block;
struct s_equipment_spawner_block;
struct s_equipment_proximity_mine_block;
struct s_equipment_motion_tracker_noise_block;
struct s_equipment_invisibility_block;
struct s_equipment_invincibility_block;
struct s_equipment_regenerator_block;
struct s_equipment_new_health_pack_block;
struct s_equipment_forced_reload_block;
struct s_equipment_concussive_blast_block;
struct s_equipment_tank_mode_block;
struct s_equipment_mag_pulse_block;
struct s_equipment_hologram_block;
struct s_equipment_reactive_armor_block;
struct s_equipment_bomb_run_block;
struct s_equipment_armor_lock_block;
struct s_equipment_adrenaline_block;
struct s_equipment_lightning_strike_block;
struct s_equipment_scrambler_block;
struct s_equipment_weapon_jammer_block;
struct s_equipment_ammo_pack_block_weapon;
struct s_equipment_ammo_pack_block;
struct s_equipment_vision_block;
struct s_equipment;

struct s_equipment : s_item
{
    float use_duration;
    float activation_delay;
    short number_of_uses;
    c_flags<e_equipment_equipment_flag_bits, ushort> equipment_flags;
    float unknown9;
    float unknown10;
    float unknown11;
    c_tag_block<s_equipment_optional_unit_camera_block> override_camera;
    c_tag_block<s_equipment_health_pack_block> health_pack;
    c_tag_block<s_equipment_multiplayer_powerup_block> multiplayer_powerup;
    c_tag_block<s_equipment_spawner_block> spawner;
    c_tag_block<s_equipment_proximity_mine_block> proximity_mine;
    c_tag_block<s_equipment_motion_tracker_noise_block> motion_tracker_noise;
    byte unused[12];
    c_tag_block<s_equipment_invisibility_block> invisibility;
    c_tag_block<s_equipment_invincibility_block> invincibility;
    c_tag_block<s_equipment_regenerator_block> regenerator;
    c_tag_block<s_equipment_new_health_pack_block> new_health_pack;
    c_tag_block<s_equipment_forced_reload_block> forced_reload;
    c_tag_block<s_equipment_concussive_blast_block> concussive_blast;
    c_tag_block<s_equipment_tank_mode_block> tank_mode;
    c_tag_block<s_equipment_mag_pulse_block> mag_pulse;
    c_tag_block<s_equipment_hologram_block> hologram;
    c_tag_block<s_equipment_reactive_armor_block> reactive_armor;
    c_tag_block<s_equipment_bomb_run_block> bomb_run;
    c_tag_block<s_equipment_armor_lock_block> armor_lock;
    c_tag_block<s_equipment_adrenaline_block> adrenaline;
    c_tag_block<s_equipment_lightning_strike_block> lightning_strike;
    c_tag_block<s_equipment_scrambler_block> scrambler;
    c_tag_block<s_equipment_weapon_jammer_block> weapon_jammer;
    c_tag_block<s_equipment_ammo_pack_block> ammo_pack;
    c_tag_block<s_equipment_vision_block> vision;
    s_tag_reference hud_interface;
    s_tag_reference pickup_sound;
    s_tag_reference empty_sound;
    s_tag_reference activation_effect;
    s_tag_reference active_effect;
    s_tag_reference deactivation_effect;
    string_id enter_animation;
    string_id idle_animation;
    string_id exit_animation;
};
static_assert(sizeof(s_equipment) == 0x384);

struct s_equipment_vision_block
{
    s_tag_reference screen_effect;
    s_tag_reference damage_response;
};
static_assert(sizeof(s_equipment_vision_block) == 0x20);

struct s_equipment_ammo_pack_block
{
    ulong unknown;
    long unknown2;
    long unknown3;
    ulong unknown4;
    long unknown5;
    long unknown6;
    c_tag_block<s_equipment_ammo_pack_block_weapon> weapons;
    s_tag_reference unknown7;
};
static_assert(sizeof(s_equipment_ammo_pack_block) == 0x34);

struct s_equipment_ammo_pack_block_weapon
{
    string_id name;
    s_tag_reference weapon_object;
    long unknown;
};
static_assert(sizeof(s_equipment_ammo_pack_block_weapon) == 0x18);

struct s_equipment_weapon_jammer_block
{
    ulong unknown;
    s_tag_reference unknown2;
    long unknown3;
    long unknown4;
    long unknown5;
    long unknown6;
};
static_assert(sizeof(s_equipment_weapon_jammer_block) == 0x24);

struct s_equipment_scrambler_block
{
    ulong unknown;
    s_tag_reference unknown2;
    long unknown3;
    long unknown4;
    long unknown5;
    long unknown6;
};
static_assert(sizeof(s_equipment_scrambler_block) == 0x24);

struct s_equipment_lightning_strike_block
{
    ulong melee_time_reduction;
    s_tag_reference unknown_effect;
};
static_assert(sizeof(s_equipment_lightning_strike_block) == 0x14);

struct s_equipment_adrenaline_block
{
    float unknown;
    s_tag_reference activation_effect;
    s_tag_reference unknown3;
};
static_assert(sizeof(s_equipment_adrenaline_block) == 0x24);

struct s_equipment_armor_lock_block
{
    s_tag_reference collision_damage;
    s_tag_reference unknown_collision_damage;
};
static_assert(sizeof(s_equipment_armor_lock_block) == 0x20);

struct s_equipment_bomb_run_block
{
    long unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    s_tag_reference projectile;
    s_tag_reference throw_sound;
};
static_assert(sizeof(s_equipment_bomb_run_block) == 0x34);

struct s_equipment_reactive_armor_block
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    s_tag_reference unknown4;
    s_tag_reference unknown5;
    s_tag_reference unknown6;
    s_tag_reference unknown7;
};
static_assert(sizeof(s_equipment_reactive_armor_block) == 0x4C);

struct s_equipment_hologram_block
{
    ulong unknown;
    s_tag_reference active_effect;
    s_tag_reference unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    s_tag_reference death_effect;
    ulong unknown6;
    ulong unknown7;
    s_tag_data function;
    s_tag_reference nav_point_hud;
};
static_assert(sizeof(s_equipment_hologram_block) == 0x6C);

struct s_equipment_mag_pulse_block
{
    s_tag_reference unknown;
    s_tag_reference unknown2;
    s_tag_reference unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_equipment_mag_pulse_block) == 0x34);

struct s_equipment_tank_mode_block
{
    string_id new_player_material;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    s_tag_reference active_hud;
};
static_assert(sizeof(s_equipment_tank_mode_block) == 0x28);

struct s_equipment_concussive_blast_block
{
    s_tag_reference unknown;
    s_tag_reference unknown2;
};
static_assert(sizeof(s_equipment_concussive_blast_block) == 0x20);

struct s_equipment_forced_reload_block
{
    s_tag_reference effect;
    ulong unknown;
};
static_assert(sizeof(s_equipment_forced_reload_block) == 0x14);

struct s_equipment_new_health_pack_block
{
    float health_given;
    float shields_given;
};
static_assert(sizeof(s_equipment_new_health_pack_block) == 0x8);

struct s_equipment_regenerator_block
{
    s_tag_reference regenerating_effect;
};
static_assert(sizeof(s_equipment_regenerator_block) == 0x10);

struct s_equipment_invincibility_block
{
    string_id new_player_material;
    short new_player_material_global_index;
    short unknown;
    float unknown2;
    s_tag_reference activation_effect;
    s_tag_reference active_effect;
};
static_assert(sizeof(s_equipment_invincibility_block) == 0x2C);

struct s_equipment_invisibility_block
{
    float invisibility_duration;
    float invisibility_fade_time;
};
static_assert(sizeof(s_equipment_invisibility_block) == 0x8);

struct s_equipment_motion_tracker_noise_block
{
    float arm_time;
    float noise_radius;
    long noise_count;
    float flash_radius;
};
static_assert(sizeof(s_equipment_motion_tracker_noise_block) == 0x10);

struct s_equipment_proximity_mine_block
{
    s_tag_reference explosion_effect;
    s_tag_reference explosion_damage_effect;
    float arm_time;
    float self_destruct_time;
    float trigger_time;
    float trigger_velocity;
};
static_assert(sizeof(s_equipment_proximity_mine_block) == 0x30);

struct s_equipment_spawner_block
{
    s_tag_reference spawned_object;
    s_tag_reference spawned_effect;
    float spawn_radius;
    float spawn_z_offset;
    float spawn_area_radius;
    float spawn_velocity;
    c_enum<e_equipment_spawner_block_type, short> type;
    byte unused[2];
};
static_assert(sizeof(s_equipment_spawner_block) == 0x34);

struct s_equipment_multiplayer_powerup_block
{
    c_enum<e_equipment_multiplayer_powerup_block_flavor, long> flavor;
};
static_assert(sizeof(s_equipment_multiplayer_powerup_block) == 0x4);

struct s_equipment_health_pack_block
{
    ulong unknown;
    ulong unknown2;
    float shields_given;
    s_tag_reference unknown3;
    s_tag_reference unknown4;
    s_tag_reference unknown5;
};
static_assert(sizeof(s_equipment_health_pack_block) == 0x3C);

struct s_equipment_optional_unit_camera_block
{
    c_flags<e_equipment_optional_unit_camera_block_flag_bits, ushort> flags;
    byte unused[2];
    string_id camera_marker_name;
    string_id camera_submerged_marker_name;
    real pitch_auto_level;
    real_bounds pitch_range;
    c_tag_block<s_equipment_optional_unit_camera_block_camera_track> camera_tracks;
    real unknown2;
    real unknown3;
    real unknown4;
    c_tag_block<s_equipment_optional_unit_camera_block_camera_acceleration_block> camera_acceleration;
};
static_assert(sizeof(s_equipment_optional_unit_camera_block) == 0x3C);

struct s_equipment_optional_unit_camera_block_camera_acceleration_block
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
};
static_assert(sizeof(s_equipment_optional_unit_camera_block_camera_acceleration_block) == 0x4C);

struct s_equipment_optional_unit_camera_block_camera_track
{
    s_tag_reference track;
};
static_assert(sizeof(s_equipment_optional_unit_camera_block_camera_track) == 0x10);

