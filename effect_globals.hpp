/* ---------- enums */


/* ---------- structures */

struct s_effect_globals_unknown_block_unknown_block2;
struct s_effect_globals_unknown_block;
struct s_effect_globals;

struct s_effect_globals
{
    c_tag_block<s_effect_globals_unknown_block> unknown;
};
static_assert(sizeof(s_effect_globals) == 0xC);

struct s_effect_globals_unknown_block
{
    ulong unknown;
    ulong unknown2;
    c_tag_block<s_effect_globals_unknown_block_unknown_block2> unknown3;
};
static_assert(sizeof(s_effect_globals_unknown_block) == 0x14);

struct s_effect_globals_unknown_block_unknown_block2
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_effect_globals_unknown_block_unknown_block2) == 0x10);

