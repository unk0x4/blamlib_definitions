/* ---------- enums */


/* ---------- structures */

struct s_new_cinematic_lighting_unknown_block;
struct s_new_cinematic_lighting_lighting_block;
struct s_new_cinematic_lighting;

struct s_new_cinematic_lighting
{
    c_tag_block<s_new_cinematic_lighting_unknown_block> unknown1;
    c_tag_block<s_new_cinematic_lighting_lighting_block> lighting;
    float unknown2;
};
static_assert(sizeof(s_new_cinematic_lighting) == 0x1C);

struct s_new_cinematic_lighting_lighting_block
{
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
    s_tag_reference light;
};
static_assert(sizeof(s_new_cinematic_lighting_lighting_block) == 0x20);

struct s_new_cinematic_lighting_unknown_block
{
    long unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
};
static_assert(sizeof(s_new_cinematic_lighting_unknown_block) == 0x20);

