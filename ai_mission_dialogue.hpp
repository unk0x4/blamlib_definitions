/* ---------- enums */


/* ---------- structures */

struct s_ai_mission_dialogue_line_variant;
struct s_ai_mission_dialogue_line;
struct s_ai_mission_dialogue;

struct s_ai_mission_dialogue
{
    c_tag_block<s_ai_mission_dialogue_line> lines;
};
static_assert(sizeof(s_ai_mission_dialogue) == 0xC);

struct s_ai_mission_dialogue_line
{
    string_id name;
    c_tag_block<s_ai_mission_dialogue_line_variant> variants;
    string_id default_sound_effect;
};
static_assert(sizeof(s_ai_mission_dialogue_line) == 0x14);

struct s_ai_mission_dialogue_line_variant
{
    string_id designation;
    s_tag_reference sound;
    string_id sound_effect;
};
static_assert(sizeof(s_ai_mission_dialogue_line_variant) == 0x18);

