/* ---------- enums */

enum e_game_engine_settings_definition_flags
{
    _game_engine_settings_definition_flags_unused_bit = 0
};

enum e_game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance
{
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_unchanged = 0,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_10 = 1,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_50 = 2,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_90 = 3,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_100 = 4,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_110 = 5,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_150 = 6,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_200 = 7,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_300 = 8,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_500 = 9,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_1000 = 10,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_2000 = 11,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance_invulnerable = 12
};

enum e_game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier
{
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier_unchanged = 0,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier_no_shields = 1,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier_normal_shields = 2,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier_2_x_overshields = 3,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier_3_x_overshields = 4,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier_4_x_overshields = 5
};

enum e_game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate
{
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_unchanged = 0,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_25 = 1,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_10 = 2,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_5 = 3,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_0 = 4,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_50 = 5,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_90 = 6,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_100 = 7,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_110 = 8,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate_200 = 9
};

enum e_game_engine_settings_definition_trait_profile_shields_and_health_block_headshot_immunity
{
    _game_engine_settings_definition_trait_profile_shields_and_health_block_headshot_immunity_unchanged = 0,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_headshot_immunity_enabled = 1,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_headshot_immunity_disabled = 2
};

enum e_game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism
{
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism_unchanged = 0,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism_disabled = 1,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism_10 = 2,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism_25 = 3,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism_50 = 4,
    _game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism_100 = 5
};

enum e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier
{
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_unchanged = 0,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_0 = 1,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_25 = 2,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_50 = 3,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_75 = 4,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_90 = 5,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_100 = 6,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_110 = 7,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_125 = 8,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_150 = 9,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_200 = 10,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_300 = 11,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier_instant_kill = 12
};

enum e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_regeneration
{
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_regeneration_unchanged = 0,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_regeneration_enabled = 1,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_regeneration_disabled = 2
};

enum e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_weapon_pickup
{
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_weapon_pickup_unchanged = 0,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_weapon_pickup_enabled = 1,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_weapon_pickup_disabled = 2
};

enum e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_infinite_ammo
{
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_infinite_ammo_unchanged = 0,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_infinite_ammo_disabled = 1,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_infinite_ammo_enabled = 2
};

enum e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_count
{
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_count_unchanged = 0,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_count_map_default = 1,
    _game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_count_none = 2
};

enum e_game_engine_settings_definition_trait_profile_movement_block_player_speed
{
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_unchanged = 0,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_25 = 1,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_50 = 2,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_75 = 3,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_90 = 4,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_100 = 5,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_110 = 6,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_125 = 7,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_150 = 8,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_200 = 9,
    _game_engine_settings_definition_trait_profile_movement_block_player_speed_300 = 10
};

enum e_game_engine_settings_definition_trait_profile_movement_block_player_gravity
{
    _game_engine_settings_definition_trait_profile_movement_block_player_gravity_unchanged = 0,
    _game_engine_settings_definition_trait_profile_movement_block_player_gravity_50 = 1,
    _game_engine_settings_definition_trait_profile_movement_block_player_gravity_75 = 2,
    _game_engine_settings_definition_trait_profile_movement_block_player_gravity_100 = 3,
    _game_engine_settings_definition_trait_profile_movement_block_player_gravity_150 = 4,
    _game_engine_settings_definition_trait_profile_movement_block_player_gravity_200 = 5
};

enum e_game_engine_settings_definition_trait_profile_movement_block_vehicle_use
{
    _game_engine_settings_definition_trait_profile_movement_block_vehicle_use_unchanged = 0,
    _game_engine_settings_definition_trait_profile_movement_block_vehicle_use_none = 1,
    _game_engine_settings_definition_trait_profile_movement_block_vehicle_use_passenger_only = 2,
    _game_engine_settings_definition_trait_profile_movement_block_vehicle_use_full_use = 3
};

enum e_game_engine_settings_definition_trait_profile_appearance_block_active_camo
{
    _game_engine_settings_definition_trait_profile_appearance_block_active_camo_unchanged = 0,
    _game_engine_settings_definition_trait_profile_appearance_block_active_camo_disabled = 1,
    _game_engine_settings_definition_trait_profile_appearance_block_active_camo_bad_camo = 2,
    _game_engine_settings_definition_trait_profile_appearance_block_active_camo_poor_camo = 3,
    _game_engine_settings_definition_trait_profile_appearance_block_active_camo_good_camo = 4
};

enum e_game_engine_settings_definition_trait_profile_appearance_block_waypoint
{
    _game_engine_settings_definition_trait_profile_appearance_block_waypoint_unchanged = 0,
    _game_engine_settings_definition_trait_profile_appearance_block_waypoint_none = 1,
    _game_engine_settings_definition_trait_profile_appearance_block_waypoint_visible_to_allies = 2,
    _game_engine_settings_definition_trait_profile_appearance_block_waypoint_visible_to_everyone = 3
};

enum e_game_engine_settings_definition_trait_profile_appearance_block_aura
{
    _game_engine_settings_definition_trait_profile_appearance_block_aura_unchanged = 0,
    _game_engine_settings_definition_trait_profile_appearance_block_aura_disabled = 1,
    _game_engine_settings_definition_trait_profile_appearance_block_aura_team = 2,
    _game_engine_settings_definition_trait_profile_appearance_block_aura_black = 3,
    _game_engine_settings_definition_trait_profile_appearance_block_aura_white = 4
};

enum e_game_engine_settings_definition_trait_profile_appearance_block_forced_color
{
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_unchanged = 0,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_off = 1,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_red = 2,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_blue = 3,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_green = 4,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_orange = 5,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_purple = 6,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_gold = 7,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_brown = 8,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_pink = 9,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_white = 10,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_black = 11,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_zombie = 12,
    _game_engine_settings_definition_trait_profile_appearance_block_forced_color_pink_unused = 13
};

enum e_game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode
{
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode_unchanged = 0,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode_disabled = 1,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode_ally_movement = 2,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode_player_movement = 3,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode_player_locations = 4
};

enum e_game_engine_settings_definition_trait_profile_sensor_motion_tracker_range
{
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_unchanged = 0,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_10_m = 1,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_15_m = 2,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_25_m = 3,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_50_m = 4,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_75_m = 5,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_100_m = 6,
    _game_engine_settings_definition_trait_profile_sensor_motion_tracker_range_150_m = 7
};

enum e_game_engine_settings_definition_base_variant_general_setting_flags
{
    _game_engine_settings_definition_base_variant_general_setting_flags_teams_enabled_bit = 0,
    _game_engine_settings_definition_base_variant_general_setting_flags_round_resets_players_bit = 1,
    _game_engine_settings_definition_base_variant_general_setting_flags_round_resets_map_bit = 2
};

enum e_game_engine_settings_definition_base_variant_general_setting_round_resets
{
    _game_engine_settings_definition_base_variant_general_setting_round_resets_nothing = 0,
    _game_engine_settings_definition_base_variant_general_setting_round_resets_players_only = 1,
    _game_engine_settings_definition_base_variant_general_setting_round_resets_everything = 2
};

enum e_game_engine_settings_definition_base_variant_respawn_setting_flags
{
    _game_engine_settings_definition_base_variant_respawn_setting_flags_inherit_respawn_time_bit = 0,
    _game_engine_settings_definition_base_variant_respawn_setting_flags_respawn_with_team_bit = 1,
    _game_engine_settings_definition_base_variant_respawn_setting_flags_respawn_at_location_bit = 2,
    _game_engine_settings_definition_base_variant_respawn_setting_flags_respawn_on_kills_bit = 3
};

enum e_game_engine_settings_definition_base_variant_social_setting_flags
{
    _game_engine_settings_definition_base_variant_social_setting_flags_observers_enabled_bit = 0,
    _game_engine_settings_definition_base_variant_social_setting_flags_team_changing_enabled_bit = 1,
    _game_engine_settings_definition_base_variant_social_setting_flags_balanced_team_changing_bit = 2,
    _game_engine_settings_definition_base_variant_social_setting_flags_friendly_fire_enabled_bit = 3,
    _game_engine_settings_definition_base_variant_social_setting_flags_betrayal_booting_enabled_bit = 4,
    _game_engine_settings_definition_base_variant_social_setting_flags_enemy_voice_enabled_bit = 5,
    _game_engine_settings_definition_base_variant_social_setting_flags_open_channel_voice_enabled_bit = 6,
    _game_engine_settings_definition_base_variant_social_setting_flags_dead_player_voice_enabled_bit = 7
};

enum e_game_engine_settings_definition_base_variant_map_override_flags
{
    _game_engine_settings_definition_base_variant_map_override_flags_grenades_on_map_bit = 0,
    _game_engine_settings_definition_base_variant_map_override_flags_indestructable_vehicles_bit = 1
};

enum e_game_engine_settings_definition_slayer_variant_team_scoring
{
    _game_engine_settings_definition_slayer_variant_team_scoring_sum_of_team = 0,
    _game_engine_settings_definition_slayer_variant_team_scoring_minimum_score = 1,
    _game_engine_settings_definition_slayer_variant_team_scoring_maximum_score = 2,
    _game_engine_settings_definition_slayer_variant_team_scoring_default = 3
};

enum e_game_engine_settings_definition_oddball_variant_flags
{
    _game_engine_settings_definition_oddball_variant_flags_autopickup_enabled_bit = 0,
    _game_engine_settings_definition_oddball_variant_flags_ball_effect_enabled_bit = 1
};

enum e_game_engine_settings_definition_oddball_variant_team_scoring
{
    _game_engine_settings_definition_oddball_variant_team_scoring_sum_of_team = 0,
    _game_engine_settings_definition_oddball_variant_team_scoring_minimum_score = 1,
    _game_engine_settings_definition_oddball_variant_team_scoring_maximum_score = 2,
    _game_engine_settings_definition_oddball_variant_team_scoring_default = 3
};

enum e_game_engine_settings_definition_capture_the_flag_variant_flags
{
    _game_engine_settings_definition_capture_the_flag_variant_flags_flag_at_home_to_score_bit = 0
};

enum e_game_engine_settings_definition_capture_the_flag_variant_home_flag_waypoint
{
    _game_engine_settings_definition_capture_the_flag_variant_home_flag_waypoint_unknown1 = 0,
    _game_engine_settings_definition_capture_the_flag_variant_home_flag_waypoint_unknown2 = 1,
    _game_engine_settings_definition_capture_the_flag_variant_home_flag_waypoint_unknown3 = 2,
    _game_engine_settings_definition_capture_the_flag_variant_home_flag_waypoint_not_in_single = 3
};

enum e_game_engine_settings_definition_capture_the_flag_variant_game_mode
{
    _game_engine_settings_definition_capture_the_flag_variant_game_mode_multiple = 0,
    _game_engine_settings_definition_capture_the_flag_variant_game_mode_single = 1,
    _game_engine_settings_definition_capture_the_flag_variant_game_mode_neutral = 2
};

enum e_game_engine_settings_definition_capture_the_flag_variant_respawn_on_capture
{
    _game_engine_settings_definition_capture_the_flag_variant_respawn_on_capture_disabled = 0,
    _game_engine_settings_definition_capture_the_flag_variant_respawn_on_capture_on_ally_capture = 1,
    _game_engine_settings_definition_capture_the_flag_variant_respawn_on_capture_on_enemy_capture = 2,
    _game_engine_settings_definition_capture_the_flag_variant_respawn_on_capture_on_any_capture = 3
};

enum e_game_engine_settings_definition_assault_variant_flags
{
    _game_engine_settings_definition_assault_variant_flags_reset_on_disarm_bit = 0
};

enum e_game_engine_settings_definition_assault_variant_respawn_on_capture
{
    _game_engine_settings_definition_assault_variant_respawn_on_capture_disabled = 0,
    _game_engine_settings_definition_assault_variant_respawn_on_capture_on_ally_capture = 1,
    _game_engine_settings_definition_assault_variant_respawn_on_capture_on_enemy_capture = 2,
    _game_engine_settings_definition_assault_variant_respawn_on_capture_on_any_capture = 3
};

enum e_game_engine_settings_definition_assault_variant_game_mode
{
    _game_engine_settings_definition_assault_variant_game_mode_multiple = 0,
    _game_engine_settings_definition_assault_variant_game_mode_single = 1,
    _game_engine_settings_definition_assault_variant_game_mode_neutral = 2
};

enum e_game_engine_settings_definition_assault_variant_enemy_bomb_waypoint
{
    _game_engine_settings_definition_assault_variant_enemy_bomb_waypoint_unknown1 = 0,
    _game_engine_settings_definition_assault_variant_enemy_bomb_waypoint_unknown2 = 1,
    _game_engine_settings_definition_assault_variant_enemy_bomb_waypoint_unknown3 = 2,
    _game_engine_settings_definition_assault_variant_enemy_bomb_waypoint_not_in_single = 3
};

enum e_game_engine_settings_definition_infection_variant_flags
{
    _game_engine_settings_definition_infection_variant_flags_respawn_on_haven_move_bit = 0
};

enum e_game_engine_settings_definition_infection_variant_safe_havens
{
    _game_engine_settings_definition_infection_variant_safe_havens_none = 0,
    _game_engine_settings_definition_infection_variant_safe_havens_random = 1,
    _game_engine_settings_definition_infection_variant_safe_havens_sequence = 2
};

enum e_game_engine_settings_definition_infection_variant_next_zombie
{
    _game_engine_settings_definition_infection_variant_next_zombie_most_points = 0,
    _game_engine_settings_definition_infection_variant_next_zombie_first_infected = 1,
    _game_engine_settings_definition_infection_variant_next_zombie_unchanged = 2,
    _game_engine_settings_definition_infection_variant_next_zombie_random = 3
};

enum e_game_engine_settings_definition_king_of_the_hill_variant_flags
{
    _game_engine_settings_definition_king_of_the_hill_variant_flags_opaque_hill_bit = 0
};

enum e_game_engine_settings_definition_king_of_the_hill_variant_team_scoring
{
    _game_engine_settings_definition_king_of_the_hill_variant_team_scoring_sum = 0,
    _game_engine_settings_definition_king_of_the_hill_variant_team_scoring_minimum = 1,
    _game_engine_settings_definition_king_of_the_hill_variant_team_scoring_maximum = 2,
    _game_engine_settings_definition_king_of_the_hill_variant_team_scoring_default = 3
};

enum e_game_engine_settings_definition_king_of_the_hill_variant_hill_movement
{
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_no_movement = 0,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after10_seconds = 1,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after15_seconds = 2,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after30_seconds = 3,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after1_minute = 4,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after2_minutes = 5,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after3_minutes = 6,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after4_minutes = 7,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_after5_minutes = 8
};

enum e_game_engine_settings_definition_king_of_the_hill_variant_hill_movement_order
{
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_order_random = 0,
    _game_engine_settings_definition_king_of_the_hill_variant_hill_movement_order_sequence = 1
};

enum e_game_engine_settings_definition_territories_variant_flags
{
    _game_engine_settings_definition_territories_variant_flags_one_sided_bit = 0,
    _game_engine_settings_definition_territories_variant_flags_lock_after_first_capture_bit = 1
};

enum e_game_engine_settings_definition_territories_variant_respawn_on_capture
{
    _game_engine_settings_definition_territories_variant_respawn_on_capture_disabled = 0,
    _game_engine_settings_definition_territories_variant_respawn_on_capture_on_ally_capture = 1,
    _game_engine_settings_definition_territories_variant_respawn_on_capture_on_enemy_capture = 2,
    _game_engine_settings_definition_territories_variant_respawn_on_capture_on_any_capture = 3
};

enum e_game_engine_settings_definition_juggernaut_variant_flags
{
    _game_engine_settings_definition_juggernaut_variant_flags_allied_against_juggernaut_bit = 0,
    _game_engine_settings_definition_juggernaut_variant_flags_respawn_on_lone_juggernaut_bit = 1,
    _game_engine_settings_definition_juggernaut_variant_flags_goal_zones_enabled_bit = 2
};

enum e_game_engine_settings_definition_juggernaut_variant_first_juggernaut
{
    _game_engine_settings_definition_juggernaut_variant_first_juggernaut_random = 0,
    _game_engine_settings_definition_juggernaut_variant_first_juggernaut_first_kill = 1,
    _game_engine_settings_definition_juggernaut_variant_first_juggernaut_first_death = 2
};

enum e_game_engine_settings_definition_juggernaut_variant_next_juggernaut
{
    _game_engine_settings_definition_juggernaut_variant_next_juggernaut_killer = 0,
    _game_engine_settings_definition_juggernaut_variant_next_juggernaut_killed = 1,
    _game_engine_settings_definition_juggernaut_variant_next_juggernaut_unchanged = 2,
    _game_engine_settings_definition_juggernaut_variant_next_juggernaut_random = 3
};

enum e_game_engine_settings_definition_juggernaut_variant_goal_zone_movement
{
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_no_movement = 0,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after10_seconds = 1,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after15_seconds = 2,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after30_seconds = 3,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after1_minute = 4,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after2_minutes = 5,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after3_minutes = 6,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after4_minutes = 7,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_after5_minutes = 8,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_on_arrival = 9,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_movement_on_new_juggernaut = 10
};

enum e_game_engine_settings_definition_juggernaut_variant_goal_zone_order
{
    _game_engine_settings_definition_juggernaut_variant_goal_zone_order_random = 0,
    _game_engine_settings_definition_juggernaut_variant_goal_zone_order_sequence = 1
};

enum e_game_engine_settings_definition_vip_variant_flags
{
    _game_engine_settings_definition_vip_variant_flags_single_vip_bit = 0,
    _game_engine_settings_definition_vip_variant_flags_goal_zones_enabled_bit = 1,
    _game_engine_settings_definition_vip_variant_flags_end_round_on_vip_death_bit = 2
};

enum e_game_engine_settings_definition_vip_variant_next_vip
{
    _game_engine_settings_definition_vip_variant_next_vip_random = 0,
    _game_engine_settings_definition_vip_variant_next_vip_unknown = 1,
    _game_engine_settings_definition_vip_variant_next_vip_next_death = 2,
    _game_engine_settings_definition_vip_variant_next_vip_unchanged = 3
};

enum e_game_engine_settings_definition_vip_variant_goal_zone_movement
{
    _game_engine_settings_definition_vip_variant_goal_zone_movement_no_movement = 0,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after10_seconds = 1,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after15_seconds = 2,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after30_seconds = 3,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after1_minute = 4,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after2_minutes = 5,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after3_minutes = 6,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after4_minutes = 7,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_after5_minutes = 8,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_on_arrival = 9,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_on_new_vip = 10
};

enum e_game_engine_settings_definition_vip_variant_goal_zone_movement_order
{
    _game_engine_settings_definition_vip_variant_goal_zone_movement_order_random = 0,
    _game_engine_settings_definition_vip_variant_goal_zone_movement_order_sequence = 1
};

enum e_game_engine_settings_definition_sandbox_editor_variant_flags
{
    _game_engine_settings_definition_sandbox_editor_variant_flags_open_channel_voice_enabled_bit = 0
};

enum e_game_engine_settings_definition_sandbox_editor_variant_edit_mode
{
    _game_engine_settings_definition_sandbox_editor_variant_edit_mode_everyone = 0,
    _game_engine_settings_definition_sandbox_editor_variant_edit_mode_leader_only = 1
};


/* ---------- structures */

struct s_game_engine_settings_definition_trait_profile_shields_and_health_block;
struct s_game_engine_settings_definition_trait_profile_weapons_and_damage_block;
struct s_game_engine_settings_definition_trait_profile_movement_block;
struct s_game_engine_settings_definition_trait_profile_appearance_block;
struct s_game_engine_settings_definition_trait_profile_sensor;
struct s_game_engine_settings_definition_trait_profile;
struct s_game_engine_settings_definition_base_variant_general_setting;
struct s_game_engine_settings_definition_base_variant_respawn_setting;
struct s_game_engine_settings_definition_base_variant_social_setting;
struct s_game_engine_settings_definition_base_variant_map_override;
struct s_game_engine_settings_definition_base_variant;
struct s_game_engine_settings_definition_slayer_variant;
struct s_game_engine_settings_definition_oddball_variant;
struct s_game_engine_settings_definition_capture_the_flag_variant;
struct s_game_engine_settings_definition_assault_variant;
struct s_game_engine_settings_definition_infection_variant;
struct s_game_engine_settings_definition_king_of_the_hill_variant;
struct s_game_engine_settings_definition_territories_variant;
struct s_game_engine_settings_definition_juggernaut_variant;
struct s_game_engine_settings_definition_vip_variant;
struct s_game_engine_settings_definition_sandbox_editor_variant;
struct s_game_engine_settings_definition;

struct s_game_engine_settings_definition
{
    c_flags<e_game_engine_settings_definition_flags, long> flags;
    c_tag_block<s_game_engine_settings_definition_trait_profile> trait_profiles;
    c_tag_block<s_game_engine_settings_definition_slayer_variant> slayer_variants;
    c_tag_block<s_game_engine_settings_definition_oddball_variant> oddball_variants;
    c_tag_block<s_game_engine_settings_definition_capture_the_flag_variant> capture_the_flag_variants;
    c_tag_block<s_game_engine_settings_definition_assault_variant> assault_variants;
    c_tag_block<s_game_engine_settings_definition_infection_variant> infection_variants;
    c_tag_block<s_game_engine_settings_definition_king_of_the_hill_variant> king_of_the_hill_variants;
    c_tag_block<s_game_engine_settings_definition_territories_variant> territories_variants;
    c_tag_block<s_game_engine_settings_definition_juggernaut_variant> juggernaut_variants;
    c_tag_block<s_game_engine_settings_definition_vip_variant> vip_variants;
    c_tag_block<s_game_engine_settings_definition_sandbox_editor_variant> sandbox_editor_variants;
    ulong unknown2;
};
static_assert(sizeof(s_game_engine_settings_definition) == 0x8C);

struct s_game_engine_settings_definition_sandbox_editor_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_sandbox_editor_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_sandbox_editor_variant_edit_mode, short> edit_mode;
    short editor_respawn_time;
    string_id editor_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_sandbox_editor_variant) == 0x64);

struct s_game_engine_settings_definition_vip_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_vip_variant_flags, long> flags;
    short score_to_win;
    short unknown;
    c_enum<e_game_engine_settings_definition_vip_variant_next_vip, short> next_vip;
    c_enum<e_game_engine_settings_definition_vip_variant_goal_zone_movement, short> goal_zone_movement;
    c_enum<e_game_engine_settings_definition_vip_variant_goal_zone_movement_order, short> goal_zone_movement_order;
    char kill_points;
    char vip_takedown_points;
    char kill_as_vip_points;
    char vip_death_points;
    char goal_arrival_points;
    char suicide_points;
    char vip_betrayal_points;
    char betrayal_points;
    char vip_proximity_trait_radius;
    char unknown2;
    string_id vip_team_trait_profile;
    string_id vip_proximity_trait_profile;
    string_id vip_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_vip_variant) == 0x7C);

struct s_game_engine_settings_definition_juggernaut_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_juggernaut_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_juggernaut_variant_first_juggernaut, short> first_juggernaut;
    c_enum<e_game_engine_settings_definition_juggernaut_variant_next_juggernaut, short> next_juggernaut;
    c_enum<e_game_engine_settings_definition_juggernaut_variant_goal_zone_movement, short> goal_zone_movement;
    c_enum<e_game_engine_settings_definition_juggernaut_variant_goal_zone_order, short> goal_zone_order;
    short score_to_win;
    short unknown;
    char kill_points;
    char takedown_points;
    char kill_as_juggernaut_points;
    char goal_arrival_points;
    char suicide_points;
    char betrayal_points;
    char next_juggernaut_delay;
    char unknown2;
    string_id juggernaut_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_juggernaut_variant) == 0x74);

struct s_game_engine_settings_definition_territories_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_territories_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_territories_variant_respawn_on_capture, short> respawn_on_capture;
    short territory_capture_time;
    short sudden_death_time;
    short unknown;
    string_id defender_trait_profile;
    string_id attacker_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_territories_variant) == 0x6C);

struct s_game_engine_settings_definition_king_of_the_hill_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_king_of_the_hill_variant_flags, long> flags;
    short score_to_win;
    short unknown;
    c_enum<e_game_engine_settings_definition_king_of_the_hill_variant_team_scoring, short> team_scoring;
    c_enum<e_game_engine_settings_definition_king_of_the_hill_variant_hill_movement, short> hill_movement;
    c_enum<e_game_engine_settings_definition_king_of_the_hill_variant_hill_movement_order, short> hill_movement_order;
    short unknown2;
    char on_hill_points;
    char uncontested_control_points;
    char off_hill_points;
    char kill_points;
    string_id on_hill_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_king_of_the_hill_variant) == 0x70);

struct s_game_engine_settings_definition_infection_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_infection_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_infection_variant_safe_havens, short> safe_havens;
    c_enum<e_game_engine_settings_definition_infection_variant_next_zombie, short> next_zombie;
    short initial_zombie_count;
    short safe_haven_movement_time;
    char zombie_kill_points;
    char infection_points;
    char safe_haven_arrival_points;
    char suicide_points;
    char betrayal_points;
    char last_man_standing_bonus;
    char unknown;
    char unknown2;
    string_id zombie_trait_profile;
    string_id alpha_zombie_trait_profile;
    string_id on_haven_trait_profile;
    string_id last_human_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_infection_variant) == 0x7C);

struct s_game_engine_settings_definition_assault_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_assault_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_assault_variant_respawn_on_capture, short> respawn_on_capture;
    c_enum<e_game_engine_settings_definition_assault_variant_game_mode, short> game_mode;
    c_enum<e_game_engine_settings_definition_assault_variant_enemy_bomb_waypoint, short> enemy_bomb_waypoint;
    short sudden_death_time;
    short detonations_to_win;
    short unknown;
    short unknown2;
    short unknown3;
    short unknown4;
    short bomb_reset_time;
    short bomb_arming_time;
    short bomb_disarming_time;
    short bomb_fuse_time;
    short unknown5;
    string_id bomb_carrier_trait_profile;
    string_id unknown_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_assault_variant) == 0x80);

struct s_game_engine_settings_definition_capture_the_flag_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_capture_the_flag_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_capture_the_flag_variant_home_flag_waypoint, short> home_flag_waypoint;
    c_enum<e_game_engine_settings_definition_capture_the_flag_variant_game_mode, short> game_mode;
    c_enum<e_game_engine_settings_definition_capture_the_flag_variant_respawn_on_capture, short> respawn_on_capture;
    short flag_return_time;
    short sudden_death_time;
    short score_to_win;
    short unknown;
    short flag_reset_time;
    string_id flag_carrier_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_capture_the_flag_variant) == 0x70);

struct s_game_engine_settings_definition_oddball_variant : s_game_engine_settings_definition_base_variant
{
    c_flags<e_game_engine_settings_definition_oddball_variant_flags, long> flags;
    c_enum<e_game_engine_settings_definition_oddball_variant_team_scoring, short> team_scoring;
    short points_to_win;
    short unknown;
    char carrying_points;
    char kill_points;
    char ball_kill_points;
    char ball_carrier_kill_points;
    char ball_count;
    char unknown2;
    short initial_ball_delay;
    short ball_respawn_delay;
    string_id ball_carrier_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_oddball_variant) == 0x70);

struct s_game_engine_settings_definition_slayer_variant : s_game_engine_settings_definition_base_variant
{
    c_enum<e_game_engine_settings_definition_slayer_variant_team_scoring, short> team_scoring;
    short points_to_win;
    short unknown;
    char kill_points;
    char assist_points;
    char death_points;
    char suicide_points;
    char betrayal_points;
    char leader_kill_bonus;
    char elimination_bonus;
    char assassination_bonus;
    char headshot_bonus;
    char beatdown_bonus;
    char sticky_bonus;
    char splatter_bonus;
    char spree_bonus;
    char unknown2;
    string_id leader_trait_profile;
};
static_assert(sizeof(s_game_engine_settings_definition_slayer_variant) == 0x70);

struct s_game_engine_settings_definition_base_variant
{
    char name_ascii[32];
    string_id name;
    string_id description;
    c_tag_block<s_game_engine_settings_definition_base_variant_general_setting> general_settings;
    c_tag_block<s_game_engine_settings_definition_base_variant_respawn_setting> respawn_settings;
    c_tag_block<s_game_engine_settings_definition_base_variant_social_setting> social_settings;
    c_tag_block<s_game_engine_settings_definition_base_variant_map_override> map_overrides;
};
static_assert(sizeof(s_game_engine_settings_definition_base_variant) == 0x58);

struct s_game_engine_settings_definition_base_variant_map_override
{
    c_flags<e_game_engine_settings_definition_base_variant_map_override_flags, long> flags;
    string_id base_player_trait_profile;
    string_id weapon_set;
    string_id vehicle_set;
    string_id overshield_trait_profile;
    string_id active_camo_trait_profile;
    string_id custom_powerup_trait_profile;
    char overshield_trait_duration;
    char active_camo_trait_duration;
    char custom_powerup_trait_duration;
    char unknown;
};
static_assert(sizeof(s_game_engine_settings_definition_base_variant_map_override) == 0x20);

struct s_game_engine_settings_definition_base_variant_social_setting
{
    c_flags<e_game_engine_settings_definition_base_variant_social_setting_flags, long> flags;
};
static_assert(sizeof(s_game_engine_settings_definition_base_variant_social_setting) == 0x4);

struct s_game_engine_settings_definition_base_variant_respawn_setting
{
    c_flags<e_game_engine_settings_definition_base_variant_respawn_setting_flags, ushort> flags;
    char lives_per_round;
    char shared_team_lives;
    uchar unknown;
    uchar respawn_time;
    uchar suicide_penalty;
    uchar betrayal_penalty;
    uchar respawn_time_growth;
    char unknown1;
    char unknown2;
    char unknown3;
    string_id respawn_trait_profile;
    char respawn_trait_duration;
    char unknown4;
    char unknown5;
    char unknown6;
};
static_assert(sizeof(s_game_engine_settings_definition_base_variant_respawn_setting) == 0x14);

struct s_game_engine_settings_definition_base_variant_general_setting
{
    c_flags<e_game_engine_settings_definition_base_variant_general_setting_flags, long> flags;
    char time_limit;
    char number_of_rounds;
    char early_victory_win_count;
    c_enum<e_game_engine_settings_definition_base_variant_general_setting_round_resets, char> round_resets;
};
static_assert(sizeof(s_game_engine_settings_definition_base_variant_general_setting) == 0x8);

struct s_game_engine_settings_definition_trait_profile
{
    string_id name;
    c_tag_block<s_game_engine_settings_definition_trait_profile_shields_and_health_block> shields_and_health;
    c_tag_block<s_game_engine_settings_definition_trait_profile_weapons_and_damage_block> weapons_and_damage;
    c_tag_block<s_game_engine_settings_definition_trait_profile_movement_block> movement;
    c_tag_block<s_game_engine_settings_definition_trait_profile_appearance_block> appearance;
    c_tag_block<s_game_engine_settings_definition_trait_profile_sensor> sensors;
};
static_assert(sizeof(s_game_engine_settings_definition_trait_profile) == 0x40);

struct s_game_engine_settings_definition_trait_profile_sensor
{
    c_enum<e_game_engine_settings_definition_trait_profile_sensor_motion_tracker_mode, long> motion_tracker_mode;
    c_enum<e_game_engine_settings_definition_trait_profile_sensor_motion_tracker_range, long> motion_tracker_range;
};
static_assert(sizeof(s_game_engine_settings_definition_trait_profile_sensor) == 0x8);

struct s_game_engine_settings_definition_trait_profile_appearance_block
{
    c_enum<e_game_engine_settings_definition_trait_profile_appearance_block_active_camo, char> active_camo;
    c_enum<e_game_engine_settings_definition_trait_profile_appearance_block_waypoint, char> waypoint;
    c_enum<e_game_engine_settings_definition_trait_profile_appearance_block_aura, char> aura;
    c_enum<e_game_engine_settings_definition_trait_profile_appearance_block_forced_color, char> forced_color;
};
static_assert(sizeof(s_game_engine_settings_definition_trait_profile_appearance_block) == 0x4);

struct s_game_engine_settings_definition_trait_profile_movement_block
{
    c_enum<e_game_engine_settings_definition_trait_profile_movement_block_player_speed, char> player_speed;
    c_enum<e_game_engine_settings_definition_trait_profile_movement_block_player_gravity, char> player_gravity;
    c_enum<e_game_engine_settings_definition_trait_profile_movement_block_vehicle_use, char> vehicle_use;
    char unknown;
};
static_assert(sizeof(s_game_engine_settings_definition_trait_profile_movement_block) == 0x4);

struct s_game_engine_settings_definition_trait_profile_weapons_and_damage_block
{
    c_enum<e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_damage_modifier, char> damage_modifier;
    c_enum<e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_regeneration, char> grenade_regeneration;
    c_enum<e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_weapon_pickup, char> weapon_pickup;
    c_enum<e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_infinite_ammo, char> infinite_ammo;
    string_id primary_weapon;
    string_id secondary_weapon;
    c_enum<e_game_engine_settings_definition_trait_profile_weapons_and_damage_block_grenade_count, short> grenade_count;
    char unknown;
    char unknown2;
};
static_assert(sizeof(s_game_engine_settings_definition_trait_profile_weapons_and_damage_block) == 0x10);

struct s_game_engine_settings_definition_trait_profile_shields_and_health_block
{
    c_enum<e_game_engine_settings_definition_trait_profile_shields_and_health_block_damage_resistance, char> damage_resistance;
    c_enum<e_game_engine_settings_definition_trait_profile_shields_and_health_block_shield_multiplier, char> shield_multiplier;
    c_enum<e_game_engine_settings_definition_trait_profile_shields_and_health_block_shield_recharge_rate, char> shield_recharge_rate;
    c_enum<e_game_engine_settings_definition_trait_profile_shields_and_health_block_headshot_immunity, char> headshot_immunity;
    c_enum<e_game_engine_settings_definition_trait_profile_shields_and_health_block_shield_vampirism, char> shield_vampirism;
    char unknown;
    char unknown2;
    char unknown3;
};
static_assert(sizeof(s_game_engine_settings_definition_trait_profile_shields_and_health_block) == 0x8);

