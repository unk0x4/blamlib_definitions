/* ---------- enums */


/* ---------- structures */

struct s_chud_animation_definition_position_block_animation_block;
struct s_tag_function;
struct s_chud_animation_definition_position_block;
struct s_chud_animation_definition_rotation_block_animation_block;
struct s_chud_animation_definition_rotation_block;
struct s_chud_animation_definition_size_block_animation_block;
struct s_chud_animation_definition_size_block;
struct s_chud_animation_definition_color_block_animation_block;
struct s_chud_animation_definition_color_block;
struct s_chud_animation_definition_alpha_block_animation_block;
struct s_chud_animation_definition_alpha_block;
struct s_chud_animation_definition_alpha_unknown_block_animation_block;
struct s_chud_animation_definition_alpha_unknown_block;
struct s_chud_animation_definition_bitmap_block_animation_block;
struct s_chud_animation_definition_bitmap_block;
struct s_chud_animation_definition;

struct s_chud_animation_definition
{
    ushort flags;
    short unknown;
    c_tag_block<s_chud_animation_definition_position_block> position;
    c_tag_block<s_chud_animation_definition_rotation_block> rotation;
    c_tag_block<s_chud_animation_definition_size_block> size;
    c_tag_block<s_chud_animation_definition_color_block> color;
    c_tag_block<s_chud_animation_definition_alpha_block> alpha;
    c_tag_block<s_chud_animation_definition_alpha_unknown_block> alpha_unknown;
    c_tag_block<s_chud_animation_definition_bitmap_block> bitmap;
    long number_of_frames;
};
static_assert(sizeof(s_chud_animation_definition) == 0x5C);

struct s_chud_animation_definition_bitmap_block
{
    c_tag_block<s_chud_animation_definition_bitmap_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_bitmap_block) == 0x20);

struct s_chud_animation_definition_bitmap_block_animation_block
{
    long frame_number;
    real_point2d movement1;
    real_point2d movement2;
};
static_assert(sizeof(s_chud_animation_definition_bitmap_block_animation_block) == 0x14);

struct s_chud_animation_definition_alpha_unknown_block
{
    c_tag_block<s_chud_animation_definition_alpha_unknown_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_alpha_unknown_block) == 0x20);

struct s_chud_animation_definition_alpha_unknown_block_animation_block
{
    long frame_number;
    float alpha;
};
static_assert(sizeof(s_chud_animation_definition_alpha_unknown_block_animation_block) == 0x8);

struct s_chud_animation_definition_alpha_block
{
    c_tag_block<s_chud_animation_definition_alpha_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_alpha_block) == 0x20);

struct s_chud_animation_definition_alpha_block_animation_block
{
    long frame_number;
    float alpha;
};
static_assert(sizeof(s_chud_animation_definition_alpha_block_animation_block) == 0x8);

struct s_chud_animation_definition_color_block
{
    c_tag_block<s_chud_animation_definition_color_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_color_block) == 0x20);

struct s_chud_animation_definition_color_block_animation_block
{
    long frame_number;
    ulong unknown;
};
static_assert(sizeof(s_chud_animation_definition_color_block_animation_block) == 0x8);

struct s_chud_animation_definition_size_block
{
    c_tag_block<s_chud_animation_definition_size_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_size_block) == 0x20);

struct s_chud_animation_definition_size_block_animation_block
{
    long frame_number;
    real_point2d stretch;
};
static_assert(sizeof(s_chud_animation_definition_size_block_animation_block) == 0xC);

struct s_chud_animation_definition_rotation_block
{
    c_tag_block<s_chud_animation_definition_rotation_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_rotation_block) == 0x20);

struct s_chud_animation_definition_rotation_block_animation_block
{
    long frame_number;
    real_euler_angles3d angles;
};
static_assert(sizeof(s_chud_animation_definition_rotation_block_animation_block) == 0x10);

struct s_chud_animation_definition_position_block
{
    c_tag_block<s_chud_animation_definition_position_block_animation_block> animation;
    s_tag_function function;
};
static_assert(sizeof(s_chud_animation_definition_position_block) == 0x20);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_chud_animation_definition_position_block_animation_block
{
    long frame_number;
    real_point3d position;
};
static_assert(sizeof(s_chud_animation_definition_position_block_animation_block) == 0x10);

