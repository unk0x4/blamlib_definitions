/* ---------- enums */

enum e_game_language
{
    _game_language_english = 0,
    _game_language_japanese = 1,
    _game_language_german = 2,
    _game_language_french = 3,
    _game_language_spanish = 4,
    _game_language_mexican = 5,
    _game_language_italian = 6,
    _game_language_korean = 7,
    _game_language_chinese_traditional = 8,
    _game_language_chinese_simplified = 9,
    _game_language_portuguese = 10,
    _game_language_russian = 11
};

enum e_function_type
{
    _function_type_linear = 0,
    _function_type_late = 1,
    _function_type_very_late = 2,
    _function_type_early = 3,
    _function_type_very_early = 4,
    _function_type_cosine = 5,
    _function_type_zero = 6,
    _function_type_one = 7
};

enum e_globals_player_character_type_flags
{
    _globals_player_character_type_flags_can_sprint_bit = 0
};

enum e_globals_material_flags
{
    _globals_material_flags_flammable_bit = 0,
    _globals_material_flags_biomass_bit = 1,
    _globals_material_flags_rad_xfer_interior_bit = 2
};

enum e_globals_sweetener_inheritance_flags
{
    _globals_sweetener_inheritance_flags_sound_small_bit = 0,
    _globals_sweetener_inheritance_flags_sound_medium_bit = 1,
    _globals_sweetener_inheritance_flags_sound_large_bit = 2,
    _globals_sweetener_inheritance_flags_sound_rolling_bit = 3,
    _globals_sweetener_inheritance_flags_sound_grinding_bit = 4,
    _globals_sweetener_inheritance_flags_sound_melee_small_bit = 5,
    _globals_sweetener_inheritance_flags_sound_melee_medium_bit = 6,
    _globals_sweetener_inheritance_flags_sound_melee_large_bit = 7,
    _globals_sweetener_inheritance_flags_effect_small_bit = 8,
    _globals_sweetener_inheritance_flags_effect_medium_bit = 9,
    _globals_sweetener_inheritance_flags_effect_large_bit = 10,
    _globals_sweetener_inheritance_flags_effect_rolling_bit = 11,
    _globals_sweetener_inheritance_flags_effect_grinding_bit = 12,
    _globals_sweetener_inheritance_flags_effect_melee_bit = 13,
    _globals_sweetener_inheritance_flags_water_ripple_small_bit = 14,
    _globals_sweetener_inheritance_flags_water_ripple_medium_bit = 15,
    _globals_sweetener_inheritance_flags_water_ripple_large_bit = 16
};


/* ---------- structures */

struct s_tag_reference_block;
struct s_sound_globals_definition;
struct s_ai_globals_datum_gravemind_property_block;
struct s_ai_globals_datum_squad_template;
struct s_ai_globals_datum;
struct s_globals_damage_table_block_damage_group_armor_modifier;
struct s_globals_damage_table_block_damage_group;
struct s_globals_damage_table_block;
struct s_camera_globals_definition;
struct s_globals_player_control_block_look_function_block;
struct s_globals_player_control_block;
struct s_globals_difficulty_block;
struct s_globals_grenade;
struct s_globals_interface_tags_block_gfx_ui_string;
struct s_globals_interface_tags_block;
struct s_globals_player_information_block;
struct s_globals_player_representation_block;
struct s_globals_player_character_type;
struct s_globals_falling_damage_block;
struct s_globals_unknown_block;
struct s_globals_material_water_drag_property;
struct s_globals_material_underwater_proxy;
struct s_globals_material;
struct s_globals_cinematic_anchor_block;
struct s_globals_metagame_global_medal;
struct s_globals_metagame_global_multiplier_block;
struct s_globals_metagame_global;
struct s_globals_locale_globals_block;
struct s_tag_function;
struct s_globals_damage_reporting_type;
struct s_globals;

struct s_globals
{
    byte unused[172];
    c_enum<e_game_language, long> language;
    c_tag_block<s_tag_reference_block> havok_object_cleanup_effects;
    c_tag_block<s_sound_globals_definition> sound_globals;
    c_tag_block<s_ai_globals_datum> ai_globals_old;
    s_tag_reference ai_globals;
    c_tag_block<s_globals_damage_table_block> damage_table;
    ulong unknown45;
    ulong unknown46;
    ulong unknown47;
    c_tag_block<s_tag_reference_block> sounds_old;
    c_tag_block<s_camera_globals_definition> camera;
    c_tag_block<s_globals_player_control_block> player_control;
    c_tag_block<s_globals_difficulty_block> difficulty;
    c_tag_block<s_globals_grenade> grenades;
    ulong unknown48;
    ulong unknown49;
    ulong unknown50;
    c_tag_block<s_globals_interface_tags_block> interface_tags;
    ulong unknown51;
    ulong unknown52;
    ulong unknown53;
    ulong unknown54;
    ulong unknown55;
    ulong unknown56;
    c_tag_block<s_globals_player_information_block> player_information;
    c_tag_block<s_globals_player_representation_block> player_representation;
    c_tag_block<s_globals_player_character_type> player_character_types;
    c_tag_block<s_globals_falling_damage_block> falling_damage;
    c_tag_block<s_globals_unknown_block> unknown60;
    c_tag_block<s_globals_material> materials;
    s_tag_reference multiplayer_globals;
    s_tag_reference survival_globals;
    c_tag_block<s_globals_cinematic_anchor_block> cinematic_anchors;
    c_tag_block<s_globals_metagame_global> metagame_globals;
    locale_globals_block locale_globals[12];
    s_tag_reference rasterizer_globals;
    s_tag_reference default_camera_effect;
    s_tag_reference podium_definition;
    s_tag_reference default_wind;
    s_tag_reference default_damage_effect;
    s_tag_reference default_collision_damage;
    string_id default_water_material;
    short unknown_global_material_index;
    short unknown265;
    s_tag_reference effect_globals;
    s_tag_reference game_progression_globals;
    s_tag_reference achievement_globals;
    s_tag_reference input_globals;
    float unknown266;
    float unknown267;
    float unknown268;
    float unknown269;
    s_tag_function unknown270;
    float unknown271;
    float unknown272;
    float unknown273;
    float unknown274;
    float unknown275;
    c_tag_block<s_globals_damage_reporting_type> damage_reporting_types;
    ulong unknown276;
};
static_assert(sizeof(s_globals) == 0x608);

struct s_globals_damage_reporting_type
{
    short index;
    short version;
    char name[32];
};
static_assert(sizeof(s_globals_damage_reporting_type) == 0x24);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_globals_locale_globals_block
{
    ulong unknown1;
    ulong unknown2;
    long string_count;
    long locale_table_size;
    ulong locale_index_table_address;
    ulong locale_data_index_address;
    s_tag_data index_table_hash;
    s_tag_data string_data_hash;
    ulong unknown3;
};
static_assert(sizeof(s_globals_locale_globals_block) == 0x44);

struct s_globals_metagame_global
{
    c_tag_block<s_globals_metagame_global_medal> medals;
    c_tag_block<s_globals_metagame_global_multiplier_block> difficulty;
    c_tag_block<s_globals_metagame_global_multiplier_block> primary_skulls;
    c_tag_block<s_globals_metagame_global_multiplier_block> secondary_skulls;
    long unknown;
    long death_penalty;
    long betrayal_penalty;
    long unknown2;
    float multikill_window;
    float emp_window;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    long first_weapon_spree;
    long second_weapon_spree;
    long killing_spree;
    long killing_frenzy;
    long running_riot;
    long rampage;
    long untouchable;
    long invincible;
    long double_kill;
    long triple_kill;
    long overkill;
    long killtacular;
    long killtrocity;
    long killimanjaro;
    long killtastrophe;
    long killpocalypse;
    long killionaire;
};
static_assert(sizeof(s_globals_metagame_global) == 0x98);

struct s_globals_metagame_global_multiplier_block
{
    float multiplier;
};
static_assert(sizeof(s_globals_metagame_global_multiplier_block) == 0x4);

struct s_globals_metagame_global_medal
{
    float multiplier;
    long awarded_points;
    long medal_uptime;
    string_id event_name;
};
static_assert(sizeof(s_globals_metagame_global_medal) == 0x10);

struct s_globals_cinematic_anchor_block
{
    s_tag_reference cinematic_anchor;
    float fov_constant;
    float unknown2;
};
static_assert(sizeof(s_globals_cinematic_anchor_block) == 0x18);

struct s_globals_material
{
    string_id name;
    string_id parent_name;
    short runtime_material_index;
    c_flags<e_globals_material_flags, ushort> flags;
    string_id general_armor;
    string_id specific_armor;
    long physics_flags;
    float friction;
    float restitution;
    float density;
    c_tag_block<s_globals_material_water_drag_property> water_drag_properties;
    s_tag_reference breakable_surface;
    s_tag_reference sound_sweetener_small;
    s_tag_reference sound_sweetener_medium;
    s_tag_reference sound_sweetener_large;
    s_tag_reference sound_sweetener_rolling;
    s_tag_reference sound_sweetener_grinding;
    s_tag_reference sound_sweetener_melee_small;
    s_tag_reference sound_sweetener_melee_medium;
    s_tag_reference sound_sweetener_melee_large;
    s_tag_reference effect_sweetener_small;
    s_tag_reference effect_sweetener_medium;
    s_tag_reference effect_sweetener_large;
    s_tag_reference effect_sweetener_rolling;
    s_tag_reference effect_sweetener_grinding;
    s_tag_reference effect_sweetener_melee;
    s_tag_reference water_ripple_small;
    s_tag_reference water_ripple_medium;
    s_tag_reference water_ripple_large;
    c_flags<e_globals_sweetener_inheritance_flags, long> inheritance_flags;
    s_tag_reference material_effects;
    c_tag_block<s_globals_material_underwater_proxy> underwater_proxies;
    ulong unknown2;
    short unknown3;
    short unknown4;
};
static_assert(sizeof(s_globals_material) == 0x178);

struct s_globals_material_underwater_proxy
{
    string_id surface_name;
    string_id submerged_name;
    short surface_index;
    short submerged_index;
};
static_assert(sizeof(s_globals_material_underwater_proxy) == 0xC);

struct s_globals_material_water_drag_property
{
    float unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
};
static_assert(sizeof(s_globals_material_water_drag_property) == 0x28);

struct s_globals_unknown_block
{
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
};
static_assert(sizeof(s_globals_unknown_block) == 0xC);

struct s_globals_falling_damage_block
{
    real_bounds harmful_falling_distance_bounds;
    s_tag_reference falling_damage;
    s_tag_reference unknown;
    s_tag_reference soft_landing;
    s_tag_reference hard_landing;
    s_tag_reference script_damage;
    float terminal_velocity;
    s_tag_reference distance_damage;
    float unknown2;
    float unknown3;
    float unknown4;
};
static_assert(sizeof(s_globals_falling_damage_block) == 0x78);

struct s_globals_player_character_type
{
    string_id name;
    c_flags<e_globals_player_character_type_flags, uchar> flags;
    char player_information;
    char player_control;
    char campaign_representation;
    char multiplayer_representation;
    char multiplayer_armor_customization;
    char chud_globals;
    char first_person_interface;
};
static_assert(sizeof(s_globals_player_character_type) == 0xC);

struct s_globals_player_representation_block
{
    string_id name;
    ulong flags;
    s_tag_reference first_person_hands;
    s_tag_reference first_person_body;
    s_tag_reference third_person_unit;
    string_id third_person_variant;
    s_tag_reference binoculars_zoom_in_sound;
    s_tag_reference binoculars_zoom_out_sound;
    s_tag_reference combat_dialogue;
};
static_assert(sizeof(s_globals_player_representation_block) == 0x6C);

struct s_globals_player_information_block
{
    float walking_speed;
    float run_forward;
    float run_backward;
    float run_sideways;
    float run_acceleration;
    float sneak_forward;
    float sneak_backward;
    float sneak_sideways;
    float sneak_acceleration;
    float airborne_acceleration;
    real_point3d grenade_origin;
    float stun_movement_penalty;
    float stun_turning_penalty;
    float stun_jumping_penalty;
    real_bounds stun_time_range;
    real_bounds first_person_idle_time_range;
    float first_person_skip_fraction;
    float unknown1;
    s_tag_reference unknown2;
    s_tag_reference unknown3;
    s_tag_reference unknown4;
    long binoculars_zoom_count;
    real_bounds binocular_zoom_range;
    ulong unknown5;
    ulong unknown6;
    s_tag_reference flashlight_on;
    s_tag_reference flashlight_off;
    s_tag_reference default_damage_response;
};
static_assert(sizeof(s_globals_player_information_block) == 0xCC);

struct s_globals_interface_tags_block
{
    s_tag_reference spinner;
    s_tag_reference obsolete;
    s_tag_reference screen_color_table;
    s_tag_reference hud_color_table;
    s_tag_reference editor_color_table;
    s_tag_reference dialog_color_table;
    s_tag_reference motion_sensor_sweep_bitmap;
    s_tag_reference motion_sensor_sweep_bitmap_mask;
    s_tag_reference multiplayer_hud_bitmap;
    s_tag_reference hud_digits_definition;
    s_tag_reference motion_sensor_blip_bitmap;
    s_tag_reference interface_goo_map1;
    s_tag_reference interface_goo_map2;
    s_tag_reference interface_goo_map3;
    s_tag_reference main_menu_ui_globals;
    s_tag_reference single_player_ui_globals;
    s_tag_reference multiplayer_ui_globals;
    s_tag_reference hud_globals;
    c_tag_block<s_globals_interface_tags_block_gfx_ui_string> gfx_ui_strings;
};
static_assert(sizeof(s_globals_interface_tags_block) == 0x12C);

struct s_globals_interface_tags_block_gfx_ui_string
{
    char name[32];
    s_tag_reference strings;
};
static_assert(sizeof(s_globals_interface_tags_block_gfx_ui_string) == 0x30);

struct s_globals_grenade
{
    short maximum_count;
    short unknown;
    s_tag_reference throwing_effect;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    s_tag_reference equipment;
    s_tag_reference projectile;
};
static_assert(sizeof(s_globals_grenade) == 0x44);

struct s_globals_difficulty_block
{
    float easy_enemy_damage;
    float normal_enemy_damage;
    float hard_enemy_damage;
    float impossible_enemy_damage;
    float easy_enemy_vitality;
    float normal_enemy_vitality;
    float hard_enemy_vitality;
    float impossible_enemy_vitality;
    float easy_enemy_shield;
    float normal_enemy_shield;
    float hard_enemy_shield;
    float impossible_enemy_shield;
    float easy_enemy_recharge;
    float normal_enemy_recharge;
    float hard_enemy_recharge;
    float impossible_enemy_recharge;
    float easy_friend_damage;
    float normal_friend_damage;
    float hard_friend_damage;
    float impossible_friend_damage;
    float easy_friend_vitality;
    float normal_friend_vitality;
    float hard_friend_vitality;
    float impossible_friend_vitality;
    float easy_friend_shield;
    float normal_friend_shield;
    float hard_friend_shield;
    float impossible_friend_shield;
    float easy_friend_recharge;
    float normal_friend_recharge;
    float hard_friend_recharge;
    float impossible_friend_recharge;
    float easy_infection_forms;
    float normal_infection_forms;
    float hard_infection_forms;
    float impossible_infection_forms;
    float easy_unknown;
    float normal_unknown;
    float hard_unknown;
    float impossible_unknown;
    float easy_rate_of_fire;
    float normal_rate_of_fire;
    float hard_rate_of_fire;
    float impossible_rate_of_fire;
    float easy_projectile_error;
    float normal_projectile_error;
    float hard_projectile_error;
    float impossible_projectile_error;
    float easy_burst_error;
    float normal_burst_error;
    float hard_burst_error;
    float impossible_burst_error;
    float easy_target_delay;
    float normal_target_delay;
    float hard_target_delay;
    float impossible_target_delay;
    float easy_burst_separation;
    float normal_burst_separation;
    float hard_burst_separation;
    float impossible_burst_separation;
    float easy_target_tracking;
    float normal_target_tracking;
    float hard_target_tracking;
    float impossible_target_tracking;
    float easy_target_leading;
    float normal_target_leading;
    float hard_target_leading;
    float impossible_target_leading;
    float easy_overcharge_chance;
    float normal_overcharge_chance;
    float hard_overcharge_chance;
    float impossible_overcharge_chance;
    float easy_special_fire_delay;
    float normal_special_fire_delay;
    float hard_special_fire_delay;
    float impossible_special_fire_delay;
    float easy_guidance_vs_player;
    float normal_guidance_vs_player;
    float hard_guidance_vs_player;
    float impossible_guidance_vs_player;
    float easy_melee_delay_base;
    float normal_melee_delay_base;
    float hard_melee_delay_base;
    float impossible_melee_delay_base;
    float easy_melee_delay_scale;
    float normal_melee_delay_scale;
    float hard_melee_delay_scale;
    float impossible_melee_delay_scale;
    float easy_unknown2;
    float normal_unknown2;
    float hard_unknown2;
    float impossible_unknown2;
    float easy_grenade_chance_scale;
    float normal_grenade_chance_scale;
    float hard_grenade_chance_scale;
    float impossible_grenade_chance_scale;
    float easy_grenade_timer_scale;
    float normal_grenade_timer_scale;
    float hard_grenade_timer_scale;
    float impossible_grenade_timer_scale;
    float easy_unknown3;
    float normal_unknown3;
    float hard_unknown3;
    float impossible_unknown3;
    float easy_unknown4;
    float normal_unknown4;
    float hard_unknown4;
    float impossible_unknown4;
    float easy_unknown5;
    float normal_unknown5;
    float hard_unknown5;
    float impossible_unknown5;
    float easy_major_upgrade_normal;
    float normal_major_upgrade_normal;
    float hard_major_upgrade_normal;
    float impossible_major_upgrade_normal;
    float easy_major_upgrade_few;
    float normal_major_upgrade_few;
    float hard_major_upgrade_few;
    float impossible_major_upgrade_few;
    float easy_major_upgrade_many;
    float normal_major_upgrade_many;
    float hard_major_upgrade_many;
    float impossible_major_upgrade_many;
    float easy_player_vehicle_ram_chance;
    float normal_player_vehicle_ram_chance;
    float hard_player_vehicle_ram_chance;
    float impossible_player_vehicle_ram_chance;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
    ulong unknown23;
    ulong unknown24;
    ulong unknown25;
    ulong unknown26;
    ulong unknown27;
    ulong unknown28;
    ulong unknown29;
    ulong unknown30;
    ulong unknown31;
    ulong unknown32;
    ulong unknown33;
};
static_assert(sizeof(s_globals_difficulty_block) == 0x284);

struct s_globals_player_control_block
{
    float magnetism_friction;
    float magnetism_adhesion;
    float inconsequential_target_scale;
    real_point2d crosshair_location;
    float seconds_to_start;
    float seconds_to_full_speed;
    float decay_rate;
    float full_speed_multiplier;
    float pegged_magnitude;
    float pegged_angular_threshold;
    float unknown4;
    float unknown5;
    float look_default_pitch_rate;
    float look_default_yaw_rate;
    float look_peg_threshold;
    float look_yaw_acceleration_time;
    float look_yaw_acceleration_scale;
    float look_pitch_acceleration_time;
    float look_pitch_acceleration_scale;
    float look_autoleveling_scale;
    float unknown8;
    float unknown9;
    float gravity_scale;
    short unknown10;
    short minimum_autoleveling_ticks;
    c_tag_block<s_globals_player_control_block_look_function_block> look_function;
};
static_assert(sizeof(s_globals_player_control_block) == 0x70);

struct s_globals_player_control_block_look_function_block
{
    float scale;
};
static_assert(sizeof(s_globals_player_control_block_look_function_block) == 0x0);

struct s_camera_globals_definition
{
    s_tag_reference default_unit_camera_track;
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
    c_enum<e_function_type, short> boost_function;
    c_enum<e_function_type, short> hoist_function;
    float unknown27;
    float unknown28;
    float unknown29;
    float unknown30;
    float unknown31;
    float unknown32;
    float unknown33;
    float unknown34;
    float unknown35;
    float unknown36;
    float unknown37;
    float unknown38;
};
static_assert(sizeof(s_camera_globals_definition) == 0xA4);

struct s_globals_damage_table_block
{
    c_tag_block<s_globals_damage_table_block_damage_group> damage_groups;
};
static_assert(sizeof(s_globals_damage_table_block) == 0xC);

struct s_globals_damage_table_block_damage_group
{
    string_id name;
    c_tag_block<s_globals_damage_table_block_damage_group_armor_modifier> armor_modifiers;
};
static_assert(sizeof(s_globals_damage_table_block_damage_group) == 0x10);

struct s_globals_damage_table_block_damage_group_armor_modifier
{
    string_id name;
    float damage_multiplier;
};
static_assert(sizeof(s_globals_damage_table_block_damage_group_armor_modifier) == 0x8);

struct s_ai_globals_datum
{
    float infantry_on_ai_weapon_damage_scale;
    float vehicle_on_ai_weapon_damage_scale;
    float player_on_ai_weapon_damage_scale;
    float danger_broadly_facing;
    float danger_shooting_near;
    float danger_shooting_at;
    float danger_extremely_close;
    float danger_shield_damage;
    float danger_extended_shield_damage;
    float danger_body_damage;
    float danger_extended_body_damage;
    s_tag_reference global_dialogue;
    string_id default_mission_dialogue_sound_effect;
    float jump_down;
    float jump_step;
    float jump_crouch;
    float jump_stand;
    float jump_storey;
    float jump_tower;
    float max_jump_down_height_down;
    float max_jump_down_height_step;
    float max_jump_down_height_crouch;
    float max_jump_down_height_stand;
    float max_jump_down_height_storey;
    float max_jump_down_height_tower;
    real_bounds hoist_step;
    real_bounds hoist_crouch;
    real_bounds hoist_stand;
    real_bounds vault_step;
    real_bounds vault_crouch;
    float search_range_infantry;
    float search_range_flying;
    float search_range_vehicle;
    float search_range_giant;
    c_tag_block<s_ai_globals_datum_gravemind_property_block> gravemind_properties;
    float scary_target_threshold;
    float scary_weapon_threshold;
    float player_scariness;
    float berserking_actor_scariness;
    float kamikazeing_actor_scariness;
    float invincible_actor_scariness;
    float minimum_death_time;
    float projectile_distance;
    float idle_clump_distance;
    float dangerous_clump_distance;
    float conver_search_duration;
    float task_search_duration;
    float unknown7;
    c_tag_block<s_tag_reference_block> styles;
    c_tag_block<s_tag_reference_block> formations;
    c_tag_block<s_ai_globals_datum_squad_template> squad_templates;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
};
static_assert(sizeof(s_ai_globals_datum) == 0x144);

struct s_ai_globals_datum_squad_template
{
    s_tag_reference template;
};
static_assert(sizeof(s_ai_globals_datum_squad_template) == 0x10);

struct s_ai_globals_datum_gravemind_property_block
{
    float minimum_retreat_time;
    float ideal_retreat_time;
    float maximum_retreat_time;
};
static_assert(sizeof(s_ai_globals_datum_gravemind_property_block) == 0xC);

struct s_sound_globals_definition
{
    s_tag_reference sound_classes;
    s_tag_reference sound_effects;
    s_tag_reference sound_mix;
    s_tag_reference sound_combat_dialogue_constants;
    s_tag_reference sound_global_propagation;
    s_tag_reference gfx_ui_sounds;
};
static_assert(sizeof(s_sound_globals_definition) == 0x60);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

