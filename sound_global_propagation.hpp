/* ---------- enums */


/* ---------- structures */

struct s_sound_global_propagation;

struct s_sound_global_propagation
{
    s_tag_reference underwater_environment;
    s_tag_reference underwater_loop;
    ulong unknown;
    ulong unknown2;
    s_tag_reference enter_underater;
    s_tag_reference exit_underwater;
};
static_assert(sizeof(s_sound_global_propagation) == 0x48);

