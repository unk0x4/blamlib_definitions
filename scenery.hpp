/* ---------- enums */

enum e_scenery_pathfinding_policy
{
    _scenery_pathfinding_policy_cut_out = 0,
    _scenery_pathfinding_policy_static = 1,
    _scenery_pathfinding_policy_dynamic = 2,
    _scenery_pathfinding_policy_none = 3
};

enum e_scenery_scenery_flag_bits
{
    _scenery_scenery_flag_bits_physically_simulates_bit = 0,
    _scenery_scenery_flag_bits_use_complex_activation_bit = 1
};

enum e_scenery_lightmapping_policy
{
    _scenery_lightmapping_policy_per_vertex = 0,
    _scenery_lightmapping_policy_per_pixel_not_implemented = 1,
    _scenery_lightmapping_policy_dynamic = 2
};


/* ---------- structures */

struct s_scenery;

struct s_scenery : s_game_object
{
    c_enum<e_scenery_pathfinding_policy, short> pathfinding_policy;
    c_flags<e_scenery_scenery_flag_bits, ushort> scenery_flags;
    c_enum<e_scenery_lightmapping_policy, short> lightmapping_policy;
    byte unused2[2];
};
static_assert(sizeof(s_scenery) == 0x128);

