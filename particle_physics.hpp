/* ---------- enums */

enum e_particle_physics_flags
{
    _particle_physics_flags_physics_bit = 0,
    _particle_physics_flags_collide_with_structure_bit = 1,
    _particle_physics_flags_collide_with_media_bit = 2,
    _particle_physics_flags_collide_with_scenery_bit = 3,
    _particle_physics_flags_collide_with_vehicles_bit = 4,
    _particle_physics_flags_collide_with_bipeds_bit = 5,
    _particle_physics_flags_swarm_bit = 6,
    _particle_physics_flags_wind_bit = 7
};

enum e_particle_physics_movement_type
{
    _particle_physics_movement_type_physics = 0,
    _particle_physics_movement_type_collider = 1,
    _particle_physics_movement_type_swarm = 2,
    _particle_physics_movement_type_wind = 3
};

enum e_tag_mapping_variable_type
{
    _tag_mapping_variable_type_particle_age = 0,
    _tag_mapping_variable_type_emitter_age = 1,
    _tag_mapping_variable_type_particle_random = 2,
    _tag_mapping_variable_type_emitter_random = 3,
    _tag_mapping_variable_type_particle_random1 = 4,
    _tag_mapping_variable_type_particle_random2 = 5,
    _tag_mapping_variable_type_particle_random3 = 6,
    _tag_mapping_variable_type_particle_random4 = 7,
    _tag_mapping_variable_type_emitter_random1 = 8,
    _tag_mapping_variable_type_emitter_random2 = 9,
    _tag_mapping_variable_type_emitter_time = 10,
    _tag_mapping_variable_type_system_lod = 11,
    _tag_mapping_variable_type_game_time = 12,
    _tag_mapping_variable_type_effect_a_scale = 13,
    _tag_mapping_variable_type_effect_b_scale = 14,
    _tag_mapping_variable_type_physics_rotation = 15,
    _tag_mapping_variable_type_location_random = 16,
    _tag_mapping_variable_type_distance_from_emitter = 17,
    _tag_mapping_variable_type_particle_simulation_a = 18,
    _tag_mapping_variable_type_particle_simulation_b = 19,
    _tag_mapping_variable_type_particle_velocity = 20,
    _tag_mapping_variable_type_invalid = 21
};

enum e_tag_mapping_output_modifier
{
    _tag_mapping_output_modifier_none = 0,
    _tag_mapping_output_modifier_plus = 1,
    _tag_mapping_output_modifier_times = 2
};


/* ---------- structures */

struct s_tag_function;
struct s_tag_mapping;
struct s_particle_physics_movement_parameter;
struct s_particle_physics_movement;
struct s_particle_physics;

struct s_particle_physics
{
    s_tag_reference template;
    c_flags<e_particle_physics_flags, ulong> flags;
    c_tag_block<s_particle_physics_movement> movements;
    byte unused1[12];
};
static_assert(sizeof(s_particle_physics) == 0x2C);

struct s_particle_physics_movement
{
    c_enum<e_particle_physics_movement_type, short> type;
    uchar flags;
    uchar unused;
    c_tag_block<s_particle_physics_movement_parameter> parameters;
    long unknown2;
    long unknown3;
};
static_assert(sizeof(s_particle_physics_movement) == 0x18);

struct s_particle_physics_movement_parameter
{
    long parameter_id;
    s_tag_mapping property;
};
static_assert(sizeof(s_particle_physics_movement_parameter) == 0x24);

struct s_tag_mapping
{
    c_enum<e_tag_mapping_variable_type, char> input_variable;
    c_enum<e_tag_mapping_variable_type, char> range_variable;
    c_enum<e_tag_mapping_output_modifier, char> output_modifier;
    c_enum<e_tag_mapping_variable_type, char> output_modifier_input;
    s_tag_function function;
    float runtime_m_constant_value;
    uchar runtime_m_flags;
    byte unused[3];
};
static_assert(sizeof(s_tag_mapping) == 0x20);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

