/* ---------- enums */


/* ---------- structures */

struct s_cinematic_scene_index;
struct s_tag_reference_block;
struct s_cinematic;

struct s_cinematic
{
    ulong scenes_flags;
    ulong scenes_expanded_flags;
    c_tag_block<s_cinematic_scene_index> shots;
    s_tag_reference scenario;
    long zoneset;
    string_id name;
    short unknown1;
    byte unused_padding[2];
    long unknown2;
    long unknown3;
    long unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    long unknown8;
    long unknown9;
    long unknown10;
    long unknown11;
    long unknown12;
    s_tag_reference unknown13;
    s_tag_data import_script_header;
    c_tag_block<s_tag_reference_block> cinematic_scenes;
    s_tag_data import_script_footer;
    s_tag_data import_script_skip;
};
static_assert(sizeof(s_cinematic) == 0xB4);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

struct s_cinematic_scene_index
{
    ulong shot_flags;
};
static_assert(sizeof(s_cinematic_scene_index) == 0x4);

