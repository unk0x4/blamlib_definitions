/* ---------- enums */

enum e_multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags
{
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_has_requirement_bit = 0,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_has_special_requirement_bit = 1,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit2_bit = 2,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit3_bit = 3,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit4_bit = 4,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit5_bit = 5,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit6_bit = 6,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit7_bit = 7,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit8_bit = 8,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit9_bit = 9,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit10_bit = 10,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit11_bit = 11,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit12_bit = 12,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit13_bit = 13,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit14_bit = 14,
    _multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags_bit15_bit = 15
};

enum e_multiplayer_globals_runtime_block_event_block_type
{
    _multiplayer_globals_runtime_block_event_block_type_general = 0,
    _multiplayer_globals_runtime_block_event_block_type_flavor = 1,
    _multiplayer_globals_runtime_block_event_block_type_slayer = 2,
    _multiplayer_globals_runtime_block_event_block_type_capture_the_flag = 3,
    _multiplayer_globals_runtime_block_event_block_type_oddball = 4,
    _multiplayer_globals_runtime_block_event_block_type_unused = 5,
    _multiplayer_globals_runtime_block_event_block_type_king_of_the_hill = 6,
    _multiplayer_globals_runtime_block_event_block_type_vip = 7,
    _multiplayer_globals_runtime_block_event_block_type_juggernaut = 8,
    _multiplayer_globals_runtime_block_event_block_type_territories = 9,
    _multiplayer_globals_runtime_block_event_block_type_assault = 10,
    _multiplayer_globals_runtime_block_event_block_type_infection = 11,
    _multiplayer_globals_runtime_block_event_block_type_survival = 12,
    _multiplayer_globals_runtime_block_event_block_type_unknown = 13
};

enum e_multiplayer_globals_runtime_block_event_block_audience
{
    _multiplayer_globals_runtime_block_event_block_audience_cause_player = 0,
    _multiplayer_globals_runtime_block_event_block_audience_cause_team = 1,
    _multiplayer_globals_runtime_block_event_block_audience_effect_player = 2,
    _multiplayer_globals_runtime_block_event_block_audience_effect_team = 3,
    _multiplayer_globals_runtime_block_event_block_audience_all = 4
};

enum e_multiplayer_globals_runtime_block_event_block_team
{
    _multiplayer_globals_runtime_block_event_block_team_none_player_only = 0,
    _multiplayer_globals_runtime_block_event_block_team_cause = 1,
    _multiplayer_globals_runtime_block_event_block_team_effect = 2,
    _multiplayer_globals_runtime_block_event_block_team_all = 3
};

enum e_multiplayer_globals_runtime_block_event_block_required_field
{
    _multiplayer_globals_runtime_block_event_block_required_field_none = 0,
    _multiplayer_globals_runtime_block_event_block_required_field_cause_player = 1,
    _multiplayer_globals_runtime_block_event_block_required_field_cause_team = 2,
    _multiplayer_globals_runtime_block_event_block_required_field_effect_player = 3,
    _multiplayer_globals_runtime_block_event_block_required_field_effect_team = 4
};

enum e_multiplayer_globals_runtime_block_state_response_state
{
    _multiplayer_globals_runtime_block_state_response_state_waiting_for_space_to_clear = 0,
    _multiplayer_globals_runtime_block_state_response_state_observing = 1,
    _multiplayer_globals_runtime_block_state_response_state_respawning_soon = 2,
    _multiplayer_globals_runtime_block_state_response_state_sitting_out = 3,
    _multiplayer_globals_runtime_block_state_response_state_out_of_lives = 4,
    _multiplayer_globals_runtime_block_state_response_state_playing_winning = 5,
    _multiplayer_globals_runtime_block_state_response_state_playing_tied = 6,
    _multiplayer_globals_runtime_block_state_response_state_playing_losing = 7,
    _multiplayer_globals_runtime_block_state_response_state_game_over_won = 8,
    _multiplayer_globals_runtime_block_state_response_state_game_over_tied = 9,
    _multiplayer_globals_runtime_block_state_response_state_game_over_lost = 10,
    _multiplayer_globals_runtime_block_state_response_state_game_over_tied2 = 11,
    _multiplayer_globals_runtime_block_state_response_state_you_have_flag = 12,
    _multiplayer_globals_runtime_block_state_response_state_enemy_has_flag = 13,
    _multiplayer_globals_runtime_block_state_response_state_flag_not_home = 14,
    _multiplayer_globals_runtime_block_state_response_state_carrying_oddball = 15,
    _multiplayer_globals_runtime_block_state_response_state_you_are_juggernaut = 16,
    _multiplayer_globals_runtime_block_state_response_state_you_control_hill = 17,
    _multiplayer_globals_runtime_block_state_response_state_switching_sides_soon = 18,
    _multiplayer_globals_runtime_block_state_response_state_player_recently_started = 19,
    _multiplayer_globals_runtime_block_state_response_state_you_have_bomb = 20,
    _multiplayer_globals_runtime_block_state_response_state_flag_contested = 21,
    _multiplayer_globals_runtime_block_state_response_state_bomb_contested = 22,
    _multiplayer_globals_runtime_block_state_response_state_limited_lives_multiple = 23,
    _multiplayer_globals_runtime_block_state_response_state_limited_lives_single = 24,
    _multiplayer_globals_runtime_block_state_response_state_limited_lives_final = 25,
    _multiplayer_globals_runtime_block_state_response_state_playing_winning_unlimited = 26,
    _multiplayer_globals_runtime_block_state_response_state_playing_tied_unlimited = 27,
    _multiplayer_globals_runtime_block_state_response_state_playing_losing_unlimited = 28
};


/* ---------- structures */

struct s_multiplayer_globals_universal_block_team_color;
struct s_multiplayer_globals_universal_block_armor_customization_block_region_permutation_variant_block;
struct s_multiplayer_globals_universal_block_armor_customization_block_region_permutation;
struct s_multiplayer_globals_universal_block_armor_customization_block_region;
struct s_multiplayer_globals_universal_block_armor_customization_block;
struct s_multiplayer_globals_universal_block_consumable;
struct s_multiplayer_globals_universal_block_energy_regeneration_block;
struct s_multiplayer_globals_universal_block_game_variant_weapon;
struct s_multiplayer_globals_universal_block_game_variant_vehicle;
struct s_multiplayer_globals_universal_block_game_variant_equipment_block;
struct s_multiplayer_globals_universal_block_weapon_set_substitution;
struct s_multiplayer_globals_universal_block_weapon_set;
struct s_multiplayer_globals_universal_block_vehicle_set_substitution;
struct s_multiplayer_globals_universal_block_vehicle_set;
struct s_multiplayer_globals_universal_block_podium_animation_stance_animation;
struct s_multiplayer_globals_universal_block_podium_animation_move_animation;
struct s_multiplayer_globals_universal_block_podium_animation;
struct s_multiplayer_globals_universal_block;
struct s_multiplayer_globals_runtime_block_sound;
struct s_multiplayer_globals_runtime_block_looping_sound;
struct s_multiplayer_globals_runtime_block_event_block;
struct s_multiplayer_globals_runtime_block_multiplayer_constant_weapon;
struct s_multiplayer_globals_runtime_block_multiplayer_constant_vehicle;
struct s_multiplayer_globals_runtime_block_multiplayer_constant_projectile;
struct s_multiplayer_globals_runtime_block_multiplayer_constant_equipment_block;
struct s_multiplayer_globals_runtime_block_multiplayer_constant;
struct s_multiplayer_globals_runtime_block_state_response;
struct s_multiplayer_globals_runtime_block;
struct s_multiplayer_globals;

struct s_multiplayer_globals
{
    c_tag_block<s_multiplayer_globals_universal_block> universal;
    c_tag_block<s_multiplayer_globals_runtime_block> runtime;
};
static_assert(sizeof(s_multiplayer_globals) == 0x18);

struct s_multiplayer_globals_runtime_block
{
    s_tag_reference sandbox_editor_unit;
    s_tag_reference sandbox_editor_object;
    s_tag_reference flag;
    s_tag_reference ball;
    s_tag_reference bomb;
    s_tag_reference vip_zone;
    s_tag_reference in_game_strings;
    s_tag_reference unknown;
    s_tag_reference unknown2;
    s_tag_reference unknown3;
    s_tag_reference unknown4;
    s_tag_reference unknown5;
    c_tag_block<s_multiplayer_globals_runtime_block_sound> sounds;
    c_tag_block<s_multiplayer_globals_runtime_block_looping_sound> looping_sounds;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> unknown_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> general_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> flavor_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> slayer_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> ctf_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> oddball_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> king_of_the_hill_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> vip_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> juggernaut_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> territories_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> assault_events;
    c_tag_block<s_multiplayer_globals_runtime_block_event_block> infection_events;
    long default_frag_grenade_count;
    long default_plasma_grenade_count;
    c_tag_block<s_multiplayer_globals_runtime_block_multiplayer_constant> multiplayer_constants;
    c_tag_block<s_multiplayer_globals_runtime_block_state_response> state_responses;
    s_tag_reference scoreboard_emblem_bitmap;
    s_tag_reference scoreboard_dead_emblem_bitmap;
    s_tag_reference default_shape_shader;
    s_tag_reference unknown6;
    s_tag_reference ctf_intro_ui;
    s_tag_reference slayer_intro_ui;
    s_tag_reference oddball_intro_ui;
    s_tag_reference king_of_the_hill_intro_ui;
    s_tag_reference sandbox_intro_ui;
    s_tag_reference vip_intro_ui;
    s_tag_reference juggernaut_intro_ui;
    s_tag_reference territories_intro_ui;
    s_tag_reference assault_intro_ui;
    s_tag_reference infection_intro_ui;
    s_tag_reference menu_music1;
    s_tag_reference menu_music2;
    s_tag_reference menu_music3;
    s_tag_reference menu_music4;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block) == 0x2A8);

struct s_multiplayer_globals_runtime_block_state_response
{
    ushort flags;
    short unknown;
    c_enum<e_multiplayer_globals_runtime_block_state_response_state, short> state;
    short unknown2;
    string_id free_for_all_message;
    string_id team_message;
    s_tag_reference unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_state_response) == 0x24);

struct s_multiplayer_globals_runtime_block_multiplayer_constant
{
    float unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
    float unknown25;
    float unknown26;
    c_tag_block<s_multiplayer_globals_runtime_block_multiplayer_constant_weapon> weapons;
    c_tag_block<s_multiplayer_globals_runtime_block_multiplayer_constant_vehicle> vehicles;
    c_tag_block<s_multiplayer_globals_runtime_block_multiplayer_constant_projectile> projectiles;
    c_tag_block<s_multiplayer_globals_runtime_block_multiplayer_constant_equipment_block> equipment;
    float unknown27;
    float unknown28;
    float unknown29;
    float unknown30;
    float unknown31;
    float unknown32;
    float unknown33;
    float unknown34;
    float unknown35;
    float unknown36;
    float unknown37;
    float unknown38;
    float unknown39;
    float unknown40;
    float unknown41;
    float unknown42;
    float unknown43;
    float unknown44;
    float unknown45;
    float unknown46;
    float unknown47;
    float unknown48;
    float unknown49;
    float unknown50;
    float unknown51;
    float unknown52;
    float unknown53;
    float unknown54;
    float unknown55;
    float unknown56;
    float unknown57;
    float unknown58;
    float unknown59;
    float unknown60;
    float unknown61;
    float unknown62;
    float unknown63;
    float unknown64;
    float unknown65;
    float unknown66;
    float maximum_random_spawn_bias;
    float teleporter_recharge_time;
    float grenade_danger_weight;
    float grenade_danger_inner_radius;
    float grenade_danger_outer_radius;
    float grenade_danger_lead_time;
    float vehicle_danger_minimum_speed;
    float vehicle_danger_weight;
    float vehicle_danger_radius;
    float vehicle_danger_lead_time;
    float vehicle_nearby_player_distance;
    s_tag_reference hill_shader;
    float unknown67;
    float unknown68;
    float unknown69;
    float unknown70;
    s_tag_reference bomb_explode_effect;
    s_tag_reference unknown71;
    s_tag_reference bomb_explode_damage_effect;
    s_tag_reference bomb_defuse_effect;
    s_tag_reference cursor_impact_effect;
    string_id bomb_defusal_string;
    string_id blocked_teleporter_string;
    long unknown72;
    string_id unknown73;
    string_id spawn_allowed_default_respawn_string;
    string_id spawn_at_player_looking_at_self_string;
    string_id spawn_at_player_looking_at_target_string;
    string_id spawn_at_player_looking_at_potential_target_string;
    string_id spawn_at_territory_allowed_looking_at_target_string;
    string_id spawn_at_territory_allowed_looking_at_potential_target_string;
    string_id player_out_of_lives_string;
    string_id invalid_spawn_target_string;
    string_id targetted_player_enemies_nearby_string;
    string_id targetted_player_unfriendly_team_string;
    string_id targetted_player_is_dead_string;
    string_id targetted_player_in_combat_string;
    string_id targetted_player_too_far_from_owned_flag_string;
    string_id no_available_netpoints_string;
    string_id netpoint_contested_string;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_multiplayer_constant) == 0x220);

struct s_multiplayer_globals_runtime_block_multiplayer_constant_equipment_block
{
    s_tag_reference type;
    float unknown;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_multiplayer_constant_equipment_block) == 0x14);

struct s_multiplayer_globals_runtime_block_multiplayer_constant_projectile
{
    s_tag_reference type;
    float unknown;
    float unknown2;
    float unknown3;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_multiplayer_constant_projectile) == 0x1C);

struct s_multiplayer_globals_runtime_block_multiplayer_constant_vehicle
{
    s_tag_reference type;
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_multiplayer_constant_vehicle) == 0x20);

struct s_multiplayer_globals_runtime_block_multiplayer_constant_weapon
{
    s_tag_reference type;
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_multiplayer_constant_weapon) == 0x20);

struct s_multiplayer_globals_runtime_block_event_block
{
    ushort flags;
    c_enum<e_multiplayer_globals_runtime_block_event_block_type, short> type;
    string_id event;
    c_enum<e_multiplayer_globals_runtime_block_event_block_audience, short> audience;
    short unknown;
    c_enum<e_multiplayer_globals_runtime_block_event_block_team, short> team;
    short unknown2;
    string_id display_string;
    string_id display_medal;
    ulong unknown3;
    ulong unknown4;
    c_enum<e_multiplayer_globals_runtime_block_event_block_required_field, short> required_field;
    c_enum<e_multiplayer_globals_runtime_block_event_block_required_field, short> excluded_audience;
    c_enum<e_multiplayer_globals_runtime_block_event_block_required_field, short> required_field2;
    c_enum<e_multiplayer_globals_runtime_block_event_block_required_field, short> excluded_audience2;
    string_id primary_string;
    long primary_string_duration;
    string_id plural_display_string;
    float sound_delay_announcer_only;
    ushort sound_flags;
    short unknown5;
    s_tag_reference english_sound;
    s_tag_reference japanese_sound;
    s_tag_reference german_sound;
    s_tag_reference french_sound;
    s_tag_reference spanish_sound;
    s_tag_reference latin_american_spanish_sound;
    s_tag_reference italian_sound;
    s_tag_reference korean_sound;
    s_tag_reference chinese_traditional_sound;
    s_tag_reference chinese_simplified_sound;
    s_tag_reference portuguese_sound;
    s_tag_reference polish_sound;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_event_block) == 0x10C);

struct s_multiplayer_globals_runtime_block_looping_sound
{
    s_tag_reference type;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_looping_sound) == 0x10);

struct s_multiplayer_globals_runtime_block_sound
{
    s_tag_reference type;
};
static_assert(sizeof(s_multiplayer_globals_runtime_block_sound) == 0x10);

struct s_multiplayer_globals_universal_block
{
    s_tag_reference random_player_name_strings;
    s_tag_reference team_name_strings;
    c_tag_block<s_multiplayer_globals_universal_block_team_color> team_colors;
    c_tag_block<s_multiplayer_globals_universal_block_armor_customization_block> armor_customization;
    c_tag_block<s_multiplayer_globals_universal_block_consumable> equipment;
    c_tag_block<s_multiplayer_globals_universal_block_energy_regeneration_block> energy_regeneration;
    s_tag_reference multiplayer_strings;
    s_tag_reference sandbox_ui_strings;
    s_tag_reference sandbox_ui_properties;
    c_tag_block<s_multiplayer_globals_universal_block_game_variant_weapon> game_variant_weapons;
    c_tag_block<s_multiplayer_globals_universal_block_game_variant_vehicle> game_variant_vehicles;
    c_tag_block<s_multiplayer_globals_universal_block_game_variant_equipment_block> game_variant_equipment;
    c_tag_block<s_multiplayer_globals_universal_block_weapon_set> weapon_sets;
    c_tag_block<s_multiplayer_globals_universal_block_vehicle_set> vehicle_sets;
    c_tag_block<s_multiplayer_globals_universal_block_podium_animation> podium_animations;
    s_tag_reference engine_settings;
};
static_assert(sizeof(s_multiplayer_globals_universal_block) == 0xD8);

struct s_multiplayer_globals_universal_block_podium_animation
{
    s_tag_reference animation_graph;
    string_id default_unarmed;
    string_id default_armed;
    c_tag_block<s_multiplayer_globals_universal_block_podium_animation_stance_animation> stance_animations;
    c_tag_block<s_multiplayer_globals_universal_block_podium_animation_move_animation> move_animations;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_podium_animation) == 0x30);

struct s_multiplayer_globals_universal_block_podium_animation_move_animation
{
    char name[32];
    string_id in_animation;
    string_id loop_animation;
    string_id out_animation;
    long unknown;
    s_tag_reference primary_weapon;
    s_tag_reference secondary_weapon;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_podium_animation_move_animation) == 0x0);

struct s_multiplayer_globals_universal_block_podium_animation_stance_animation
{
    char name[32];
    string_id base_animation;
    string_id loop_animation;
    string_id unarmed_transition;
    string_id armed_transition;
    float unknown;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_podium_animation_stance_animation) == 0x0);

struct s_multiplayer_globals_universal_block_vehicle_set
{
    string_id name;
    c_tag_block<s_multiplayer_globals_universal_block_vehicle_set_substitution> substitutions;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_vehicle_set) == 0x10);

struct s_multiplayer_globals_universal_block_vehicle_set_substitution
{
    string_id original_vehicle;
    string_id substituted_vehicle;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_vehicle_set_substitution) == 0x8);

struct s_multiplayer_globals_universal_block_weapon_set
{
    string_id name;
    c_tag_block<s_multiplayer_globals_universal_block_weapon_set_substitution> substitutions;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_weapon_set) == 0x10);

struct s_multiplayer_globals_universal_block_weapon_set_substitution
{
    string_id original_weapon;
    string_id substituted_weapon;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_weapon_set_substitution) == 0x8);

struct s_multiplayer_globals_universal_block_game_variant_equipment_block
{
    string_id name;
    s_tag_reference grenade;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_game_variant_equipment_block) == 0x14);

struct s_multiplayer_globals_universal_block_game_variant_vehicle
{
    string_id name;
    s_tag_reference vehicle;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_game_variant_vehicle) == 0x14);

struct s_multiplayer_globals_universal_block_game_variant_weapon
{
    string_id name;
    float random_chance;
    s_tag_reference weapon;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_game_variant_weapon) == 0x18);

struct s_multiplayer_globals_universal_block_energy_regeneration_block
{
    long duration;
    long energy_level;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_energy_regeneration_block) == 0x8);

struct s_multiplayer_globals_universal_block_consumable
{
    string_id name;
    s_tag_reference object;
    short type;
    byte unused[2];
};
static_assert(sizeof(s_multiplayer_globals_universal_block_consumable) == 0x18);

struct s_multiplayer_globals_universal_block_armor_customization_block
{
    string_id character_name;
    c_tag_block<s_multiplayer_globals_universal_block_armor_customization_block_region> regions;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_armor_customization_block) == 0x10);

struct s_multiplayer_globals_universal_block_armor_customization_block_region
{
    string_id name;
    c_tag_block<s_multiplayer_globals_universal_block_armor_customization_block_region_permutation> permutations;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_armor_customization_block_region) == 0x10);

struct s_multiplayer_globals_universal_block_armor_customization_block_region_permutation
{
    string_id name;
    string_id description;
    c_flags<e_multiplayer_globals_universal_block_armor_customization_block_region_permutation_flags, ushort> flags;
    short unknown;
    string_id achievement_requirement;
    c_tag_block<s_multiplayer_globals_universal_block_armor_customization_block_region_permutation_variant_block> variant;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_armor_customization_block_region_permutation) == 0x1C);

struct s_multiplayer_globals_universal_block_armor_customization_block_region_permutation_variant_block
{
    string_id region;
    string_id permutation;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_armor_customization_block_region_permutation_variant_block) == 0x8);

struct s_multiplayer_globals_universal_block_team_color
{
    real_rgb_color color;
};
static_assert(sizeof(s_multiplayer_globals_universal_block_team_color) == 0xC);

