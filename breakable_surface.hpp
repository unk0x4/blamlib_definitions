/* ---------- enums */


/* ---------- structures */

struct s_breakable_surface;

struct s_breakable_surface
{
    float maximum_vitality;
    s_tag_reference effect;
    s_tag_reference sound;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    s_tag_reference crack_bitmap;
    s_tag_reference hole_bitmap;
};
static_assert(sizeof(s_breakable_surface) == 0x54);

