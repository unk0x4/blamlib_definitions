/* ---------- enums */

enum e_gui_bitmap_widget_definition_blend_method
{
    _gui_bitmap_widget_definition_blend_method_standard = 0,
    _gui_bitmap_widget_definition_blend_method_unknown = 1,
    _gui_bitmap_widget_definition_blend_method_unknown2 = 2,
    _gui_bitmap_widget_definition_blend_method_alpha = 3,
    _gui_bitmap_widget_definition_blend_method_overlay = 4,
    _gui_bitmap_widget_definition_blend_method_unknown3 = 5,
    _gui_bitmap_widget_definition_blend_method_lighter_color = 6,
    _gui_bitmap_widget_definition_blend_method_unknown4 = 7,
    _gui_bitmap_widget_definition_blend_method_unknown5 = 8,
    _gui_bitmap_widget_definition_blend_method_unknown6 = 9,
    _gui_bitmap_widget_definition_blend_method_inverted_alpha = 10,
    _gui_bitmap_widget_definition_blend_method_unknown7 = 11,
    _gui_bitmap_widget_definition_blend_method_unknown8 = 12,
    _gui_bitmap_widget_definition_blend_method_unknown9 = 13
};


/* ---------- structures */

struct s_gui_definition;
struct s_gui_bitmap_widget_definition;

struct s_gui_bitmap_widget_definition
{
    ulong flags;
    s_gui_definition gui_render_block;
    s_tag_reference bitmap;
    s_tag_reference unknown2;
    c_enum<e_gui_bitmap_widget_definition_blend_method, short> blend_method;
    short unknown3;
    short sprite_index;
    short unknown4;
    string_id data_source_name;
    string_id sprite_data_source_name;
};
static_assert(sizeof(s_gui_bitmap_widget_definition) == 0x5C);

struct s_gui_definition
{
    string_id name;
    short unknown;
    short layer;
    short widescreen_y_min;
    short widescreen_x_min;
    short widescreen_y_max;
    short widescreen_x_max;
    short standard_y_min;
    short standard_x_min;
    short standard_y_max;
    short standard_x_max;
    s_tag_reference animation;
};
static_assert(sizeof(s_gui_definition) == 0x28);

