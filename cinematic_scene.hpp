/* ---------- enums */

enum e_cinematic_scene_object_block_object_flags
{
    _cinematic_scene_object_block_object_flags_placed_manually_in_sapien_bit = 0,
    _cinematic_scene_object_block_object_flags_object_comes_from_game_bit = 1,
    _cinematic_scene_object_block_object_flags_special_case_like_player0_bit = 2,
    _cinematic_scene_object_block_object_flags_effect_object_bit = 3,
    _cinematic_scene_object_block_object_flags_no_lightmap_shadow_bit = 4,
    _cinematic_scene_object_block_object_flags_apply_player_customization_bit = 5,
    _cinematic_scene_object_block_object_flags_apply_first_person_player_customization_bit = 6,
    _cinematic_scene_object_block_object_flags_iwillanimatethe_englishlipsyncmanually_bit = 7,
    _cinematic_scene_object_block_object_flags_primary_cortana_bit = 8,
    _cinematic_scene_object_block_object_flags_preload_textures_bit = 9
};

enum e_cinematic_scene_override_creation_flags
{
    _cinematic_scene_override_creation_flags_single_player_bit = 0,
    _cinematic_scene_override_creation_flags_two_player_coop_bit = 1,
    _cinematic_scene_override_creation_flags_three_player_coop_bit = 2,
    _cinematic_scene_override_creation_flags_four_player_coop_bit = 3
};

enum e_cinematic_scene_shot_block_shot_flags
{
    _cinematic_scene_shot_block_shot_flags_instant_auto_exposure_bit = 0,
    _cinematic_scene_shot_block_shot_flags_force_exposure_bit = 1,
    _cinematic_scene_shot_block_shot_flags_generate_looping_script_bit = 2
};

enum e_cinematic_scene_shot_block_lighting_block_lighting_flags
{
    _cinematic_scene_shot_block_lighting_block_lighting_flags_persists_across_shots_bit = 0
};

enum e_cinematic_scene_shot_block_music_block_music_flags
{
    _cinematic_scene_shot_block_music_block_music_flags_stop_music_at_frame_rather_than_starting_it_bit = 0
};

enum e_cinematic_scene_shot_block_function_block_key_frame_key_frame_flags
{
    _cinematic_scene_shot_block_function_block_key_frame_key_frame_flags_clear_function_bit = 0
};

enum e_cinematic_scene_camera_frame_flag_bits
{
    _cinematic_scene_camera_frame_flag_bits_enable_depth_of_field_bit = 0
};


/* ---------- structures */

struct s_cinematic_scene_object_block_attachments_block;
struct s_cinematic_scene_object_block;
struct s_cinematic_scene_shot_block_lighting_block;
struct s_cinematic_scene_shot_block_clip_block_clip_object;
struct s_cinematic_scene_shot_block_clip_block;
struct s_cinematic_scene_shot_block_dialogue_block;
struct s_cinematic_scene_shot_block_music_block;
struct s_cinematic_scene_shot_block_effect_block;
struct s_cinematic_scene_shot_block_function_block_key_frame;
struct s_cinematic_scene_shot_block_function_block;
struct s_cinematic_scene_shot_block_screen_effect_block;
struct s_cinematic_scene_shot_block_cortana_effect_block;
struct s_cinematic_scene_shot_block_import_script_block;
struct s_cinematic_scene_shot_block_user_input_constraints_block;
struct s_cinematic_scene_camera_frame;
struct s_cinematic_scene_shot_block;
struct s_cinematic_scene_texture_camera_block_camera_shot_block_frame_block;
struct s_cinematic_scene_texture_camera_block_camera_shot_block;
struct s_cinematic_scene_texture_camera_block;
struct s_cinematic_scene;

struct s_cinematic_scene
{
    string_id name;
    char anchor_name[32];
    ulong unknown1;
    s_tag_data import_script_header;
    c_tag_block<s_cinematic_scene_object_block> objects;
    c_tag_block<s_cinematic_scene_shot_block> shots;
    c_tag_block<s_cinematic_scene_texture_camera_block> texture_cameras;
    s_tag_data import_script_footer;
    ulong version;
};
static_assert(sizeof(s_cinematic_scene) == 0x78);

struct s_cinematic_scene_texture_camera_block
{
    string_id name;
    string_id unknown;
    c_tag_block<s_cinematic_scene_texture_camera_block_camera_shot_block> shots;
};
static_assert(sizeof(s_cinematic_scene_texture_camera_block) == 0x14);

struct s_cinematic_scene_texture_camera_block_camera_shot_block
{
    c_tag_block<s_cinematic_scene_texture_camera_block_camera_shot_block_frame_block> frames;
};
static_assert(sizeof(s_cinematic_scene_texture_camera_block_camera_shot_block) == 0xC);

struct s_cinematic_scene_texture_camera_block_camera_shot_block_frame_block
{
    ulong unknown_index;
    s_cinematic_scene_camera_frame camera_frame;
};
static_assert(sizeof(s_cinematic_scene_texture_camera_block_camera_shot_block_frame_block) == 0x48);

struct s_cinematic_scene_shot_block
{
    s_tag_data import_script_header;
    c_flags<e_cinematic_scene_shot_block_shot_flags, long> flags;
    float environment_darken;
    float forced_exposure;
    c_tag_block<s_cinematic_scene_shot_block_lighting_block> lighting;
    c_tag_block<s_cinematic_scene_shot_block_clip_block> clips;
    c_tag_block<s_cinematic_scene_shot_block_dialogue_block> dialogue;
    c_tag_block<s_cinematic_scene_shot_block_music_block> music;
    c_tag_block<s_cinematic_scene_shot_block_effect_block> effects;
    c_tag_block<s_cinematic_scene_shot_block_function_block> functions;
    c_tag_block<s_cinematic_scene_shot_block_screen_effect_block> screen_effects;
    c_tag_block<s_cinematic_scene_shot_block_cortana_effect_block> cortana_effects;
    c_tag_block<s_cinematic_scene_shot_block_import_script_block> import_scripts;
    c_tag_block<s_cinematic_scene_shot_block_user_input_constraints_block> user_input_constraints;
    s_tag_data import_script_footer;
    long frame_count;
    c_tag_block<s_cinematic_scene_camera_frame> camera_frames;
};
static_assert(sizeof(s_cinematic_scene_shot_block) == 0xBC);

struct s_cinematic_scene_camera_frame
{
    real_point3d camera_position;
    real_vector3d camera_forward;
    real_vector3d camera_up;
    float unknown7;
    float unknown8;
    float focal_length;
    c_flags<e_cinematic_scene_camera_frame_flag_bits, long> flags;
    float near_focal_plane_distance;
    float far_focal_plane_distance;
    float focal_depth;
    float blur_amount;
};
static_assert(sizeof(s_cinematic_scene_camera_frame) == 0x44);

struct s_cinematic_scene_shot_block_user_input_constraints_block
{
    long frame;
    long ticks;
    rectangle2d maximum_look_angles;
    float frictional_force;
};
static_assert(sizeof(s_cinematic_scene_shot_block_user_input_constraints_block) == 0x14);

struct s_cinematic_scene_shot_block_import_script_block
{
    long frame;
    s_tag_data import_script;
};
static_assert(sizeof(s_cinematic_scene_shot_block_import_script_block) == 0x18);

struct s_cinematic_scene_shot_block_cortana_effect_block
{
    s_tag_reference cortana_effect;
    ulong unknown;
};
static_assert(sizeof(s_cinematic_scene_shot_block_cortana_effect_block) == 0x14);

struct s_cinematic_scene_shot_block_screen_effect_block
{
    s_tag_reference screen_effect;
    long start_frame;
    long stop_frame;
};
static_assert(sizeof(s_cinematic_scene_shot_block_screen_effect_block) == 0x18);

struct s_cinematic_scene_shot_block_function_block
{
    long object_index;
    string_id target_function_name;
    c_tag_block<s_cinematic_scene_shot_block_function_block_key_frame> key_frames;
};
static_assert(sizeof(s_cinematic_scene_shot_block_function_block) == 0x14);

struct s_cinematic_scene_shot_block_function_block_key_frame
{
    c_flags<e_cinematic_scene_shot_block_function_block_key_frame_key_frame_flags, long> flags;
    long frame;
    float value;
    float interpolation_time;
};
static_assert(sizeof(s_cinematic_scene_shot_block_function_block_key_frame) == 0x10);

struct s_cinematic_scene_shot_block_effect_block
{
    s_tag_reference effect;
    long frame;
    string_id marker;
    long owner_object_index;
};
static_assert(sizeof(s_cinematic_scene_shot_block_effect_block) == 0x1C);

struct s_cinematic_scene_shot_block_music_block
{
    c_flags<e_cinematic_scene_shot_block_music_block_music_flags, long> flags;
    s_tag_reference sound;
    long frame;
};
static_assert(sizeof(s_cinematic_scene_shot_block_music_block) == 0x18);

struct s_cinematic_scene_shot_block_dialogue_block
{
    s_tag_reference sound;
    long frame;
    float scale;
    string_id lipsync_actor;
    string_id default_sound_effect;
    string_id subtitle;
};
static_assert(sizeof(s_cinematic_scene_shot_block_dialogue_block) == 0x24);

struct s_cinematic_scene_shot_block_clip_block
{
    real_point3d plane_center;
    real_point3d plane_direction;
    ulong frame_start;
    ulong frame_end;
    c_tag_block<s_cinematic_scene_shot_block_clip_block_clip_object> objects;
};
static_assert(sizeof(s_cinematic_scene_shot_block_clip_block) == 0x2C);

struct s_cinematic_scene_shot_block_clip_block_clip_object
{
    ulong object_index;
};
static_assert(sizeof(s_cinematic_scene_shot_block_clip_block_clip_object) == 0x4);

struct s_cinematic_scene_shot_block_lighting_block
{
    c_flags<e_cinematic_scene_shot_block_lighting_block_lighting_flags, long> flags;
    s_tag_reference cinematic_light;
    long object_index;
    string_id marker;
};
static_assert(sizeof(s_cinematic_scene_shot_block_lighting_block) == 0x1C);

struct s_cinematic_scene_object_block
{
    char import_name[32];
    string_id identifier;
    string_id variant_name;
    s_tag_reference puppet_animation;
    s_tag_reference puppet_object;
    c_flags<e_cinematic_scene_object_block_object_flags, long> flags;
    long unknown_shot_flags;
    c_flags<e_cinematic_scene_override_creation_flags, long> override_creation_flags;
    s_tag_data import_override_creation_script;
    c_tag_block<s_cinematic_scene_object_block_attachments_block> attachments;
};
static_assert(sizeof(s_cinematic_scene_object_block) == 0x74);

struct s_cinematic_scene_object_block_attachments_block
{
    string_id object_marker_name;
    char attachment_object_name[32];
    string_id attachment_marker_name;
    s_tag_reference attachment_type;
};
static_assert(sizeof(s_cinematic_scene_object_block_attachments_block) == 0x38);

