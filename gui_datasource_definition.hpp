/* ---------- enums */


/* ---------- structures */

struct s_gui_datasource_definition_datum_integer_value;
struct s_gui_datasource_definition_datum_string_value;
struct s_gui_datasource_definition_datum_stringid_value;
struct s_gui_datasource_definition_datum;
struct s_gui_datasource_definition;

struct s_gui_datasource_definition
{
    string_id name;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    c_tag_block<s_gui_datasource_definition_datum> data;
};
static_assert(sizeof(s_gui_datasource_definition) == 0x1C);

struct s_gui_datasource_definition_datum
{
    c_tag_block<s_gui_datasource_definition_datum_integer_value> integer_values;
    c_tag_block<s_gui_datasource_definition_datum_string_value> string_values;
    c_tag_block<s_gui_datasource_definition_datum_stringid_value> stringid_values;
    string_id unknown;
};
static_assert(sizeof(s_gui_datasource_definition_datum) == 0x28);

struct s_gui_datasource_definition_datum_stringid_value
{
    string_id data_type;
    string_id value;
};
static_assert(sizeof(s_gui_datasource_definition_datum_stringid_value) == 0x8);

struct s_gui_datasource_definition_datum_string_value
{
    string_id data_type;
    char value[32];
};
static_assert(sizeof(s_gui_datasource_definition_datum_string_value) == 0x24);

struct s_gui_datasource_definition_datum_integer_value
{
    string_id data_type;
    long value;
};
static_assert(sizeof(s_gui_datasource_definition_datum_integer_value) == 0x8);

