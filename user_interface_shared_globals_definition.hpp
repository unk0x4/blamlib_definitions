/* ---------- enums */

enum e_user_interface_shared_globals_definition_alert_icon
{
    _user_interface_shared_globals_definition_alert_icon_none = 0,
    _user_interface_shared_globals_definition_alert_icon_download = 1,
    _user_interface_shared_globals_definition_alert_icon_pause = 2,
    _user_interface_shared_globals_definition_alert_icon_upload = 3,
    _user_interface_shared_globals_definition_alert_icon_checkbox = 4
};

enum e_user_interface_shared_globals_definition_dialog_default_option
{
    _user_interface_shared_globals_definition_dialog_default_option_option1 = 0,
    _user_interface_shared_globals_definition_dialog_default_option_option2 = 1,
    _user_interface_shared_globals_definition_dialog_default_option_option3 = 2,
    _user_interface_shared_globals_definition_dialog_default_option_option4 = 3
};


/* ---------- structures */

struct s_user_interface_shared_globals_definition_text_color_block;
struct s_user_interface_shared_globals_definition_player_color_player_text_color_block;
struct s_user_interface_shared_globals_definition_player_color_team_text_color_block;
struct s_user_interface_shared_globals_definition_player_color_player_ui_color_block;
struct s_user_interface_shared_globals_definition_player_color_team_ui_color_block;
struct s_user_interface_shared_globals_definition_player_color;
struct s_user_interface_shared_globals_definition_alert;
struct s_user_interface_shared_globals_definition_dialog;
struct s_user_interface_shared_globals_definition_global_data_source;
struct s_user_interface_shared_globals_definition_ui_widget_biped;
struct s_tag_function;
struct s_user_interface_shared_globals_definition_arg_block;
struct s_user_interface_shared_globals_definition;

struct s_user_interface_shared_globals_definition
{
    short inc_text_update_period;
    short inc_text_block_character;
    float near_clip_plane_distance;
    float projection_plane_distance;
    float far_clip_plane_distance;
    s_tag_reference global_strings;
    s_tag_reference damage_type_strings;
    s_tag_reference unknown_strings;
    s_tag_reference main_menu_music;
    long music_fade_time;
    real_argb_color color;
    real_argb_color text_color;
    c_tag_block<s_user_interface_shared_globals_definition_text_color_block> text_colors;
    c_tag_block<s_user_interface_shared_globals_definition_player_color> player_colors;
    s_tag_reference ui_sounds;
    c_tag_block<s_user_interface_shared_globals_definition_alert> alerts;
    c_tag_block<s_user_interface_shared_globals_definition_dialog> dialogs;
    c_tag_block<s_user_interface_shared_globals_definition_global_data_source> global_data_sources;
    float widescreen_bitmap_scale_x;
    float widescreen_bitmap_scale_y;
    float standard_bitmap_scale_x;
    float standard_bitmap_scale_y;
    float menu_blur_x;
    float menu_blur_y;
    c_tag_block<s_user_interface_shared_globals_definition_ui_widget_biped> ui_widget_bipeds;
    string_id unknown_player1;
    string_id unknown_player2;
    string_id unknown_player3;
    string_id unknown_player4;
    char ui_elite_biped_name[32];
    char ui_elite_ai_squad_name[32];
    string_id ui_elite_ai_location_name;
    char ui_odst1_biped_name[32];
    char ui_odst1_ai_squad_name[32];
    string_id ui_odst1_ai_location_name;
    char ui_mickey_biped_name[32];
    char ui_mickey_ai_squad_name[32];
    string_id ui_mickey_ai_location_name;
    char ui_romeo_biped_name[32];
    char ui_romeo_ai_squad_name[32];
    string_id ui_romeo_ai_location_name;
    char ui_dutch_biped_name[32];
    char ui_dutch_ai_squad_name[32];
    string_id ui_dutch_ai_location_name;
    char ui_johnson_biped_name[32];
    char ui_johnson_ai_squad_name[32];
    string_id ui_johnson_ai_location_name;
    char ui_odst2_biped_name[32];
    char ui_odst2_ai_squad_name[32];
    string_id ui_odst2_ai_location_name;
    char ui_odst3_biped_name[32];
    char ui_odst3_ai_squad_name[32];
    string_id ui_odst3_ai_location_name;
    char ui_odst4_biped_name[32];
    char ui_odst4_ai_squad_name[32];
    string_id ui_odst4_ai_location_name;
    long single_scroll_speed;
    long scroll_speed_transition_wait_time;
    long held_scroll_speed;
    long attract_video_idle_wait;
    s_tag_function unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    real_argb_color unknown17;
    real_argb_color unknown18;
    string_id unknown19;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
    c_tag_block<s_user_interface_shared_globals_definition_arg_block> arg;
};
static_assert(sizeof(s_user_interface_shared_globals_definition) == 0x3CC);

struct s_user_interface_shared_globals_definition_arg_block
{
    string_id name;
    s_tag_reference audio;
    s_tag_reference timing;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_arg_block) == 0x24);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_user_interface_shared_globals_definition_ui_widget_biped
{
    char appearance_biped_name[32];
    char appearance_ai_squad_name[32];
    string_id appearance_ai_location_name;
    char roster_player1_biped_name[32];
    char roster_player1_ai_squad_name[32];
    string_id roster_player1_ai_location_name;
    char roster_player2_biped_name[32];
    char roster_player2_ai_squad_name[32];
    string_id roster_player2_ai_location_name;
    char roster_player3_biped_name[32];
    char roster_player3_ai_squad_name[32];
    string_id roster_player3_ai_location_name;
    char roster_player4_biped_name[32];
    char roster_player4_ai_squad_name[32];
    string_id roster_player4_ai_location_name;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_ui_widget_biped) == 0x154);

struct s_user_interface_shared_globals_definition_global_data_source
{
    s_tag_reference data_source;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_global_data_source) == 0x10);

struct s_user_interface_shared_globals_definition_dialog
{
    string_id name;
    short unknown;
    short unknown2;
    string_id title;
    string_id body;
    string_id option1;
    string_id option2;
    string_id option3;
    string_id option4;
    string_id key_legend;
    c_enum<e_user_interface_shared_globals_definition_dialog_default_option, short> default_option;
    short unknown3;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_dialog) == 0x28);

struct s_user_interface_shared_globals_definition_alert
{
    string_id name;
    uchar flags;
    char unknown;
    c_enum<e_user_interface_shared_globals_definition_alert_icon, char> icon;
    char unknown2;
    string_id title;
    string_id body;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_alert) == 0x10);

struct s_user_interface_shared_globals_definition_player_color
{
    c_tag_block<s_user_interface_shared_globals_definition_player_color_player_text_color_block> player_text_color;
    c_tag_block<s_user_interface_shared_globals_definition_player_color_team_text_color_block> team_text_color;
    c_tag_block<s_user_interface_shared_globals_definition_player_color_player_ui_color_block> player_ui_color;
    c_tag_block<s_user_interface_shared_globals_definition_player_color_team_ui_color_block> team_ui_color;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_player_color) == 0x30);

struct s_user_interface_shared_globals_definition_player_color_team_ui_color_block
{
    real_argb_color color;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_player_color_team_ui_color_block) == 0x10);

struct s_user_interface_shared_globals_definition_player_color_player_ui_color_block
{
    real_argb_color color;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_player_color_player_ui_color_block) == 0x10);

struct s_user_interface_shared_globals_definition_player_color_team_text_color_block
{
    real_argb_color color;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_player_color_team_text_color_block) == 0x10);

struct s_user_interface_shared_globals_definition_player_color_player_text_color_block
{
    real_argb_color color;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_player_color_player_text_color_block) == 0x10);

struct s_user_interface_shared_globals_definition_text_color_block
{
    string_id name;
    real_argb_color color;
};
static_assert(sizeof(s_user_interface_shared_globals_definition_text_color_block) == 0x14);

