/* ---------- enums */


/* ---------- structures */

struct s_mod_globals_definition_player_character_set_player_character;
struct s_mod_globals_definition_player_character_set;
struct s_mod_globals_definition_player_character_reference;
struct s_mod_globals_definition;

struct s_mod_globals_definition
{
    c_tag_block<s_mod_globals_definition_player_character_set> player_character_sets;
    c_tag_block<s_mod_globals_definition_player_character_reference> player_characters;
};
static_assert(sizeof(s_mod_globals_definition) == 0x18);

struct s_mod_globals_definition_player_character_reference
{
    s_tag_reference player_character;
};
static_assert(sizeof(s_mod_globals_definition_player_character_reference) == 0x10);

struct s_mod_globals_definition_player_character_set
{
    char display_name[32];
    string_id name;
    float random_chance;
    c_tag_block<s_mod_globals_definition_player_character_set_player_character> characters;
};
static_assert(sizeof(s_mod_globals_definition_player_character_set) == 0x34);

struct s_mod_globals_definition_player_character_set_player_character
{
    char display_name[32];
    string_id name;
    float random_chance;
};
static_assert(sizeof(s_mod_globals_definition_player_character_set_player_character) == 0x28);

