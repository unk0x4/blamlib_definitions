/* ---------- enums */

enum e_character_general_flags
{
    _character_general_flags_swarm_bit = 0,
    _character_general_flags_flying_bit = 1,
    _character_general_flags_dual_wields_bit = 2,
    _character_general_flags_uses_gravemind_bit = 3,
    _character_general_flags_do_not_trade_weapon_bit = 5,
    _character_general_flags_do_not_stow_weapon_bit = 6,
    _character_general_flags_hero_character_bit = 7,
    _character_general_flags_leader_independent_positioning_bit = 8,
    _character_general_flags_has_active_camo_bit = 9,
    _character_general_flags_use_head_marker_for_looking_bit = 10,
    _character_general_flags_space_character_bit = 11,
    _character_general_flags_do_not_drop_equipment_bit = 12,
    _character_general_flags_do_not_allow_crouch_bit = 13,
    _character_general_flags_do_not_allow_moving_crouch_bit = 14,
    _character_general_flags_critical_betrayal_bit = 15,
    _character_general_flags_deathless_critical_betrayal_bit = 16,
    _character_general_flags_armor_prevents_assassination_bit = 17,
    _character_general_flags_drop_all_weapons_on_death_bit = 18,
    _character_general_flags_drop_no_weapons_on_death_bit = 19,
    _character_general_flags_shield_prevents_assassination_bit = 20,
    _character_general_flags_cannot_be_assassinated_bit = 21
};

enum e_ai_actor_type
{
    _ai_actor_type_elite = 0,
    _ai_actor_type_jackal = 1,
    _ai_actor_type_grunt = 2,
    _ai_actor_type_hunter = 3,
    _ai_actor_type_engineer = 4,
    _ai_actor_type_assassin = 5,
    _ai_actor_type_player = 6,
    _ai_actor_type_marine = 7,
    _ai_actor_type_crew = 8,
    _ai_actor_type_combat_form = 9,
    _ai_actor_type_infection_form = 10,
    _ai_actor_type_carrier_form = 11,
    _ai_actor_type_monitor = 12,
    _ai_actor_type_sentinel = 13,
    _ai_actor_type_none = 14,
    _ai_actor_type_mounted_weapon = 15,
    _ai_actor_type_brute = 16,
    _ai_actor_type_prophet = 17,
    _ai_actor_type_bugger = 18,
    _ai_actor_type_juggernaut = 19,
    _ai_actor_type_pure_form_stealth = 20,
    _ai_actor_type_pure_form_tank = 21,
    _ai_actor_type_pure_form_ranged = 22,
    _ai_actor_type_scarab = 23,
    _ai_actor_type_guardian = 24
};

enum e_ai_follower_positioning
{
    _ai_follower_positioning_in_front_of_me = 0,
    _ai_follower_positioning_behind_me = 1,
    _ai_follower_positioning_tight = 2
};

enum e_character_default_grenade_type
{
    _character_default_grenade_type_none = 0,
    _character_default_grenade_type_human_fragmentation = 1,
    _character_default_grenade_type_covenant_plasma = 2,
    _character_default_grenade_type_brute_claymore = 3,
    _character_default_grenade_type_firebomb = 4
};

enum e_character_behavior_tree_root
{
    _character_behavior_tree_root_default = 0,
    _character_behavior_tree_root_flying = 1
};

enum e_character_vitality_flags
{
    _character_vitality_flags_auto_resurrect_bit = 0
};

enum e_character_perception_mode
{
    _character_perception_mode_idle = 0,
    _character_perception_mode_alert = 1,
    _character_perception_mode_combat = 2,
    _character_perception_mode_search = 3,
    _character_perception_mode_patrol = 4,
    _character_perception_mode_vehicle_idle = 5,
    _character_perception_mode_vehicle_alert = 6,
    _character_perception_mode_vehicle_combat = 7
};

enum e_character_perception_flags
{
    _character_perception_flags_character_can_see_in_darkness_bit = 0,
    _character_perception_flags_ignore_tracking_projectiles_bit = 1,
    _character_perception_flags_ignore_minor_tracking_projectiles_bit = 2
};

enum e_character_movement_flags
{
    _character_movement_flags_danger_crouch_allow_movement_bit = 0,
    _character_movement_flags_no_side_step_bit = 1,
    _character_movement_flags_prefer_to_combat_near_friends_bit = 2,
    _character_movement_flags_allow_boosted_jump_bit = 3,
    _character_movement_flags_perch_bit = 4,
    _character_movement_flags_climb_bit = 5,
    _character_movement_flags_prefer_wall_movement_bit = 6,
    _character_movement_flags_has_flying_mode_bit = 7,
    _character_movement_flags_disallow_crouch_bit = 8,
    _character_movement_flags_disallow_all_movement_bit = 9,
    _character_movement_flags_always_use_search_points_bit = 10,
    _character_movement_flags_keep_moving_bit = 11,
    _character_movement_flags_cure_isolation_jump_bit = 12,
    _character_movement_flags_gain_elevation_bit = 13,
    _character_movement_flags_reposition_distant_bit = 14,
    _character_movement_flags_only_use_aerial_firing_positions_bit = 15,
    _character_movement_flags_use_high_priority_path_finding_bit = 16,
    _character_movement_flags_lower_weapon_when_no_alert_movement_override_bit = 17,
    _character_movement_flags_phase_bit = 18,
    _character_movement_flags_no_override_when_firing_bit = 19,
    _character_movement_flags_no_stow_during_idle_activities_bit = 20,
    _character_movement_flags_flip_any_vehicle_bit = 21,
    _character_movement_flags_bound_along_path_bit = 22
};

enum e_ai_size
{
    _ai_size_none = 0,
    _ai_size_tiny = 1,
    _ai_size_small = 2,
    _ai_size_medium = 3,
    _ai_size_large = 4,
    _ai_size_huge = 5,
    _ai_size_immobile = 6
};

enum e_character_jump_height
{
    _character_jump_height_none = 0,
    _character_jump_height_down = 1,
    _character_jump_height_step = 2,
    _character_jump_height_crouch = 3,
    _character_jump_height_stand = 4,
    _character_jump_height_storey = 5,
    _character_jump_height_tower = 6,
    _character_jump_height_infinite = 7
};

enum e_character_movement_hint_flags
{
    _character_movement_hint_flags_vault_step_bit = 0,
    _character_movement_hint_flags_vault_crouch_bit = 1,
    _character_movement_hint_flags_unused0_bit = 2,
    _character_movement_hint_flags_unused1_bit = 3,
    _character_movement_hint_flags_unused2_bit = 4,
    _character_movement_hint_flags_mount_step_bit = 5,
    _character_movement_hint_flags_mount_crouch_bit = 6,
    _character_movement_hint_flags_mount_stand_bit = 7,
    _character_movement_hint_flags_unused3_bit = 8,
    _character_movement_hint_flags_unused4_bit = 9,
    _character_movement_hint_flags_unused5_bit = 10,
    _character_movement_hint_flags_hoist_crouch_bit = 11,
    _character_movement_hint_flags_hoist_stand_bit = 12,
    _character_movement_hint_flags_unused6_bit = 13,
    _character_movement_hint_flags_unused7_bit = 14,
    _character_movement_hint_flags_unused8_bit = 15
};

enum e_character_engage_flags
{
    _character_engage_flags_defend_threat_axis_bit = 0,
    _character_engage_flags_fight_constant_movement_bit = 1,
    _character_engage_flags_flight_fight_constant_movement_bit = 2,
    _character_engage_flags_disallow_combat_crouching_bit = 3,
    _character_engage_flags_disallow_crouch_shooting_bit = 4,
    _character_engage_flags_fight_stable_bit = 5,
    _character_engage_flags_throw_should_lob_bit = 6,
    _character_engage_flags_allow_positioning_beyond_ideal_range_bit = 7,
    _character_engage_flags_can_suppress_bit = 8,
    _character_engage_flags_prefers_bunker_bit = 9,
    _character_engage_flags_burst_inhibits_evasion_bit = 10
};

enum e_character_charge_flags
{
    _character_charge_flags_offhand_melee_allowed_bit = 0,
    _character_charge_flags_berserk_whenever_charge_bit = 1,
    _character_charge_flags_do_not_use_berserk_mode_bit = 2,
    _character_charge_flags_do_not_stow_weapon_during_berserk_bit = 3,
    _character_charge_flags_allow_dialogue_while_berserking_bit = 4,
    _character_charge_flags_do_not_play_berserk_animation_bit = 5,
    _character_charge_flags_do_not_shoot_during_charge_bit = 6,
    _character_charge_flags_allow_leap_with_ranged_weapons_bit = 7,
    _character_charge_flags_permanent_berserk_once_initiated_bit = 8
};

enum e_character_cover_flags
{
    _character_cover_flags_use_phasing_bit = 1
};

enum e_character_retreat_flags
{
    _character_retreat_flags_zig_zag_when_fleeing_bit = 0
};

enum e_character_search_flags
{
    _character_search_flags_crouch_on_investigate_bit = 0,
    _character_search_flags_walk_on_pursuit_bit = 1,
    _character_search_flags_search_forever_bit = 2,
    _character_search_flags_search_exclusively_bit = 3,
    _character_search_flags_search_points_only_while_walking_bit = 4
};

enum e_character_pre_search_flags
{
    _character_pre_search_flags_flag1_bit = 0
};

enum e_character_boarding_flags
{
    _character_boarding_flags_airborne_boarding_bit = 0
};

enum e_character_weapon_flags
{
    _character_weapon_flags_bursting_inhibits_movement_bit = 0,
    _character_weapon_flags_must_crouch_to_shoot_bit = 1,
    _character_weapon_flags_use_extended_safe_to_save_range_bit = 2,
    _character_weapon_flags_fixed_aiming_vector_bit = 3,
    _character_weapon_flags_aim_at_feet_bit = 4,
    _character_weapon_flags_force_aim_from_barrel_position_bit = 5,
    _character_weapon_flags_favor_for_long_range_bit = 6,
    _character_weapon_flags_favor_for_close_range_bit = 7,
    _character_weapon_flags_favor_against_vehicles_bit = 8,
    _character_weapon_flags_favored_special_weapon_bit = 9,
    _character_weapon_flags_bursting_inhibits_evasion_bit = 10
};

enum e_character_weapon_special_fire_mode
{
    _character_weapon_special_fire_mode_none = 0,
    _character_weapon_special_fire_mode_overcharge = 1,
    _character_weapon_special_fire_mode_secondary_trigger = 2
};

enum e_character_weapon_special_fire_situation
{
    _character_weapon_special_fire_situation_never = 0,
    _character_weapon_special_fire_situation_enemy_visible = 1,
    _character_weapon_special_fire_situation_enemy_out_of_sight = 2,
    _character_weapon_special_fire_situation_strafing = 3
};

enum e_character_grenade_type
{
    _character_grenade_type_human_fragmentation = 0,
    _character_grenade_type_covenant_plasma = 1,
    _character_grenade_type_brute_claymore = 2,
    _character_grenade_type_firebomb = 3
};

enum e_character_grenade_trajectory_type
{
    _character_grenade_trajectory_type_toss = 0,
    _character_grenade_trajectory_type_lob = 1,
    _character_grenade_trajectory_type_bounce = 2
};

enum e_character_metagame_flags
{
    _character_metagame_flags_must_have_active_seats_bit = 0
};

enum e_character_metagame_unit
{
    _character_metagame_unit_brute = 0,
    _character_metagame_unit_grunt = 1,
    _character_metagame_unit_jackal = 2,
    _character_metagame_unit_marine = 3,
    _character_metagame_unit_bugger = 4,
    _character_metagame_unit_hunter = 5,
    _character_metagame_unit_flood_infection = 6,
    _character_metagame_unit_flood_carrier = 7,
    _character_metagame_unit_flood_combat = 8,
    _character_metagame_unit_flood_pureform = 9,
    _character_metagame_unit_sentinel = 10,
    _character_metagame_unit_elite = 11,
    _character_metagame_unit_turret = 12,
    _character_metagame_unit_mongoose = 13,
    _character_metagame_unit_warthog = 14,
    _character_metagame_unit_scorpion = 15,
    _character_metagame_unit_hornet = 16,
    _character_metagame_unit_pelican = 17,
    _character_metagame_unit_shade = 18,
    _character_metagame_unit_watchtower = 19,
    _character_metagame_unit_ghost = 20,
    _character_metagame_unit_chopper = 21,
    _character_metagame_unit_mauler = 22,
    _character_metagame_unit_wraith = 23,
    _character_metagame_unit_banshee = 24,
    _character_metagame_unit_phantom = 25,
    _character_metagame_unit_scarab = 26,
    _character_metagame_unit_guntower = 27,
    _character_metagame_unit_engineer = 28,
    _character_metagame_unit_engineer_recharge_station = 29
};

enum e_character_metagame_classification
{
    _character_metagame_classification_infantry = 0,
    _character_metagame_classification_leader = 1,
    _character_metagame_classification_hero = 2,
    _character_metagame_classification_specialist = 3,
    _character_metagame_classification_light_vehicle = 4,
    _character_metagame_classification_heavy_vehicle = 5,
    _character_metagame_classification_giant_vehicle = 6,
    _character_metagame_classification_standard_vehicle = 7
};


/* ---------- structures */

struct s_character_dialogue_variation;
struct s_character_variant;
struct s_character_unit_dialogue;
struct s_character_general_properties;
struct s_character_vitality_properties;
struct s_character_placement_properties;
struct s_character_perception_properties;
struct s_character_look_properties;
struct s_character_movement_properties;
struct s_character_flocking_properties;
struct s_character_swarm_properties;
struct s_character_ready_properties;
struct s_character_engage_properties;
struct s_character_charge_difficulty_limit;
struct s_character_charge_properties;
struct s_character_evasion_properties;
struct s_character_cover_properties;
struct s_character_retreat_properties;
struct s_character_search_properties;
struct s_character_pre_search_properties;
struct s_character_idle_properties;
struct s_character_vocalization_properties;
struct s_character_boarding_properties;
struct s_character_bunker_properties;
struct s_character_guardian_properties;
struct s_character_combatform_properties;
struct s_character_engineer_properties;
struct s_character_inspect_properties;
struct s_character_scarab_properties;
struct s_character_firing_pattern;
struct s_character_weapons_properties;
struct s_character_firing_pattern_properties;
struct s_character_grenades_properties;
struct s_character_vehicle_properties;
struct s_character_morph_properties;
struct s_character_equipment_usage_condition;
struct s_character_equipment_properties;
struct s_character_metagame_properties;
struct s_character_act_attachment;
struct s_character;

struct s_character
{
    ulong flags;
    s_tag_reference parent_character;
    s_tag_reference unit;
    s_tag_reference creature;
    s_tag_reference style;
    s_tag_reference major_character;
    c_tag_block<s_character_variant> variants;
    c_tag_block<s_character_unit_dialogue> unit_dialogue;
    c_tag_block<s_character_general_properties> general_properties;
    c_tag_block<s_character_vitality_properties> vitality_properties;
    c_tag_block<s_character_placement_properties> placement_properties;
    c_tag_block<s_character_perception_properties> perception_properties;
    c_tag_block<s_character_look_properties> look_properties;
    c_tag_block<s_character_movement_properties> movement_properties;
    c_tag_block<s_character_flocking_properties> flocking_properties;
    c_tag_block<s_character_swarm_properties> swarm_properties;
    c_tag_block<s_character_ready_properties> ready_properties;
    c_tag_block<s_character_engage_properties> engage_properties;
    c_tag_block<s_character_charge_properties> charge_properties;
    c_tag_block<s_character_evasion_properties> evasion_properties;
    c_tag_block<s_character_cover_properties> cover_properties;
    c_tag_block<s_character_retreat_properties> retreat_properties;
    c_tag_block<s_character_search_properties> search_properties;
    c_tag_block<s_character_pre_search_properties> pre_search_properties;
    c_tag_block<s_character_idle_properties> idle_properties;
    c_tag_block<s_character_vocalization_properties> vocalization_properties;
    c_tag_block<s_character_boarding_properties> boarding_properties;
    c_tag_block<s_character_bunker_properties> bunker_properties;
    c_tag_block<s_character_guardian_properties> guardian_properties;
    c_tag_block<s_character_combatform_properties> combatform_properties;
    c_tag_block<s_character_engineer_properties> engineer_properties;
    c_tag_block<s_character_inspect_properties> inspect_properties;
    c_tag_block<s_character_scarab_properties> scarab_properties;
    c_tag_block<s_character_weapons_properties> weapons_properties;
    c_tag_block<s_character_firing_pattern_properties> firing_pattern_properties;
    c_tag_block<s_character_grenades_properties> grenades_properties;
    c_tag_block<s_character_vehicle_properties> vehicle_properties;
    c_tag_block<s_character_morph_properties> morph_properties;
    c_tag_block<s_character_equipment_properties> equipment_properties;
    c_tag_block<s_character_metagame_properties> metagame_properties;
    c_tag_block<s_character_act_attachment> act_attachments;
};
static_assert(sizeof(s_character) == 0x1F8);

struct s_character_act_attachment
{
    string_id activity_name;
    s_tag_reference crate;
    string_id crate_marker_name;
    string_id unit_marker_name;
};
static_assert(sizeof(s_character_act_attachment) == 0x1C);

struct s_character_metagame_properties
{
    c_flags<e_character_metagame_flags, uchar> flags;
    c_enum<e_character_metagame_unit, char> unit;
    c_enum<e_character_metagame_classification, char> classification;
    byte pad0[1];
    short points;
    byte pad1[2];
};
static_assert(sizeof(s_character_metagame_properties) == 0x8);

struct s_character_equipment_properties
{
    s_tag_reference equipment;
    ulong unknown;
    float usage_chance;
    c_tag_block<s_character_equipment_usage_condition> usage_conditions;
};
static_assert(sizeof(s_character_equipment_properties) == 0x24);

struct s_character_equipment_usage_condition
{
    short unknown;
    short unknown2;
    ulong unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_character_equipment_usage_condition) == 0xC);

struct s_character_morph_properties
{
    s_tag_reference morph_character1;
    s_tag_reference morph_character2;
    s_tag_reference morph_character3;
    s_tag_reference morph_muffin;
    s_tag_reference morph_weapon1;
    s_tag_reference morph_weapon2;
    s_tag_reference morph_weapon3;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    s_tag_reference character;
    ulong unknown9;
    string_id unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
    ulong unknown23;
    ulong unknown24;
    ulong unknown25;
};
static_assert(sizeof(s_character_morph_properties) == 0xE4);

struct s_character_vehicle_properties
{
    s_tag_reference unit;
    s_tag_reference style;
    ulong vehicle_flags;
    float ai_pathfinding_radius;
    float ai_destination_radius;
    float ai_deceleration_distance;
    float ai_turning_radius;
    float ai_inner_turning_radius;
    float ai_ideal_turning_radius;
    real ai_banshee_steering_maximum;
    float ai_max_steering_angle;
    float ai_max_steering_delta;
    float ai_oversteering_scale;
    real_bounds ai_oversteering_bounds;
    float ai_side_slip_distance;
    float ai_avoidance_distance;
    float ai_minimum_urgency;
    real unknown;
    ulong unknown2;
    float ai_throttle_maximum;
    float ai_goal_minimum_throttle_scale;
    float ai_turn_minimum_throttle_scale;
    float ai_direction_minimum_throttle_scale;
    float ai_acceleration_scale;
    float ai_throttle_blend;
    float theoretical_max_speed;
    float error_scale;
    real ai_allowable_aim_deviation_angle;
    float ai_charge_tight_angle_distance;
    float ai_charge_tight_angle;
    float ai_charge_repeat_timeout;
    float ai_charge_look_ahead_time;
    float ai_consider_distance;
    float ai_charge_abort_distance;
    float vehicle_ram_timeout;
    float ram_paralysis_time;
    float ai_cover_damage_threshold;
    float ai_cover_minimum_distance;
    float ai_cover_time;
    float ai_cover_minimum_boost_distance;
    float turtling_recent_damage_threshold;
    float turtling_minimum_time;
    float turtling_timeout;
    c_enum<e_ai_size, short> obstacle_ignore_size;
    short unknown3;
    short unknown4;
    short unknown5;
};
static_assert(sizeof(s_character_vehicle_properties) == 0xD0);

struct s_character_grenades_properties
{
    long grenades_flags;
    c_enum<e_character_grenade_type, short> grenade_type;
    c_enum<e_character_grenade_trajectory_type, short> trajectory_type;
    long minimum_enemy_count;
    float enemy_radius;
    float grenade_ideal_velocity;
    float grenade_velocity;
    real_bounds grenade_range;
    float collateral_damage_radius;
    float grenade_chance;
    float grenade_throw_delay;
    float grenade_uncover_chance;
    float anti_vehicle_grenade_chance;
    short_bounds dropped_grenade_count_bounds;
    float don_t_drop_grenades_chance;
};
static_assert(sizeof(s_character_grenades_properties) == 0x3C);

struct s_character_firing_pattern_properties
{
    s_tag_reference weapon;
    c_tag_block<s_character_firing_pattern> firing_patterns;
};
static_assert(sizeof(s_character_firing_pattern_properties) == 0x1C);

struct s_character_weapons_properties
{
    c_flags<e_character_weapon_flags, long> flags;
    s_tag_reference weapon;
    float maximum_firing_range;
    float minimum_firing_range;
    real_bounds normal_combat_range;
    float bombardment_range;
    float max_special_target_distance;
    real_bounds timid_combat_range;
    real_bounds aggressive_combat_range;
    float super_ballistic_range;
    real_bounds ballistic_firing_bounds;
    real_bounds ballistic_fraction_bounds;
    real_bounds first_burst_delay_time_bounds;
    float surprise_delay_time;
    float surprise_fire_wildly_time;
    float death_fire_wildly_chance;
    float death_fire_wildly_time;
    real_vector3d custom_stand_gun_offset;
    real_vector3d custom_crouch_gun_offset;
    c_enum<e_character_weapon_special_fire_mode, short> special_fire_mode;
    c_enum<e_character_weapon_special_fire_situation, short> special_fire_situation;
    float special_fire_chance;
    float special_fire_delay;
    float special_damage_modifier;
    real special_projectile_error;
    real_bounds drop_weapon_loaded_bounds;
    short_bounds drop_weapon_ammo_bounds;
    real_bounds normal_accuracy_bounds;
    float normal_accuracy_time;
    real_bounds heroic_accuracy_bounds;
    float heroic_accuracy_time;
    real_bounds legendary_accuracy_bounds;
    float legendary_accuracy_time;
    c_tag_block<s_character_firing_pattern> firing_patterns;
    s_tag_reference weapon_melee_damage;
};
static_assert(sizeof(s_character_weapons_properties) == 0xE0);

struct s_character_firing_pattern
{
    float rate_of_fire;
    float target_tracking;
    float target_leading;
    float burst_origin_radius;
    real burst_origin_angle;
    real_bounds burst_return_length_bounds;
    real burst_return_angle;
    real_bounds burst_duration_bounds;
    real_bounds burst_separation_bounds;
    float weapon_damage_modifier;
    real projectile_error;
    real burst_angular_velocity;
    real maximum_error_angle;
};
static_assert(sizeof(s_character_firing_pattern) == 0x40);

struct s_character_scarab_properties
{
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
};
static_assert(sizeof(s_character_scarab_properties) == 0x18);

struct s_character_inspect_properties
{
    float stop_distance;
    real_bounds inspect_time;
    real_bounds search_range;
};
static_assert(sizeof(s_character_inspect_properties) == 0x14);

struct s_character_engineer_properties
{
    float death_height;
    float death_rise_time;
    float death_detonation_time;
    float shield_boost_radius;
    float shield_boost_period;
    float shield_boost_strenght;
    float detonation_shield_threshold;
    float detonation_body_vitality;
    float proximity_radius;
    float proximity_detonation_chance;
    s_tag_reference proximity_equipment;
};
static_assert(sizeof(s_character_engineer_properties) == 0x38);

struct s_character_combatform_properties
{
    float berserk_distance;
    float berserk_chance;
};
static_assert(sizeof(s_character_combatform_properties) == 0x8);

struct s_character_guardian_properties
{
    float surgetime;
    float surgedelay;
    float proximity_surge;
    float phasetime;
    float phasedistance;
    float targetphasedistance;
};
static_assert(sizeof(s_character_guardian_properties) == 0x18);

struct s_character_bunker_properties
{
    float unknown1;
    long unknown2;
};
static_assert(sizeof(s_character_bunker_properties) == 0x8);

struct s_character_boarding_properties
{
    c_flags<e_character_boarding_flags, long> flags;
    float maximum_distance;
    float abort_distance;
    float maximum_speed;
    float board_time;
};
static_assert(sizeof(s_character_boarding_properties) == 0x14);

struct s_character_vocalization_properties
{
    float character_skip_fraction;
    float look_comment_time;
    float look_long_comment_time;
};
static_assert(sizeof(s_character_vocalization_properties) == 0xC);

struct s_character_idle_properties
{
    byte unused[4];
    real_bounds idle_pose_delay_time;
    real_bounds wander_delay_time;
};
static_assert(sizeof(s_character_idle_properties) == 0x14);

struct s_character_pre_search_properties
{
    c_flags<e_character_pre_search_flags, long> flags;
    real_bounds minimum_pre_search_time;
    real_bounds maximum_pre_search_time;
    float minimum_certainty_radius;
    ulong unknown;
    real_bounds minimum_suppressing_time;
    short unknown2;
    short unknown3;
};
static_assert(sizeof(s_character_pre_search_properties) == 0x28);

struct s_character_search_properties
{
    c_flags<e_character_search_flags, long> flags;
    real_bounds search_time;
    float search_distance;
    real_bounds uncover_distance_bounds;
    real_bounds vocalization_time;
};
static_assert(sizeof(s_character_search_properties) == 0x20);

struct s_character_retreat_properties
{
    c_flags<e_character_retreat_flags, long> flags;
    float shield_threshold;
    float scary_target_threshold;
    float danger_threshold;
    float proximity_threshold;
    real_bounds forced_cower_time_bounds;
    real_bounds cower_time_bounds;
    float proximity_ambush_threshold;
    float awareness_ambush_threshold;
    float leader_dead_retreat_chance;
    float peer_dead_retreat_chance;
    float second_peer_dead_retreat_chance;
    float flee_timeout;
    real zig_zag_angle;
    float zig_zag_period;
    float retreat_grenade_chance;
    s_tag_reference backup_weapon;
};
static_assert(sizeof(s_character_retreat_properties) == 0x58);

struct s_character_cover_properties
{
    c_flags<e_character_cover_flags, long> flags;
    real_bounds hide_behind_cover_time;
    float cover_vitality_threshold;
    float cover_shield_fraction;
    float cover_check_delay;
    float cover_danger_threshold;
    float danger_upper_threshold;
    real_bounds cover_chance_bounds;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    float proximity_self_preserve;
    float disallow_cover_distance;
    float proximity_melee_distance;
    float unreachable_enemy_danger_threashold;
    float scary_target_threshold;
    float vitality_fraction_shield_equipment;
    float recent_damage_shield_equipment;
    float minimum_enemy_distance;
};
static_assert(sizeof(s_character_cover_properties) == 0x54);

struct s_character_evasion_properties
{
    float evasion_danger_threshold;
    float evasion_delay_timer;
    float evasion_chance;
    float evasion_proximity_threshold;
    float dive_retreat_chance;
};
static_assert(sizeof(s_character_evasion_properties) == 0x14);

struct s_character_charge_properties
{
    c_flags<e_character_charge_flags, long> flags;
    float melee_consider_range;
    float melee_chance;
    float melee_attack_range;
    float melee_abort_range;
    float melee_attack_timeout;
    float melee_attack_delay_timer;
    real_bounds melee_leap_range;
    float melee_leap_chance;
    float ideal_leap_velocity;
    float max_leap_velocity;
    float melee_leap_ballistic;
    float melee_delay_timer;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    s_tag_reference berserk_weapon;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    c_tag_block<s_character_charge_difficulty_limit> difficulty_limits;
};
static_assert(sizeof(s_character_charge_properties) == 0x7C);

struct s_character_charge_difficulty_limit
{
    short maximum_kamikaze_count;
    short maximum_berserk_count;
    short minimum_berserk_count;
};
static_assert(sizeof(s_character_charge_difficulty_limit) == 0x6);

struct s_character_engage_properties
{
    c_flags<e_character_engage_flags, long> flags;
    ulong unknown;
    float crouch_danger_threshold;
    float stand_danger_threshold;
    float fight_danger_move_threshold;
    float fight_danger_move_threshold_cooldown;
    s_tag_reference override_grenade_projectile;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
};
static_assert(sizeof(s_character_engage_properties) == 0x38);

struct s_character_ready_properties
{
    real_bounds read_time_bounds;
};
static_assert(sizeof(s_character_ready_properties) == 0x8);

struct s_character_swarm_properties
{
    short scatter_killed_count;
    byte unused[2];
    float scatter_radius;
    float scatter_time;
    real_bounds hound_distance;
    real_bounds infection_time;
    float perlin_offset_scale;
    real_bounds offset_period;
    float perlin_idle_movement_threshold;
    float perlin_combat_movement_threshold;
    float stuck_time;
    float stuck_distance;
};
static_assert(sizeof(s_character_swarm_properties) == 0x38);

struct s_character_flocking_properties
{
    float deceleration_distance;
    float normalized_speed;
    float buffer_distance;
    real_bounds throttle_threshold_b_ounds;
    float deceleration_stop_time;
};
static_assert(sizeof(s_character_flocking_properties) == 0x18);

struct s_character_movement_properties
{
    c_flags<e_character_movement_flags, long> flags;
    float pathfinding_radius;
    float destination_radius;
    float dive_grenade_chance;
    c_enum<e_ai_size, short> obstacle_leap_minimum_size;
    c_enum<e_ai_size, short> obstacle_leap_maximum_size;
    c_enum<e_ai_size, short> obstacle_ignore_size;
    c_enum<e_ai_size, short> obstace_smashable_size;
    byte unused[2];
    c_enum<e_character_jump_height, short> jump_height;
    c_flags<e_character_movement_hint_flags, long> hint_flags;
    float unknown1;
    float unknown2;
    float unknown3;
};
static_assert(sizeof(s_character_movement_properties) == 0x2C);

struct s_character_look_properties
{
    real_euler_angles2d maximum_aiming_deviation;
    real_euler_angles2d maximum_looking_deviation;
    real_euler_angles2d runtime_aiming_deviation_cosines;
    real_euler_angles2d runtime_looking_deviation_cosines;
    real noncombat_look_delta_left;
    real noncombat_look_delta_right;
    real combat_look_delta_left;
    real combat_look_delta_right;
    real_bounds noncombat_idle_looking;
    real_bounds noncombat_idle_aiming;
    real_bounds combat_idle_looking;
    real_bounds combat_idle_aiming;
};
static_assert(sizeof(s_character_look_properties) == 0x50);

struct s_character_perception_properties
{
    c_enum<e_character_perception_mode, short> mode;
    c_flags<e_character_perception_flags, ushort> flags;
    float max_vision_distance;
    real central_vision_angle;
    real max_vision_angle;
    real peripheral_vision_angle;
    float peripheral_distance;
    float hearing_distance;
    float notice_projectile_chance;
    float notice_vehicle_chance;
    float perception_time;
    float first_acknowledge_surprise_distance;
};
static_assert(sizeof(s_character_perception_properties) == 0x2C);

struct s_character_placement_properties
{
    byte unused[4];
    float few_upgrade_chance_easy;
    float few_upgrade_chance_normal;
    float few_upgrade_chance_heroic;
    float few_upgrade_chance_legendary;
    float normal_upgrade_chance_easy;
    float normal_upgrade_chance_normal;
    float normal_upgrade_chance_heroic;
    float normal_upgrade_chance_legendary;
    float many_upgrade_chance_easy;
    float many_upgrade_chance_normal;
    float many_upgrade_chance_heroic;
    float many_upgrade_chance_legendary;
};
static_assert(sizeof(s_character_placement_properties) == 0x34);

struct s_character_vitality_properties
{
    c_flags<e_character_vitality_flags, long> flags;
    float normal_body_vitality;
    float normal_shield_vitality;
    float legendary_body_vitality;
    float legendary_shield_vitality;
    float body_recharge_fraction;
    float soft_ping_shield_threshold;
    float soft_ping_noshield_threshold;
    float soft_ping_cooldown_time;
    float hard_ping_shield_threshold;
    float hard_ping_no_shield_threshold;
    float hard_ping_cooldown_time;
    float current_damage_decay_delay;
    float current_damage_decay_time;
    float recent_damage_decay_delay;
    float recent_damage_decay_time;
    float body_recharge_delay_time;
    float body_recharge_time;
    float shield_recharge_delay_time;
    float shield_recharge_time;
    float stun_threshold;
    real_bounds stun_time_bounds;
    float extended_shield_damage_threshold;
    float extended_body_damage_threshold;
    float suicide_radius;
    float runtime_body_recharge_velocity;
    float runtime_shield_recharge_velocity;
    s_tag_reference resurrect_weapon;
};
static_assert(sizeof(s_character_vitality_properties) == 0x80);

struct s_character_general_properties
{
    c_flags<e_character_general_flags, long> flags;
    c_enum<e_ai_actor_type, short> actor_type;
    short rank;
    c_enum<e_ai_follower_positioning, short> follower_positioning;
    byte unused[2];
    float maximum_leader_distance;
    float maximum_player_dialogue_distance;
    float scariness;
    c_enum<e_character_default_grenade_type, short> default_grenade_type;
    c_enum<e_character_behavior_tree_root, short> behavior_tree_root;
};
static_assert(sizeof(s_character_general_properties) == 0x1C);

struct s_character_unit_dialogue
{
    c_tag_block<s_character_dialogue_variation> dialogue_variations;
};
static_assert(sizeof(s_character_unit_dialogue) == 0xC);

struct s_character_variant
{
    string_id variant_name;
    short variant_index;
    byte unused[2];
    c_tag_block<s_character_dialogue_variation> dialogue_variations;
};
static_assert(sizeof(s_character_variant) == 0x14);

struct s_character_dialogue_variation
{
    s_tag_reference dialogue;
    string_id name;
    float weight;
};
static_assert(sizeof(s_character_dialogue_variation) == 0x18);

