/* ---------- enums */

enum e_scenario_structure_bsp_flags
{
    _scenario_structure_bsp_flags_has_instance_groups_bit = 0,
    _scenario_structure_bsp_flags_surface_to_triangle_mapping_remapped_bit = 1,
    _scenario_structure_bsp_flags_external_references_converted_to_io_bit = 2,
    _scenario_structure_bsp_flags_structure_mopp_needs_rebuilt_bit = 3,
    _scenario_structure_bsp_flags_structure_prefab_materials_need_postprocessing_bit = 4,
    _scenario_structure_bsp_flags_serialized_havok_data_converted_to_target_platform_bit = 5
};

enum e_scenario_structure_bsp_content_policy_flags
{
    _scenario_structure_bsp_content_policy_flags_has_working_pathfinding_bit = 0,
    _scenario_structure_bsp_content_policy_flags_convex_decomposition_enabled_bit = 1
};

enum e_scenario_structure_bsp_structure_bsp_compatibility
{
    _scenario_structure_bsp_structure_bsp_compatibility_use_mopp_index_patch_bit = 0
};

enum e_scenario_structure_bsp_collision_material_flags
{
    _scenario_structure_bsp_collision_material_flags_is_seam_bit = 0
};

enum e_scenario_structure_bsp_cluster_portal_flags
{
    _scenario_structure_bsp_cluster_portal_flags_none = 0,
    _scenario_structure_bsp_cluster_portal_flags_ai_cant_hear_through_this_shit = 1,
    _scenario_structure_bsp_cluster_portal_flags_one_way = 2,
    _scenario_structure_bsp_cluster_portal_flags_door = 4,
    _scenario_structure_bsp_cluster_portal_flags_no_way = 8,
    _scenario_structure_bsp_cluster_portal_flags_one_way_reversed = 16,
    _scenario_structure_bsp_cluster_portal_flags_no_one_can_hear_through_this = 32
};

enum e_blam_shape_type
{
    _blam_shape_type_sphere = 0,
    _blam_shape_type_pill = 1,
    _blam_shape_type_box = 2,
    _blam_shape_type_triangle = 3,
    _blam_shape_type_polyhedron = 4,
    _blam_shape_type_multi_sphere = 5,
    _blam_shape_type_triangle_mesh = 6,
    _blam_shape_type_compound_shape = 7,
    _blam_shape_type_unused0 = 8,
    _blam_shape_type_unused1 = 9,
    _blam_shape_type_unused2 = 10,
    _blam_shape_type_unused3 = 11,
    _blam_shape_type_unused4 = 12,
    _blam_shape_type_unused5 = 13,
    _blam_shape_type_list = 14,
    _blam_shape_type_mopp = 15
};

enum e_game_object_type_halo3_odst
{
    _game_object_type_halo3_odst_none = -1,
    _game_object_type_halo3_odst_biped = 0,
    _game_object_type_halo3_odst_vehicle = 1,
    _game_object_type_halo3_odst_weapon = 2,
    _game_object_type_halo3_odst_equipment = 3,
    _game_object_type_halo3_odst_alternate_reality_device = 4,
    _game_object_type_halo3_odst_terminal = 5,
    _game_object_type_halo3_odst_projectile = 6,
    _game_object_type_halo3_odst_scenery = 7,
    _game_object_type_halo3_odst_machine = 8,
    _game_object_type_halo3_odst_control = 9,
    _game_object_type_halo3_odst_sound_scenery = 10,
    _game_object_type_halo3_odst_crate = 11,
    _game_object_type_halo3_odst_creature = 12,
    _game_object_type_halo3_odst_giant = 13,
    _game_object_type_halo3_odst_effect_scenery = 14
};

enum e_render_material_property_type_halo3
{
    _render_material_property_type_halo3_lightmap_resolution = 0,
    _render_material_property_type_halo3_lightmap_power = 1,
    _render_material_property_type_halo3_lightmap_half_life = 2,
    _render_material_property_type_halo3_lightmap_diffuse_scale = 3
};

enum e_sector_flags
{
    _sector_flags_sector_walkable_bit = 0,
    _sector_flags_sector_breakable_bit = 1,
    _sector_flags_sector_mobile_bit = 2,
    _sector_flags_sector_bsp_source_bit = 3,
    _sector_flags_floor_bit = 4,
    _sector_flags_ceiling_bit = 5,
    _sector_flags_wall_north_bit = 6,
    _sector_flags_wall_south_bit = 7,
    _sector_flags_wall_east_bit = 8,
    _sector_flags_wall_west_bit = 9,
    _sector_flags_crouchable_bit = 10,
    _sector_flags_aligned_bit = 11,
    _sector_flags_sector_step_bit = 12,
    _sector_flags_sector_interior_bit = 13,
    _sector_flags_bit14_bit = 14,
    _sector_flags_bit15_bit = 15
};

enum e_link_flags
{
    _link_flags_sector_link_from_collision_edge_bit = 0,
    _link_flags_sector_intersection_link_bit = 1,
    _link_flags_sector_link_bsp2_d_creation_error_bit = 2,
    _link_flags_sector_link_topology_error_bit = 3,
    _link_flags_sector_link_chain_error_bit = 4,
    _link_flags_sector_link_both_sectors_walkable_bit = 5,
    _link_flags_sector_link_magic_hanging_link_bit = 6,
    _link_flags_sector_link_threshold_bit = 7,
    _link_flags_sector_link_crouchable_bit = 8,
    _link_flags_sector_link_wall_base_bit = 9,
    _link_flags_sector_link_ledge_bit = 10,
    _link_flags_sector_link_leanable_bit = 11,
    _link_flags_sector_link_start_corner_bit = 12,
    _link_flags_sector_link_end_corner_bit = 13,
    _link_flags_bit14_bit = 14,
    _link_flags_bit15_bit = 15
};

enum e_scenario_scenario_instance_source
{
    _scenario_scenario_instance_source_structure = 0,
    _scenario_scenario_instance_source_editor = 1,
    _scenario_scenario_instance_source_dynamic = 2,
    _scenario_scenario_instance_source_legacy = 3
};

enum e_pathfinding_hint_hint_type
{
    _pathfinding_hint_hint_type_intersection_link = 0,
    _pathfinding_hint_hint_type_jump_link = 1,
    _pathfinding_hint_hint_type_climb_link = 2,
    _pathfinding_hint_hint_type_vault_link = 3,
    _pathfinding_hint_hint_type_mount_link = 4,
    _pathfinding_hint_hint_type_hoist_link = 5,
    _pathfinding_hint_hint_type_wall_jump_link = 6,
    _pathfinding_hint_hint_type_breakable_floor = 7,
    _pathfinding_hint_hint_type_unknown8 = 8,
    _pathfinding_hint_hint_type_unknown9 = 9,
    _pathfinding_hint_hint_type_unknown_a = 10
};

enum e_scenario_structure_bsp_sound_environment_type
{
    _scenario_structure_bsp_sound_environment_type_default = 0,
    _scenario_structure_bsp_sound_environment_type_interior_narrow = 1,
    _scenario_structure_bsp_sound_environment_type_interior_small = 2,
    _scenario_structure_bsp_sound_environment_type_interior_medium = 3,
    _scenario_structure_bsp_sound_environment_type_interior_large = 4,
    _scenario_structure_bsp_sound_environment_type_exterior_small = 5,
    _scenario_structure_bsp_sound_environment_type_exterior_medium = 6,
    _scenario_structure_bsp_sound_environment_type_exterior_large = 7,
    _scenario_structure_bsp_sound_environment_type_exterior_half_open = 8,
    _scenario_structure_bsp_sound_environment_type_exterior_open = 9
};

enum e_scenario_structure_bsp_background_sound_scale_flags
{
    _scenario_structure_bsp_background_sound_scale_flags_override_default_scale_bit = 0,
    _scenario_structure_bsp_background_sound_scale_flags_use_adjacent_cluster_as_portal_scale_bit = 1,
    _scenario_structure_bsp_background_sound_scale_flags_use_adjacent_cluster_as_exterior_scale_bit = 2,
    _scenario_structure_bsp_background_sound_scale_flags_scale_with_weather_intensity_bit = 3
};

enum e_scenario_structure_bsp_environment_object_palette_block_object_type
{
    _scenario_structure_bsp_environment_object_palette_block_object_type_biped_bit = 0,
    _scenario_structure_bsp_environment_object_palette_block_object_type_vehicle_bit = 1,
    _scenario_structure_bsp_environment_object_palette_block_object_type_weapon_bit = 2,
    _scenario_structure_bsp_environment_object_palette_block_object_type_equipment_bit = 3,
    _scenario_structure_bsp_environment_object_palette_block_object_type_arg_device_bit = 4,
    _scenario_structure_bsp_environment_object_palette_block_object_type_terminal_bit = 5,
    _scenario_structure_bsp_environment_object_palette_block_object_type_projectile_bit = 6,
    _scenario_structure_bsp_environment_object_palette_block_object_type_scenery_bit = 7,
    _scenario_structure_bsp_environment_object_palette_block_object_type_machine_bit = 8,
    _scenario_structure_bsp_environment_object_palette_block_object_type_control_bit = 9,
    _scenario_structure_bsp_environment_object_palette_block_object_type_sound_scenery_bit = 10,
    _scenario_structure_bsp_environment_object_palette_block_object_type_crate_bit = 11,
    _scenario_structure_bsp_environment_object_palette_block_object_type_creature_bit = 12,
    _scenario_structure_bsp_environment_object_palette_block_object_type_giant_bit = 13,
    _scenario_structure_bsp_environment_object_palette_block_object_type_effect_scenery_bit = 14
};

enum e_scenario_structure_bsp_environment_object_flags
{
    _scenario_structure_bsp_environment_object_flags_scripts_always_run_bit = 0
};

enum e_scenario_structure_bsp_instanced_geometry_instance_flags
{
    _scenario_structure_bsp_instanced_geometry_instance_flags_contains_split_lighting_parts_bit = 0,
    _scenario_structure_bsp_instanced_geometry_instance_flags_render_only_bit = 1,
    _scenario_structure_bsp_instanced_geometry_instance_flags_does_not_block_aoe_damage_bit = 2,
    _scenario_structure_bsp_instanced_geometry_instance_flags_collidable_bit = 3,
    _scenario_structure_bsp_instanced_geometry_instance_flags_contains_decal_parts_bit = 4,
    _scenario_structure_bsp_instanced_geometry_instance_flags_contains_water_parts_bit = 5,
    _scenario_structure_bsp_instanced_geometry_instance_flags_negative_scale_bit = 6,
    _scenario_structure_bsp_instanced_geometry_instance_flags_outside_map_bit = 7,
    _scenario_structure_bsp_instanced_geometry_instance_flags_seam_colliding_bit = 8,
    _scenario_structure_bsp_instanced_geometry_instance_flags_contains_deferred_reflections_bit = 9,
    _scenario_structure_bsp_instanced_geometry_instance_flags_remove_from_shadow_geometry_bit = 10,
    _scenario_structure_bsp_instanced_geometry_instance_flags_cinema_only_bit = 11,
    _scenario_structure_bsp_instanced_geometry_instance_flags_exclude_from_cinema_bit = 12,
    _scenario_structure_bsp_instanced_geometry_instance_flags_disable_fx_bit = 13,
    _scenario_structure_bsp_instanced_geometry_instance_flags_disable_play_collision_bit = 14,
    _scenario_structure_bsp_instanced_geometry_instance_flags_disable_bullet_collision_bit = 15
};

enum e_scenery_pathfinding_policy
{
    _scenery_pathfinding_policy_cut_out = 0,
    _scenery_pathfinding_policy_static = 1,
    _scenery_pathfinding_policy_dynamic = 2,
    _scenery_pathfinding_policy_none = 3
};

enum e_scenery_lightmapping_policy
{
    _scenery_lightmapping_policy_per_vertex = 0,
    _scenery_lightmapping_policy_per_pixel_not_implemented = 1,
    _scenery_lightmapping_policy_dynamic = 2
};

enum e_mesh_flags
{
    _mesh_flags_mesh_has_vertex_color_bit = 0,
    _mesh_flags_use_region_index_for_sorting_bit = 1,
    _mesh_flags_can_be_rendered_in_draw_bundles_bit = 2,
    _mesh_flags_mesh_is_custom_shadow_caster_bit = 3,
    _mesh_flags_mesh_is_unindexed_bit = 4,
    _mesh_flags_mash_should_render_in_z_prepass_bit = 5,
    _mesh_flags_mesh_has_water_bit = 6,
    _mesh_flags_mesh_has_decal_bit = 7
};

enum e_render_geometry_runtime_flags
{
    _render_geometry_runtime_flags_processed_bit = 0,
    _render_geometry_runtime_flags_available_bit = 1,
    _render_geometry_runtime_flags_has_valid_budgets_bit = 2,
    _render_geometry_runtime_flags_manual_resource_calibration_bit = 3,
    _render_geometry_runtime_flags_keep_raw_geometry_bit = 4,
    _render_geometry_runtime_flags_do_not_use_compressed_vertex_positions_bit = 5,
    _render_geometry_runtime_flags_pca_animation_table_sorted_bit = 6,
    _render_geometry_runtime_flags_has_dual_quat_bit = 7
};

enum e_part_part_type_new
{
    _part_part_type_new_not_drawn = 0,
    _part_part_type_new_opaque_shadow_only = 1,
    _part_part_type_new_opaque_shadow_casting = 2,
    _part_part_type_new_opaque_nonshadowing = 3,
    _part_part_type_new_transparent = 4,
    _part_part_type_new_lightmap_only = 5
};

enum e_part_part_flags_new
{
    _part_part_flags_new_can_be_rendered_in_draw_bundles_bit = 0,
    _part_part_flags_new_per_vertex_lightmap_part_bit = 1,
    _part_part_flags_new_render_in_z_prepass_bit = 2,
    _part_part_flags_new_is_water_part_bit = 3,
    _part_part_flags_new_draw_cull_distance_medium_bit = 4,
    _part_part_flags_new_draw_cull_distance_close_bit = 5,
    _part_part_flags_new_draw_cull_rendering_shields_bit = 6,
    _part_part_flags_new_draw_cull_rendering_active_camo_bit = 7
};

enum e_vertex_buffer_format
{
    _vertex_buffer_format_invalid = 0,
    _vertex_buffer_format_world = 1,
    _vertex_buffer_format_rigid = 2,
    _vertex_buffer_format_skinned = 3,
    _vertex_buffer_format_static_per_pixel = 4,
    _vertex_buffer_format_unknown5 = 5,
    _vertex_buffer_format_static_per_vertex = 6,
    _vertex_buffer_format_unknown7 = 7,
    _vertex_buffer_format_unused8 = 8,
    _vertex_buffer_format_ambient_prt = 9,
    _vertex_buffer_format_linear_prt = 10,
    _vertex_buffer_format_quadratic_prt = 11,
    _vertex_buffer_format_unknown_c = 12,
    _vertex_buffer_format_unknown_d = 13,
    _vertex_buffer_format_static_per_vertex_color = 14,
    _vertex_buffer_format_unknown_f = 15,
    _vertex_buffer_format_unused10 = 16,
    _vertex_buffer_format_unused11 = 17,
    _vertex_buffer_format_unused12 = 18,
    _vertex_buffer_format_unused13 = 19,
    _vertex_buffer_format_tiny_position = 20,
    _vertex_buffer_format_unknown15 = 21,
    _vertex_buffer_format_unknown16 = 22,
    _vertex_buffer_format_unknown17 = 23,
    _vertex_buffer_format_decorator = 24,
    _vertex_buffer_format_particle_model = 25,
    _vertex_buffer_format_unknown1_a = 26,
    _vertex_buffer_format_unknown1_b = 27,
    _vertex_buffer_format_unknown1_c = 28,
    _vertex_buffer_format_unused1_d = 29,
    _vertex_buffer_format_world2 = 30,
    _vertex_buffer_format_rigid_compressed = 36,
    _vertex_buffer_format_skinned_compressed = 37
};

enum e_index_buffer_format
{
    _index_buffer_format_point_list = 0,
    _index_buffer_format_line_list = 1,
    _index_buffer_format_line_strip = 2,
    _index_buffer_format_triangle_list = 3,
    _index_buffer_format_triangle_fan = 4,
    _index_buffer_format_triangle_strip = 5
};

enum e_vertex_type
{
    _vertex_type_world = 0,
    _vertex_type_rigid = 1,
    _vertex_type_skinned = 2,
    _vertex_type_particle_model = 3,
    _vertex_type_flat_world = 4,
    _vertex_type_flat_rigid = 5,
    _vertex_type_flat_skinned = 6,
    _vertex_type_screen = 7,
    _vertex_type_debug = 8,
    _vertex_type_transparent = 9,
    _vertex_type_particle = 10,
    _vertex_type_contrail = 11,
    _vertex_type_light_volume = 12,
    _vertex_type_simple_chud = 13,
    _vertex_type_fancy_chud = 14,
    _vertex_type_decorator = 15,
    _vertex_type_tiny_position = 16,
    _vertex_type_patchy_fog = 17,
    _vertex_type_water = 18,
    _vertex_type_ripple = 19,
    _vertex_type_implicit = 20,
    _vertex_type_beam = 21,
    _vertex_type_dual_quat = 22
};

enum e_prt_sh_type
{
    _prt_sh_type_none = 0,
    _prt_sh_type_ambient = 1,
    _prt_sh_type_linear = 2,
    _prt_sh_type_quadratic = 3
};

enum e_primitive_type
{
    _primitive_type_point_list = 0,
    _primitive_type_line_list = 1,
    _primitive_type_line_strip = 2,
    _primitive_type_triangle_list = 3,
    _primitive_type_triangle_fan = 4,
    _primitive_type_triangle_strip = 5
};

enum e_render_geometry_compression_flags
{
    _render_geometry_compression_flags_compressed_position_bit = 0,
    _render_geometry_compression_flags_compressed_texcoord_bit = 1,
    _render_geometry_compression_flags_compression_optimized_bit = 2
};

enum e_old_raw_page_flags
{
    _old_raw_page_flags_use_checksum_bit = 0,
    _old_raw_page_flags_in_resources_bit = 1,
    _old_raw_page_flags_in_textures_bit = 2,
    _old_raw_page_flags_in_textures_b_bit = 3,
    _old_raw_page_flags_in_audio_bit = 4,
    _old_raw_page_flags_in_resources_b_bit = 5,
    _old_raw_page_flags_in_mods_bit = 6,
    _old_raw_page_flags_use_checksum2_bit = 7,
    _old_raw_page_flags_location_mask_bit = 1
};

enum e_tag_resource_type_gen3
{
    _tag_resource_type_gen3_none = -1,
    _tag_resource_type_gen3_collision = 0,
    _tag_resource_type_gen3_bitmap = 1,
    _tag_resource_type_gen3_bitmap_interleaved = 2,
    _tag_resource_type_gen3_sound = 3,
    _tag_resource_type_gen3_animation = 4,
    _tag_resource_type_gen3_render_geometry = 5,
    _tag_resource_type_gen3_bink = 6,
    _tag_resource_type_gen3_pathfinding = 7
};

enum e_leaf_flags
{
    _leaf_flags_contains_double_sided_surfaces_bit = 0
};

enum e_surface_flags
{
    _surface_flags_two_sided_bit = 0,
    _surface_flags_invisible_bit = 1,
    _surface_flags_climbable_bit = 2,
    _surface_flags_breakable_bit = 3,
    _surface_flags_invalid_bit = 4,
    _surface_flags_conveyor_bit = 5,
    _surface_flags_slip_bit = 6,
    _surface_flags_plane_negated_bit = 7
};


/* ---------- structures */

struct s_scenario_structure_bsp_seam_identifier_edge;
struct s_scenario_structure_bsp_seam_identifier_cluster;
struct s_scenario_structure_bsp_seam_identifier;
struct s_edge_to_seam_mapping;
struct s_scenario_structure_bsp_collision_material;
struct s_scenario_structure_bsp_leaf;
struct s_surfaces_planes;
struct s_plane_reference;
struct s_scenario_structure_bsp_cluster_portal_vertex;
struct s_scenario_structure_bsp_cluster_portal;
struct s_scenario_structure_bsp_unknown_block2;
struct s_scenario_structure_bsp_fog_block;
struct s_scenario_structure_bsp_camera_effect;
struct s_scenario_structure_bsp_detail_object_unknown_block_unknown_block2;
struct s_scenario_structure_bsp_detail_object_unknown_block;
struct s_scenario_structure_bsp_detail_object;
struct s_scenario_structure_bsp_cluster_portal;
struct s_hkp_referenced_object;
struct s_hkp_shape;
struct s_hkp_shape_container;
struct s_hkp_shape_collection;
struct s_hkp_single_shape_container;
struct s_hkp_mopp_bv_tree_shape;
struct s_code_info;
struct s_hk_array_base;
struct s_hkp_mopp_code;
struct s_tag_hkp_mopp_code;
struct s_scenario_structure_bsp_cluster_instanced_geometry_physics_data;
struct s_scenario_structure_bsp_cluster_seam;
struct s_scenario_structure_bsp_cluster_decorator_grid_decorator_info;
struct s_scenario_structure_bsp_cluster_decorator_grid;
struct s_game_object_type;
struct s_scenario_structure_bsp_cluster_object_placement;
struct s_scenario_structure_bsp_cluster_unknown_block2;
struct s_scenario_structure_bsp_cluster;
struct s_render_material_property_type;
struct s_render_material_property;
struct s_render_material;
struct s_scenario_structure_bsp_sky_owner_cluster_block;
struct s_scenario_structure_bsp_conveyor_surface;
struct s_breakable_surface_bits;
struct s_sector;
struct s_link;
struct s_reference;
struct s_bsp2_d_node;
struct s_vertex;
struct s_object_reference_bsp_reference_bsp2_d_ref;
struct s_object_reference_bsp_reference;
struct s_scenario_object_type;
struct s_object_reference;
struct s_pathfinding_hint;
struct s_instanced_geometry_reference;
struct s_giant_pathfinding_block;
struct s_seam_link_index_block;
struct s_seam;
struct s_jump_seam_jump_index_block;
struct s_jump_seam;
struct s_door;
struct s_tag_pathfinding;
struct s_scenario_structure_bsp_background_sound_environment_palette_block;
struct s_scenario_structure_bsp_marker;
struct s_tag_reference_block;
struct s_scenario_structure_bsp_runtime_light;
struct s_scenario_structure_bsp_runtime_decal;
struct s_scenario_structure_bsp_environment_object_palette_block;
struct s_scenario_structure_bsp_environment_object;
struct s_collision_geometry_shape;
struct s_c_mopp_bv_tree_shape;
struct s_collision_bsp_physics_definition;
struct s_scenario_structure_bsp_instanced_geometry_instance;
struct s_part;
struct s_sub_part;
struct s_vertex_buffer_definition;
struct s_index_buffer_definition;
struct s_mesh_instanced_geometry_block_contents_block;
struct s_mesh_instanced_geometry_block;
struct s_mesh_water_block;
struct s_mesh;
struct s_render_geometry_compression;
struct s_render_geometry_bounding_sphere;
struct s_render_geometry_unknown_block;
struct s_render_geometry_geometry_tag_resource;
struct s_render_geometry_mopp_cluster_visiblity;
struct s_render_geometry_per_mesh_node_map_node_index;
struct s_render_geometry_per_mesh_node_map;
struct s_render_geometry_per_mesh_subpart_visibility_block;
struct s_render_geometry_static_per_pixel_lighting;
struct s_resource_page;
struct s_resource_fixup_location;
struct s_resource_interop_location;
struct s_resource_data;
struct s_pageable_resource;
struct s_tag_resource_reference;
struct s_render_geometry;
struct s_scenario_structure_bsp_unknown_sound_clusters_block_portal_designator_block;
struct s_scenario_structure_bsp_unknown_sound_clusters_block_interior_cluster_index_block;
struct s_scenario_structure_bsp_unknown_sound_clusters_block;
struct s_scenario_structure_bsp_transparent_plane;
struct s_scenario_structure_bsp_breakable_surface_key_table_block;
struct s_scenario_structure_bsp_leaf_system;
struct s_bsp3_d_node;
struct s_plane;
struct s_leaf;
struct s_bsp2_d_reference;
struct s_bsp2_d_node;
struct s_surface;
struct s_edge;
struct s_vertex;
struct s_collision_geometry;
struct s_large_bsp3_d_node;
struct s_large_bsp2_d_reference;
struct s_large_bsp2_d_node;
struct s_large_surface;
struct s_large_edge;
struct s_large_vertex;
struct s_large_collision_bsp_block;
struct s_instanced_geometry_block;
struct s_havok_datum_havok_geometry;
struct s_havok_datum;
struct s_structure_bsp_tag_resources;
struct s_scenario_structure_bsp;

struct s_scenario_structure_bsp
{
    long import_info_checksum;
    c_flags<e_scenario_structure_bsp_flags, ushort> flags;
    c_flags<e_scenario_structure_bsp_content_policy_flags, ushort> content_policy_flags;
    c_flags<e_scenario_structure_bsp_content_policy_flags, ushort> failed_content_policy_flags;
    c_flags<e_scenario_structure_bsp_structure_bsp_compatibility, ushort> compatibility_flags;
    ulong unknown3;
    c_tag_block<s_scenario_structure_bsp_seam_identifier> seam_identifiers;
    c_tag_block<s_edge_to_seam_mapping> edge_to_seams;
    c_tag_block<s_scenario_structure_bsp_collision_material> collision_materials;
    c_tag_block<s_scenario_structure_bsp_leaf> leaves;
    real_bounds world_bounds_x;
    real_bounds world_bounds_y;
    real_bounds world_bounds_z;
    c_tag_block<s_surfaces_planes> surface_planes;
    c_tag_block<s_plane_reference> planes;
    byte unknown_unused1[12];
    c_tag_block<s_scenario_structure_bsp_cluster_portal> cluster_portals;
    c_tag_block<s_scenario_structure_bsp_unknown_block2> unknown14;
    c_tag_block<s_scenario_structure_bsp_fog_block> fog;
    c_tag_block<s_scenario_structure_bsp_camera_effect> camera_effects;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
    c_tag_block<s_scenario_structure_bsp_detail_object> detail_objects;
    c_tag_block<s_scenario_structure_bsp_cluster> clusters;
    c_tag_block<s_render_material> materials;
    c_tag_block<s_scenario_structure_bsp_sky_owner_cluster_block> sky_owner_cluster;
    c_tag_block<s_scenario_structure_bsp_conveyor_surface> conveyor_surfaces;
    c_tag_block<s_breakable_surface_bits> breakable_surfaces;
    c_tag_block<s_tag_pathfinding> pathfinding_data;
    ulong unknown30;
    ulong unknown31;
    ulong unknown32;
    c_tag_block<s_scenario_structure_bsp_background_sound_environment_palette_block> background_sound_environment_palette;
    ulong unknown33;
    ulong unknown34;
    ulong unknown35;
    ulong unknown36;
    ulong unknown37;
    ulong unknown38;
    ulong unknown39;
    ulong unknown40;
    ulong unknown41;
    ulong unknown42;
    ulong unknown43;
    c_tag_block<s_scenario_structure_bsp_marker> markers;
    c_tag_block<s_tag_reference_block> lights;
    c_tag_block<s_scenario_structure_bsp_runtime_light> runtime_lights;
    c_tag_block<s_scenario_structure_bsp_runtime_decal> runtime_decals;
    c_tag_block<s_scenario_structure_bsp_environment_object_palette_block> environment_object_palette;
    c_tag_block<s_scenario_structure_bsp_environment_object> environment_objects;
    ulong unknown45;
    ulong unknown46;
    ulong unknown47;
    ulong unknown48;
    ulong unknown49;
    ulong unknown50;
    ulong unknown51;
    ulong unknown52;
    ulong unknown53;
    ulong unknown54;
    c_tag_block<s_scenario_structure_bsp_instanced_geometry_instance> instanced_geometry_instances;
    c_tag_block<s_tag_reference_block> decorators;
    s_render_geometry decorator_geometry;
    c_tag_block<s_scenario_structure_bsp_unknown_sound_clusters_block> unknown_sound_clusters_a;
    c_tag_block<s_scenario_structure_bsp_unknown_sound_clusters_block> unknown_sound_clusters_b;
    c_tag_block<s_scenario_structure_bsp_unknown_sound_clusters_block> unknown_sound_clusters_c;
    c_tag_block<s_scenario_structure_bsp_transparent_plane> transparent_planes;
    ulong unknown64;
    ulong unknown65;
    ulong unknown66;
    c_tag_block<s_tag_hkp_mopp_code> collision_mopp_codes;
    ulong unknown67;
    real_point3d collision_world_bounds_lower;
    real_point3d collision_world_bounds_upper;
    c_tag_block<s_tag_hkp_mopp_code> breakable_surface_mopp_codes;
    c_tag_block<s_scenario_structure_bsp_breakable_surface_key_table_block> breakable_surface_key_table;
    ulong unknown68;
    ulong unknown69;
    ulong unknown70;
    ulong unknown71;
    ulong unknown72;
    ulong unknown73;
    s_render_geometry geometry;
    c_tag_block<s_scenario_structure_bsp_leaf_system> leaf_systems;
    c_tag_block<s_structure_bsp_tag_resources> tag_resources;
    s_tag_resource_reference collision_bsp_resource;
    s_tag_resource_reference pathfinding_resource;
    long unknown86;
    ulong unknown87;
    ulong unknown88;
    ulong unknown89;
};
static_assert(sizeof(s_scenario_structure_bsp) == 0x3AC);

struct s_structure_bsp_tag_resources
{
    c_tag_block<s_collision_geometry> collision_bsps;
    c_tag_block<s_large_collision_bsp_block> large_collision_bsps;
    c_tag_block<s_instanced_geometry_block> instanced_geometry;
    c_tag_block<s_havok_datum> havok_data;
};
static_assert(sizeof(s_structure_bsp_tag_resources) == 0x30);

struct s_havok_datum
{
    long prefab_index;
    c_tag_block<s_havok_datum_havok_geometry> havok_geometries;
    c_tag_block<s_havok_datum_havok_geometry> havok_inverted_geometries;
    real_point3d shapes_bounds_minimum;
    real_point3d shapes_bounds_maximum;
};
static_assert(sizeof(s_havok_datum) == 0x34);

struct s_havok_datum_havok_geometry
{
    long unknown;
    long collision_type;
    long shape_count;
    tag_data data;
};
static_assert(sizeof(s_havok_datum_havok_geometry) == 0x20);

struct s_instanced_geometry_block
{
    long checksum;
    real_point3d bounding_sphere_offset;
    float bounding_sphere_radius;
    s_collision_geometry collision_info;
    c_tag_block<s_collision_geometry> collision_geometries;
    c_tag_block<s_tag_hkp_mopp_code> collision_mopp_codes;
    c_tag_block<s_breakable_surface_bits> breakable_surfaces;
    c_tag_block<s_surfaces_planes> surface_planes;
    c_tag_block<s_plane_reference> planes;
    short mesh_index;
    short compression_index;
    float unknown4;
    c_tag_block<s_tag_hkp_mopp_code> unknown_bsp_physics;
    ulong runtime_pointer;
};
static_assert(sizeof(s_instanced_geometry_block) == 0xC8);

struct s_large_collision_bsp_block
{
    c_tag_block<s_large_bsp3_d_node> bsp3_d_nodes;
    c_tag_block<s_plane> planes;
    c_tag_block<s_leaf> leaves;
    c_tag_block<s_large_bsp2_d_reference> bsp2_d_references;
    c_tag_block<s_large_bsp2_d_node> bsp2_d_nodes;
    c_tag_block<s_large_surface> surfaces;
    c_tag_block<s_large_edge> edges;
    c_tag_block<s_large_vertex> vertices;
};
static_assert(sizeof(s_large_collision_bsp_block) == 0x60);

struct s_large_vertex
{
    real_point3d point;
    long first_edge;
    long sink;
};
static_assert(sizeof(s_large_vertex) == 0x14);

struct s_large_edge
{
    long start_vertex;
    long end_vertex;
    long forward_edge;
    long reverse_edge;
    long left_surface;
    long right_surface;
};
static_assert(sizeof(s_large_edge) == 0x18);

struct s_large_surface
{
    long plane;
    long first_edge;
    short material;
    short unknown;
    short breakable_surface;
    c_flags<e_surface_flags, uchar> flags;
    char best_plane_calculation_vertex;
};
static_assert(sizeof(s_large_surface) == 0x10);

struct s_large_bsp2_d_node
{
    real_plane2d plane;
    long left_child;
    long right_child;
};
static_assert(sizeof(s_large_bsp2_d_node) == 0x14);

struct s_large_bsp2_d_reference
{
    long plane_index;
    long bsp2_d_node_index;
};
static_assert(sizeof(s_large_bsp2_d_reference) == 0x8);

struct s_large_bsp3_d_node
{
    long plane;
    long back_child;
    long front_child;
};
static_assert(sizeof(s_large_bsp3_d_node) == 0xC);

struct s_collision_geometry
{
    c_tag_block<s_bsp3_d_node> bsp3_d_nodes;
    c_tag_block<s_plane> planes;
    c_tag_block<s_leaf> leaves;
    c_tag_block<s_bsp2_d_reference> bsp2_d_references;
    c_tag_block<s_bsp2_d_node> bsp2_d_nodes;
    c_tag_block<s_surface> surfaces;
    c_tag_block<s_edge> edges;
    c_tag_block<s_vertex> vertices;
};
static_assert(sizeof(s_collision_geometry) == 0x60);

struct s_vertex
{
    real_point3d point;
    ushort first_edge;
    short sink;
};
static_assert(sizeof(s_vertex) == 0x10);

struct s_edge
{
    ushort start_vertex;
    ushort end_vertex;
    ushort forward_edge;
    ushort reverse_edge;
    ushort left_surface;
    ushort right_surface;
};
static_assert(sizeof(s_edge) == 0xC);

struct s_surface
{
    ushort plane;
    ushort first_edge;
    short material_index;
    short breakable_surface_set;
    short breakable_surface_index;
    c_flags<e_surface_flags, uchar> flags;
    uchar best_plane_calculation_vertex;
};
static_assert(sizeof(s_surface) == 0xC);

struct s_bsp2_d_node
{
    real_plane2d plane;
    short left_child;
    short right_child;
};
static_assert(sizeof(s_bsp2_d_node) == 0x10);

struct s_bsp2_d_reference
{
    short plane_index;
    short bsp2_d_node_index;
};
static_assert(sizeof(s_bsp2_d_reference) == 0x4);

struct s_leaf
{
    c_flags<e_leaf_flags, uchar> flags;
    c_flags<e_leaf_flags, uchar> flags2;
    ushort bsp2_d_reference_count;
    ulong first_bsp2_d_reference;
};
static_assert(sizeof(s_leaf) == 0x8);

struct s_plane
{
    real_plane3d value;
};
static_assert(sizeof(s_plane) == 0x10);

struct s_bsp3_d_node
{
    u_int64 value;
};
static_assert(sizeof(s_bsp3_d_node) == 0x8);

struct s_scenario_structure_bsp_leaf_system
{
    short unknown;
    short unknown2;
    s_tag_reference leaf_system2;
};
static_assert(sizeof(s_scenario_structure_bsp_leaf_system) == 0x14);

struct s_scenario_structure_bsp_breakable_surface_key_table_block
{
    short instanced_geometry_index;
    char breakable_surface_index;
    uchar breakable_surface_sub_index;
    long seed_surface_index;
    real_bounds x;
    real_bounds y;
    real_bounds z;
};
static_assert(sizeof(s_scenario_structure_bsp_breakable_surface_key_table_block) == 0x20);

struct s_scenario_structure_bsp_transparent_plane
{
    short mesh_index;
    short part_index;
    real_plane3d plane;
};
static_assert(sizeof(s_scenario_structure_bsp_transparent_plane) == 0x14);

struct s_scenario_structure_bsp_unknown_sound_clusters_block
{
    short background_sound_environment_index;
    short unknown;
    c_tag_block<s_scenario_structure_bsp_unknown_sound_clusters_block_portal_designator_block> portal_designators;
    c_tag_block<s_scenario_structure_bsp_unknown_sound_clusters_block_interior_cluster_index_block> interior_cluster_indices;
};
static_assert(sizeof(s_scenario_structure_bsp_unknown_sound_clusters_block) == 0x1C);

struct s_scenario_structure_bsp_unknown_sound_clusters_block_interior_cluster_index_block
{
    short interior_cluster_index;
};
static_assert(sizeof(s_scenario_structure_bsp_unknown_sound_clusters_block_interior_cluster_index_block) == 0x2);

struct s_scenario_structure_bsp_unknown_sound_clusters_block_portal_designator_block
{
    short portal_designator;
};
static_assert(sizeof(s_scenario_structure_bsp_unknown_sound_clusters_block_portal_designator_block) == 0x2);

struct s_render_geometry
{
    c_flags<e_render_geometry_runtime_flags, long> runtime_flags;
    c_tag_block<s_mesh> meshes;
    c_tag_block<s_render_geometry_compression> compression;
    c_tag_block<s_render_geometry_bounding_sphere> bounding_spheres;
    c_tag_block<s_render_geometry_unknown_block> unknown2;
    c_tag_block<s_render_geometry_geometry_tag_resource> geometry_tag_resources;
    c_tag_block<s_render_geometry_mopp_cluster_visiblity> mesh_cluster_visibility;
    c_tag_block<s_render_geometry_per_mesh_node_map> per_mesh_node_maps;
    c_tag_block<s_render_geometry_per_mesh_subpart_visibility_block> per_mesh_subpart_visibility;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    c_tag_block<s_render_geometry_static_per_pixel_lighting> instanced_geometry_per_pixel_lighting;
    s_tag_resource_reference resource;
};
static_assert(sizeof(s_render_geometry) == 0x84);

struct s_tag_resource_reference
{
    s_pageable_resource pageable_resource;
    long unused;
};
static_assert(sizeof(s_tag_resource_reference) == 0x8);

struct s_pageable_resource
{
    s_resource_page page;
    s_resource_data resource;
};
static_assert(sizeof(s_pageable_resource) == 0x6C);

struct s_resource_data
{
    s_tag_reference parent_tag;
    ushort salt;
    c_enum<e_tag_resource_type_gen3, char> resource_type;
    uchar flags;
    s_tag_data definition_data;
    cache_address definition_address;
    c_tag_block<s_resource_fixup_location> fixup_locations;
    c_tag_block<s_resource_interop_location> interop_locations;
    long unknown2;
};
static_assert(sizeof(s_resource_data) == 0x48);

struct s_resource_interop_location
{
    cache_address address;
    long resource_structure_type_index;
};
static_assert(sizeof(s_resource_interop_location) == 0x8);

struct s_resource_fixup_location
{
    ulong block_offset;
    cache_address address;
    long type;
    long offset;
    long raw_address;
};
static_assert(sizeof(s_resource_fixup_location) == 0x8);

struct s_resource_page
{
    short salt;
    c_flags<e_old_raw_page_flags, uchar> old_flags;
    char compression_codec_index;
    long index;
    ulong compressed_block_size;
    ulong uncompressed_block_size;
    long crc_checksum;
    ulong unknown_size;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_resource_page) == 0x24);

struct s_render_geometry_static_per_pixel_lighting
{
    c_tag_block<long> unused_vertex_buffer;
    short vertex_buffer_index;
    byte unused[2];
    s_vertex_buffer_definition vertex_buffer;
};
static_assert(sizeof(s_render_geometry_static_per_pixel_lighting) == 0x10);

struct s_render_geometry_per_mesh_subpart_visibility_block
{
    c_tag_block<s_render_geometry_bounding_sphere> bounding_spheres;
};
static_assert(sizeof(s_render_geometry_per_mesh_subpart_visibility_block) == 0xC);

struct s_render_geometry_per_mesh_node_map
{
    c_tag_block<s_render_geometry_per_mesh_node_map_node_index> node_indices;
};
static_assert(sizeof(s_render_geometry_per_mesh_node_map) == 0xC);

struct s_render_geometry_per_mesh_node_map_node_index
{
    uchar node;
};
static_assert(sizeof(s_render_geometry_per_mesh_node_map_node_index) == 0x1);

struct s_render_geometry_mopp_cluster_visiblity
{
    s_tag_data mopp_data;
    c_tag_block<short> unknown_mesh_part_indices_count;
};
static_assert(sizeof(s_render_geometry_mopp_cluster_visiblity) == 0x20);

struct s_render_geometry_geometry_tag_resource
{
    c_tag_block<float> vertex_buffer;
    c_tag_block<short> index_buffer;
    byte unused[20];
};
static_assert(sizeof(s_render_geometry_geometry_tag_resource) == 0x2C);

struct s_render_geometry_unknown_block
{
    uchar unknown_byte1;
    uchar unknown_byte2;
    short unknown2;
    s_tag_data unknown3;
};
static_assert(sizeof(s_render_geometry_unknown_block) == 0x18);

struct s_render_geometry_bounding_sphere
{
    real_plane3d plane;
    real_point3d position;
    float radius;
    s_byte node_indices[4];
    single node_weights[3];
};
static_assert(sizeof(s_render_geometry_bounding_sphere) == 0x30);

struct s_render_geometry_compression
{
    c_flags<e_render_geometry_compression_flags, ushort> flags;
    byte unused[2];
    real_bounds x;
    real_bounds y;
    real_bounds z;
    real_bounds u;
    real_bounds v;
};
static_assert(sizeof(s_render_geometry_compression) == 0x2C);

struct s_mesh
{
    c_tag_block<s_part> parts;
    c_tag_block<s_sub_part> sub_parts;
    int16 vertex_buffer_indices[8];
    int16 index_buffer_indices[2];
    vertex_buffer_definition resource_vertex_buffers[0];
    index_buffer_definition resource_index_buffers[0];
    c_flags<e_mesh_flags, uchar> flags;
    char rigid_node_index;
    c_enum<e_vertex_type, uchar> type;
    c_enum<e_prt_sh_type, uchar> prt_type;
    c_enum<e_primitive_type, char> index_buffer_type;
    byte unused3[3];
    c_tag_block<s_mesh_instanced_geometry_block> instanced_geometry;
    c_tag_block<s_mesh_water_block> water;
};
static_assert(sizeof(s_mesh) == 0x4C);

struct s_mesh_water_block
{
    short value;
};
static_assert(sizeof(s_mesh_water_block) == 0x2);

struct s_mesh_instanced_geometry_block
{
    short section1;
    short section2;
    c_tag_block<s_mesh_instanced_geometry_block_contents_block> contents;
};
static_assert(sizeof(s_mesh_instanced_geometry_block) == 0x10);

struct s_mesh_instanced_geometry_block_contents_block
{
    short value;
};
static_assert(sizeof(s_mesh_instanced_geometry_block_contents_block) == 0x2);

struct s_index_buffer_definition
{
    c_enum<e_index_buffer_format, long> format;
    tag_data data;
    byte unused[8];
};
static_assert(sizeof(s_index_buffer_definition) == 0x20);

struct s_vertex_buffer_definition
{
    long count;
    c_enum<e_vertex_buffer_format, short> format;
    short vertex_size;
    tag_data data;
    byte unused[4];
};
static_assert(sizeof(s_vertex_buffer_definition) == 0x20);

struct s_sub_part
{
    ushort first_index;
    ushort index_count;
    short part_index;
    ushort vertex_count;
};
static_assert(sizeof(s_sub_part) == 0x8);

struct s_part
{
    short material_index;
    short transparent_sorting_index;
    ushort first_index_old;
    ushort index_count_old;
    short first_sub_part_index;
    short sub_part_count;
    c_enum<e_part_part_type_new, char> type_new;
    c_flags<e_part_part_flags_new, uchar> flags_new;
    ushort vertex_count;
};
static_assert(sizeof(s_part) == 0x10);

struct s_scenario_structure_bsp_instanced_geometry_instance
{
    float scale;
    real_matrix4_x3 matrix;
    short definition_index;
    c_flags<e_scenario_structure_bsp_instanced_geometry_instance_flags, ushort> flags;
    short lod_data_index;
    short compression_index;
    ulong seam_bit_vector;
    real_point3d world_bounding_sphere_center;
    real_bounds bounding_sphere_radius_bounds;
    string_id name;
    c_enum<e_scenery_pathfinding_policy, short> pathfinding_policy;
    c_enum<e_scenery_lightmapping_policy, short> lightmapping_policy;
    float lightmap_resolution_scale;
    c_tag_block<s_collision_bsp_physics_definition> bsp_physics;
    short group_index;
    short group_list_index;
    c_flags<e_mesh_flags, uchar> mesh_override_flags;
    short unknown7;
};
static_assert(sizeof(s_scenario_structure_bsp_instanced_geometry_instance) == 0x74);

struct s_collision_bsp_physics_definition
{
    s_collision_geometry_shape geometry_shape;
    s_c_mopp_bv_tree_shape mopp_bv_tree_shape;
};
static_assert(sizeof(s_collision_bsp_physics_definition) == 0x80);

struct s_c_mopp_bv_tree_shape : s_hkp_mopp_bv_tree_shape
{
    float scale;
};
static_assert(sizeof(s_c_mopp_bv_tree_shape) == 0x20);

struct s_collision_geometry_shape : s_hkp_shape_collection
{
    byte padding1[8];
    real_quaternion a_a_b_b_center;
    real_quaternion a_a_b_b_half_extents;
    s_tag_reference_short model;
    ulong collision_bsp_address;
    ulong large_collision_bsp_address;
    char bsp_index;
    uchar collision_geometry_shape_type;
    ushort collision_geometry_shape_key;
    float scale;
    byte padding2[12];
};
static_assert(sizeof(s_collision_geometry_shape) == 0x60);

struct s_scenario_structure_bsp_environment_object
{
    char name[32];
    real_quaternion rotation;
    real_point3d position;
    float scale;
    short palette_index;
    c_flags<e_scenario_structure_bsp_environment_object_flags, uchar> flags;
    byte unused[1];
    long unique_id;
    tag exported_object_type;
    char scenario_object_name[32];
};
static_assert(sizeof(s_scenario_structure_bsp_environment_object) == 0x6C);

struct s_scenario_structure_bsp_environment_object_palette_block
{
    s_tag_reference definition;
    s_tag_reference model;
    c_flags<e_scenario_structure_bsp_environment_object_palette_block_object_type, long> object_type;
};
static_assert(sizeof(s_scenario_structure_bsp_environment_object_palette_block) == 0x24);

struct s_scenario_structure_bsp_runtime_decal
{
    short palette_index;
    char yaw;
    char pitch;
    real_quaternion rotation;
    real_point3d position;
    float scale;
};
static_assert(sizeof(s_scenario_structure_bsp_runtime_decal) == 0x24);

struct s_scenario_structure_bsp_runtime_light
{
    short light_index;
};
static_assert(sizeof(s_scenario_structure_bsp_runtime_light) == 0x2);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

struct s_scenario_structure_bsp_marker
{
    char name[32];
    real_quaternion rotation;
    real_point3d position;
};
static_assert(sizeof(s_scenario_structure_bsp_marker) == 0x3C);

struct s_scenario_structure_bsp_background_sound_environment_palette_block
{
    string_id name;
    s_tag_reference sound_environment;
    c_enum<e_scenario_structure_bsp_sound_environment_type, long> type;
    float reverb_cutoff_distance;
    float reverb_interpolation_speed;
    s_tag_reference ambience_background_sound;
    s_tag_reference ambience_inside_cluster_sound;
    float ambience_cutoff_distance;
    c_flags<e_scenario_structure_bsp_background_sound_scale_flags, long> ambience_scale_flags;
    float ambience_interior_scale;
    float ambience_portal_scale;
    float ambience_exterior_scale;
    float ambience_interpolation_speed;
};
static_assert(sizeof(s_scenario_structure_bsp_background_sound_environment_palette_block) == 0x58);

struct s_tag_pathfinding
{
    c_tag_block<s_sector> sectors;
    c_tag_block<s_link> links;
    c_tag_block<s_reference> references;
    c_tag_block<s_bsp2_d_node> bsp2_d_nodes;
    c_tag_block<s_vertex> vertices;
    c_tag_block<s_object_reference> object_references;
    c_tag_block<s_pathfinding_hint> pathfinding_hints;
    c_tag_block<s_instanced_geometry_reference> instanced_geometry_references;
    long structure_checksum;
    c_tag_block<long> unused_block;
    c_tag_block<s_giant_pathfinding_block> giant_pathfinding;
    c_tag_block<s_seam> seams;
    c_tag_block<s_jump_seam> jump_seams;
    c_tag_block<s_door> doors;
};
static_assert(sizeof(s_tag_pathfinding) == 0xA0);

struct s_door
{
    short scenario_object_index;
    byte unused[2];
};
static_assert(sizeof(s_door) == 0x4);

struct s_jump_seam
{
    short user_jump_index;
    uchar dest_only;
    byte unused[1];
    float length;
    c_tag_block<s_jump_seam_jump_index_block> jump_indices;
};
static_assert(sizeof(s_jump_seam) == 0x14);

struct s_jump_seam_jump_index_block
{
    short jump_index;
    byte unused[2];
};
static_assert(sizeof(s_jump_seam_jump_index_block) == 0x4);

struct s_seam
{
    c_tag_block<s_seam_link_index_block> link_indices;
};
static_assert(sizeof(s_seam) == 0xC);

struct s_seam_link_index_block
{
    long link_index;
};
static_assert(sizeof(s_seam_link_index_block) == 0x4);

struct s_giant_pathfinding_block
{
    long bsp2_d_index;
};
static_assert(sizeof(s_giant_pathfinding_block) == 0x4);

struct s_instanced_geometry_reference
{
    short pathfinding_object_index;
    short unknown;
};
static_assert(sizeof(s_instanced_geometry_reference) == 0x4);

struct s_pathfinding_hint
{
    c_enum<e_pathfinding_hint_hint_type, short> hint_type;
    short next_hint_index;
    int32 data[4];
};
static_assert(sizeof(s_pathfinding_hint) == 0x14);

struct s_object_reference
{
    ushort flags;
    byte unused[2];
    c_tag_block<s_object_reference_bsp_reference> bsps;
    long object_unique_id;
    short origin_bsp_index;
    s_scenario_object_type object_type;
    c_enum<e_scenario_scenario_instance_source, char> source;
};
static_assert(sizeof(s_object_reference) == 0x18);

struct s_scenario_object_type
{
    c_enum<e_game_object_type_halo3_odst, char> halo3_odst;
};
static_assert(sizeof(s_scenario_object_type) == 0x1);

struct s_object_reference_bsp_reference
{
    long bsp_index;
    short node_index;
    byte unused[2];
    c_tag_block<s_object_reference_bsp_reference_bsp2_d_ref> bsp2_d_refs;
    long vertex_offset;
};
static_assert(sizeof(s_object_reference_bsp_reference) == 0x18);

struct s_object_reference_bsp_reference_bsp2_d_ref
{
    datum_handle index;
};
static_assert(sizeof(s_object_reference_bsp_reference_bsp2_d_ref) == 0x4);

struct s_vertex
{
    real_point3d position;
};
static_assert(sizeof(s_vertex) == 0xC);

struct s_bsp2_d_node
{
    real_plane2d plane;
    long left_child;
    long right_child;
};
static_assert(sizeof(s_bsp2_d_node) == 0x14);

struct s_reference
{
    long node_or_sector_index;
};
static_assert(sizeof(s_reference) == 0x4);

struct s_link
{
    short vertex1;
    short vertex2;
    c_flags<e_link_flags, ushort> link_flags;
    short hint_index;
    ushort forward_link;
    ushort reverse_link;
    short left_sector;
    short right_sector;
};
static_assert(sizeof(s_link) == 0x10);

struct s_sector
{
    c_flags<e_sector_flags, ushort> pathfinding_sector_flags;
    short hint_index;
    long first_link;
};
static_assert(sizeof(s_sector) == 0x8);

struct s_breakable_surface_bits
{
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
};
static_assert(sizeof(s_breakable_surface_bits) == 0x20);

struct s_scenario_structure_bsp_conveyor_surface
{
    real_vector3d u;
    real_vector3d v;
};
static_assert(sizeof(s_scenario_structure_bsp_conveyor_surface) == 0x18);

struct s_scenario_structure_bsp_sky_owner_cluster_block
{
    short value;
};
static_assert(sizeof(s_scenario_structure_bsp_sky_owner_cluster_block) == 0x2);

struct s_render_material
{
    s_tag_reference render_method;
    c_tag_block<s_render_material_property> properties;
    long unknown;
    char breakable_surface_index;
    char unknown2;
    char unknown3;
    char unknown4;
};
static_assert(sizeof(s_render_material) == 0x24);

struct s_render_material_property
{
    s_render_material_property_type type;
    long int_value;
    float real_value;
};
static_assert(sizeof(s_render_material_property) == 0xC);

struct s_render_material_property_type
{
    c_enum<e_render_material_property_type_halo3, long> halo3;
};
static_assert(sizeof(s_render_material_property_type) == 0x4);

struct s_scenario_structure_bsp_cluster
{
    real_bounds bounds_x;
    real_bounds bounds_y;
    real_bounds bounds_z;
    char unknown;
    char scenario_sky_index;
    char camera_effect_index;
    char unknown2;
    short background_sound_environment_index;
    short sound_clusters_a_index;
    short unknown3;
    short unknown4;
    short unknown5;
    short unknown6;
    short unknown7;
    short runtime_decal_start_index;
    short runtime_decal_entry_count;
    short flags;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    c_tag_block<s_scenario_structure_bsp_cluster_portal> portals;
    s_scenario_structure_bsp_cluster_instanced_geometry_physics_data instanced_geometry_physics;
    short mesh_index;
    short unknown20;
    c_tag_block<s_scenario_structure_bsp_cluster_seam> seams;
    c_tag_block<s_scenario_structure_bsp_cluster_decorator_grid> decorator_grids;
    ulong unknown21;
    ulong unknown22;
    ulong unknown23;
    c_tag_block<s_scenario_structure_bsp_cluster_object_placement> object_placements;
    c_tag_block<s_scenario_structure_bsp_cluster_unknown_block2> unknown25;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster) == 0xDC);

struct s_scenario_structure_bsp_cluster_unknown_block2
{
    real_point3d object_location;
    short placement_index;
    short object_type;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_unknown_block2) == 0x10);

struct s_scenario_structure_bsp_cluster_object_placement
{
    s_game_object_type object_type;
    short placement_index;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_object_placement) == 0x4);

struct s_game_object_type
{
    c_enum<e_game_object_type_halo3_odst, char> halo3_odst;
    char unknown2;
};
static_assert(sizeof(s_game_object_type) == 0x2);

struct s_scenario_structure_bsp_cluster_decorator_grid
{
    short amount;
    s_scenario_structure_bsp_cluster_decorator_grid_decorator_info info;
    long vertex_buffer_offset;
    real_point3d position;
    float radius;
    real_point3d grid_size;
    real_point3d bounding_sphere_offset;
    c_tag_block<tiny_position_vertex> vertices;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_decorator_grid) == 0x34);

struct s_scenario_structure_bsp_cluster_decorator_grid_decorator_info
{
    short palette_index;
    short variant;
    short vertex_buffer_index;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_decorator_grid_decorator_info) == 0x6);

struct s_scenario_structure_bsp_cluster_seam
{
    char seam_index;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_seam) == 0x1);

struct s_scenario_structure_bsp_cluster_instanced_geometry_physics_data : s_hkp_shape_collection
{
    s_tag_reference structure_bsp;
    long cluster_index;
    s_hkp_mopp_bv_tree_shape shape;
    c_tag_block<s_tag_hkp_mopp_code> mopp_codes;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_instanced_geometry_physics_data) == 0x54);

struct s_tag_hkp_mopp_code : s_hkp_mopp_code
{
    c_tag_block<uchar> data;
    byte padding3[4];
};
static_assert(sizeof(s_tag_hkp_mopp_code) == 0x40);

struct s_hkp_mopp_code
{
    ulong vf_table_address;
    s_hkp_referenced_object referenced_object;
    s_tag_data padding1;
    s_code_info info;
    s_hk_array_base array_base;
    s_tag_data padding2;
};
static_assert(sizeof(s_hkp_mopp_code) == 0x30);

struct s_hk_array_base
{
    ulong data_address;
    ulong size;
    ulong capacity_and_flags;
};
static_assert(sizeof(s_hk_array_base) == 0xC);

struct s_code_info
{
    real_quaternion offset;
};
static_assert(sizeof(s_code_info) == 0x10);

struct s_hkp_mopp_bv_tree_shape : s_hkp_shape
{
    s_hkp_single_shape_container child;
    ulong mopp_code_address;
};
static_assert(sizeof(s_hkp_mopp_bv_tree_shape) == 0x1C);

struct s_hkp_single_shape_container : s_hkp_shape_container
{
    c_enum<e_blam_shape_type, short> type;
    short index;
};
static_assert(sizeof(s_hkp_single_shape_container) == 0x8);

struct s_hkp_shape_collection : s_hkp_shape
{
    s_hkp_shape_container container;
    boolean disable_welding;
    byte padding[3];
};
static_assert(sizeof(s_hkp_shape_collection) == 0x18);

struct s_hkp_shape_container
{
    ulong vf_table_address;
};
static_assert(sizeof(s_hkp_shape_container) == 0x4);

struct s_hkp_shape
{
    ulong vf_table_address;
    s_hkp_referenced_object referenced_object;
    ulong user_data_address;
    ulong type;
};
static_assert(sizeof(s_hkp_shape) == 0x10);

struct s_hkp_referenced_object
{
    ushort size_and_flags;
    ushort reference_count;
};
static_assert(sizeof(s_hkp_referenced_object) == 0x4);

struct s_scenario_structure_bsp_cluster_portal
{
    short portal_index;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_portal) == 0x2);

struct s_scenario_structure_bsp_detail_object
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    c_tag_block<s_scenario_structure_bsp_detail_object_unknown_block> unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
};
static_assert(sizeof(s_scenario_structure_bsp_detail_object) == 0x34);

struct s_scenario_structure_bsp_detail_object_unknown_block
{
    c_tag_block<s_scenario_structure_bsp_detail_object_unknown_block_unknown_block2> unknown;
    s_tag_data unknown2;
};
static_assert(sizeof(s_scenario_structure_bsp_detail_object_unknown_block) == 0x20);

struct s_scenario_structure_bsp_detail_object_unknown_block_unknown_block2
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_scenario_structure_bsp_detail_object_unknown_block_unknown_block2) == 0x10);

struct s_scenario_structure_bsp_camera_effect
{
    string_id name;
    s_tag_reference effect;
    char unknown;
    char unknown2;
    char unknown3;
    char unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
};
static_assert(sizeof(s_scenario_structure_bsp_camera_effect) == 0x30);

struct s_scenario_structure_bsp_fog_block
{
    string_id name;
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_scenario_structure_bsp_fog_block) == 0x8);

struct s_scenario_structure_bsp_unknown_block2
{
    char name[32];
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
};
static_assert(sizeof(s_scenario_structure_bsp_unknown_block2) == 0x78);

struct s_scenario_structure_bsp_cluster_portal
{
    short back_cluster;
    short front_cluster;
    long plane_index;
    real_point3d centroid;
    float bounding_radius;
    c_enum<e_scenario_structure_bsp_cluster_portal_flags, long> flags;
    c_tag_block<s_scenario_structure_bsp_cluster_portal_vertex> vertices;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_portal) == 0x28);

struct s_scenario_structure_bsp_cluster_portal_vertex
{
    real_point3d position;
};
static_assert(sizeof(s_scenario_structure_bsp_cluster_portal_vertex) == 0xC);

struct s_plane_reference
{
    short unknown1;
    short cluster_index;
};
static_assert(sizeof(s_plane_reference) == 0x4);

struct s_surfaces_planes
{
    long plane_index_new;
    long plane_count_new;
};
static_assert(sizeof(s_surfaces_planes) == 0x8);

struct s_scenario_structure_bsp_leaf
{
    uchar cluster_new;
};
static_assert(sizeof(s_scenario_structure_bsp_leaf) == 0x1);

struct s_scenario_structure_bsp_collision_material
{
    s_tag_reference render_method;
    short runtime_global_material_index;
    short conveyor_surface_index;
    short seam_mapping_index;
    c_flags<e_scenario_structure_bsp_collision_material_flags, ushort> flags;
};
static_assert(sizeof(s_scenario_structure_bsp_collision_material) == 0x18);

struct s_edge_to_seam_mapping
{
    short seam_index;
    short seam_identifier_index_edge_mapping_index;
};
static_assert(sizeof(s_edge_to_seam_mapping) == 0x4);

struct s_scenario_structure_bsp_seam_identifier
{
    int32 seam_i_ds[4];
    c_tag_block<s_scenario_structure_bsp_seam_identifier_edge> edge_mapping;
    c_tag_block<s_scenario_structure_bsp_seam_identifier_cluster> cluster_mapping;
};
static_assert(sizeof(s_scenario_structure_bsp_seam_identifier) == 0x28);

struct s_scenario_structure_bsp_seam_identifier_cluster
{
    long cluster_index;
    real_point3d cluster_center;
};
static_assert(sizeof(s_scenario_structure_bsp_seam_identifier_cluster) == 0x10);

struct s_scenario_structure_bsp_seam_identifier_edge
{
    long structure_edge_index;
};
static_assert(sizeof(s_scenario_structure_bsp_seam_identifier_edge) == 0x4);

