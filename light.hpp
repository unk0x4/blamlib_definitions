/* ---------- enums */

enum e_light_type
{
    _light_type_sphere = 0,
    _light_type_projective = 1
};


/* ---------- structures */

struct s_tag_function;
struct s_light;

struct s_light
{
    ulong flags;
    c_enum<e_light_type, short> type;
    short unknown;
    float light_range;
    float near_width;
    float height_stretch;
    float field_of_view;
    string_id function_name1;
    string_id function_name2;
    short unknown5;
    short unknown6;
    ulong unknown7;
    s_tag_function function1;
    string_id function_name3;
    string_id function_name4;
    short unknown8;
    short unknown9;
    ulong unknown10;
    s_tag_function function2;
    s_tag_reference gel_map;
    float frustum_minimum_view_distance;
    float max_intensity_range;
    float unknown12;
    float unknown13;
    char unknown14;
    char unknown15;
    char unknown16;
    char unknown17;
    s_tag_reference lens_flare;
};
static_assert(sizeof(s_light) == 0x94);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

