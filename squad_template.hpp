/* ---------- enums */

enum e_squad_template_cell_template_difficulty_flags
{
    _squad_template_cell_template_difficulty_flags_easy_bit = 0,
    _squad_template_cell_template_difficulty_flags_normal_bit = 1,
    _squad_template_cell_template_difficulty_flags_heroic_bit = 2,
    _squad_template_cell_template_difficulty_flags_legendary_bit = 3
};

enum e_character_grenade_type
{
    _character_grenade_type_human_fragmentation = 0,
    _character_grenade_type_covenant_plasma = 1,
    _character_grenade_type_brute_claymore = 2,
    _character_grenade_type_firebomb = 3
};


/* ---------- structures */

struct s_squad_template_cell_template_object_block;
struct s_squad_template_cell_template;
struct s_squad_template;

struct s_squad_template
{
    string_id name;
    c_tag_block<s_squad_template_cell_template> cell_templates;
};
static_assert(sizeof(s_squad_template) == 0x10);

struct s_squad_template_cell_template
{
    string_id name;
    c_flags<e_squad_template_cell_template_difficulty_flags, ushort> difficulty_flags;
    byte padding1[2];
    short minimum_round;
    short maximum_round;
    short unknown2;
    short unknown3;
    short count;
    short unknown4;
    c_tag_block<s_squad_template_cell_template_object_block> characters;
    c_tag_block<s_squad_template_cell_template_object_block> initial_weapons;
    c_tag_block<s_squad_template_cell_template_object_block> initial_secondary_weapons;
    c_tag_block<s_squad_template_cell_template_object_block> initial_equipment;
    c_enum<e_character_grenade_type, short> grenade_type;
    byte padding2[2];
    s_tag_reference vehicle;
    string_id vehicle_variant;
    string_id activity_name;
};
static_assert(sizeof(s_squad_template_cell_template) == 0x60);

struct s_squad_template_cell_template_object_block
{
    c_flags<e_squad_template_cell_template_difficulty_flags, ushort> difficulty_flags;
    byte padding1[2];
    short minimum_round;
    short maximum_round;
    ulong unknown3;
    s_tag_reference object;
    short probability;
    byte padding2[2];
};
static_assert(sizeof(s_squad_template_cell_template_object_block) == 0x20);

