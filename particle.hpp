/* ---------- enums */

enum e_particle_flags
{
    _particle_flags_dies_at_rest_bit = 0,
    _particle_flags_dies_on_structure_collision_bit = 1,
    _particle_flags_dies_in_water_bit = 2,
    _particle_flags_dies_in_air_bit = 3,
    _particle_flags_has_sweetener_bit = 4,
    _particle_flags_uses_cheap_shader_bit = 5,
    _particle_flags_bit6_bit = 6,
    _particle_flags_has_attachment_bit = 7
};

enum e_particle_attachment_trigger
{
    _particle_attachment_trigger_birth = 0,
    _particle_attachment_trigger_collision = 1,
    _particle_attachment_trigger_death = 2,
    _particle_attachment_trigger_first_collision = 3
};

enum e_tag_mapping_variable_type
{
    _tag_mapping_variable_type_particle_age = 0,
    _tag_mapping_variable_type_emitter_age = 1,
    _tag_mapping_variable_type_particle_random = 2,
    _tag_mapping_variable_type_emitter_random = 3,
    _tag_mapping_variable_type_particle_random1 = 4,
    _tag_mapping_variable_type_particle_random2 = 5,
    _tag_mapping_variable_type_particle_random3 = 6,
    _tag_mapping_variable_type_particle_random4 = 7,
    _tag_mapping_variable_type_emitter_random1 = 8,
    _tag_mapping_variable_type_emitter_random2 = 9,
    _tag_mapping_variable_type_emitter_time = 10,
    _tag_mapping_variable_type_system_lod = 11,
    _tag_mapping_variable_type_game_time = 12,
    _tag_mapping_variable_type_effect_a_scale = 13,
    _tag_mapping_variable_type_effect_b_scale = 14,
    _tag_mapping_variable_type_physics_rotation = 15,
    _tag_mapping_variable_type_location_random = 16,
    _tag_mapping_variable_type_distance_from_emitter = 17,
    _tag_mapping_variable_type_particle_simulation_a = 18,
    _tag_mapping_variable_type_particle_simulation_b = 19,
    _tag_mapping_variable_type_particle_velocity = 20,
    _tag_mapping_variable_type_invalid = 21
};

enum e_particle_particle_billboard_style
{
    _particle_particle_billboard_style_screen_facing = 0,
    _particle_particle_billboard_style_camera_facing = 1,
    _particle_particle_billboard_style_parallel_to_direction = 2,
    _particle_particle_billboard_style_perpendicular_to_direction = 3,
    _particle_particle_billboard_style_vertical = 4,
    _particle_particle_billboard_style_horizontal = 5,
    _particle_particle_billboard_style_local_vertical = 6,
    _particle_particle_billboard_style_local_horizontal = 7,
    _particle_particle_billboard_style_world_particle_models = 8,
    _particle_particle_billboard_style_velocity_horizontal_particle_models = 9,
    _particle_particle_billboard_style_local = 10
};

enum e_render_method_option_option_block_option_data_type
{
    _render_method_option_option_block_option_data_type_sampler = 0,
    _render_method_option_option_block_option_data_type_float4 = 1,
    _render_method_option_option_block_option_data_type_float = 2,
    _render_method_option_option_block_option_data_type_integer = 3,
    _render_method_option_option_block_option_data_type_boolean = 4,
    _render_method_option_option_block_option_data_type_integer_color = 5
};

enum e_render_method_shader_function_function_type
{
    _render_method_shader_function_function_type_value = 0,
    _render_method_shader_function_function_type_color = 1,
    _render_method_shader_function_function_type_scale_uniform = 2,
    _render_method_shader_function_function_type_scale_x = 3,
    _render_method_shader_function_function_type_scale_y = 4,
    _render_method_shader_function_function_type_translation_x = 5,
    _render_method_shader_function_function_type_translation_y = 6,
    _render_method_shader_function_function_type_frame_index = 7,
    _render_method_shader_function_function_type_alpha = 8,
    _render_method_shader_function_function_type_change_color_primary = 9,
    _render_method_shader_function_function_type_change_color_secondary = 10,
    _render_method_shader_function_function_type_change_color_tertiary = 11,
    _render_method_shader_function_function_type_change_color_quaternary = 12,
    _render_method_shader_function_function_type_change_color_quinary = 13
};

enum e_render_method_shader_property_texture_constant_sampler_filter_mode
{
    _render_method_shader_property_texture_constant_sampler_filter_mode_trilinear = 0,
    _render_method_shader_property_texture_constant_sampler_filter_mode_point = 1,
    _render_method_shader_property_texture_constant_sampler_filter_mode_bilinear = 2,
    _render_method_shader_property_texture_constant_sampler_filter_mode_unused_00 = 3,
    _render_method_shader_property_texture_constant_sampler_filter_mode_anisotropic_2_x = 4,
    _render_method_shader_property_texture_constant_sampler_filter_mode_unused_01 = 5,
    _render_method_shader_property_texture_constant_sampler_filter_mode_anisotropic_4_x = 6,
    _render_method_shader_property_texture_constant_sampler_filter_mode_lightprobe_texture_array = 7,
    _render_method_shader_property_texture_constant_sampler_filter_mode_texture_array_quadlinear = 8,
    _render_method_shader_property_texture_constant_sampler_filter_mode_texture_array_quadanisotropic_2_x = 9
};

enum e_render_method_shader_property_alpha_blend_mode
{
    _render_method_shader_property_alpha_blend_mode_opaque = 0,
    _render_method_shader_property_alpha_blend_mode_additive = 1,
    _render_method_shader_property_alpha_blend_mode_multiply = 2,
    _render_method_shader_property_alpha_blend_mode_alpha_blend = 3,
    _render_method_shader_property_alpha_blend_mode_double_multiply = 4,
    _render_method_shader_property_alpha_blend_mode_pre_multiplied_alpha = 5,
    _render_method_shader_property_alpha_blend_mode_maximum = 6,
    _render_method_shader_property_alpha_blend_mode_multiply_add = 7,
    _render_method_shader_property_alpha_blend_mode_add_src_times_dstalpha = 8,
    _render_method_shader_property_alpha_blend_mode_add_src_times_srcalpha = 9,
    _render_method_shader_property_alpha_blend_mode_inv_alpha_blend = 10,
    _render_method_shader_property_alpha_blend_mode_separate_alpha_blend = 11,
    _render_method_shader_property_alpha_blend_mode_separate_alpha_blend_additive = 12
};

enum e_render_method_shader_property_blend_mode_flags
{
    _render_method_shader_property_blend_mode_flags_bit0_bit = 0,
    _render_method_shader_property_blend_mode_flags_enable_alpha_test_bit = 1,
    _render_method_shader_property_blend_mode_flags_sfx_distort_force_alpha_blend_bit = 2
};

enum e_render_method_render_method_render_flags
{
    _render_method_render_method_render_flags_ignore_fog_bit = 0,
    _render_method_render_method_render_flags_use_sky_atmosphere_properties_bit = 1,
    _render_method_render_method_render_flags_uses_depth_camera_bit = 2,
    _render_method_render_method_render_flags_disable_with_shields_bit = 3,
    _render_method_render_method_render_flags_enable_with_shields_bit = 4
};

enum e_sorting_layer
{
    _sorting_layer_invalid = 0,
    _sorting_layer_pre_pass = 1,
    _sorting_layer_normal = 2,
    _sorting_layer_post_pass = 3
};

enum e_tag_mapping_output_modifier
{
    _tag_mapping_output_modifier_none = 0,
    _tag_mapping_output_modifier_plus = 1,
    _tag_mapping_output_modifier_times = 2
};

enum e_particle_animation_flags
{
    _particle_animation_flags_frame_animation_one_shot_bit = 0,
    _particle_animation_flags_can_animate_backwards_bit = 1
};


/* ---------- structures */

struct s_particle_attachment;
struct s_render_method_render_method_definition_option_index;
struct s_tag_function;
struct s_render_method_shader_function;
struct s_render_method_import_datum;
struct s_render_method_shader_property_texture_constant_packed_sampler_address_mode;
struct s_render_method_template_tag_block_index;
struct s_render_method_shader_property_texture_constant;
struct s_render_method_shader_property_real_constant;
struct s_render_method_shader_property_parameter_table;
struct s_render_method_shader_property_parameter_mapping;
struct s_render_method_shader_property;
struct s_render_method;
struct s_tag_mapping;
struct s_particle_runtime_m_sprites_block;
struct s_particle_runtime_m_frames_block;
struct s_particle;

struct s_particle
{
    c_flags<e_particle_flags, long> flags;
    c_tag_block<s_particle_attachment> attachments;
    long appearance_flags;
    c_enum<e_particle_particle_billboard_style, short> particle_billboard_style;
    short runtime_m_texture_array_size;
    short first_sequence_index;
    short sequence_count;
    float low_resolution_switch_distance;
    float center_offset_x;
    float curvature;
    float edge_range;
    float edge_cutoff;
    float motion_blur_translation_scale;
    float motion_blur_rotation_scale;
    float motion_blur_aspect_scale;
    s_render_method render_method;
    s_tag_mapping aspect_ratio;
    s_tag_mapping color;
    s_tag_mapping intensity;
    s_tag_mapping alpha;
    c_flags<e_particle_animation_flags, long> animation_flags;
    s_tag_mapping frame_index;
    s_tag_mapping animation_rate;
    s_tag_mapping palette_animation;
    s_tag_reference particle_model;
    ulong runtime_m_used_particle_states;
    ulong runtime_m_constant_per_particle_properties;
    ulong runtime_m_constant_over_time_properties;
    c_tag_block<s_particle_runtime_m_sprites_block> runtime_m_sprites;
    c_tag_block<s_particle_runtime_m_frames_block> runtime_m_frames;
};
static_assert(sizeof(s_particle) == 0x194);

struct s_particle_runtime_m_frames_block
{
    single runtime_m_count[4];
};
static_assert(sizeof(s_particle_runtime_m_frames_block) == 0x10);

struct s_particle_runtime_m_sprites_block
{
    single runtime_gpu_sprite_array[4];
};
static_assert(sizeof(s_particle_runtime_m_sprites_block) == 0x10);

struct s_tag_mapping
{
    c_enum<e_tag_mapping_variable_type, char> input_variable;
    c_enum<e_tag_mapping_variable_type, char> range_variable;
    c_enum<e_tag_mapping_output_modifier, char> output_modifier;
    c_enum<e_tag_mapping_variable_type, char> output_modifier_input;
    s_tag_function function;
    float runtime_m_constant_value;
    uchar runtime_m_flags;
    byte unused[3];
};
static_assert(sizeof(s_tag_mapping) == 0x20);

struct s_render_method
{
    s_tag_reference base_render_method;
    c_tag_block<s_render_method_render_method_definition_option_index> render_method_definition_option_indices;
    c_tag_block<s_render_method_import_datum> import_data;
    c_tag_block<s_render_method_shader_property> shader_properties;
    c_flags<e_render_method_render_method_render_flags, ushort> render_flags;
    c_enum<e_sorting_layer, uchar> sorting_layer;
    uchar version;
    long sky_atmosphere_properties_index;
    long unknown2;
};
static_assert(sizeof(s_render_method) == 0x40);

struct s_render_method_shader_property
{
    s_tag_reference template;
    c_tag_block<s_render_method_shader_property_texture_constant> texture_constants;
    c_tag_block<s_render_method_shader_property_real_constant> real_constants;
    c_tag_block<ulong> integer_constants;
    ulong boolean_constants;
    c_tag_block<s_render_method_template_tag_block_index> entry_points;
    c_tag_block<s_render_method_shader_property_parameter_table> parameter_tables;
    c_tag_block<s_render_method_shader_property_parameter_mapping> parameters;
    c_tag_block<s_render_method_shader_function> functions;
    c_enum<e_render_method_shader_property_alpha_blend_mode, ulong> alpha_blend_mode;
    c_flags<e_render_method_shader_property_blend_mode_flags, ulong> blend_flags;
    ulong unknown8;
    int16 queryable_properties[8];
};
static_assert(sizeof(s_render_method_shader_property) == 0x84);

struct s_render_method_shader_property_parameter_mapping
{
    short register_index;
    uchar function_index;
    uchar source_index;
};
static_assert(sizeof(s_render_method_shader_property_parameter_mapping) == 0x4);

struct s_render_method_shader_property_parameter_table
{
    s_render_method_template_tag_block_index texture;
    s_render_method_template_tag_block_index real_vertex;
    s_render_method_template_tag_block_index real_pixel;
};
static_assert(sizeof(s_render_method_shader_property_parameter_table) == 0x6);

struct s_render_method_shader_property_real_constant
{
    single values[4];
};
static_assert(sizeof(s_render_method_shader_property_real_constant) == 0x10);

struct s_render_method_shader_property_texture_constant
{
    s_tag_reference bitmap;
    short bitmap_index;
    s_render_method_shader_property_texture_constant_packed_sampler_address_mode sampler_address_mode;
    c_enum<e_render_method_shader_property_texture_constant_sampler_filter_mode, uchar> filter_mode;
    char extern_mode;
    char x_form_argument_index;
    s_render_method_template_tag_block_index functions;
};
static_assert(sizeof(s_render_method_shader_property_texture_constant) == 0x18);

struct s_render_method_template_tag_block_index
{
    ushort integer;
};
static_assert(sizeof(s_render_method_template_tag_block_index) == 0x2);

struct s_render_method_shader_property_texture_constant_packed_sampler_address_mode
{
    uchar sampler_address_uv;
};
static_assert(sizeof(s_render_method_shader_property_texture_constant_packed_sampler_address_mode) == 0x1);

struct s_render_method_import_datum
{
    string_id name;
    c_enum<e_render_method_option_option_block_option_data_type, ulong> type;
    s_tag_reference bitmap;
    float default_real_value;
    long default_int_bool_value;
    short sampler_flags;
    short sampler_filter_mode;
    short default_address_mode;
    short address_u;
    short address_v;
    short unknown9;
    ulong unknown10;
    c_tag_block<s_render_method_shader_function> functions;
};
static_assert(sizeof(s_render_method_import_datum) == 0x3C);

struct s_render_method_shader_function
{
    c_enum<e_render_method_shader_function_function_type, long> type;
    string_id input_name;
    string_id range_name;
    float time_period;
    s_tag_function function;
};
static_assert(sizeof(s_render_method_shader_function) == 0x24);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_render_method_render_method_definition_option_index
{
    short option_index;
};
static_assert(sizeof(s_render_method_render_method_definition_option_index) == 0x2);

struct s_particle_attachment
{
    s_tag_reference type;
    c_enum<e_particle_attachment_trigger, char> trigger;
    uchar skip_fraction;
    c_enum<e_tag_mapping_variable_type, char> primary_scale;
    c_enum<e_tag_mapping_variable_type, char> secondary_scale;
};
static_assert(sizeof(s_particle_attachment) == 0x14);

