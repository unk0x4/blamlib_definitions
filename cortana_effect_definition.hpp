/* ---------- enums */

enum e_cortana_effect_definition_cortana_effect_flags
{
    _cortana_effect_definition_cortana_effect_flags_use_cosine_interpolation_bit = 0
};


/* ---------- structures */

struct s_cortana_effect_definition_sound_block;
struct s_cortana_effect_definition_scalar_block_dynamic_value_frame_block;
struct s_tag_function;
struct s_cortana_effect_definition_scalar_block_dynamic_value;
struct s_cortana_effect_definition_scalar_block;
struct s_cortana_effect_definition_color_block_dynamic_value_frame_block;
struct s_cortana_effect_definition_color_block_dynamic_value;
struct s_cortana_effect_definition_color_block;
struct s_cortana_effect_definition_postprocessing_block;
struct s_cortana_effect_definition_gravemind_block;
struct s_cortana_effect_definition_cortana_block;
struct s_cortana_effect_definition;

struct s_cortana_effect_definition
{
    string_id scenario_name;
    long frame_count;
    c_tag_block<s_cortana_effect_definition_sound_block> sounds;
    s_tag_data data1;
    s_tag_data data2;
    s_tag_reference cinematic_scene;
    string_id anchor_name;
    c_tag_block<s_cortana_effect_definition_postprocessing_block> post_processing;
    c_tag_block<s_cortana_effect_definition_gravemind_block> gravemind;
    c_tag_block<s_cortana_effect_definition_cortana_block> unused;
    c_tag_block<s_cortana_effect_definition_cortana_block> cortana;
};
static_assert(sizeof(s_cortana_effect_definition) == 0x80);

struct s_cortana_effect_definition_cortana_block
{
    c_tag_block<s_cortana_effect_definition_scalar_block> solarize;
    c_tag_block<s_cortana_effect_definition_color_block> doubling;
    c_tag_block<s_cortana_effect_definition_scalar_block> colorize;
};
static_assert(sizeof(s_cortana_effect_definition_cortana_block) == 0x24);

struct s_cortana_effect_definition_gravemind_block
{
    c_tag_block<s_cortana_effect_definition_color_block> background_color;
    c_tag_block<s_cortana_effect_definition_color_block> unused;
    c_tag_block<s_cortana_effect_definition_scalar_block> tentacles_in;
    c_tag_block<s_cortana_effect_definition_color_block> vignette;
};
static_assert(sizeof(s_cortana_effect_definition_gravemind_block) == 0x30);

struct s_cortana_effect_definition_postprocessing_block
{
    c_tag_block<s_cortana_effect_definition_scalar_block> fov;
    c_tag_block<s_cortana_effect_definition_color_block> hue;
    c_tag_block<s_cortana_effect_definition_color_block> saturation;
    c_tag_block<s_cortana_effect_definition_scalar_block> rumble;
    c_tag_block<s_cortana_effect_definition_scalar_block> hud_brightness;
    c_tag_block<s_cortana_effect_definition_scalar_block> hud_shake_amount;
};
static_assert(sizeof(s_cortana_effect_definition_postprocessing_block) == 0x48);

struct s_cortana_effect_definition_color_block
{
    c_flags<e_cortana_effect_definition_cortana_effect_flags, long> flags;
    float inmix_value1;
    float inmix_value2;
    float inmix_value3;
    float basevalue1;
    float basevalue2;
    float basevalue3;
    c_tag_block<s_cortana_effect_definition_color_block_dynamic_value> dynamic_values;
    float outmix_value1;
    float outmix_value2;
    float outmix_value3;
};
static_assert(sizeof(s_cortana_effect_definition_color_block) == 0x34);

struct s_cortana_effect_definition_color_block_dynamic_value
{
    c_tag_block<s_cortana_effect_definition_color_block_dynamic_value_frame_block> frames;
    s_tag_function frame_function;
};
static_assert(sizeof(s_cortana_effect_definition_color_block_dynamic_value) == 0x20);

struct s_cortana_effect_definition_color_block_dynamic_value_frame_block
{
    long frame;
    float dynamicvalue1;
    float dynamicvalue2;
    float dynamicvalue3;
};
static_assert(sizeof(s_cortana_effect_definition_color_block_dynamic_value_frame_block) == 0x10);

struct s_cortana_effect_definition_scalar_block
{
    c_flags<e_cortana_effect_definition_cortana_effect_flags, long> flags;
    float inmix_value1;
    float inmix_value2;
    float inmix_value3;
    float basevalue1;
    float basevalue2;
    c_tag_block<s_cortana_effect_definition_scalar_block_dynamic_value> dynamic_values;
    float outmix_value1;
    float outmix_value2;
    float outmix_value3;
};
static_assert(sizeof(s_cortana_effect_definition_scalar_block) == 0x30);

struct s_cortana_effect_definition_scalar_block_dynamic_value
{
    c_tag_block<s_cortana_effect_definition_scalar_block_dynamic_value_frame_block> frames;
    s_tag_function frame_function;
};
static_assert(sizeof(s_cortana_effect_definition_scalar_block_dynamic_value) == 0x20);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_cortana_effect_definition_scalar_block_dynamic_value_frame_block
{
    long frame;
    float dynamicvalue1;
    float dynamicvalue2;
};
static_assert(sizeof(s_cortana_effect_definition_scalar_block_dynamic_value_frame_block) == 0xC);

struct s_cortana_effect_definition_sound_block
{
    ulong unknown;
    s_tag_reference sound;
};
static_assert(sizeof(s_cortana_effect_definition_sound_block) == 0x14);

