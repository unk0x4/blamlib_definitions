/* ---------- enums */


/* ---------- structures */

struct s_sound_dialogue_constants;

struct s_sound_dialogue_constants
{
    float almost_never;
    float rarely;
    float somewhat;
    float often;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_sound_dialogue_constants) == 0x28);

