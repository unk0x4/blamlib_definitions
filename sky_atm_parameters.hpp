/* ---------- enums */

enum e_sorting_layer
{
    _sorting_layer_invalid = 0,
    _sorting_layer_pre_pass = 1,
    _sorting_layer_normal = 2,
    _sorting_layer_post_pass = 3
};


/* ---------- structures */

struct s_sky_atm_parameters_atmosphere_property;
struct s_sky_atm_parameters_underwater_block;
struct s_sky_atm_parameters;

struct s_sky_atm_parameters
{
    long unknown1;
    s_tag_reference fog_bitmap;
    float texture_repeat_rate;
    float distance_between_sheets;
    float depth_fade_factor;
    float unknown5;
    float falloff_start_distance;
    float distance_falloff_power;
    float transparent_sort_distance;
    c_enum<e_sorting_layer, uchar> transparent_sorting_layer;
    byte unused[3];
    c_tag_block<s_sky_atm_parameters_atmosphere_property> atmosphere_properties;
    c_tag_block<s_sky_atm_parameters_underwater_block> underwater;
};
static_assert(sizeof(s_sky_atm_parameters) == 0x4C);

struct s_sky_atm_parameters_underwater_block
{
    string_id name;
    float murkiness;
    real_rgb_color color;
};
static_assert(sizeof(s_sky_atm_parameters_underwater_block) == 0x14);

struct s_sky_atm_parameters_atmosphere_property
{
    short unknown1;
    short unknown2;
    string_id name;
    float light_source_y;
    float light_source_x;
    real_rgb_color fog_color;
    float brightness;
    float fog_gradient_threshold;
    float light_intensity;
    float sky_invisibility_through_fog;
    float unknown3;
    float unknown4;
    float light_source_spread;
    ulong unknown5;
    float fog_intensity;
    float unknown6;
    float tint_cyan;
    float tint_magenta;
    float tint_yellow;
    float fog_intensity_cyan;
    float fog_intensity_magenta;
    float fog_intensity_yellow;
    float background_color_red;
    float background_color_green;
    float background_color_blue;
    float tint_red;
    float tint2_green;
    float tint2_blue;
    float fog_intensity2;
    float start_distance;
    float end_distance;
    float fog_velocity_x;
    float fog_velocity_y;
    float fog_velocity_z;
    s_tag_reference weather_effect;
    ulong unknown7;
    ulong unknown8;
};
static_assert(sizeof(s_sky_atm_parameters_atmosphere_property) == 0xA4);

