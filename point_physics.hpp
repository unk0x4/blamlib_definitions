/* ---------- enums */

enum e_point_physics_flag_bits
{
    _point_physics_flag_bits_unused_bit = 0,
    _point_physics_flag_bits_collides_with_structures_bit = 1,
    _point_physics_flag_bits_collides_with_water_surface_bit = 2,
    _point_physics_flag_bits_uses_simple_wind_bit = 3,
    _point_physics_flag_bits_uses_damped_wind_bit = 4,
    _point_physics_flag_bits_no_gravity_bit = 5
};


/* ---------- structures */

struct s_point_physics;

struct s_point_physics
{
    c_flags<e_point_physics_flag_bits, long> flags;
    float runtime_mass_over_radius_cubed;
    float runtime_inverse_density;
    float unknown;
    byte unused1[16];
    float density;
    float air_friction;
    float water_friction;
    float surface_friction;
    float elasticity;
    byte unused2[12];
};
static_assert(sizeof(s_point_physics) == 0x40);

