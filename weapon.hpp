/* ---------- enums */

enum e_weapon_flags_new_weapon_flags
{
    _weapon_flags_new_weapon_flags_vertical_heat_display_bit = 0,
    _weapon_flags_new_weapon_flags_mutually_exclusive_triggers_bit = 1,
    _weapon_flags_new_weapon_flags_attacks_automatically_on_bump_bit = 2,
    _weapon_flags_new_weapon_flags_must_be_readied_bit = 3,
    _weapon_flags_new_weapon_flags_does_not_count_towards_maximum_bit = 4,
    _weapon_flags_new_weapon_flags_aim_assists_only_when_zoomed_bit = 5,
    _weapon_flags_new_weapon_flags_prevents_grenade_throwing_bit = 6,
    _weapon_flags_new_weapon_flags_must_be_picked_up_bit = 7,
    _weapon_flags_new_weapon_flags_holds_triggers_when_dropped_bit = 8,
    _weapon_flags_new_weapon_flags_prevents_melee_attack_bit = 9,
    _weapon_flags_new_weapon_flags_detonates_when_dropped_bit = 10,
    _weapon_flags_new_weapon_flags_cannot_fire_at_maximum_age_bit = 11,
    _weapon_flags_new_weapon_flags_secondary_trigger_overrides_grenades_bit = 12,
    _weapon_flags_new_weapon_flags_support_weapon_bit = 13,
    _weapon_flags_new_weapon_flags_hide_fp_weapon_when_in_iron_sights_bit = 14,
    _weapon_flags_new_weapon_flags_a_is_use_weapon_melee_damage_bit = 15,
    _weapon_flags_new_weapon_flags_prevents_binoculars_bit = 16,
    _weapon_flags_new_weapon_flags_loop_fp_firing_animation_bit = 17,
    _weapon_flags_new_weapon_flags_prevents_crouching_bit = 18,
    _weapon_flags_new_weapon_flags_cannot_fire_while_boosting_bit = 19,
    _weapon_flags_new_weapon_flags_uses_empty_melee_on_empty_bit = 20,
    _weapon_flags_new_weapon_flags_third_person_camera_bit = 21,
    _weapon_flags_new_weapon_flags_can_be_dual_wielded_bit = 22,
    _weapon_flags_new_weapon_flags_can_only_be_dual_wielded_bit = 23,
    _weapon_flags_new_weapon_flags_melee_only_bit = 24,
    _weapon_flags_new_weapon_flags_cannot_fire_if_parent_dead_bit = 25,
    _weapon_flags_new_weapon_flags_weapon_ages_with_each_kill_bit = 26,
    _weapon_flags_new_weapon_flags_allows_unaimed_lunge_bit = 27,
    _weapon_flags_new_weapon_flags_cannot_be_used_by_player_bit = 28
};

enum e_weapon_secondary_trigger_mode
{
    _weapon_secondary_trigger_mode_normal = 0,
    _weapon_secondary_trigger_mode_slaved_to_primary = 1,
    _weapon_secondary_trigger_mode_inhibits_primary = 2,
    _weapon_secondary_trigger_mode_loads_alternate_ammunition = 3,
    _weapon_secondary_trigger_mode_loads_multiple_primary_ammunition = 4
};

enum e_damage_reporting_type
{
    _damage_reporting_type_guardians_unknown = 0,
    _damage_reporting_type_guardians = 1,
    _damage_reporting_type_falling_damage = 2,
    _damage_reporting_type_generic_collision = 3,
    _damage_reporting_type_armor_lock_crush = 4,
    _damage_reporting_type_generic_melee = 5,
    _damage_reporting_type_generic_explosion = 6,
    _damage_reporting_type_magnum = 7,
    _damage_reporting_type_plasma_pistol = 8,
    _damage_reporting_type_needler = 9,
    _damage_reporting_type_mauler = 10,
    _damage_reporting_type_smg = 11,
    _damage_reporting_type_plasma_rifle = 12,
    _damage_reporting_type_battle_rifle = 13,
    _damage_reporting_type_carbine = 14,
    _damage_reporting_type_shotgun = 15,
    _damage_reporting_type_sniper_rifle = 16,
    _damage_reporting_type_beam_rifle = 17,
    _damage_reporting_type_assault_rifle = 18,
    _damage_reporting_type_spiker = 19,
    _damage_reporting_type_fuel_rod_cannon = 20,
    _damage_reporting_type_missile_pod = 21,
    _damage_reporting_type_rocket_launcher = 22,
    _damage_reporting_type_spartan_laser = 23,
    _damage_reporting_type_brute_shot = 24,
    _damage_reporting_type_flamethrower = 25,
    _damage_reporting_type_sentinel_gun = 26,
    _damage_reporting_type_energy_sword = 27,
    _damage_reporting_type_gravity_hammer = 28,
    _damage_reporting_type_frag_grenade = 29,
    _damage_reporting_type_plasma_grenade = 30,
    _damage_reporting_type_spike_grenade = 31,
    _damage_reporting_type_firebomb_grenade = 32,
    _damage_reporting_type_flag = 33,
    _damage_reporting_type_bomb = 34,
    _damage_reporting_type_bomb_explosion = 35,
    _damage_reporting_type_ball = 36,
    _damage_reporting_type_machinegun_turret = 37,
    _damage_reporting_type_plasma_cannon = 38,
    _damage_reporting_type_plasma_mortar = 39,
    _damage_reporting_type_plasma_turret = 40,
    _damage_reporting_type_shade_turret = 41,
    _damage_reporting_type_banshee = 42,
    _damage_reporting_type_ghost = 43,
    _damage_reporting_type_mongoose = 44,
    _damage_reporting_type_scorpion = 45,
    _damage_reporting_type_scorpion_gunner = 46,
    _damage_reporting_type_spectre = 47,
    _damage_reporting_type_spectre_gunner = 48,
    _damage_reporting_type_warthog = 49,
    _damage_reporting_type_warthog_gunner = 50,
    _damage_reporting_type_warthog_gauss_turret = 51,
    _damage_reporting_type_wraith = 52,
    _damage_reporting_type_wraith_gunner = 53,
    _damage_reporting_type_tank = 54,
    _damage_reporting_type_chopper = 55,
    _damage_reporting_type_hornet = 56,
    _damage_reporting_type_mantis = 57,
    _damage_reporting_type_prowler = 58,
    _damage_reporting_type_sentinel_beam = 59,
    _damage_reporting_type_sentinel_rpg = 60,
    _damage_reporting_type_teleporter = 61,
    _damage_reporting_type_tripmine = 62,
    _damage_reporting_type_dmr = 63
};

enum e_weapon_movement_penalized
{
    _weapon_movement_penalized_always = 0,
    _weapon_movement_penalized_when_zoomed = 1,
    _weapon_movement_penalized_when_zoomed_or_reloading = 2
};

enum e_weapon_multiplayer_weapon_type
{
    _weapon_multiplayer_weapon_type_none = 0,
    _weapon_multiplayer_weapon_type_ctf_flag = 1,
    _weapon_multiplayer_weapon_type_oddball_ball = 2,
    _weapon_multiplayer_weapon_type_headhunter_head = 3,
    _weapon_multiplayer_weapon_type_juggernaut_powerup = 4
};

enum e_weapon_weapon_type
{
    _weapon_weapon_type_undefined = 0,
    _weapon_weapon_type_shotgun = 1,
    _weapon_weapon_type_needler = 2,
    _weapon_weapon_type_plasma_pistol = 3,
    _weapon_weapon_type_plasma_rifle = 4,
    _weapon_weapon_type_rocket_launcher = 5,
    _weapon_weapon_type_energy_sword = 6,
    _weapon_weapon_type_spartan_laser = 7
};

enum e_weapon_tracking_type
{
    _weapon_tracking_type_no_tracking = 0,
    _weapon_tracking_type_human_tracking = 1,
    _weapon_tracking_type_covenant_tracking = 2
};

enum e_weapon_special_hud_version
{
    _weapon_special_hud_version_default_no_outline2 = -28,
    _weapon_special_hud_version_default30 = 0,
    _weapon_special_hud_version_ammo31 = 1,
    _weapon_special_hud_version_damage32 = 2,
    _weapon_special_hud_version_accuracy33 = 3,
    _weapon_special_hud_version_rate_of_fire34 = 4,
    _weapon_special_hud_version_range35 = 5,
    _weapon_special_hud_version_power36 = 6
};

enum e_weapon_trigger_button_used
{
    _weapon_trigger_button_used_right_trigger = 0,
    _weapon_trigger_button_used_left_trigger = 1,
    _weapon_trigger_button_used_melee_attack = 2,
    _weapon_trigger_button_used_automated_fire = 3,
    _weapon_trigger_button_used_right_bumper = 4
};

enum e_weapon_trigger_behavior
{
    _weapon_trigger_behavior_spew = 0,
    _weapon_trigger_behavior_latch = 1,
    _weapon_trigger_behavior_latch_autofire = 2,
    _weapon_trigger_behavior_charge = 3,
    _weapon_trigger_behavior_latch_zoom = 4,
    _weapon_trigger_behavior_latch_rocketlauncher = 5,
    _weapon_trigger_behavior_spew_charge = 6
};

enum e_weapon_trigger_prediction
{
    _weapon_trigger_prediction_none = 0,
    _weapon_trigger_prediction_spew = 1,
    _weapon_trigger_prediction_charge = 2
};

enum e_weapon_trigger_secondary_action
{
    _weapon_trigger_secondary_action_fire = 0,
    _weapon_trigger_secondary_action_charge = 1,
    _weapon_trigger_secondary_action_track = 2,
    _weapon_trigger_secondary_action_fire_other = 3
};

enum e_weapon_trigger_primary_action
{
    _weapon_trigger_primary_action_fire = 0,
    _weapon_trigger_primary_action_charge = 1,
    _weapon_trigger_primary_action_track = 2,
    _weapon_trigger_primary_action_fire_other = 3
};

enum e_weapon_trigger_overcharge_action
{
    _weapon_trigger_overcharge_action_none = 0,
    _weapon_trigger_overcharge_action_explode = 1,
    _weapon_trigger_overcharge_action_discharge = 2
};

enum e_barrel_flags
{
    _barrel_flags_tracks_fired_projectile_bit = 0,
    _barrel_flags_random_firing_effects_bit = 1,
    _barrel_flags_can_fire_with_partial_ammo_bit = 2,
    _barrel_flags_projectiles_use_weapon_origin_bit = 3,
    _barrel_flags_ejects_during_chamber_bit = 4,
    _barrel_flags_use_error_when_unzoomed_bit = 5,
    _barrel_flags_projectile_vector_cannot_be_adjusted_bit = 6,
    _barrel_flags_projectiles_have_identical_error_bit = 7,
    _barrel_flags_projectiles_fire_parallel_bit = 8,
    _barrel_flags_cant_fire_when_others_firing_bit = 9,
    _barrel_flags_cant_fire_when_others_recovering_bit = 10,
    _barrel_flags_dont_clear_fire_bit_after_recovering_bit = 11,
    _barrel_flags_stagger_fire_across_multiple_markers_bit = 12,
    _barrel_flags_plasma_pistol_bit = 13,
    _barrel_flags_shrine_defender_bit = 14,
    _barrel_flags_hornet_weapons_bit = 15,
    _barrel_flags_bit16_bit = 16,
    _barrel_flags_bit17_bit = 17,
    _barrel_flags_bit18_bit = 18,
    _barrel_flags_projectile_fires_in_marker_direction_bit = 19,
    _barrel_flags_bit20_bit = 20
};

enum e_weapon_barrel_prediction_type
{
    _weapon_barrel_prediction_type_none = 0,
    _weapon_barrel_prediction_type_continuous = 1,
    _weapon_barrel_prediction_type_instant = 2
};

enum e_weapon_barrel_firing_noise
{
    _weapon_barrel_firing_noise_silent = 0,
    _weapon_barrel_firing_noise_medium = 1,
    _weapon_barrel_firing_noise_loud = 2,
    _weapon_barrel_firing_noise_shout = 3,
    _weapon_barrel_firing_noise_quiet = 4
};

enum e_weapon_barrel_distribution_function
{
    _weapon_barrel_distribution_function_point = 0,
    _weapon_barrel_distribution_function_horizontal_fan = 1
};

enum e_weapon_barrel_angle_change_function
{
    _weapon_barrel_angle_change_function_linear = 0,
    _weapon_barrel_angle_change_function_early = 1,
    _weapon_barrel_angle_change_function_very_early = 2,
    _weapon_barrel_angle_change_function_late = 3,
    _weapon_barrel_angle_change_function_very_late = 4,
    _weapon_barrel_angle_change_function_cosine = 5,
    _weapon_barrel_angle_change_function_one = 6,
    _weapon_barrel_angle_change_function_zero = 7
};


/* ---------- structures */

struct s_weapon_flags;
struct s_damage_reporting_type;
struct s_unit_target_tracking_block_tracking_type;
struct s_unit_target_tracking_block;
struct s_weapon_first_person_block;
struct s_game_object_predicted_resource;
struct s_weapon_magazine_magazine_equipment_block;
struct s_weapon_magazine;
struct s_weapon_trigger;
struct s_barrel_flags;
struct s_weapon_barrel_firing_penalty_function_block;
struct s_weapon_barrel_firing_crouched_penalty_function_block;
struct s_weapon_barrel_moving_penalty_function_block;
struct s_weapon_barrel_turning_penalty_function_block;
struct s_weapon_barrel_dual_firing_penalty_function_block;
struct s_weapon_barrel_dual_firing_crouched_penalty_function_block;
struct s_weapon_barrel_dual_moving_penalty_function_block;
struct s_weapon_barrel_dual_turning_penalty_function_block;
struct s_weapon_first_person_offset_block;
struct s_weapon_barrel_firing_effect;
struct s_weapon_barrel;
struct s_weapon;

struct s_weapon : s_item
{
    s_weapon_flags weapon_flags;
    ulong more_flags;
    string_id unknown8;
    c_enum<e_weapon_secondary_trigger_mode, short> secondary_trigger_mode;
    short maximum_alternate_shots_loaded;
    float turn_on_time;
    float ready_time;
    s_tag_reference ready_effect;
    s_tag_reference ready_damage_effect;
    float heat_recovery_threshold;
    float overheated_threshold;
    float heat_detonation_threshold;
    float heat_detonation_fraction;
    float heat_loss_per_second;
    float heat_illumination;
    float overheated_heat_loss_per_second;
    s_tag_reference overheated;
    s_tag_reference overheated_damage_effect;
    s_tag_reference detonation;
    s_tag_reference detonation_damage_effect2;
    s_tag_reference player_melee_damage;
    s_tag_reference player_melee_response;
    real damage_pyramid_angles_y;
    real damage_pyramid_angles_p;
    float damage_pyramid_depth;
    s_tag_reference first_hit_damage;
    s_tag_reference first_hit_response;
    s_tag_reference secondd_hit_damage;
    s_tag_reference second_hit_response;
    s_tag_reference third_hit_damage;
    s_tag_reference third_hit_response;
    s_tag_reference lunge_melee_damage;
    s_tag_reference lunge_melee_response;
    s_tag_reference gun_gun_clang_damage;
    s_tag_reference gun_gun_clang_response;
    s_tag_reference gun_sword_clang_damage;
    s_tag_reference gun_sword_clang_response;
    s_tag_reference clash_effect;
    s_damage_reporting_type melee_damage_reporting_type;
    char unknown9;
    short magnification_levels;
    real_bounds magnification_bounds;
    ulong magnification_flags;
    float weapon_switch_ready_speed0_default;
    real autoaim_angle;
    float autoaim_range_long;
    float autoaim_range_short;
    float autoaim_safe_radius;
    real magnetism_angle;
    float magnetism_range_long;
    float magnetism_range_short;
    float magnetism_safe_radius;
    real deviation_angle;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    c_tag_block<s_unit_target_tracking_block> target_tracking;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    c_enum<e_weapon_movement_penalized, short> movement_penalized;
    short unknown20;
    float forwards_movement_penalty;
    float sideways_movement_penalty;
    float ai_scariness;
    float weapon_power_on_time;
    float weapon_power_off_time;
    s_tag_reference weapon_power_on_effect;
    s_tag_reference weapon_power_off_effect;
    float age_heat_recovery_penalty;
    float age_rate_of_fire_penalty;
    float age_misfire_start;
    float age_misfire_chance;
    s_tag_reference pickup_sound;
    s_tag_reference zoom_in_sound;
    s_tag_reference zoom_out_sound;
    float active_camo_ding;
    float camo_regrowth_rate;
    ulong unknown22;
    ulong unknown23;
    string_id weapon_class;
    string_id weapon_name;
    c_enum<e_weapon_multiplayer_weapon_type, short> multiplayer_weapon_type;
    c_enum<e_weapon_weapon_type, short> weapon_type;
    c_enum<e_weapon_tracking_type, short> tracking;
    short unknown_enum;
    c_enum<e_weapon_special_hud_version, long> special_hud_version;
    long special_hud_icon;
    c_tag_block<s_weapon_first_person_block> first_person;
    s_tag_reference hud_interface;
    c_tag_block<s_game_object_predicted_resource> predicted_weapon_resources;
    c_tag_block<s_weapon_magazine> magazines;
    c_tag_block<s_weapon_trigger> triggers;
    c_tag_block<s_weapon_barrel> barrels;
    float unknown25;
    float unknown26;
    float maximum_movement_acceleration;
    float maximum_movement_velocity;
    float maximum_turning_acceleration;
    float maximum_turning_velocity;
    s_tag_reference deployed_vehicle;
    s_tag_reference deployed_weapon;
    s_tag_reference age_model;
    s_tag_reference age_weapon;
    s_tag_reference aged_material_effects;
    float hammer_age_per_use;
    ulong unknown_sword_age_per_use;
    real_vector3d first_person_weapon_offset;
    real_vector2d first_person_scope_size;
    real_bounds third_person_pitch_bounds;
    float zoom_transition_time;
    float melee_weapon_delay;
    float ready_animation_duration;
    string_id weapon_holster_marker;
};
static_assert(sizeof(s_weapon) == 0x558);

struct s_weapon_barrel
{
    s_barrel_flags flags;
    real_bounds rounds_per_second;
    float acceleration_time;
    float deceleration_time;
    float barrel_spin_scale;
    float blurred_rate_of_fire;
    short_bounds shots_per_fire;
    float fire_recovery_time;
    float soft_recovery_fraction;
    short magazine_index;
    short rounds_per_shot;
    short minimum_rounds_loaded;
    short rounds_between_tracers;
    string_id optional_barrel_marker_name;
    c_enum<e_weapon_barrel_prediction_type, short> prediction_type;
    c_enum<e_weapon_barrel_firing_noise, short> firing_noise;
    float error_acceleration_time;
    float error_deceleration_time;
    real_bounds damage_error;
    real base_turning_speed;
    real_bounds dynamic_turning_speed;
    float dual_wield_error_acceleration_time;
    float dual_wield_error_deceleration_time;
    ulong unknown;
    ulong unknown2;
    real dual_wield_minimum_error;
    real_bounds dual_wield_error_angle;
    float dual_wield_damage_scale;
    c_enum<e_weapon_barrel_distribution_function, short> projectile_distribution_function;
    short projectiles_per_shot;
    real projectile_distribution_angle;
    real projectile_minimum_error;
    real_bounds projectile_error_angle;
    float reload_penalty;
    float switch_penalty;
    float zoom_penalty;
    float jump_penalty;
    c_tag_block<s_weapon_barrel_firing_penalty_function_block> firing_penalty_function;
    c_tag_block<s_weapon_barrel_firing_crouched_penalty_function_block> firing_crouched_penalty_function;
    c_tag_block<s_weapon_barrel_moving_penalty_function_block> moving_penalty_function;
    c_tag_block<s_weapon_barrel_turning_penalty_function_block> turning_penalty_function;
    float error_angle_maximum_rotation;
    c_tag_block<s_weapon_barrel_dual_firing_penalty_function_block> dual_firing_penalty_function;
    c_tag_block<s_weapon_barrel_dual_firing_crouched_penalty_function_block> dual_firing_crouched_penalty_function;
    c_tag_block<s_weapon_barrel_dual_moving_penalty_function_block> dual_moving_penalty_function;
    c_tag_block<s_weapon_barrel_dual_turning_penalty_function_block> dual_turning_penalty_function;
    float dual_error_angle_maximum_rotation;
    c_tag_block<s_weapon_first_person_offset_block> first_person_offsets;
    s_damage_reporting_type damage_reporting_type;
    char unknown3;
    short unknown4;
    s_tag_reference initial_projectile;
    s_tag_reference trailing_projectile;
    s_tag_reference damage_effect;
    s_tag_reference crate_projectile;
    float crate_projectile_speed;
    float ejection_port_recovery_time;
    float illumination_recovery_time;
    float heat_generated_per_round;
    float age_generated_per_round_mp;
    float age_generated_per_round_sp;
    float overload_time;
    real_bounds angle_change_per_shot;
    float angle_change_acceleration_time;
    float angle_change_deceleration_time;
    c_enum<e_weapon_barrel_angle_change_function, short> angle_change_function;
    short unknown5;
    float unknown6;
    float unknown7;
    float firing_effect_deceleration_time;
    ulong unknown8;
    float rate_of_fire_acceleration_time;
    float rate_of_fire_deceleration_time;
    ulong unknown9;
    float bloom_rate_of_decay;
    c_tag_block<s_weapon_barrel_firing_effect> firing_effects;
};
static_assert(sizeof(s_weapon_barrel) == 0x1AC);

struct s_weapon_barrel_firing_effect
{
    short shot_count_lower_bound;
    short shot_count_upper_bound;
    s_tag_reference firing_effect2;
    s_tag_reference misfire_effect;
    s_tag_reference empty_effect;
    s_tag_reference unknown_effect;
    s_tag_reference firing_response;
    s_tag_reference misfire_response;
    s_tag_reference empty_response;
    s_tag_reference unknown_response;
    s_tag_reference rider_firing_response;
    s_tag_reference rider_misfire_response;
    s_tag_reference rider_empty_response;
    s_tag_reference rider_unknown_response;
};
static_assert(sizeof(s_weapon_barrel_firing_effect) == 0xC4);

struct s_weapon_first_person_offset_block
{
    real_vector3d offset;
};
static_assert(sizeof(s_weapon_first_person_offset_block) == 0xC);

struct s_weapon_barrel_dual_turning_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_dual_turning_penalty_function_block) == 0x14);

struct s_weapon_barrel_dual_moving_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_dual_moving_penalty_function_block) == 0x14);

struct s_weapon_barrel_dual_firing_crouched_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_dual_firing_crouched_penalty_function_block) == 0x14);

struct s_weapon_barrel_dual_firing_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_dual_firing_penalty_function_block) == 0x14);

struct s_weapon_barrel_turning_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_turning_penalty_function_block) == 0x14);

struct s_weapon_barrel_moving_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_moving_penalty_function_block) == 0x14);

struct s_weapon_barrel_firing_crouched_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_firing_crouched_penalty_function_block) == 0x14);

struct s_weapon_barrel_firing_penalty_function_block
{
    s_tag_function function;
};
static_assert(sizeof(s_weapon_barrel_firing_penalty_function_block) == 0x14);

struct s_barrel_flags
{
    c_flags<e_barrel_flags, long> ;
};
static_assert(sizeof(s_barrel_flags) == 0x4);

struct s_weapon_trigger
{
    ulong flags;
    c_enum<e_weapon_trigger_button_used, short> button_used;
    c_enum<e_weapon_trigger_behavior, short> behavior;
    short primary_barrel;
    short secondary_barrel;
    c_enum<e_weapon_trigger_prediction, short> prediction;
    short unknown;
    float autofire_time;
    float autofire_throw;
    c_enum<e_weapon_trigger_secondary_action, short> secondary_action;
    c_enum<e_weapon_trigger_primary_action, short> primary_action;
    float charging_time;
    float charged_time;
    c_enum<e_weapon_trigger_overcharge_action, short> overcharge_action;
    ushort charge_flags;
    float charged_illumination;
    float spew_time;
    s_tag_reference charging_effect;
    s_tag_reference charging_damage_effect;
    s_tag_reference charging_response;
    float charging_age_degeneration;
    s_tag_reference unknown2;
    s_tag_reference unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_weapon_trigger) == 0x90);

struct s_weapon_magazine
{
    ulong flags;
    short rounds_recharged;
    short rounds_total_initial;
    short rounds_total_maximum;
    short rounds_total_loaded_maximum;
    short maximum_rounds_held;
    short unknown;
    float reload_time;
    short rounds_reloaded;
    short unknown2;
    float chamber_time;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    s_tag_reference reloading_effect;
    s_tag_reference reloading_damage_effect;
    s_tag_reference chambering_effect;
    s_tag_reference chambering_damage_effect;
    c_tag_block<s_weapon_magazine_magazine_equipment_block> magazine_equipment;
};
static_assert(sizeof(s_weapon_magazine) == 0x80);

struct s_weapon_magazine_magazine_equipment_block
{
    short rounds0_for_max;
    short unknown;
    s_tag_reference equipment;
};
static_assert(sizeof(s_weapon_magazine_magazine_equipment_block) == 0x14);

struct s_game_object_predicted_resource
{
    short type;
    short resource_index;
    s_tag_reference_short tag_index;
};
static_assert(sizeof(s_game_object_predicted_resource) == 0x8);

struct s_weapon_first_person_block
{
    s_tag_reference first_person_model;
    s_tag_reference first_person_animations;
};
static_assert(sizeof(s_weapon_first_person_block) == 0x20);

struct s_unit_target_tracking_block
{
    c_tag_block<s_unit_target_tracking_block_tracking_type> tracking_types;
    float acquire_time;
    float grace_time;
    float decay_time;
    s_tag_reference tracking_sound;
    s_tag_reference locked_sound;
};
static_assert(sizeof(s_unit_target_tracking_block) == 0x38);

struct s_unit_target_tracking_block_tracking_type
{
    string_id tracking_type2;
};
static_assert(sizeof(s_unit_target_tracking_block_tracking_type) == 0x4);

struct s_damage_reporting_type
{
    c_enum<e_damage_reporting_type, char> ;
};
static_assert(sizeof(s_damage_reporting_type) == 0x1);

struct s_weapon_flags
{
    c_flags<e_weapon_flags_new_weapon_flags, long> new_flags;
};
static_assert(sizeof(s_weapon_flags) == 0x4);

