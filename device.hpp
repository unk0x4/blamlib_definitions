/* ---------- enums */

enum e_device_device_flag_bits
{
    _device_device_flag_bits_position_loops_bit = 0,
    _device_device_flag_bits_use_multiplayer_boundary_bit = 1,
    _device_device_flag_bits_allow_interpolation_bit = 2,
    _device_device_flag_bits_allow_attached_players_bit = 3,
    _device_device_flag_bits_control_uses_parent_interact_scripts_bit = 4,
    _device_device_flag_bits_requires_line_of_sight_for_interaction_bit = 5,
    _device_device_flag_bits_only_active_when_parent_is_hostile_bit = 6,
    _device_device_flag_bits_is_targetable_bit = 7,
    _device_device_flag_bits_ignore_important_work_just_for_vs_bit = 8,
    _device_device_flag_bits_huge_device_bit = 9
};

enum e_device_lightmap_flag_bits
{
    _device_lightmap_flag_bits_do_not_use_in_lightmap_bit = 0,
    _device_lightmap_flag_bits_do_not_use_in_lightprobe_bit = 1
};


/* ---------- structures */

struct s_device;

struct s_device : s_game_object
{
    c_flags<e_device_device_flag_bits, long> device_flags;
    float power_transition_time;
    float power_acceleration_time;
    float position_transition_time;
    float position_acceleration_time;
    float depowered_position_transition_time;
    float depowered_position_acceleration_time;
    c_flags<e_device_lightmap_flag_bits, long> lightmap_flags;
    s_tag_reference open_up;
    s_tag_reference close_down;
    s_tag_reference opened;
    s_tag_reference closed;
    s_tag_reference depowered;
    s_tag_reference repowered;
    float delay_time;
    s_tag_reference delay_effect;
    float automatic_activation_radius;
};
static_assert(sizeof(s_device) == 0x1B8);

