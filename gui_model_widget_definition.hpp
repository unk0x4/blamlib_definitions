/* ---------- enums */


/* ---------- structures */

struct s_gui_definition;
struct s_tag_function;
struct s_gui_model_widget_definition_camera_refinement_zoom_data;
struct s_gui_model_widget_definition_camera_refinement;
struct s_gui_model_widget_definition_unknown_block;
struct s_gui_model_widget_definition_tex_cam;
struct s_gui_model_widget_definition;

struct s_gui_model_widget_definition
{
    ulong flags;
    s_gui_definition gui_render_block;
    c_tag_block<s_gui_model_widget_definition_camera_refinement> camera_control;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    float fov;
    ulong unknown8;
    c_tag_block<s_gui_model_widget_definition_unknown_block> zoom_function;
    ushort movement_left;
    ushort movement_right;
    ushort movement_up;
    ushort movement_down;
    ushort unknown14;
    ushort unknown15;
    ushort zoom_in;
    ushort zoom_out;
    ushort rotate_left;
    ushort rotate_right;
    ushort unknown20;
    ushort unknown21;
    c_tag_block<s_gui_model_widget_definition_tex_cam> texture_camera_sections;
};
static_assert(sizeof(s_gui_model_widget_definition) == 0x84);

struct s_gui_model_widget_definition_tex_cam
{
    string_id name;
    real_bounds x_bounds;
    real_bounds y_bounds;
};
static_assert(sizeof(s_gui_model_widget_definition_tex_cam) == 0x14);

struct s_gui_model_widget_definition_unknown_block
{
    s_tag_function unknown;
};
static_assert(sizeof(s_gui_model_widget_definition_unknown_block) == 0x14);

struct s_gui_model_widget_definition_camera_refinement
{
    string_id biped2;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    float unknown4;
    float biped_angle;
    ulong unknown6;
    real_point3d base_offset_new;
    real_vector3d unknown10;
    real_vector3d unknown13;
    real_vector3d unknown16;
    real_vector3d unknown19;
    real_vector3d unknown22;
    c_tag_block<s_gui_model_widget_definition_camera_refinement_zoom_data> zoom_data1;
    ulong unknown26;
    ulong unknown27;
    real unknown28;
    ulong unknown29;
    real unknown30;
    ulong unknown31;
    ulong unknown32;
    s_tag_reference unknown33;
    ulong unknown34;
};
static_assert(sizeof(s_gui_model_widget_definition_camera_refinement) == 0xA0);

struct s_gui_model_widget_definition_camera_refinement_zoom_data
{
    s_tag_function unknown;
};
static_assert(sizeof(s_gui_model_widget_definition_camera_refinement_zoom_data) == 0x14);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_gui_definition
{
    string_id name;
    short unknown;
    short layer;
    short widescreen_y_min;
    short widescreen_x_min;
    short widescreen_y_max;
    short widescreen_x_max;
    short standard_y_min;
    short standard_x_min;
    short standard_y_max;
    short standard_x_max;
    s_tag_reference animation;
};
static_assert(sizeof(s_gui_definition) == 0x28);

