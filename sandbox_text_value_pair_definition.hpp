/* ---------- enums */

enum e_sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair_expected_value_type
{
    _sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair_expected_value_type_integer_index = 0,
    _sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair_expected_value_type_stringid_reference = 1,
    _sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair_expected_value_type_incremental = 2
};


/* ---------- structures */

struct s_sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair;
struct s_sandbox_text_value_pair_definition_sandbox_text_value_pair;
struct s_sandbox_text_value_pair_definition;

struct s_sandbox_text_value_pair_definition
{
    c_tag_block<s_sandbox_text_value_pair_definition_sandbox_text_value_pair> sandbox_text_value_pairs;
};
static_assert(sizeof(s_sandbox_text_value_pair_definition) == 0xC);

struct s_sandbox_text_value_pair_definition_sandbox_text_value_pair
{
    string_id parameter_name;
    c_tag_block<s_sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair> text_value_pairs;
};
static_assert(sizeof(s_sandbox_text_value_pair_definition_sandbox_text_value_pair) == 0x10);

struct s_sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair
{
    uchar flags;
    c_enum<e_sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair_expected_value_type, char> expected_value_type;
    short unknown;
    long int_value;
    string_id ref_name;
    string_id name;
    string_id description;
};
static_assert(sizeof(s_sandbox_text_value_pair_definition_sandbox_text_value_pair_text_value_pair) == 0x14);

