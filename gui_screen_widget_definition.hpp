/* ---------- enums */

enum e_font
{
    _font_fixedsys9 = 0,
    _font_conduit16 = 1,
    _font_conduit32 = 2,
    _font_conduit23 = 3,
    _font_conduit18 = 4,
    _font_larabie_10 = 5,
    _font_pragmata_14 = 6
};

enum e_gui_screen_widget_definition_group_widget_bitmap_widget_blend_method
{
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_standard = 0,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown = 1,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown2 = 2,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_alpha = 3,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_overlay = 4,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown3 = 5,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_lighter_color = 6,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown4 = 7,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown5 = 8,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown6 = 9,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_inverted_alpha = 10,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown7 = 11,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown8 = 12,
    _gui_screen_widget_definition_group_widget_bitmap_widget_blend_method_unknown9 = 13
};


/* ---------- structures */

struct s_gui_definition;
struct s_gui_screen_widget_definition_data_source;
struct s_gui_screen_widget_definition_group_widget_list_widget_list_widget_item;
struct s_gui_screen_widget_definition_group_widget_list_widget;
struct s_gui_screen_widget_definition_group_widget_text_widget;
struct s_gui_screen_widget_definition_group_widget_bitmap_widget;
struct s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new_unknown_block;
struct s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new;
struct s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block1;
struct s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block2;
struct s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data;
struct s_gui_screen_widget_definition_group_widget_model_widget;
struct s_gui_screen_widget_definition_group_widget;
struct s_gui_screen_widget_definition_button_key_legend;
struct s_gui_screen_widget_definition;

struct s_gui_screen_widget_definition
{
    ulong flags;
    s_gui_definition gui_render_block;
    s_tag_reference strings;
    s_tag_reference parent;
    string_id default_key_legend_string;
    c_tag_block<s_gui_screen_widget_definition_data_source> data_sources;
    c_tag_block<s_gui_screen_widget_definition_group_widget> group_widgets;
    c_tag_block<s_gui_screen_widget_definition_button_key_legend> button_key_legends;
    s_tag_reference ui_sounds;
    char script_title[32];
    short script_index;
    short unknown2;
};
static_assert(sizeof(s_gui_screen_widget_definition) == 0xA8);

struct s_gui_screen_widget_definition_button_key_legend
{
    s_tag_reference legend;
};
static_assert(sizeof(s_gui_screen_widget_definition_button_key_legend) == 0x10);

struct s_gui_screen_widget_definition_group_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    c_tag_block<s_gui_screen_widget_definition_group_widget_list_widget> list_widgets;
    c_tag_block<s_gui_screen_widget_definition_group_widget_text_widget> text_widgets;
    c_tag_block<s_gui_screen_widget_definition_group_widget_bitmap_widget> bitmap_widgets;
    c_tag_block<s_gui_screen_widget_definition_group_widget_model_widget> model_widgets;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget) == 0x6C);

struct s_gui_screen_widget_definition_group_widget_model_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    c_tag_block<s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new> camera_refinements_new;
    s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data model_data;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_model_widget) == 0x94);

struct s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data
{
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    c_tag_block<s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block1> unknown11;
    short unknown12;
    short unknown13;
    short unknown14;
    short unknown15;
    short unknown16;
    short unknown17;
    short unknown18;
    short unknown19;
    short unknown20;
    short unknown21;
    short unknown22;
    short unknown23;
    c_tag_block<s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block2> unknown24;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data) == 0x4C);

struct s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block2
{
    string_id unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block2) == 0x14);

struct s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block1
{
    s_tag_data unknown;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_model_widget_model_widget_data_unknown_block1) == 0x14);

struct s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new
{
    string_id biped;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
    ulong unknown23;
    ulong unknown24;
    c_tag_block<s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new_unknown_block> unknown25;
    ulong unknown26;
    ulong unknown27;
    real unknown28;
    ulong unknown29;
    real unknown30;
    ulong unknown31;
    ulong unknown32;
    s_tag_reference unknown33;
    ulong unknown34;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new) == 0xA0);

struct s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new_unknown_block
{
    s_tag_data unknown;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_model_widget_camera_refinement_new_unknown_block) == 0x14);

struct s_gui_screen_widget_definition_group_widget_bitmap_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    s_tag_reference bitmap;
    s_tag_reference unknown2;
    c_enum<e_gui_screen_widget_definition_group_widget_bitmap_widget_blend_method, short> blend_method;
    short unknown3;
    short sprite_index;
    short unknown4;
    string_id data_source_name;
    string_id sprite_data_source_name;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_bitmap_widget) == 0x6C);

struct s_gui_screen_widget_definition_group_widget_text_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    string_id data_source_name;
    string_id text_string;
    string_id text_color;
    c_enum<e_font, short> text_font;
    short unknown2;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_text_widget) == 0x4C);

struct s_gui_screen_widget_definition_group_widget_list_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    string_id data_source_name;
    s_tag_reference skin;
    long unknown2;
    c_tag_block<s_gui_screen_widget_definition_group_widget_list_widget_list_widget_item> list_widget_items;
    s_tag_reference up_arrow_bitmap;
    s_tag_reference down_arrow_bitmap;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_list_widget) == 0x80);

struct s_gui_screen_widget_definition_group_widget_list_widget_list_widget_item
{
    ulong flags;
    s_gui_definition gui_render_block;
    string_id target;
};
static_assert(sizeof(s_gui_screen_widget_definition_group_widget_list_widget_list_widget_item) == 0x30);

struct s_gui_screen_widget_definition_data_source
{
    s_tag_reference data_source2;
};
static_assert(sizeof(s_gui_screen_widget_definition_data_source) == 0x10);

struct s_gui_definition
{
    string_id name;
    short unknown;
    short layer;
    short widescreen_y_min;
    short widescreen_x_min;
    short widescreen_y_max;
    short widescreen_x_max;
    short standard_y_min;
    short standard_x_min;
    short standard_y_max;
    short standard_x_max;
    s_tag_reference animation;
};
static_assert(sizeof(s_gui_definition) == 0x28);

