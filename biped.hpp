/* ---------- enums */

enum e_biped_biped_flag_bits
{
    _biped_biped_flag_bits_turns_without_animating_bit = 0,
    _biped_biped_flag_bits_passes_through_other_bipeds_bit = 1,
    _biped_biped_flag_bits_immune_to_falling_damage_bit = 2,
    _biped_biped_flag_bits_rotate_while_airborne_bit = 3,
    _biped_biped_flag_bits_use_limp_body_physics_bit = 4,
    _biped_biped_flag_bits_unused1_bit = 5,
    _biped_biped_flag_bits_random_speed_increase_bit = 6,
    _biped_biped_flag_bits_unused2_bit = 7,
    _biped_biped_flag_bits_spawn_death_children_on_destroy_bit = 8,
    _biped_biped_flag_bits_stunned_by_emp_damage_bit = 9,
    _biped_biped_flag_bits_dead_physics_when_stunned_bit = 10,
    _biped_biped_flag_bits_always_ragdoll_when_dead_bit = 11,
    _biped_biped_flag_bits_snaps_turns_bit = 12,
    _biped_biped_flag_bits_sync_action_always_projects_on_ground_bit = 13,
    _biped_biped_flag_bits_orient_facing_to_movement_bit = 14,
    _biped_biped_flag_bits_aim_driven_orientation_in_stationary_turns_bit = 15,
    _biped_biped_flag_bits_use_predictive_stationary_turns_bit = 16,
    _biped_biped_flag_bits_never_ragdoll_bit = 17,
    _biped_biped_flag_bits_conservatively_ragdoll_as_fallback_bit = 18
};

enum e_biped_lock_on_flag_bits
{
    _biped_lock_on_flag_bits_locked_by_human_targeting_bit = 0,
    _biped_lock_on_flag_bits_locked_by_plasma_targeting_bit = 1,
    _biped_lock_on_flag_bits_always_locked_by_human_targeting_bit = 2
};

enum e_biped_physics_flags_halo3_odst_bits
{
    _biped_physics_flags_halo3_odst_bits_centered_at_origin_bit = 0,
    _biped_physics_flags_halo3_odst_bits_shape_spherical_bit = 1,
    _biped_physics_flags_halo3_odst_bits_use_player_physics_bit = 2,
    _biped_physics_flags_halo3_odst_bits_unknown_bit = 3,
    _biped_physics_flags_halo3_odst_bits_climb_any_surface_bit = 4,
    _biped_physics_flags_halo3_odst_bits_flying_bit = 5,
    _biped_physics_flags_halo3_odst_bits_not_physical_bit = 6,
    _biped_physics_flags_halo3_odst_bits_dead_character_collision_group_bit = 7,
    _biped_physics_flags_halo3_odst_bits_suppress_ground_planes_on_bipeds_bit = 8,
    _biped_physics_flags_halo3_odst_bits_physical_ragdoll_bit = 9,
    _biped_physics_flags_halo3_odst_bits_do_not_resize_dead_spheres_bit = 10,
    _biped_physics_flags_halo3_odst_bits_multiple_shapes_bit = 11,
    _biped_physics_flags_halo3_odst_bits_extreme_slip_surface_bit = 12,
    _biped_physics_flags_halo3_odst_bits_slips_off_movers_bit = 13,
    _biped_physics_flags_halo3_odst_bits_aligns_with_ground_bit = 14
};

enum e_blam_shape_type
{
    _blam_shape_type_sphere = 0,
    _blam_shape_type_pill = 1,
    _blam_shape_type_box = 2,
    _blam_shape_type_triangle = 3,
    _blam_shape_type_polyhedron = 4,
    _blam_shape_type_multi_sphere = 5,
    _blam_shape_type_triangle_mesh = 6,
    _blam_shape_type_compound_shape = 7,
    _blam_shape_type_unused0 = 8,
    _blam_shape_type_unused1 = 9,
    _blam_shape_type_unused2 = 10,
    _blam_shape_type_unused3 = 11,
    _blam_shape_type_unused4 = 12,
    _blam_shape_type_unused5 = 13,
    _blam_shape_type_list = 14,
    _blam_shape_type_mopp = 15
};


/* ---------- structures */

struct s_biped_camera_height_block;
struct s_biped_movement_gate_block;
struct s_biped_physics_flags;
struct s_physics_model_havok_shape_base;
struct s_physics_model_shape;
struct s_physics_model_havok_shape_reference;
struct s_physics_model_sphere;
struct s_physics_model_pill;
struct s_biped_contact_point;
struct s_biped;

struct s_biped : s_unit
{
    real moving_turning_speed;
    c_flags<e_biped_biped_flag_bits, long> biped_flags;
    real stationary_turning_speed;
    ulong unknown20;
    string_id unknown21;
    float jump_velocity;
    float maximum_soft_landing_time;
    float minimum_hard_landing_time;
    float minimum_soft_landing_velocity;
    float minimum_hard_landing_velocity;
    float maximum_hard_landing_velocity;
    float death_hard_landing_velocity;
    float stun_duration;
    float stationary_standing_camera_height;
    float moving_standing_camera_height;
    float stationary_crouching_camera_height;
    float moving_crouching_camera_height;
    float crouch_transition_time;
    s_tag_function crouching_camera_function;
    c_tag_block<s_biped_camera_height_block> camera_heights;
    real camera_interpolation_start;
    real camera_interpolation_end;
    real_vector3d camera_offset;
    float root_offset_camera_scale;
    float autoaim_width;
    c_flags<e_biped_lock_on_flag_bits, long> lockon_flags;
    float lockon_distance;
    short physics_control_node_index;
    short unknown29;
    ulong unknown30;
    ulong unknown31;
    ulong unknown32;
    short pelvis_node_index;
    short head_node_index;
    ulong unknown33;
    float headshot_acceleration_scale;
    s_tag_reference area_damage_effect;
    c_tag_block<s_biped_movement_gate_block> movement_gates;
    c_tag_block<s_biped_movement_gate_block> movement_gates_crouching;
    ulong unknown36;
    ulong unknown37;
    ulong unknown38;
    ulong unknown39;
    ulong unknown40;
    ulong unknown41;
    s_biped_physics_flags physics_flags;
    float height_standing;
    float height_crouching;
    float radius;
    float mass;
    string_id living_material_name;
    string_id dead_material_name;
    short living_material_global_index;
    short dead_material_global_index;
    c_tag_block<s_physics_model_sphere> dead_sphere_shapes;
    c_tag_block<s_physics_model_pill> pill_shapes;
    c_tag_block<s_physics_model_sphere> sphere_shapes;
    real maximum_slope_angle;
    real downhill_falloff_angle;
    real downhill_cutoff_angle;
    real uphill_falloff_angle;
    real uphill_cutoff_angle;
    float downhill_velocity_scale;
    float uphill_velocity_scale;
    float unknown42;
    float unknown43;
    float unknown44;
    float unknown45;
    float unknown46;
    float unknown47;
    float unknown48;
    float unknown49;
    float unknown50;
    float unknown51;
    real bank_angle;
    float bank_apply_time;
    float bank_decay_time;
    float pitch_ratio;
    float maximum_velocity;
    float maximum_sidestep_velocity;
    float acceleration;
    float deceleration;
    real angular_velocity_maximum;
    real angular_acceleration_maximum;
    float crouch_velocity_modifier;
    c_tag_block<s_biped_contact_point> contact_points;
    s_tag_reference reanimation_character;
    s_tag_reference transformation_muffin;
    s_tag_reference death_spawn_character;
    short death_spawn_count;
    short unknown52;
    ulong unknown53;
    float unknown54;
    float unknown55;
    float unknown56;
    float unknown57;
    real unknown58;
    real unknown59;
    ulong unknown60;
    ulong unknown61;
    float unknown62;
    float unknown63;
    float unknown64;
    float unknown65;
    float unknown66;
    float unknown67;
    float unknown68;
    float unknown69;
    real unknown70;
    real unknown71;
    float unknown72;
    float unknown73;
    float unknown74;
    float unknown75;
    float unknown76;
    ulong unknown77;
};
static_assert(sizeof(s_biped) == 0x628);

struct s_biped_contact_point
{
    string_id marker_name;
};
static_assert(sizeof(s_biped_contact_point) == 0x4);

struct s_physics_model_pill : s_physics_model_shape
{
    real_vector3d bottom;
    float bottom_radius;
    real_vector3d top;
    float top_radius;
};
static_assert(sizeof(s_physics_model_pill) == 0x60);

struct s_physics_model_sphere : s_physics_model_shape
{
    s_physics_model_havok_shape_base convex_base;
    ulong field_pointer_skip;
    s_physics_model_havok_shape_reference shape_reference;
    real_vector3d translation;
    float translation_radius;
};
static_assert(sizeof(s_physics_model_sphere) == 0x70);

struct s_physics_model_havok_shape_reference
{
    c_enum<e_blam_shape_type, short> shapetype;
    short shape_index;
    ulong child_shape_size;
};
static_assert(sizeof(s_physics_model_havok_shape_reference) == 0x8);

struct s_physics_model_shape
{
    string_id name;
    short material_index;
    short runtime_material_type;
    float relative_mass_scale;
    float friction;
    float restitution;
    float volume;
    float mass;
    short mass_distribution_index;
    char phantom_index;
    char proxy_collision_group;
    s_physics_model_havok_shape_base shape_base;
    byte pad[12];
};
static_assert(sizeof(s_physics_model_shape) == 0x40);

struct s_physics_model_havok_shape_base
{
    long field_pointer_skip;
    short size;
    short count;
    long offset;
    long user_data;
    float radius;
};
static_assert(sizeof(s_physics_model_havok_shape_base) == 0x14);

struct s_biped_physics_flags
{
    c_flags<e_biped_physics_flags_halo3_odst_bits, long> halo3_odst;
};
static_assert(sizeof(s_biped_physics_flags) == 0x4);

struct s_biped_movement_gate_block
{
    float period;
    float z_offset;
    float constant_z_offset;
    float y_offset;
    s_tag_function default_function;
};
static_assert(sizeof(s_biped_movement_gate_block) == 0x24);

struct s_biped_camera_height_block
{
    string_id class;
    float standing_height_fraction;
    float crouching_height_fraction;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
};
static_assert(sizeof(s_biped_camera_height_block) == 0x18);

