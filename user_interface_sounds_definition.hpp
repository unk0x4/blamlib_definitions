/* ---------- enums */


/* ---------- structures */

struct s_user_interface_sounds_definition_atlas_sound;
struct s_user_interface_sounds_definition;

struct s_user_interface_sounds_definition
{
    s_tag_reference error;
    s_tag_reference vertical_navigation;
    s_tag_reference horizontal_navigation;
    s_tag_reference a_button;
    s_tag_reference b_button;
    s_tag_reference x_button;
    s_tag_reference y_button;
    s_tag_reference start_button;
    s_tag_reference back_button;
    s_tag_reference left_bumper;
    s_tag_reference right_bumper;
    s_tag_reference left_trigger;
    s_tag_reference right_trigger;
    s_tag_reference timer_sound;
    s_tag_reference timer_sound_zero;
    s_tag_reference alt_timer_sound;
    s_tag_reference second_alt_timer_sound;
    s_tag_reference matchmaking_advance_sound;
    s_tag_reference rank_up;
    s_tag_reference matchmaking_party_up_sound;
    c_tag_block<s_user_interface_sounds_definition_atlas_sound> atlas_sounds;
};
static_assert(sizeof(s_user_interface_sounds_definition) == 0x14C);

struct s_user_interface_sounds_definition_atlas_sound
{
    string_id name;
    s_tag_reference sound;
};
static_assert(sizeof(s_user_interface_sounds_definition_atlas_sound) == 0x14);

