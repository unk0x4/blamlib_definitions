/* ---------- enums */


/* ---------- structures */

struct s_sound_ui_sounds_ui_sound;
struct s_sound_ui_sounds;

struct s_sound_ui_sounds
{
    c_tag_block<s_sound_ui_sounds_ui_sound> ui_sounds;
    ulong unknown;
};
static_assert(sizeof(s_sound_ui_sounds) == 0x10);

struct s_sound_ui_sounds_ui_sound
{
    s_tag_reference sound;
};
static_assert(sizeof(s_sound_ui_sounds_ui_sound) == 0x10);

