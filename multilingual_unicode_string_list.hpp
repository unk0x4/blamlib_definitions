/* ---------- enums */


/* ---------- structures */

struct s_localized_string;
struct s_multilingual_unicode_string_list;

struct s_multilingual_unicode_string_list
{
    c_tag_block<s_localized_string> strings;
    s_tag_data data;
    u_int16 offset_counts[24];
};
static_assert(sizeof(s_multilingual_unicode_string_list) == 0x50);

struct s_localized_string
{
    string_id string_id;
    char string_id_str[32];
    int32 offsets[12];
};
static_assert(sizeof(s_localized_string) == 0x54);

