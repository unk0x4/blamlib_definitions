/* ---------- enums */

enum e_chud_definition_hud_widget_special_hud_type
{
    _chud_definition_hud_widget_special_hud_type_unspecial = 0,
    _chud_definition_hud_widget_special_hud_type_ammo = 1,
    _chud_definition_hud_widget_special_hud_type_crosshair_and_scope = 2,
    _chud_definition_hud_widget_special_hud_type_unit_shield_meter = 3,
    _chud_definition_hud_widget_special_hud_type_grenades = 4,
    _chud_definition_hud_widget_special_hud_type_gametype = 5,
    _chud_definition_hud_widget_special_hud_type_motion_sensor = 6,
    _chud_definition_hud_widget_special_hud_type_spike_grenade = 7,
    _chud_definition_hud_widget_special_hud_type_firebomb_grenade = 8,
    _chud_definition_hud_widget_special_hud_type_compass = 9,
    _chud_definition_hud_widget_special_hud_type_stamina = 10,
    _chud_definition_hud_widget_special_hud_type_energy_meter = 11,
    _chud_definition_hud_widget_special_hud_type_consumable = 12
};

enum e_chud_definition_hud_widget_state_datum_engine
{
    _chud_definition_hud_widget_state_datum_engine_campaign_solo_bit = 0,
    _chud_definition_hud_widget_state_datum_engine_campaign_coop_bit = 1,
    _chud_definition_hud_widget_state_datum_engine_survival_bit = 2,
    _chud_definition_hud_widget_state_datum_engine_ff_agame_bit = 3,
    _chud_definition_hud_widget_state_datum_engine_teamgame_bit = 4,
    _chud_definition_hud_widget_state_datum_engine_ctf_bit = 5,
    _chud_definition_hud_widget_state_datum_engine_slayer_bit = 6,
    _chud_definition_hud_widget_state_datum_engine_oddball_bit = 7,
    _chud_definition_hud_widget_state_datum_engine_koth_bit = 8,
    _chud_definition_hud_widget_state_datum_engine_juggernaut_bit = 9,
    _chud_definition_hud_widget_state_datum_engine_territories_bit = 10,
    _chud_definition_hud_widget_state_datum_engine_assault_bit = 11,
    _chud_definition_hud_widget_state_datum_engine_vip_bit = 12,
    _chud_definition_hud_widget_state_datum_engine_infection_bit = 13,
    _chud_definition_hud_widget_state_datum_engine_editor_bit = 14,
    _chud_definition_hud_widget_state_datum_engine_theater_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_player_type
{
    _chud_definition_hud_widget_state_datum_player_type_spartan_bit = 0,
    _chud_definition_hud_widget_state_datum_player_type_elite_bit = 1,
    _chud_definition_hud_widget_state_datum_player_type_monitor_bit = 2
};

enum e_chud_definition_hud_widget_state_datum_multiplayer
{
    _chud_definition_hud_widget_state_datum_multiplayer_offense_team_bit = 0,
    _chud_definition_hud_widget_state_datum_multiplayer_defense_team_bit = 1,
    _chud_definition_hud_widget_state_datum_multiplayer_normal_team_bit = 2,
    _chud_definition_hud_widget_state_datum_multiplayer_special_team_bit = 3,
    _chud_definition_hud_widget_state_datum_multiplayer_extraspecialteam_bit = 4,
    _chud_definition_hud_widget_state_datum_multiplayer_no_microphone_bit = 5,
    _chud_definition_hud_widget_state_datum_multiplayer_talking_disabled_bit = 6,
    _chud_definition_hud_widget_state_datum_multiplayer_tap_to_talk_bit = 7,
    _chud_definition_hud_widget_state_datum_multiplayer_talking_enabled_bit = 8,
    _chud_definition_hud_widget_state_datum_multiplayer_not_talking_bit = 9,
    _chud_definition_hud_widget_state_datum_multiplayer_talking_bit = 10
};

enum e_chud_definition_hud_widget_state_datum_resolution
{
    _chud_definition_hud_widget_state_datum_resolution_wide_full_bit = 0,
    _chud_definition_hud_widget_state_datum_resolution_wide_half_bit = 1,
    _chud_definition_hud_widget_state_datum_resolution_native_full_bit = 2,
    _chud_definition_hud_widget_state_datum_resolution_standard_full_bit = 3,
    _chud_definition_hud_widget_state_datum_resolution_wide_quarter_bit = 4,
    _chud_definition_hud_widget_state_datum_resolution_standard_half_bit = 5,
    _chud_definition_hud_widget_state_datum_resolution_native_quarter_bit = 6,
    _chud_definition_hud_widget_state_datum_resolution_standard_quarter_bit = 7
};

enum e_chud_definition_hud_widget_state_datum_multiplayer_events
{
    _chud_definition_hud_widget_state_datum_multiplayer_events_has_friends_bit = 0,
    _chud_definition_hud_widget_state_datum_multiplayer_events_has_enemies_bit = 1,
    _chud_definition_hud_widget_state_datum_multiplayer_events_variant_name_valid_bit = 2,
    _chud_definition_hud_widget_state_datum_multiplayer_events_someone_is_talking_bit = 3,
    _chud_definition_hud_widget_state_datum_multiplayer_events_is_arming_bit = 4,
    _chud_definition_hud_widget_state_datum_multiplayer_events_time_enabled_bit = 5,
    _chud_definition_hud_widget_state_datum_multiplayer_events_friends_have_x_bit = 6,
    _chud_definition_hud_widget_state_datum_multiplayer_events_enemies_have_x_bit = 7,
    _chud_definition_hud_widget_state_datum_multiplayer_events_friends_are_x_bit = 8,
    _chud_definition_hud_widget_state_datum_multiplayer_events_enemies_are_x_bit = 9,
    _chud_definition_hud_widget_state_datum_multiplayer_events_x_is_down_bit = 10,
    _chud_definition_hud_widget_state_datum_multiplayer_events_attacker_bomb_dropped_bit = 11,
    _chud_definition_hud_widget_state_datum_multiplayer_events_attacker_bomb_picked_up_bit = 12,
    _chud_definition_hud_widget_state_datum_multiplayer_events_defender_team_is_dead_bit = 13,
    _chud_definition_hud_widget_state_datum_multiplayer_events_attacker_team_is_dead_bit = 14,
    _chud_definition_hud_widget_state_datum_multiplayer_events_summary_enabled_bit = 15,
    _chud_definition_hud_widget_state_datum_multiplayer_events_unknown16_bit = 16,
    _chud_definition_hud_widget_state_datum_multiplayer_events_net_debug_enabled_bit = 17
};

enum e_chud_definition_hud_widget_state_datum_unit_base
{
    _chud_definition_hud_widget_state_datum_unit_base_texture_cam_enabled_bit = 0,
    _chud_definition_hud_widget_state_datum_unit_base_binoculars_targeted_bit = 1,
    _chud_definition_hud_widget_state_datum_unit_base_bit2_bit = 2,
    _chud_definition_hud_widget_state_datum_unit_base_bit3_bit = 3,
    _chud_definition_hud_widget_state_datum_unit_base_training_prompt_bit = 4,
    _chud_definition_hud_widget_state_datum_unit_base_objective_prompt_bit = 5,
    _chud_definition_hud_widget_state_datum_unit_base_survival_state_bit = 6,
    _chud_definition_hud_widget_state_datum_unit_base_bit7_bit = 7,
    _chud_definition_hud_widget_state_datum_unit_base_achievement1_bit = 8,
    _chud_definition_hud_widget_state_datum_unit_base_achievement2_bit = 9,
    _chud_definition_hud_widget_state_datum_unit_base_achievement3_bit = 10,
    _chud_definition_hud_widget_state_datum_unit_base_achievement4_bit = 11,
    _chud_definition_hud_widget_state_datum_unit_base_achievement5_bit = 12,
    _chud_definition_hud_widget_state_datum_unit_base_game_time_unknown_bit = 13,
    _chud_definition_hud_widget_state_datum_unit_base_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_unit_base_arg_enabled_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_editor
{
    _chud_definition_hud_widget_state_datum_editor_editor_inactive_bit = 0,
    _chud_definition_hud_widget_state_datum_editor_editor_active_bit = 1,
    _chud_definition_hud_widget_state_datum_editor_editor_holding_bit = 2,
    _chud_definition_hud_widget_state_datum_editor_editor_not_allowed_bit = 3,
    _chud_definition_hud_widget_state_datum_editor_is_editor_biped_bit = 4
};

enum e_chud_definition_hud_widget_state_datum_engine_general
{
    _chud_definition_hud_widget_state_datum_engine_general_motion_tracker10_m_bit = 0,
    _chud_definition_hud_widget_state_datum_engine_general_motion_tracker25_m_bit = 1,
    _chud_definition_hud_widget_state_datum_engine_general_motion_tracker75_m_bit = 2,
    _chud_definition_hud_widget_state_datum_engine_general_motion_tracker150_m_bit = 3,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_debug_enabled_bit = 4,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_player2_exists_bit = 5,
    _chud_definition_hud_widget_state_datum_engine_general_bit6_bit = 6,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_player3_exists_bit = 7,
    _chud_definition_hud_widget_state_datum_engine_general_bit8_bit = 8,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_player4_exists_bit = 9,
    _chud_definition_hud_widget_state_datum_engine_general_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_score_added_bit = 11,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_multikill_bit = 12,
    _chud_definition_hud_widget_state_datum_engine_general_metagame_score_removed_bit = 13,
    _chud_definition_hud_widget_state_datum_engine_general_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_engine_general_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_skulls
{
    _chud_definition_hud_widget_state_datum_skulls_iron_skull_enabled_bit = 0,
    _chud_definition_hud_widget_state_datum_skulls_black_eye_skull_enabled_bit = 1,
    _chud_definition_hud_widget_state_datum_skulls_tough_luck_skull_enabled_bit = 2,
    _chud_definition_hud_widget_state_datum_skulls_catch_skull_enabled_bit = 3,
    _chud_definition_hud_widget_state_datum_skulls_cloud_skull_enabled_bit = 4,
    _chud_definition_hud_widget_state_datum_skulls_famine_skull_enabled_bit = 5,
    _chud_definition_hud_widget_state_datum_skulls_thunderstorm_skull_enabled_bit = 6,
    _chud_definition_hud_widget_state_datum_skulls_tilt_skull_enabled_bit = 7,
    _chud_definition_hud_widget_state_datum_skulls_mythic_skull_enabled_bit = 8,
    _chud_definition_hud_widget_state_datum_skulls_assassins_skull_enabled_bit = 9,
    _chud_definition_hud_widget_state_datum_skulls_blind_skull_enabled_bit = 10,
    _chud_definition_hud_widget_state_datum_skulls_cowbell_skull_enabled_bit = 11,
    _chud_definition_hud_widget_state_datum_skulls_grunt_birthday_party_skull_enabled_bit = 12,
    _chud_definition_hud_widget_state_datum_skulls_iwhbyd_skull_enabled_bit = 13,
    _chud_definition_hud_widget_state_datum_skulls_third_person_skull_enabled_bit = 14,
    _chud_definition_hud_widget_state_datum_skulls_directors_cut_skull_enabled_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_survival_rounds
{
    _chud_definition_hud_widget_state_datum_survival_rounds_round0_bit = 0,
    _chud_definition_hud_widget_state_datum_survival_rounds_round1_bit = 1,
    _chud_definition_hud_widget_state_datum_survival_rounds_round2_bit = 2,
    _chud_definition_hud_widget_state_datum_survival_rounds_round3_bit = 3,
    _chud_definition_hud_widget_state_datum_survival_rounds_round4_bit = 4,
    _chud_definition_hud_widget_state_datum_survival_rounds_round5_bit = 5,
    _chud_definition_hud_widget_state_datum_survival_rounds_bonus_round_bit = 6
};

enum e_chud_definition_hud_widget_state_datum_survival_waves
{
    _chud_definition_hud_widget_state_datum_survival_waves_wave1_bit = 0,
    _chud_definition_hud_widget_state_datum_survival_waves_wave2_bit = 1,
    _chud_definition_hud_widget_state_datum_survival_waves_wave3_bit = 2,
    _chud_definition_hud_widget_state_datum_survival_waves_wave4_bit = 3,
    _chud_definition_hud_widget_state_datum_survival_waves_wave5_bit = 4,
    _chud_definition_hud_widget_state_datum_survival_waves_wave6_bit = 5,
    _chud_definition_hud_widget_state_datum_survival_waves_wave7_bit = 6,
    _chud_definition_hud_widget_state_datum_survival_waves_wave8_bit = 7,
    _chud_definition_hud_widget_state_datum_survival_waves_wave9_bit = 8,
    _chud_definition_hud_widget_state_datum_survival_waves_wave10_bit = 9,
    _chud_definition_hud_widget_state_datum_survival_waves_wave11_bit = 10,
    _chud_definition_hud_widget_state_datum_survival_waves_wave12_bit = 11,
    _chud_definition_hud_widget_state_datum_survival_waves_wave13_bit = 12,
    _chud_definition_hud_widget_state_datum_survival_waves_wave14_bit = 13,
    _chud_definition_hud_widget_state_datum_survival_waves_wave15_bit = 14,
    _chud_definition_hud_widget_state_datum_survival_waves_wave16_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_survival_lives
{
    _chud_definition_hud_widget_state_datum_survival_lives_lives0_bit = 0,
    _chud_definition_hud_widget_state_datum_survival_lives_lives1_bit = 1,
    _chud_definition_hud_widget_state_datum_survival_lives_lives2_bit = 2,
    _chud_definition_hud_widget_state_datum_survival_lives_lives3_bit = 3,
    _chud_definition_hud_widget_state_datum_survival_lives_lives4_bit = 4,
    _chud_definition_hud_widget_state_datum_survival_lives_lives5_bit = 5,
    _chud_definition_hud_widget_state_datum_survival_lives_lives6_bit = 6,
    _chud_definition_hud_widget_state_datum_survival_lives_lives7_bit = 7,
    _chud_definition_hud_widget_state_datum_survival_lives_lives8_bit = 8,
    _chud_definition_hud_widget_state_datum_survival_lives_lives9_bit = 9,
    _chud_definition_hud_widget_state_datum_survival_lives_lives10_bit = 10,
    _chud_definition_hud_widget_state_datum_survival_lives_lives11_bit = 11,
    _chud_definition_hud_widget_state_datum_survival_lives_lives12_bit = 12,
    _chud_definition_hud_widget_state_datum_survival_lives_lives13_bit = 13,
    _chud_definition_hud_widget_state_datum_survival_lives_lives14_bit = 14,
    _chud_definition_hud_widget_state_datum_survival_lives_lives15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_survival_difficulty
{
    _chud_definition_hud_widget_state_datum_survival_difficulty_easy_bit = 0,
    _chud_definition_hud_widget_state_datum_survival_difficulty_normal_bit = 1,
    _chud_definition_hud_widget_state_datum_survival_difficulty_heroic_bit = 2,
    _chud_definition_hud_widget_state_datum_survival_difficulty_legendary_bit = 3
};

enum e_chud_definition_hud_widget_state_datum_general_kudos
{
    _chud_definition_hud_widget_state_datum_general_kudos_pickup_frag_grenades_bit = 0,
    _chud_definition_hud_widget_state_datum_general_kudos_pickup_plasma_grenades_bit = 1,
    _chud_definition_hud_widget_state_datum_general_kudos_pickup_spike_grenades_bit = 2,
    _chud_definition_hud_widget_state_datum_general_kudos_pickup_fire_grenades_bit = 3,
    _chud_definition_hud_widget_state_datum_general_kudos_bit4_bit = 4,
    _chud_definition_hud_widget_state_datum_general_kudos_lives_added_bit = 5,
    _chud_definition_hud_widget_state_datum_general_kudos_consumable1_unknown_bit = 6,
    _chud_definition_hud_widget_state_datum_general_kudos_consumable2_unknown_bit = 7,
    _chud_definition_hud_widget_state_datum_general_kudos_consumable3_unknown_bit = 8,
    _chud_definition_hud_widget_state_datum_general_kudos_consumable4_unknown_bit = 9,
    _chud_definition_hud_widget_state_datum_general_kudos_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_general_kudos_bit11_bit = 11,
    _chud_definition_hud_widget_state_datum_general_kudos_hit_marker_low_bit = 12,
    _chud_definition_hud_widget_state_datum_general_kudos_hit_marker_medium_bit = 13,
    _chud_definition_hud_widget_state_datum_general_kudos_hit_marker_high_bit = 14,
    _chud_definition_hud_widget_state_datum_general_kudos_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_unit_zoom
{
    _chud_definition_hud_widget_state_datum_unit_zoom_binoculars_enabled_bit = 0,
    _chud_definition_hud_widget_state_datum_unit_zoom_unit_is_zoomed_level1_bit = 1,
    _chud_definition_hud_widget_state_datum_unit_zoom_unit_is_zoomed_level2_bit = 2,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit3_bit = 3,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit4_bit = 4,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit5_bit = 5,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit6_bit = 6,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit7_bit = 7,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit8_bit = 8,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit9_bit = 9,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit11_bit = 11,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit12_bit = 12,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit13_bit = 13,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_unit_zoom_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_unit_inventory
{
    _chud_definition_hud_widget_state_datum_unit_inventory_none_bit = 0,
    _chud_definition_hud_widget_state_datum_unit_inventory_is_single_wielding_bit = 1,
    _chud_definition_hud_widget_state_datum_unit_inventory_is_dual_wielding_bit = 2,
    _chud_definition_hud_widget_state_datum_unit_inventory_has_support_weapon_bit = 3,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit4_bit = 4,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit5_bit = 5,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit6_bit = 6,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit7_bit = 7,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit8_bit = 8,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit9_bit = 9,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit11_bit = 11,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit12_bit = 12,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit13_bit = 13,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_unit_inventory_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_unit_general
{
    _chud_definition_hud_widget_state_datum_unit_general_motion_tracker_enabled_bit = 0,
    _chud_definition_hud_widget_state_datum_unit_general_motion_tracker_disabled_bit = 1,
    _chud_definition_hud_widget_state_datum_unit_general_selected_frag_grenades_bit = 2,
    _chud_definition_hud_widget_state_datum_unit_general_selected_plasma_grenades_bit = 3,
    _chud_definition_hud_widget_state_datum_unit_general_selected_spike_grenades_bit = 4,
    _chud_definition_hud_widget_state_datum_unit_general_selected_fire_grenades_bit = 5,
    _chud_definition_hud_widget_state_datum_unit_general_binoculars_active_bit = 6,
    _chud_definition_hud_widget_state_datum_unit_general_binoculars_not_active_bit = 7,
    _chud_definition_hud_widget_state_datum_unit_general_third_person_camera_bit = 8,
    _chud_definition_hud_widget_state_datum_unit_general_first_person_camera_bit = 9,
    _chud_definition_hud_widget_state_datum_unit_general_is_speaking_bit = 10,
    _chud_definition_hud_widget_state_datum_unit_general_is_tapping_to_talk_bit = 11,
    _chud_definition_hud_widget_state_datum_unit_general_has_overshield_level1_bit = 12,
    _chud_definition_hud_widget_state_datum_unit_general_has_overshield_level2_bit = 13,
    _chud_definition_hud_widget_state_datum_unit_general_has_overshield_level3_bit = 14,
    _chud_definition_hud_widget_state_datum_unit_general_has_shields_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_weapon_kudos
{
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit0_bit = 0,
    _chud_definition_hud_widget_state_datum_weapon_kudos_pickup_ammo_bit = 1,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit2_bit = 2,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit3_bit = 3,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit4_bit = 4,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit5_bit = 5,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit6_bit = 6,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit7_bit = 7,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit8_bit = 8,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit9_bit = 9,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit11_bit = 11,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit12_bit = 12,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit13_bit = 13,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_weapon_kudos_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_weapon_status
{
    _chud_definition_hud_widget_state_datum_weapon_status_source_is_primary_weapon_bit = 0,
    _chud_definition_hud_widget_state_datum_weapon_status_source_is_dual_weapon_bit = 1,
    _chud_definition_hud_widget_state_datum_weapon_status_source_is_backpacked_bit = 2,
    _chud_definition_hud_widget_state_datum_weapon_status_bit3_bit = 3,
    _chud_definition_hud_widget_state_datum_weapon_status_bit4_bit = 4,
    _chud_definition_hud_widget_state_datum_weapon_status_bit5_bit = 5,
    _chud_definition_hud_widget_state_datum_weapon_status_bit6_bit = 6,
    _chud_definition_hud_widget_state_datum_weapon_status_bit7_bit = 7,
    _chud_definition_hud_widget_state_datum_weapon_status_bit8_bit = 8,
    _chud_definition_hud_widget_state_datum_weapon_status_bit9_bit = 9,
    _chud_definition_hud_widget_state_datum_weapon_status_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_weapon_status_bit11_bit = 11,
    _chud_definition_hud_widget_state_datum_weapon_status_bit12_bit = 12,
    _chud_definition_hud_widget_state_datum_weapon_status_bit13_bit = 13,
    _chud_definition_hud_widget_state_datum_weapon_status_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_weapon_status_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_weapon_target
{
    _chud_definition_hud_widget_state_datum_weapon_target_no_target_bit = 0,
    _chud_definition_hud_widget_state_datum_weapon_target_friendly_target_bit = 1,
    _chud_definition_hud_widget_state_datum_weapon_target_enemy_target_bit = 2,
    _chud_definition_hud_widget_state_datum_weapon_target_headshot_target_bit = 3,
    _chud_definition_hud_widget_state_datum_weapon_target_vulnerable_target_bit = 4,
    _chud_definition_hud_widget_state_datum_weapon_target_invincible_target_bit = 5,
    _chud_definition_hud_widget_state_datum_weapon_target_plasma_locked_bit = 6,
    _chud_definition_hud_widget_state_datum_weapon_target_plasma_locking_bit = 7,
    _chud_definition_hud_widget_state_datum_weapon_target_plasma_lock_available_bit = 8,
    _chud_definition_hud_widget_state_datum_weapon_target_bit9_bit = 9,
    _chud_definition_hud_widget_state_datum_weapon_target_bit10_bit = 10
};

enum e_chud_definition_hud_widget_state_datum_weapon_target_b
{
    _chud_definition_hud_widget_state_datum_weapon_target_b_human_locking_bit = 0,
    _chud_definition_hud_widget_state_datum_weapon_target_b_human_locked_bit = 1,
    _chud_definition_hud_widget_state_datum_weapon_target_b_human_lock_available_bit = 2
};

enum e_chud_definition_hud_widget_state_datum_player_special
{
    _chud_definition_hud_widget_state_datum_player_special_health_minor_damage_bit = 0,
    _chud_definition_hud_widget_state_datum_player_special_health_medium_damage_bit = 1,
    _chud_definition_hud_widget_state_datum_player_special_health_heavy_damage_bit = 2,
    _chud_definition_hud_widget_state_datum_player_special_shields_minor_damage_bit = 3,
    _chud_definition_hud_widget_state_datum_player_special_shields_medium_damage_bit = 4,
    _chud_definition_hud_widget_state_datum_player_special_shields_heavy_damage_bit = 5,
    _chud_definition_hud_widget_state_datum_player_special_has_frag_grenades_bit = 6,
    _chud_definition_hud_widget_state_datum_player_special_has_plasma_grenades_bit = 7,
    _chud_definition_hud_widget_state_datum_player_special_has_spike_grenades_bit = 8,
    _chud_definition_hud_widget_state_datum_player_special_has_fire_grenades_bit = 9,
    _chud_definition_hud_widget_state_datum_player_special_unknown1_bit = 10,
    _chud_definition_hud_widget_state_datum_player_special_unknown2_bit = 11
};

enum e_chud_definition_hud_widget_state_datum_weapon_special
{
    _chud_definition_hud_widget_state_datum_weapon_special_clip_below_cutoff_bit = 0,
    _chud_definition_hud_widget_state_datum_weapon_special_clip_empty_bit = 1,
    _chud_definition_hud_widget_state_datum_weapon_special_ammo_below_cutoff_bit = 2,
    _chud_definition_hud_widget_state_datum_weapon_special_ammo_empty_bit = 3,
    _chud_definition_hud_widget_state_datum_weapon_special_battery_below_cutoff_bit = 4,
    _chud_definition_hud_widget_state_datum_weapon_special_battery_empty_bit = 5,
    _chud_definition_hud_widget_state_datum_weapon_special_overheated_bit = 6
};

enum e_chud_definition_hud_widget_state_datum_inverse
{
    _chud_definition_hud_widget_state_datum_inverse_not_zoomed_in_bit = 0,
    _chud_definition_hud_widget_state_datum_inverse_not_armed_with_support_weapon_bit = 1,
    _chud_definition_hud_widget_state_datum_inverse_not_fully_armed_bit = 2,
    _chud_definition_hud_widget_state_datum_inverse_bit3_bit = 3,
    _chud_definition_hud_widget_state_datum_inverse_bit4_bit = 4,
    _chud_definition_hud_widget_state_datum_inverse_bit5_bit = 5,
    _chud_definition_hud_widget_state_datum_inverse_bit6_bit = 6,
    _chud_definition_hud_widget_state_datum_inverse_bit7_bit = 7,
    _chud_definition_hud_widget_state_datum_inverse_bit8_bit = 8,
    _chud_definition_hud_widget_state_datum_inverse_bit9_bit = 9,
    _chud_definition_hud_widget_state_datum_inverse_bit10_bit = 10,
    _chud_definition_hud_widget_state_datum_inverse_bit11_bit = 11,
    _chud_definition_hud_widget_state_datum_inverse_bit12_bit = 12,
    _chud_definition_hud_widget_state_datum_inverse_bit13_bit = 13,
    _chud_definition_hud_widget_state_datum_inverse_bit14_bit = 14,
    _chud_definition_hud_widget_state_datum_inverse_bit15_bit = 15
};

enum e_chud_definition_hud_widget_state_datum_consumable
{
    _chud_definition_hud_widget_state_datum_consumable_consumable1_low_bit = 0,
    _chud_definition_hud_widget_state_datum_consumable_consumable2_low_bit = 1,
    _chud_definition_hud_widget_state_datum_consumable_consumable3_low_bit = 2,
    _chud_definition_hud_widget_state_datum_consumable_consumable4_low_bit = 3,
    _chud_definition_hud_widget_state_datum_consumable_consumable1_empty_bit = 4,
    _chud_definition_hud_widget_state_datum_consumable_consumable2_empty_bit = 5,
    _chud_definition_hud_widget_state_datum_consumable_consumable3_empty_bit = 6,
    _chud_definition_hud_widget_state_datum_consumable_consumable4_empty_bit = 7,
    _chud_definition_hud_widget_state_datum_consumable_consumable1_available_bit = 8,
    _chud_definition_hud_widget_state_datum_consumable_consumable2_available_bit = 9,
    _chud_definition_hud_widget_state_datum_consumable_consumable3_available_bit = 10,
    _chud_definition_hud_widget_state_datum_consumable_consumable4_available_bit = 11,
    _chud_definition_hud_widget_state_datum_consumable_consumable1_disabled_a_bit = 12,
    _chud_definition_hud_widget_state_datum_consumable_consumable2_disabled_a_bit = 13,
    _chud_definition_hud_widget_state_datum_consumable_consumable3_disabled_a_bit = 14,
    _chud_definition_hud_widget_state_datum_consumable_consumable4_disabled_a_bit = 15,
    _chud_definition_hud_widget_state_datum_consumable_consumable1_disabled_b_bit = 16,
    _chud_definition_hud_widget_state_datum_consumable_consumable2_disabled_b_bit = 17,
    _chud_definition_hud_widget_state_datum_consumable_consumable3_disabled_b_bit = 18,
    _chud_definition_hud_widget_state_datum_consumable_consumable4_disabled_b_bit = 19,
    _chud_definition_hud_widget_state_datum_consumable_consumable1_active_bit = 20,
    _chud_definition_hud_widget_state_datum_consumable_consumable2_active_bit = 21,
    _chud_definition_hud_widget_state_datum_consumable_consumable3_active_bit = 22,
    _chud_definition_hud_widget_state_datum_consumable_consumable4_active_bit = 23
};

enum e_chud_definition_hud_widget_state_datum_energy_meter
{
    _chud_definition_hud_widget_state_datum_energy_meter_energy_meter1_full_bit = 0,
    _chud_definition_hud_widget_state_datum_energy_meter_energy_meter2_full_bit = 1,
    _chud_definition_hud_widget_state_datum_energy_meter_energy_meter3_full_bit = 2,
    _chud_definition_hud_widget_state_datum_energy_meter_energy_meter4_full_bit = 3,
    _chud_definition_hud_widget_state_datum_energy_meter_energy_meter5_full_bit = 4
};

enum e_chud_definition_hud_widget_placement_datum_anchor
{
    _chud_definition_hud_widget_placement_datum_anchor_top_left = 0,
    _chud_definition_hud_widget_placement_datum_anchor_top_right = 1,
    _chud_definition_hud_widget_placement_datum_anchor_bottom_right = 2,
    _chud_definition_hud_widget_placement_datum_anchor_bottom_left = 3,
    _chud_definition_hud_widget_placement_datum_anchor_center = 4,
    _chud_definition_hud_widget_placement_datum_anchor_top_edge = 5,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_a = 6,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_b = 7,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_c = 8,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_d = 9,
    _chud_definition_hud_widget_placement_datum_anchor_scoreboard_friendly = 10,
    _chud_definition_hud_widget_placement_datum_anchor_scoreboard_enemy = 11,
    _chud_definition_hud_widget_placement_datum_anchor_health_and_shield = 12,
    _chud_definition_hud_widget_placement_datum_anchor_bottom_edge = 13,
    _chud_definition_hud_widget_placement_datum_anchor_equipment_xy = 14,
    _chud_definition_hud_widget_placement_datum_anchor_equipment_y = 15,
    _chud_definition_hud_widget_placement_datum_anchor_unknown2 = 16,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated = 17,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated2 = 18,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated3 = 19,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated4 = 20,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated5 = 21,
    _chud_definition_hud_widget_placement_datum_anchor_notification = 22,
    _chud_definition_hud_widget_placement_datum_anchor_gametype = 23,
    _chud_definition_hud_widget_placement_datum_anchor_unknown4 = 24,
    _chud_definition_hud_widget_placement_datum_anchor_state_right = 25,
    _chud_definition_hud_widget_placement_datum_anchor_state_left = 26,
    _chud_definition_hud_widget_placement_datum_anchor_state_center = 27,
    _chud_definition_hud_widget_placement_datum_anchor_state_center2 = 28,
    _chud_definition_hud_widget_placement_datum_anchor_gametype_friendly = 29,
    _chud_definition_hud_widget_placement_datum_anchor_gametype_enemy = 30,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_top = 31,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player1 = 32,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player2 = 33,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player3 = 34,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player4 = 35,
    _chud_definition_hud_widget_placement_datum_anchor_theater = 36,
    _chud_definition_hud_widget_placement_datum_anchor_prompt = 37,
    _chud_definition_hud_widget_placement_datum_anchor_hologram_target = 38,
    _chud_definition_hud_widget_placement_datum_anchor_medals = 39,
    _chud_definition_hud_widget_placement_datum_anchor_survival_medals = 40,
    _chud_definition_hud_widget_placement_datum_anchor_unknown_h_o_offset3 = 41,
    _chud_definition_hud_widget_placement_datum_anchor_motion_sensor = 42
};

enum e_chud_definition_hud_widget_animation_datum_animation_flags
{
    _chud_definition_hud_widget_animation_datum_animation_flags_reverse_frames_bit = 0
};

enum e_chud_definition_hud_widget_animation_datum_animation_function
{
    _chud_definition_hud_widget_animation_datum_animation_function_default = 0,
    _chud_definition_hud_widget_animation_datum_animation_function_use_input = 1,
    _chud_definition_hud_widget_animation_datum_animation_function_use_range_input = 2,
    _chud_definition_hud_widget_animation_datum_animation_function_use_compass_target = 3,
    _chud_definition_hud_widget_animation_datum_animation_function_use_user_target = 4
};

enum e_chud_definition_hud_widget_render_datum_shader_index
{
    _chud_definition_hud_widget_render_datum_shader_index_simple = 0,
    _chud_definition_hud_widget_render_datum_shader_index_meter = 1,
    _chud_definition_hud_widget_render_datum_shader_index_text_simple = 2,
    _chud_definition_hud_widget_render_datum_shader_index_meter_shield = 3,
    _chud_definition_hud_widget_render_datum_shader_index_meter_gradient = 4,
    _chud_definition_hud_widget_render_datum_shader_index_crosshair = 5,
    _chud_definition_hud_widget_render_datum_shader_index_directional_damage = 6,
    _chud_definition_hud_widget_render_datum_shader_index_solid = 7,
    _chud_definition_hud_widget_render_datum_shader_index_sensor = 8,
    _chud_definition_hud_widget_render_datum_shader_index_meter_single_color = 9,
    _chud_definition_hud_widget_render_datum_shader_index_navpoint = 10,
    _chud_definition_hud_widget_render_datum_shader_index_medal = 11,
    _chud_definition_hud_widget_render_datum_shader_index_texture_cam = 12,
    _chud_definition_hud_widget_render_datum_shader_index_cortana_screen = 13,
    _chud_definition_hud_widget_render_datum_shader_index_cortana_camera = 14,
    _chud_definition_hud_widget_render_datum_shader_index_cortana_offscreen = 15,
    _chud_definition_hud_widget_render_datum_shader_index_cortana_screen_final = 16,
    _chud_definition_hud_widget_render_datum_shader_index_meter_chapter = 17,
    _chud_definition_hud_widget_render_datum_shader_index_meter_double_gradient = 18,
    _chud_definition_hud_widget_render_datum_shader_index_meter_radial_gradient = 19,
    _chud_definition_hud_widget_render_datum_shader_index_turbulence = 20,
    _chud_definition_hud_widget_render_datum_shader_index_emblem = 21,
    _chud_definition_hud_widget_render_datum_shader_index_cortana_composite = 22,
    _chud_definition_hud_widget_render_datum_shader_index_directional_damage_apply = 23,
    _chud_definition_hud_widget_render_datum_shader_index_really_simple = 24,
    _chud_definition_hud_widget_render_datum_shader_index_unknown = 25
};

enum e_chud_definition_hud_widget_render_datum_chud_blend_mode
{
    _chud_definition_hud_widget_render_datum_chud_blend_mode_alpha_blend = 0,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_additive = 1,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_multiply = 2,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_opaque = 3,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_double_multiply = 4,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_pre_multiplied_alpha = 5,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_maximum = 6,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_multiply_add = 7,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_add_src_times_dst_alpha = 8,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_add_src_times_src_alpha = 9,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_inv_alpha_blend = 10,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_separate_alpha_blend = 11,
    _chud_definition_hud_widget_render_datum_chud_blend_mode_separate_alpha_blend_additive = 12
};

enum e_chud_definition_hud_widget_render_datum_input_value_ho
{
    _chud_definition_hud_widget_render_datum_input_value_ho_zero = 0,
    _chud_definition_hud_widget_render_datum_input_value_ho_one = 1,
    _chud_definition_hud_widget_render_datum_input_value_ho_time = 2,
    _chud_definition_hud_widget_render_datum_input_value_ho_fade = 3,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit_health_current = 4,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit_health = 5,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit_shield_current = 6,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit_shield = 7,
    _chud_definition_hud_widget_render_datum_input_value_ho_clip_ammo_fraction = 8,
    _chud_definition_hud_widget_render_datum_input_value_ho_total_ammo_fraction = 9,
    _chud_definition_hud_widget_render_datum_input_value_ho_weapon_version_number = 10,
    _chud_definition_hud_widget_render_datum_input_value_ho_heat_fraction = 11,
    _chud_definition_hud_widget_render_datum_input_value_ho_battery_fraction = 12,
    _chud_definition_hud_widget_render_datum_input_value_ho_weapon_error_current1 = 13,
    _chud_definition_hud_widget_render_datum_input_value_ho_weapon_error_current2 = 14,
    _chud_definition_hud_widget_render_datum_input_value_ho_pickup = 15,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit_autoaimed = 16,
    _chud_definition_hud_widget_render_datum_input_value_ho_grenade = 17,
    _chud_definition_hud_widget_render_datum_input_value_ho_grenade_fraction = 18,
    _chud_definition_hud_widget_render_datum_input_value_ho_charge_fraction = 19,
    _chud_definition_hud_widget_render_datum_input_value_ho_friendly_score = 20,
    _chud_definition_hud_widget_render_datum_input_value_ho_enemy_score = 21,
    _chud_definition_hud_widget_render_datum_input_value_ho_score_to_win = 22,
    _chud_definition_hud_widget_render_datum_input_value_ho_arming_fraction = 23,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_18 = 24,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit1_x_overshield_current = 25,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit1_x_overshield = 26,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit2_x_overshield_current = 27,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit2_x_overshield = 28,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit3_x_overshield_current = 29,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit3_x_overshield = 30,
    _chud_definition_hud_widget_render_datum_input_value_ho_aim_yaw = 31,
    _chud_definition_hud_widget_render_datum_input_value_ho_aim_pitch = 32,
    _chud_definition_hud_widget_render_datum_input_value_ho_target_distance = 33,
    _chud_definition_hud_widget_render_datum_input_value_ho_target_elevation = 34,
    _chud_definition_hud_widget_render_datum_input_value_ho_editor_budget = 35,
    _chud_definition_hud_widget_render_datum_input_value_ho_editor_budget_cost = 36,
    _chud_definition_hud_widget_render_datum_input_value_ho_film_total_time = 37,
    _chud_definition_hud_widget_render_datum_input_value_ho_film_current_time = 38,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_27 = 39,
    _chud_definition_hud_widget_render_datum_input_value_ho_film_timeline_fraction1 = 40,
    _chud_definition_hud_widget_render_datum_input_value_ho_film_timeline_fraction2 = 41,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_2_a = 42,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_2_b = 43,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_time = 44,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_score_transient = 45,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_score_player1 = 46,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_score_player2 = 47,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_score_player3 = 48,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_score_player4 = 49,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_modifier = 50,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_skull_modifier = 51,
    _chud_definition_hud_widget_render_datum_input_value_ho_sensor_range = 52,
    _chud_definition_hud_widget_render_datum_input_value_ho_netdebug_latency = 53,
    _chud_definition_hud_widget_render_datum_input_value_ho_netdebug_latency_quality = 54,
    _chud_definition_hud_widget_render_datum_input_value_ho_netdebug_host_quality = 55,
    _chud_definition_hud_widget_render_datum_input_value_ho_netdebug_local_quality = 56,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_score_negative = 57,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_current_set = 58,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_current_round = 59,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_current_wave = 60,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_current_lives = 61,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_bonus_time = 62,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_bonus_score = 63,
    _chud_definition_hud_widget_render_datum_input_value_ho_survival_multiplier = 64,
    _chud_definition_hud_widget_render_datum_input_value_ho_metagame_total_modifier = 65,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement1_current = 66,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement2_current = 67,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement3_current = 68,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement4_current = 69,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement5_current = 70,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement1_goal = 71,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement2_goal = 72,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement3_goal = 73,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement4_goal = 74,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement5_goal = 75,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement1_icon = 76,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement2_icon = 77,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement3_icon = 78,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement4_icon = 79,
    _chud_definition_hud_widget_render_datum_input_value_ho_achievement5_icon = 80,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_51 = 81,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_52 = 82,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable3_icon = 83,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable4_icon = 84,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable_name = 85,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_56 = 86,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_57 = 87,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_58 = 88,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable_cooldown_text = 89,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable_cooldown_meter = 90,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_5_b = 91,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_5_c = 92,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_5_d = 93,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_5_e = 94,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable1_charge = 95,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable2_charge = 96,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable3_charge = 97,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable4_charge = 98,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_63 = 99,
    _chud_definition_hud_widget_render_datum_input_value_ho_unknown_x_64 = 100,
    _chud_definition_hud_widget_render_datum_input_value_ho_energy_meter1 = 101,
    _chud_definition_hud_widget_render_datum_input_value_ho_energy_meter2 = 102,
    _chud_definition_hud_widget_render_datum_input_value_ho_energy_meter3 = 103,
    _chud_definition_hud_widget_render_datum_input_value_ho_energy_meter4 = 104,
    _chud_definition_hud_widget_render_datum_input_value_ho_energy_meter5 = 105,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable1_cost = 106,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable2_cost = 107,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable3_cost = 108,
    _chud_definition_hud_widget_render_datum_input_value_ho_consumable4_cost = 109,
    _chud_definition_hud_widget_render_datum_input_value_ho_unit_stamina_current = 110
};

enum e_chud_definition_hud_widget_render_datum_output_color_value_ho
{
    _chud_definition_hud_widget_render_datum_output_color_value_ho_local_a = 0,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_local_b = 1,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_local_c = 2,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_local_d = 3,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_unknown4 = 4,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_unknown5 = 5,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_scoreboard_friendly = 6,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_scoreboard_enemy = 7,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_arming_team = 8,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_metagame_player1 = 9,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_metagame_player2 = 10,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_metagame_player3 = 11,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_metagame_player4 = 12,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_weapon_version = 13,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_unknown14 = 14,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic0 = 15,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic1 = 16,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic2 = 17,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic3 = 18,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic4 = 19,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic5 = 20,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic6 = 21,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic7 = 22,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic8 = 23,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic9 = 24,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic10 = 25,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic11 = 26,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic12 = 27,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic13 = 28,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic14 = 29,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic15 = 30,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic16 = 31,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic17 = 32,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic18 = 33,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_blue_waypoint_ho = 34,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic19 = 35,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic20 = 36,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic21 = 37,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic22 = 38,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic23 = 39,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic24 = 40,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic25 = 41,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic26 = 42,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic27 = 43,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic28 = 44,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic29 = 45,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic30 = 46,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic31 = 47,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic32 = 48,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic33 = 49,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic34 = 50,
    _chud_definition_hud_widget_render_datum_output_color_value_ho_global_dynamic35 = 51
};

enum e_chud_definition_hud_widget_render_datum_output_scalar
{
    _chud_definition_hud_widget_render_datum_output_scalar_input = 0,
    _chud_definition_hud_widget_render_datum_output_scalar_range_input = 1,
    _chud_definition_hud_widget_render_datum_output_scalar_local_a = 2,
    _chud_definition_hud_widget_render_datum_output_scalar_local_b = 3,
    _chud_definition_hud_widget_render_datum_output_scalar_local_c = 4,
    _chud_definition_hud_widget_render_datum_output_scalar_local_d = 5,
    _chud_definition_hud_widget_render_datum_output_scalar_unknown6 = 6,
    _chud_definition_hud_widget_render_datum_output_scalar_unknown7 = 7
};

enum e_chud_definition_hud_widget_bitmap_widget_flags
{
    _chud_definition_hud_widget_bitmap_widget_flags_mirror_horizontally_bit = 0,
    _chud_definition_hud_widget_bitmap_widget_flags_mirror_vertically_bit = 1,
    _chud_definition_hud_widget_bitmap_widget_flags_stretch_edges_bit = 2,
    _chud_definition_hud_widget_bitmap_widget_flags_enable_texture_cam_bit = 3,
    _chud_definition_hud_widget_bitmap_widget_flags_looping_bit = 4,
    _chud_definition_hud_widget_bitmap_widget_flags_bit5_bit = 5,
    _chud_definition_hud_widget_bitmap_widget_flags_player1_emblem_bit = 6,
    _chud_definition_hud_widget_bitmap_widget_flags_player2_emblem_bit = 7,
    _chud_definition_hud_widget_bitmap_widget_flags_player3_emblem_bit = 8,
    _chud_definition_hud_widget_bitmap_widget_flags_player4_emblem_bit = 9,
    _chud_definition_hud_widget_bitmap_widget_flags_bit10_bit = 10,
    _chud_definition_hud_widget_bitmap_widget_flags_bit11_bit = 11,
    _chud_definition_hud_widget_bitmap_widget_flags_bit12_bit = 12,
    _chud_definition_hud_widget_bitmap_widget_flags_bit13_bit = 13,
    _chud_definition_hud_widget_bitmap_widget_flags_input_controls_consumable_bit = 14,
    _chud_definition_hud_widget_bitmap_widget_flags_input_controls_weapon_bit = 15
};

enum e_chud_definition_hud_widget_text_widget_flags
{
    _chud_definition_hud_widget_text_widget_flags_string_is_a_number_bit = 0,
    _chud_definition_hud_widget_text_widget_flags_force2_digit_bit = 1,
    _chud_definition_hud_widget_text_widget_flags_force3_digit_bit = 2,
    _chud_definition_hud_widget_text_widget_flags_plus_suffix_bit = 3,
    _chud_definition_hud_widget_text_widget_flags_m_suffix_bit = 4,
    _chud_definition_hud_widget_text_widget_flags_tenths_decimal_bit = 5,
    _chud_definition_hud_widget_text_widget_flags_hundredths_decimal_bit = 6,
    _chud_definition_hud_widget_text_widget_flags_thousandths_decimal_bit = 7,
    _chud_definition_hud_widget_text_widget_flags_hundred_thousandths_decimal_bit = 8,
    _chud_definition_hud_widget_text_widget_flags_only_a_number_bit = 9,
    _chud_definition_hud_widget_text_widget_flags_x_suffix_bit = 10,
    _chud_definition_hud_widget_text_widget_flags_in_brackets_bit = 11,
    _chud_definition_hud_widget_text_widget_flags_time_format_s_ms_bit = 12,
    _chud_definition_hud_widget_text_widget_flags_time_format_h_m_s_bit = 13,
    _chud_definition_hud_widget_text_widget_flags_money_format_bit = 14,
    _chud_definition_hud_widget_text_widget_flags_minus_prefix_bit = 15,
    _chud_definition_hud_widget_text_widget_flags_centered_bit = 16,
    _chud_definition_hud_widget_text_widget_flags_bit17_bit = 17,
    _chud_definition_hud_widget_text_widget_flags_bit18_bit = 18,
    _chud_definition_hud_widget_text_widget_flags_bit19_bit = 19,
    _chud_definition_hud_widget_text_widget_flags_bit20_bit = 20,
    _chud_definition_hud_widget_text_widget_flags_bit21_bit = 21,
    _chud_definition_hud_widget_text_widget_flags_bit22_bit = 22,
    _chud_definition_hud_widget_text_widget_flags_bit23_bit = 23,
    _chud_definition_hud_widget_text_widget_flags_bit24_bit = 24,
    _chud_definition_hud_widget_text_widget_flags_bit25_bit = 25,
    _chud_definition_hud_widget_text_widget_flags_bit26_bit = 26,
    _chud_definition_hud_widget_text_widget_flags_bit27_bit = 27,
    _chud_definition_hud_widget_text_widget_flags_bit28_bit = 28,
    _chud_definition_hud_widget_text_widget_flags_bit29_bit = 29,
    _chud_definition_hud_widget_text_widget_flags_bit30_bit = 30
};

enum e_font
{
    _font_fixedsys9 = 0,
    _font_conduit16 = 1,
    _font_conduit32 = 2,
    _font_conduit23 = 3,
    _font_conduit18 = 4,
    _font_larabie_10 = 5,
    _font_pragmata_14 = 6
};


/* ---------- structures */

struct s_chud_definition_hud_widget_state_datum;
struct s_chud_definition_hud_widget_placement_datum;
struct s_chud_definition_hud_widget_animation_datum;
struct s_chud_definition_hud_widget_render_datum;
struct s_chud_definition_hud_widget_bitmap_widget;
struct s_chud_definition_hud_widget_text_widget;
struct s_chud_definition_hud_widget;
struct s_chud_definition;

struct s_chud_definition
{
    c_tag_block<s_chud_definition_hud_widget> hud_widgets;
    long low_clip_cutoff;
    long low_ammo_cutoff;
    long age_cutoff;
};
static_assert(sizeof(s_chud_definition) == 0x18);

struct s_chud_definition_hud_widget
{
    string_id name;
    c_enum<e_chud_definition_hud_widget_special_hud_type, short> special_hud_type;
    uchar unknown;
    uchar unknown2;
    c_tag_block<s_chud_definition_hud_widget_state_datum> state_data;
    c_tag_block<s_chud_definition_hud_widget_placement_datum> placement_data;
    c_tag_block<s_chud_definition_hud_widget_animation_datum> animation_data;
    c_tag_block<s_chud_definition_hud_widget_render_datum> render_data;
    s_tag_reference parallax_data;
    c_tag_block<s_chud_definition_hud_widget_bitmap_widget> bitmap_widgets;
    c_tag_block<s_chud_definition_hud_widget_text_widget> text_widgets;
};
static_assert(sizeof(s_chud_definition_hud_widget) == 0x60);

struct s_chud_definition_hud_widget_text_widget
{
    string_id name;
    c_enum<e_chud_definition_hud_widget_special_hud_type, short> special_hud_type;
    uchar unknown1;
    uchar unknown2;
    c_tag_block<s_chud_definition_hud_widget_state_datum> state_data;
    c_tag_block<s_chud_definition_hud_widget_placement_datum> placement_data;
    c_tag_block<s_chud_definition_hud_widget_animation_datum> animation_data;
    c_tag_block<s_chud_definition_hud_widget_render_datum> render_data;
    long widget_index;
    c_flags<e_chud_definition_hud_widget_text_widget_flags, ulong> flags;
    c_enum<e_font, short> font;
    short unknown4;
    string_id string;
};
static_assert(sizeof(s_chud_definition_hud_widget_text_widget) == 0x48);

struct s_chud_definition_hud_widget_bitmap_widget
{
    string_id name;
    c_enum<e_chud_definition_hud_widget_special_hud_type, short> special_hud_type;
    uchar unknown;
    uchar unknown2;
    c_tag_block<s_chud_definition_hud_widget_state_datum> state_data;
    c_tag_block<s_chud_definition_hud_widget_placement_datum> placement_data;
    c_tag_block<s_chud_definition_hud_widget_animation_datum> animation_data;
    c_tag_block<s_chud_definition_hud_widget_render_datum> render_data;
    long widget_index;
    c_flags<e_chud_definition_hud_widget_bitmap_widget_flags, ushort> flags;
    short unknown3;
    s_tag_reference bitmap;
    uchar bitmap_sprite_index;
    uchar unknown4;
    uchar unknown5;
    uchar unknown6;
};
static_assert(sizeof(s_chud_definition_hud_widget_bitmap_widget) == 0x54);

struct s_chud_definition_hud_widget_render_datum
{
    c_enum<e_chud_definition_hud_widget_render_datum_shader_index, short> shader_index;
    c_enum<e_chud_definition_hud_widget_render_datum_chud_blend_mode, short> blend_mode_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_input_value_ho, short> input_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_input_value_ho, short> range_input_ho;
    argb_color local_color_a;
    argb_color local_color_b;
    argb_color local_color_c;
    argb_color local_color_d;
    float local_scalar_a;
    float local_scalar_b;
    float local_scalar_c;
    float local_scalar_d;
    c_enum<e_chud_definition_hud_widget_render_datum_output_color_value_ho, short> output_color_a_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_output_color_value_ho, short> output_color_b_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_output_color_value_ho, short> output_color_c_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_output_color_value_ho, short> output_color_d_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_output_color_value_ho, short> output_color_e_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_output_color_value_ho, short> output_color_f_ho;
    c_enum<e_chud_definition_hud_widget_render_datum_output_scalar, short> output_scalar_a;
    c_enum<e_chud_definition_hud_widget_render_datum_output_scalar, short> output_scalar_b;
    c_enum<e_chud_definition_hud_widget_render_datum_output_scalar, short> output_scalar_c;
    c_enum<e_chud_definition_hud_widget_render_datum_output_scalar, short> output_scalar_d;
    c_enum<e_chud_definition_hud_widget_render_datum_output_scalar, short> output_scalar_e;
    c_enum<e_chud_definition_hud_widget_render_datum_output_scalar, short> output_scalar_f;
    short unknown2;
    short unknown3;
    short unknown4;
    short unknown5;
};
static_assert(sizeof(s_chud_definition_hud_widget_render_datum) == 0x48);

struct s_chud_definition_hud_widget_animation_datum
{
    c_flags<e_chud_definition_hud_widget_animation_datum_animation_flags, short> hud_initialize_flags;
    c_enum<e_chud_definition_hud_widget_animation_datum_animation_function, short> hud_initialize_function;
    s_tag_reference hud_initialize;
    float hud_initialize_unknown;
    c_flags<e_chud_definition_hud_widget_animation_datum_animation_flags, short> idle_flags;
    c_enum<e_chud_definition_hud_widget_animation_datum_animation_function, short> idle_function;
    s_tag_reference idle;
    float idle_unknown;
    c_flags<e_chud_definition_hud_widget_animation_datum_animation_flags, short> special_state_flags;
    c_enum<e_chud_definition_hud_widget_animation_datum_animation_function, short> special_state_function;
    s_tag_reference special_state;
    float special_state_unknown;
    c_flags<e_chud_definition_hud_widget_animation_datum_animation_flags, short> transition_in_flags;
    c_enum<e_chud_definition_hud_widget_animation_datum_animation_function, short> transition_in_function;
    s_tag_reference transition_in;
    float transition_in_unknown;
    c_flags<e_chud_definition_hud_widget_animation_datum_animation_flags, short> transition_out_flags;
    c_enum<e_chud_definition_hud_widget_animation_datum_animation_function, short> transition_out_function;
    s_tag_reference transition_out;
    float transition_out_unknown;
    c_flags<e_chud_definition_hud_widget_animation_datum_animation_flags, short> brief_state_flags;
    c_enum<e_chud_definition_hud_widget_animation_datum_animation_function, short> brief_state_function;
    s_tag_reference brief_state;
    float brief_state_unknown;
};
static_assert(sizeof(s_chud_definition_hud_widget_animation_datum) == 0x90);

struct s_chud_definition_hud_widget_placement_datum
{
    c_enum<e_chud_definition_hud_widget_placement_datum_anchor, short> anchor;
    short unknown;
    real_point2d mirror_offset;
    real_point2d offset;
    real_point2d scale;
};
static_assert(sizeof(s_chud_definition_hud_widget_placement_datum) == 0x1C);

struct s_chud_definition_hud_widget_state_datum
{
    c_flags<e_chud_definition_hud_widget_state_datum_engine, ushort> engine_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_player_type, ushort> player_type_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_multiplayer, ushort> multiplayer_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_resolution, ushort> resolution_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_multiplayer_events, ulong> multiplayer_events_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_unit_base, ulong> unit_base_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_editor, ushort> editor_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_engine_general, ushort> engine_general_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_skulls, ushort> skull_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_survival_rounds, ushort> survival_round_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_survival_waves, ushort> survival_wave_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_survival_lives, ushort> survival_lives_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_survival_difficulty, ushort> survival_difficulty_flags;
    ushort unused;
    c_flags<e_chud_definition_hud_widget_state_datum_general_kudos, ushort> general_kudos_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_unit_zoom, ushort> unit_zoom_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_unit_inventory, ushort> unit_inventory_flags;
    ushort unused3;
    c_flags<e_chud_definition_hud_widget_state_datum_unit_general, ushort> unit_general_flags;
    ushort unused4;
    c_flags<e_chud_definition_hud_widget_state_datum_weapon_kudos, ushort> weapon_kudos_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_weapon_status, ushort> weapon_status_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_weapon_target, ushort> weapon_target_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_weapon_target_b, ushort> weapon_target_b_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_player_special, ushort> player_special_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_weapon_special, ushort> weapon_special_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_inverse, ushort> inverse_flags;
    short unused_flags4;
    c_flags<e_chud_definition_hud_widget_state_datum_consumable, ulong> consumable_flags;
    c_flags<e_chud_definition_hud_widget_state_datum_energy_meter, ulong> energy_meter_flags;
};
static_assert(sizeof(s_chud_definition_hud_widget_state_datum) == 0x44);

