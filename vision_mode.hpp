/* ---------- enums */


/* ---------- structures */

struct s_vision_mode_ping_parameters_block;
struct s_tag_function;
struct s_vision_mode_ping_color_block;
struct s_vision_mode;

struct s_vision_mode
{
    char unknown1;
    char unknown2;
    char unknown3;
    char unknown4;
    s_vision_mode_ping_parameters_block ping;
    s_vision_mode_ping_parameters_block vehicle_ping;
    s_vision_mode_ping_parameters_block observer_ping;
    s_tag_reference ping_sound;
    s_vision_mode_ping_color_block weapon_ping_color;
    s_vision_mode_ping_color_block ally_ping_color;
    s_vision_mode_ping_color_block enemy_ping_color;
    s_vision_mode_ping_color_block objective_ping_color;
    s_vision_mode_ping_color_block environment_ping_color;
    s_tag_reference vision_mask;
    s_tag_reference vision_camera_fx;
};
static_assert(sizeof(s_vision_mode) == 0x194);

struct s_vision_mode_ping_color_block
{
    s_tag_function primary_colour_function;
    s_tag_function secondary_colour_function;
    float alpha;
    float primary_color_scale;
    float secondary_colour_scale;
    float overlapping_dimming_factor;
};
static_assert(sizeof(s_vision_mode_ping_color_block) == 0x38);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_vision_mode_ping_parameters_block
{
    float ping_distance;
    float vision_through_walls_distance;
    float ping_interval;
    float ping_pulse_duration;
    float unused;
    float ping_delay_seconds;
};
static_assert(sizeof(s_vision_mode_ping_parameters_block) == 0x18);

