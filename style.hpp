/* ---------- enums */

enum e_style_combat_status_decay_options
{
    _style_combat_status_decay_options_latch_at_idle = 0,
    _style_combat_status_decay_options_latch_at_alert = 1,
    _style_combat_status_decay_options_latch_at_combat = 2
};

enum e_style_style_control_flags
{
    _style_style_control_flags_new_behaviors_default_to_on_bit = 0,
    _style_style_control_flags_do_not_force_on_default_behaviors_bit = 1
};

enum e_style_special_movement_block_special_movement_flags
{
    _style_special_movement_block_special_movement_flags_jump_bit = 0,
    _style_special_movement_block_special_movement_flags_climb_bit = 1,
    _style_special_movement_block_special_movement_flags_vault_bit = 2,
    _style_special_movement_block_special_movement_flags_mount_bit = 3,
    _style_special_movement_block_special_movement_flags_hoist_bit = 4,
    _style_special_movement_block_special_movement_flags_wall_jump_bit = 5
};


/* ---------- structures */

struct s_style_special_movement_block;
struct s_style_behavior_list_block;
struct s_style;

struct s_style
{
    char name[32];
    c_enum<e_style_combat_status_decay_options, short> combat_status_decay_options;
    short unknown;
    c_flags<e_style_style_control_flags, long> style_control;
    int32 behaviors[7];
    c_tag_block<s_style_special_movement_block> special_movement;
    c_tag_block<s_style_behavior_list_block> behavior_list;
};
static_assert(sizeof(s_style) == 0x5C);

struct s_style_behavior_list_block
{
    char behavior_name[32];
};
static_assert(sizeof(s_style_behavior_list_block) == 0x20);

struct s_style_special_movement_block
{
    c_flags<e_style_special_movement_block_special_movement_flags, long> special_movement1;
};
static_assert(sizeof(s_style_special_movement_block) == 0x4);

