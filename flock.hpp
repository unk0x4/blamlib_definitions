/* ---------- enums */


/* ---------- structures */

struct s_flock;

struct s_flock
{
    float forward_scale;
    float sink_scale;
    float average_throttle;
    float maximum_throttle;
    float movement_weight_threshold;
    float danger_radius;
    float danger_scale;
    float target_scale;
    float target_distance;
    float target_delay_time;
    float target_kill_chance;
    float ai_destroy_chance;
    float random_offset_scale;
    real_bounds random_offset_period;
    float neighborhood_radius;
    real perception_angle;
    float avoidance_scale;
    float avoidance_radius;
    float alignment_scale;
    float position_scale;
    real_bounds position_radii;
};
static_assert(sizeof(s_flock) == 0x5C);

