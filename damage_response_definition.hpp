/* ---------- enums */

enum e_damage_response_definition_response_block_response_type
{
    _damage_response_definition_response_block_response_type_shielded = 0,
    _damage_response_definition_response_block_response_type_unshielded = 1,
    _damage_response_definition_response_block_response_type_all = 2
};

enum e_damage_response_definition_response_block_fade_function
{
    _damage_response_definition_response_block_fade_function_linear = 0,
    _damage_response_definition_response_block_fade_function_late = 1,
    _damage_response_definition_response_block_fade_function_very_late = 2,
    _damage_response_definition_response_block_fade_function_early = 3,
    _damage_response_definition_response_block_fade_function_very_early = 4,
    _damage_response_definition_response_block_fade_function_cosine = 5,
    _damage_response_definition_response_block_fade_function_zero = 6,
    _damage_response_definition_response_block_fade_function_one = 7
};

enum e_damage_response_definition_response_block_falloff_function
{
    _damage_response_definition_response_block_falloff_function_linear = 0,
    _damage_response_definition_response_block_falloff_function_late = 1,
    _damage_response_definition_response_block_falloff_function_very_late = 2,
    _damage_response_definition_response_block_falloff_function_early = 3,
    _damage_response_definition_response_block_falloff_function_very_early = 4,
    _damage_response_definition_response_block_falloff_function_cosine = 5,
    _damage_response_definition_response_block_falloff_function_zero = 6,
    _damage_response_definition_response_block_falloff_function_one = 7
};

enum e_damage_response_definition_response_block_wobble_function
{
    _damage_response_definition_response_block_wobble_function_one = 0,
    _damage_response_definition_response_block_wobble_function_zero = 1,
    _damage_response_definition_response_block_wobble_function_cosine = 2,
    _damage_response_definition_response_block_wobble_function_cosine_variable_period = 3,
    _damage_response_definition_response_block_wobble_function_diagonal_wave = 4,
    _damage_response_definition_response_block_wobble_function_diagonal_wave_variable_period = 5,
    _damage_response_definition_response_block_wobble_function_slide = 6,
    _damage_response_definition_response_block_wobble_function_slide_variable_period = 7,
    _damage_response_definition_response_block_wobble_function_noise = 8,
    _damage_response_definition_response_block_wobble_function_jitter = 9,
    _damage_response_definition_response_block_wobble_function_wander = 10,
    _damage_response_definition_response_block_wobble_function_spark = 11
};


/* ---------- structures */

struct s_tag_function;
struct s_damage_response_definition_response_block;
struct s_damage_response_definition;

struct s_damage_response_definition
{
    c_tag_block<s_damage_response_definition_response_block> responses;
    byte unused[12];
};
static_assert(sizeof(s_damage_response_definition) == 0x18);

struct s_damage_response_definition_response_block
{
    c_enum<e_damage_response_definition_response_block_response_type, short> response_type;
    short unknown;
    short unknown2;
    short unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    float unknown7;
    ulong unknown8;
    ulong unknown9;
    float unknown10;
    float unknown11;
    short unknown12;
    short unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    short unknown18;
    short unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
    float unknown25;
    float unknown26;
    float low_frequency_vibration_duration;
    s_tag_function low_frequency_vibration_function;
    float high_frequency_vibration_duration;
    s_tag_function high_frequency_vibration_function;
    float duration;
    c_enum<e_damage_response_definition_response_block_fade_function, short> fade_function;
    short unknown27;
    real rotation;
    float pushback;
    float jitter_min;
    float jitter_max;
    float duration2;
    c_enum<e_damage_response_definition_response_block_falloff_function, short> falloff_function;
    short unknown28;
    float random_translation;
    real random_rotation;
    c_enum<e_damage_response_definition_response_block_wobble_function, short> wobble_function;
    short unknown29;
    float wobble_function_period;
    float wobble_weight;
};
static_assert(sizeof(s_damage_response_definition_response_block) == 0xC0);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

