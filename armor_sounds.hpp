/* ---------- enums */


/* ---------- structures */

struct s_tag_reference_block;
struct s_armor_sounds_armor_sound;
struct s_armor_sounds;

struct s_armor_sounds
{
    c_tag_block<s_armor_sounds_armor_sound> armor_sounds2;
    ulong unknown;
};
static_assert(sizeof(s_armor_sounds) == 0x10);

struct s_armor_sounds_armor_sound
{
    c_tag_block<s_tag_reference_block> unknown1;
    c_tag_block<s_tag_reference_block> unknown2;
    c_tag_block<s_tag_reference_block> unknown3;
};
static_assert(sizeof(s_armor_sounds_armor_sound) == 0x24);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

