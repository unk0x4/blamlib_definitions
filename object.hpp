/* ---------- enums */

enum e_game_object_type_halo3_odst
{
    _game_object_type_halo3_odst_none = -1,
    _game_object_type_halo3_odst_biped = 0,
    _game_object_type_halo3_odst_vehicle = 1,
    _game_object_type_halo3_odst_weapon = 2,
    _game_object_type_halo3_odst_equipment = 3,
    _game_object_type_halo3_odst_alternate_reality_device = 4,
    _game_object_type_halo3_odst_terminal = 5,
    _game_object_type_halo3_odst_projectile = 6,
    _game_object_type_halo3_odst_scenery = 7,
    _game_object_type_halo3_odst_machine = 8,
    _game_object_type_halo3_odst_control = 9,
    _game_object_type_halo3_odst_sound_scenery = 10,
    _game_object_type_halo3_odst_crate = 11,
    _game_object_type_halo3_odst_creature = 12,
    _game_object_type_halo3_odst_giant = 13,
    _game_object_type_halo3_odst_effect_scenery = 14
};

enum e_game_object_flags
{
    _game_object_flags_does_not_cast_shadow_bit = 0,
    _game_object_flags_search_cardinal_direction_maps_bit = 1,
    _game_object_flags_bit2_bit = 2,
    _game_object_flags_not_a_pathfinding_obstacle_bit = 3,
    _game_object_flags_extension_of_parent_bit = 4,
    _game_object_flags_does_not_cause_collision_damage_bit = 5,
    _game_object_flags_early_mover_bit = 6,
    _game_object_flags_early_mover_localized_physics_bit = 7,
    _game_object_flags_use_static_massive_lightmap_sample_bit = 8,
    _game_object_flags_object_scales_attachments_bit = 9,
    _game_object_flags_inherits_players_appearance_bit = 10,
    _game_object_flags_dead_bipeds_cant_localize_bit = 11,
    _game_object_flags_attach_to_clusters_by_dynamic_sphere_bit = 12,
    _game_object_flags_effects_do_not_spawn_objects_in_multiplayer_bit = 13,
    _game_object_flags_bit14_bit = 14,
    _game_object_flags_bit15_bit = 15
};

enum e_game_object_lightmap_shadow_mode
{
    _game_object_lightmap_shadow_mode_default = 0,
    _game_object_lightmap_shadow_mode_never = 1,
    _game_object_lightmap_shadow_mode_always = 2,
    _game_object_lightmap_shadow_mode_blur = 3
};

enum e_game_object_sweetener_size
{
    _game_object_sweetener_size_small = 0,
    _game_object_sweetener_size_medium = 1,
    _game_object_sweetener_size_large = 2
};

enum e_game_object_water_density
{
    _game_object_water_density_default = 0,
    _game_object_water_density_least = 1,
    _game_object_water_density_some = 2,
    _game_object_water_density_equal = 3,
    _game_object_water_density_more = 4,
    _game_object_water_density_more_still = 5,
    _game_object_water_density_lots_more = 6
};

enum e_game_object_ai_property_flags
{
    _game_object_ai_property_flags_destroyable_cover_bit = 0,
    _game_object_ai_property_flags_pathfinding_ignore_when_dead_bit = 1,
    _game_object_ai_property_flags_dynamic_cover_bit = 2,
    _game_object_ai_property_flags_non_flight_blocking_bit = 3,
    _game_object_ai_property_flags_dynamic_cover_from_centre_bit = 4,
    _game_object_ai_property_flags_has_corner_markers_bit = 5,
    _game_object_ai_property_flags_idle_when_flying_bit = 6
};

enum e_game_object_object_size
{
    _game_object_object_size_default = 0,
    _game_object_object_size_tiny = 1,
    _game_object_object_size_small = 2,
    _game_object_object_size_medium = 3,
    _game_object_object_size_large = 4,
    _game_object_object_size_huge = 5,
    _game_object_object_size_immobile = 6
};

enum e_game_object_ai_distance
{
    _game_object_ai_distance_none = 0,
    _game_object_ai_distance_down = 1,
    _game_object_ai_distance_step = 2,
    _game_object_ai_distance_crouch = 3,
    _game_object_ai_distance_stand = 4,
    _game_object_ai_distance_storey = 5,
    _game_object_ai_distance_tower = 6,
    _game_object_ai_distance_infinite = 7
};

enum e_game_object_function_flags
{
    _game_object_function_flags_invert_bit = 0,
    _game_object_function_flags_mapping_does_not_controls_active_bit = 1,
    _game_object_function_flags_always_active_bit = 2,
    _game_object_function_flags_random_time_offset_bit = 3,
    _game_object_function_flags_always_exports_value_bit = 4,
    _game_object_function_flags_turn_off_with_uses_magnitude_bit = 5
};

enum e_game_object_attachment_atlas_flags
{
    _game_object_attachment_atlas_flags_gameplay_vision_mode_bit = 0,
    _game_object_attachment_atlas_flags_theater_vision_mode_bit = 1
};

enum e_game_object_attachment_change_color
{
    _game_object_attachment_change_color_none = 0,
    _game_object_attachment_change_color_primary = 1,
    _game_object_attachment_change_color_secondary = 2,
    _game_object_attachment_change_color_tertiary = 3,
    _game_object_attachment_change_color_quaternary = 4
};

enum e_game_object_attachment_flags
{
    _game_object_attachment_flags_force_always_on_bit = 0,
    _game_object_attachment_flags_effect_size_scale_from_object_scale_bit = 1
};

enum e_game_object_change_color_function_scale_flags
{
    _game_object_change_color_function_scale_flags_blend_in_hsv_bit = 0,
    _game_object_change_color_function_scale_flags_more_colors_bit = 1
};

enum e_game_object_multiplayer_object_block_engine_flags
{
    _game_object_multiplayer_object_block_engine_flags_capture_the_flag_bit = 0,
    _game_object_multiplayer_object_block_engine_flags_slayer_bit = 1,
    _game_object_multiplayer_object_block_engine_flags_oddball_bit = 2,
    _game_object_multiplayer_object_block_engine_flags_king_of_the_hill_bit = 3,
    _game_object_multiplayer_object_block_engine_flags_juggernaut_bit = 4,
    _game_object_multiplayer_object_block_engine_flags_territories_bit = 5,
    _game_object_multiplayer_object_block_engine_flags_assault_bit = 6,
    _game_object_multiplayer_object_block_engine_flags_vip_bit = 7,
    _game_object_multiplayer_object_block_engine_flags_infection_bit = 8,
    _game_object_multiplayer_object_block_engine_flags_bit9_bit = 9
};

enum e_game_object_multiplayer_object_block_type
{
    _game_object_multiplayer_object_block_type_ordinary = 0,
    _game_object_multiplayer_object_block_type_weapon = 1,
    _game_object_multiplayer_object_block_type_grenade = 2,
    _game_object_multiplayer_object_block_type_projectile = 3,
    _game_object_multiplayer_object_block_type_powerup = 4,
    _game_object_multiplayer_object_block_type_equipment = 5,
    _game_object_multiplayer_object_block_type_light_land_vehicle = 6,
    _game_object_multiplayer_object_block_type_heavy_land_vehicle = 7,
    _game_object_multiplayer_object_block_type_flying_vehicle = 8,
    _game_object_multiplayer_object_block_type_teleporter2_way = 9,
    _game_object_multiplayer_object_block_type_teleporter_sender = 10,
    _game_object_multiplayer_object_block_type_teleporter_receiver = 11,
    _game_object_multiplayer_object_block_type_player_spawn_location = 12,
    _game_object_multiplayer_object_block_type_player_respawn_zone = 13,
    _game_object_multiplayer_object_block_type_hold_spawn_objective = 14,
    _game_object_multiplayer_object_block_type_capture_spawn_objective = 15,
    _game_object_multiplayer_object_block_type_hold_destination_objective = 16,
    _game_object_multiplayer_object_block_type_capture_destination_objective = 17,
    _game_object_multiplayer_object_block_type_hill_objective = 18,
    _game_object_multiplayer_object_block_type_infection_haven_objective = 19,
    _game_object_multiplayer_object_block_type_territory_objective = 20,
    _game_object_multiplayer_object_block_type_vip_boundary_objective = 21,
    _game_object_multiplayer_object_block_type_vip_destination_objective = 22,
    _game_object_multiplayer_object_block_type_juggernaut_destination_objective = 23
};

enum e_game_object_multiplayer_object_block_teleporter_passability_flags
{
    _game_object_multiplayer_object_block_teleporter_passability_flags_disallow_players_bit = 0,
    _game_object_multiplayer_object_block_teleporter_passability_flags_allow_light_land_vehicles_bit = 1,
    _game_object_multiplayer_object_block_teleporter_passability_flags_allow_heavy_land_vehicles_bit = 2,
    _game_object_multiplayer_object_block_teleporter_passability_flags_allow_flying_vehicles_bit = 3,
    _game_object_multiplayer_object_block_teleporter_passability_flags_allow_projectiles_bit = 4
};

enum e_game_object_multiplayer_object_block_flags
{
    _game_object_multiplayer_object_block_flags_only_render_in_editor_bit = 0,
    _game_object_multiplayer_object_block_flags_valid_initial_player_spawn_bit = 1,
    _game_object_multiplayer_object_block_flags_fixed_boundary_orientation_bit = 2,
    _game_object_multiplayer_object_block_flags_inherit_owning_team_color_bit = 3,
    _game_object_multiplayer_object_block_flags_bit4_bit = 4,
    _game_object_multiplayer_object_block_flags_bit5_bit = 5,
    _game_object_multiplayer_object_block_flags_bit6_bit = 6,
    _game_object_multiplayer_object_block_flags_bit7_bit = 7,
    _game_object_multiplayer_object_block_flags_bit8_bit = 8,
    _game_object_multiplayer_object_block_flags_bit9_bit = 9,
    _game_object_multiplayer_object_block_flags_bit10_bit = 10,
    _game_object_multiplayer_object_block_flags_bit11_bit = 11,
    _game_object_multiplayer_object_block_flags_bit12_bit = 12,
    _game_object_multiplayer_object_block_flags_bit13_bit = 13,
    _game_object_multiplayer_object_block_flags_bit14_bit = 14,
    _game_object_multiplayer_object_block_flags_bit15_bit = 15
};

enum e_game_object_multiplayer_object_block_boundary_shape
{
    _game_object_multiplayer_object_block_boundary_shape_none = 0,
    _game_object_multiplayer_object_block_boundary_shape_sphere = 1,
    _game_object_multiplayer_object_block_boundary_shape_cylinder = 2,
    _game_object_multiplayer_object_block_boundary_shape_box = 3,
    _game_object_multiplayer_object_block_boundary_shape_count = 4
};

enum e_game_object_multiplayer_object_block_spawn_timer_type
{
    _game_object_multiplayer_object_block_spawn_timer_type_starts_on_death = 0,
    _game_object_multiplayer_object_block_spawn_timer_type_starts_on_disturbance = 1
};

enum e_game_object_pathfinding_sphere_flags
{
    _game_object_pathfinding_sphere_flags_remains_when_open_bit = 0,
    _game_object_pathfinding_sphere_flags_vehicle_only_bit = 1,
    _game_object_pathfinding_sphere_flags_with_sectors_bit = 2
};


/* ---------- structures */

struct s_game_object_type;
struct s_game_object_early_mover_property;
struct s_game_object_ai_property;
struct s_tag_function;
struct s_game_object_function;
struct s_game_object_attachment;
struct s_tag_reference_block;
struct s_game_object_change_color_initial_permutation;
struct s_game_object_change_color_function;
struct s_game_object_change_color;
struct s_game_object_node_map;
struct s_game_object_multiplayer_object_block_boundary_shader;
struct s_game_object_multiplayer_object_block;
struct s_game_object_pathfinding_sphere;
struct s_game_object;

struct s_game_object
{
    s_game_object_type object_type;
    c_flags<e_game_object_flags, ushort> object_flags;
    float bounding_radius;
    real_point3d bounding_offset;
    float acceleration_scale;
    c_enum<e_game_object_lightmap_shadow_mode, short> lightmap_shadow_mode;
    c_enum<e_game_object_sweetener_size, char> sweetener_size;
    c_enum<e_game_object_water_density, char> water_density;
    long runtime_flags;
    float dynamic_light_sphere_radius;
    real_point3d dynamic_light_sphere_offset;
    string_id default_model_variant;
    s_tag_reference model;
    s_tag_reference crate_object;
    s_tag_reference collision_damage;
    c_tag_block<s_game_object_early_mover_property> early_mover_properties;
    s_tag_reference creation_effect;
    s_tag_reference material_effects;
    s_tag_reference armor_sounds;
    s_tag_reference melee_impact;
    c_tag_block<s_game_object_ai_property> ai_properties;
    c_tag_block<s_game_object_function> functions;
    short hud_text_message_index;
    short secondary_flags;
    c_tag_block<s_game_object_attachment> attachments;
    c_tag_block<s_tag_reference_block> widgets;
    c_tag_block<s_game_object_change_color> change_colors;
    c_tag_block<s_game_object_node_map> node_maps;
    c_tag_block<s_game_object_multiplayer_object_block> multiplayer_object;
    c_tag_block<s_tag_reference_block> reviving_equipment;
    c_tag_block<s_game_object_pathfinding_sphere> pathfinding_spheres;
};
static_assert(sizeof(s_game_object) == 0x120);

struct s_game_object_pathfinding_sphere
{
    short node;
    c_flags<e_game_object_pathfinding_sphere_flags, ushort> flags;
    real_point3d center;
    float radius;
};
static_assert(sizeof(s_game_object_pathfinding_sphere) == 0x14);

struct s_game_object_multiplayer_object_block
{
    c_flags<e_game_object_multiplayer_object_block_engine_flags, ushort> engine_flags;
    c_enum<e_game_object_multiplayer_object_block_type, char> type;
    c_flags<e_game_object_multiplayer_object_block_teleporter_passability_flags, uchar> teleporter_passability;
    c_flags<e_game_object_multiplayer_object_block_flags, ushort> flags;
    c_enum<e_game_object_multiplayer_object_block_boundary_shape, char> boundary_shape;
    c_enum<e_game_object_multiplayer_object_block_spawn_timer_type, char> spawn_timer_type;
    short spawn_time;
    short abandon_time;
    float boundary_width;
    float boundary_length;
    float boundary_top;
    float boundary_bottom;
    float respawn_zone_weight;
    float respawn_zone_unknown2;
    float respawn_zone_unknown3;
    string_id boundary_center_marker;
    string_id spawned_object_marker_name;
    s_tag_reference spawned_object;
    string_id nyi_boundary_material;
    boundary_shader boundary_shaders[4];
};
static_assert(sizeof(s_game_object_multiplayer_object_block) == 0xC4);

struct s_game_object_multiplayer_object_block_boundary_shader
{
    s_tag_reference standard_shader;
    s_tag_reference opaque_shader;
};
static_assert(sizeof(s_game_object_multiplayer_object_block_boundary_shader) == 0x20);

struct s_game_object_node_map
{
    char target_node;
};
static_assert(sizeof(s_game_object_node_map) == 0x1);

struct s_game_object_change_color
{
    c_tag_block<s_game_object_change_color_initial_permutation> initial_permutations;
    c_tag_block<s_game_object_change_color_function> functions;
};
static_assert(sizeof(s_game_object_change_color) == 0x18);

struct s_game_object_change_color_function
{
    byte unused[4];
    c_flags<e_game_object_change_color_function_scale_flags, long> scale_flags;
    real_rgb_color color_lower_bound;
    real_rgb_color color_upper_bound;
    string_id darken_by;
    string_id scale_by;
};
static_assert(sizeof(s_game_object_change_color_function) == 0x28);

struct s_game_object_change_color_initial_permutation
{
    float weight;
    real_rgb_color color_lower_bound;
    real_rgb_color color_upper_bound;
    string_id variant_name;
};
static_assert(sizeof(s_game_object_change_color_initial_permutation) == 0x20);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

struct s_game_object_attachment
{
    c_flags<e_game_object_attachment_atlas_flags, long> atlas_flags;
    s_tag_reference type;
    string_id marker;
    c_enum<e_game_object_attachment_change_color, short> change_color;
    c_flags<e_game_object_attachment_flags, ushort> flags;
    string_id primary_scale;
    string_id secondary_scale;
};
static_assert(sizeof(s_game_object_attachment) == 0x24);

struct s_game_object_function
{
    c_flags<e_game_object_function_flags, long> flags;
    string_id import_name;
    string_id export_name;
    string_id turn_off_with;
    float minimum_value;
    s_tag_function default_function;
    string_id scale_by;
};
static_assert(sizeof(s_game_object_function) == 0x2C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_game_object_ai_property
{
    c_flags<e_game_object_ai_property_flags, long> flags;
    string_id ai_type_name;
    c_enum<e_game_object_object_size, short> size;
    c_enum<e_game_object_ai_distance, short> leap_jump_speed;
};
static_assert(sizeof(s_game_object_ai_property) == 0xC);

struct s_game_object_early_mover_property
{
    string_id name;
    real_bounds x_bounds;
    real_bounds y_bounds;
    real_bounds z_bounds;
    real_euler_angles3d angles;
};
static_assert(sizeof(s_game_object_early_mover_property) == 0x28);

struct s_game_object_type
{
    c_enum<e_game_object_type_halo3_odst, char> halo3_odst;
    char unknown2;
};
static_assert(sizeof(s_game_object_type) == 0x2);

