/* ---------- enums */


/* ---------- structures */

struct s_simulation_interpolation_single_domain_interpolation_velocity_bumps_structure;
struct s_simulation_interpolation_single_domain_interpolation_blending_structure;
struct s_simulation_interpolation_single_domain_interpolation;
struct s_simulation_interpolation;

struct s_simulation_interpolation
{
    float position_client_ignore_tolerance;
    float angular_speed_threshold;
    float position_warp_threshold;
    float position_warp_threshold_xy;
    float position_warp_threshold_rotation;
    s_simulation_interpolation_single_domain_interpolation position_while_controlled;
    s_simulation_interpolation_single_domain_interpolation rotation_while_controlled;
    s_simulation_interpolation_single_domain_interpolation position_while_not_controlled;
    s_simulation_interpolation_single_domain_interpolation rotation_while_not_controlled;
};
static_assert(sizeof(s_simulation_interpolation) == 0xF4);

struct s_simulation_interpolation_single_domain_interpolation
{
    float discrepency_threshold;
    float coming_to_rest_speed;
    float coming_to_rest_maximum_ignoreable_error;
    s_simulation_interpolation_single_domain_interpolation_velocity_bumps_structure velocity_bumps;
    s_simulation_interpolation_single_domain_interpolation_blending_structure blending;
};
static_assert(sizeof(s_simulation_interpolation_single_domain_interpolation) == 0x38);

struct s_simulation_interpolation_single_domain_interpolation_blending_structure
{
    real_bounds object_speed;
    float fraction_at_minimum_object_speed;
    float fraction_at_maximum_object_speed;
    float minimum_speed_at_minimum_object_speed;
    float minimum_speed_at_maximum_object_speed;
};
static_assert(sizeof(s_simulation_interpolation_single_domain_interpolation_blending_structure) == 0x18);

struct s_simulation_interpolation_single_domain_interpolation_velocity_bumps_structure
{
    float velocity_scale;
    real_bounds velocity;
    float velocity_difference_ignore_threshold;
    float velocity_difference_absolute_ignore_threshold;
};
static_assert(sizeof(s_simulation_interpolation_single_domain_interpolation_velocity_bumps_structure) == 0x14);

