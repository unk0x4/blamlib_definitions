/* ---------- enums */

enum e_camo_flags
{
    _camo_flags_also_apply_to_object_children_bit = 0
};


/* ---------- structures */

struct s_tag_function;
struct s_camo_mapping;
struct s_camo;

struct s_camo
{
    c_flags<e_camo_flags, ushort> flags;
    byte unused[2];
    s_camo_mapping active_camo_amount;
    s_camo_mapping shadow_amount;
};
static_assert(sizeof(s_camo) == 0x3C);

struct s_camo_mapping
{
    string_id input_variable;
    string_id range_variable;
    s_tag_function mapping;
};
static_assert(sizeof(s_camo_mapping) == 0x1C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

