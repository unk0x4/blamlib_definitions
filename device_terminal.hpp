/* ---------- enums */

enum e_terminal_terminal_screen_dialect_type_flags
{
    _terminal_terminal_screen_dialect_type_flags_military_bit = 0,
    _terminal_terminal_screen_dialect_type_flags_public_bit = 1,
    _terminal_terminal_screen_dialect_type_flags_private_bit = 2,
    _terminal_terminal_screen_dialect_type_flags_librarian_bit = 3,
    _terminal_terminal_screen_dialect_type_flags_didact_bit = 4,
    _terminal_terminal_screen_dialect_type_flags_mendicant_bit = 5,
    _terminal_terminal_screen_dialect_type_flags_offensive_bit = 6,
    _terminal_terminal_screen_dialect_type_flags_gravemind_bit = 7,
    _terminal_terminal_screen_dialect_type_flags_fleet_bit = 8,
    _terminal_terminal_screen_dialect_type_flags_spark_bit = 9
};

enum e_terminal_terminal_screen_terminal_image
{
    _terminal_terminal_screen_terminal_image_none = -1,
    _terminal_terminal_screen_terminal_image_image1 = 0,
    _terminal_terminal_screen_terminal_image_image2 = 1,
    _terminal_terminal_screen_terminal_image_image3 = 2,
    _terminal_terminal_screen_terminal_image_image4 = 3,
    _terminal_terminal_screen_terminal_image_image5 = 4,
    _terminal_terminal_screen_terminal_image_image6 = 5,
    _terminal_terminal_screen_terminal_image_image7 = 6,
    _terminal_terminal_screen_terminal_image_image8 = 7
};


/* ---------- structures */

struct s_terminal_terminal_screen;
struct s_terminal;

struct s_terminal : s_device
{
    byte unused1[4];
    string_id action_string;
    short terminal_index;
    byte unused2[2];
    float terminal_exposure;
    s_tag_reference activation_sound;
    s_tag_reference deactivation_sound;
    s_tag_reference translate_sound1;
    s_tag_reference translate_sound2;
    s_tag_reference error_sound;
    s_terminal_terminal_screen easy_screen;
    s_terminal_terminal_screen normal_screen;
    s_terminal_terminal_screen heroic_screen;
    s_terminal_terminal_screen legendary_screen;
};
static_assert(sizeof(s_terminal) == 0x2F8);

struct s_terminal_terminal_screen
{
    s_tag_reference dummy_strings;
    s_tag_reference story_strings;
    c_flags<e_terminal_terminal_screen_dialect_type_flags, short> dummy_dialect_types;
    c_flags<e_terminal_terminal_screen_dialect_type_flags, short> story_dialect_types;
    c_enum<e_terminal_terminal_screen_terminal_image, short> dummy_blf_image;
    c_enum<e_terminal_terminal_screen_terminal_image, short> story_blf_image;
    s_tag_reference error_strings;
};
static_assert(sizeof(s_terminal_terminal_screen) == 0x38);

