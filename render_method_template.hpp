/* ---------- enums */

enum e_entry_point_bit_mask
{
    _entry_point_bit_mask_default_bit = 0,
    _entry_point_bit_mask_albedo_bit = 1,
    _entry_point_bit_mask_static_default_bit = 2,
    _entry_point_bit_mask_static_per_pixel_bit = 3,
    _entry_point_bit_mask_static_per_vertex_bit = 4,
    _entry_point_bit_mask_static_sh_bit = 5,
    _entry_point_bit_mask_static_prt_ambient_bit = 6,
    _entry_point_bit_mask_static_prt_linear_bit = 7,
    _entry_point_bit_mask_static_prt_quadratic_bit = 8,
    _entry_point_bit_mask_dynamic_light_bit = 9,
    _entry_point_bit_mask_shadow_generate_bit = 10,
    _entry_point_bit_mask_shadow_apply_bit = 11,
    _entry_point_bit_mask_active_camo_bit = 12,
    _entry_point_bit_mask_lightmap_debug_mode_bit = 13,
    _entry_point_bit_mask_static_per_vertex_color_bit = 14,
    _entry_point_bit_mask_water_tessellation_bit = 15,
    _entry_point_bit_mask_water_shading_bit = 16,
    _entry_point_bit_mask_dynamic_light_cinematic_bit = 17,
    _entry_point_bit_mask_z_only_bit = 18,
    _entry_point_bit_mask_sfx_distort_bit = 19
};


/* ---------- structures */

struct s_render_method_template_tag_block_index;
struct s_render_method_template_parameter_table;
struct s_render_method_template_parameter_mapping;
struct s_render_method_template_shader_argument;
struct s_render_method_template;

struct s_render_method_template
{
    s_tag_reference vertex_shader;
    s_tag_reference pixel_shader;
    c_flags<e_entry_point_bit_mask, long> valid_entry_points;
    c_tag_block<s_render_method_template_tag_block_index> entry_points;
    c_tag_block<s_render_method_template_parameter_table> parameter_tables;
    c_tag_block<s_render_method_template_parameter_mapping> parameters;
    c_tag_block<s_render_method_template_shader_argument> real_parameter_names;
    c_tag_block<s_render_method_template_shader_argument> integer_parameter_names;
    c_tag_block<s_render_method_template_shader_argument> boolean_parameter_names;
    c_tag_block<s_render_method_template_shader_argument> texture_parameter_names;
    byte unused[12];
};
static_assert(sizeof(s_render_method_template) == 0x84);

struct s_render_method_template_shader_argument
{
    string_id name;
};
static_assert(sizeof(s_render_method_template_shader_argument) == 0x4);

struct s_render_method_template_parameter_mapping
{
    ushort register_index;
    uchar argument_index;
    uchar unknown;
};
static_assert(sizeof(s_render_method_template_parameter_mapping) == 0x4);

struct s_render_method_template_parameter_table
{
    tag_block_index values[14];
};
static_assert(sizeof(s_render_method_template_parameter_table) == 0x1C);

struct s_render_method_template_tag_block_index
{
    ushort integer;
};
static_assert(sizeof(s_render_method_template_tag_block_index) == 0x2);

