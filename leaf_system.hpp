/* ---------- enums */

enum e_leaf_system_flag_bits
{
    _leaf_system_flag_bits_collides_structure_bit = 0,
    _leaf_system_flag_bits_collides_objects_bit = 1,
    _leaf_system_flag_bits_collides_water_bit = 2,
    _leaf_system_flag_bits_affected_by_explosions_bit = 3
};


/* ---------- structures */

struct s_leaf_system_leaf_type;
struct s_leaf_system;

struct s_leaf_system
{
    c_flags<e_leaf_system_flag_bits, long> flags;
    string_id marker_attach_name;
    s_tag_reference bitmap_sprite_plate;
    real_bounds emission_rate;
    float lifetime;
    real_bounds wind_bounds;
    float wind_scale;
    float time_scale;
    real_bounds fade_distance;
    float emission_sphrere_radius;
    float movement_cylinder_radius;
    float fade_in_time;
    float fade_out_time;
    c_tag_block<s_leaf_system_leaf_type> leaf_types;
};
static_assert(sizeof(s_leaf_system) == 0x58);

struct s_leaf_system_leaf_type
{
    short bitmap_sprite_index;
    byte unused[2];
    float frequency;
    float mass;
    real_bounds geometry_scale;
    float flitteriness;
    float flitteriness_swing_arm_length;
    float flitteriness_scale;
    float flitteriness_speed;
    float flitteriness_leaves_phase;
    float tumble_scale;
    float rotation_scale;
    float starting_velocity;
    real_bounds air_friction_bounds;
};
static_assert(sizeof(s_leaf_system_leaf_type) == 0x3C);

