/* ---------- enums */


/* ---------- structures */

struct s_user_interface_globals_definition_screen_widget;
struct s_user_interface_globals_definition;

struct s_user_interface_globals_definition
{
    s_tag_reference shared_ui_globals;
    s_tag_reference editable_settings;
    s_tag_reference matchmaking_hopper_strings;
    c_tag_block<s_user_interface_globals_definition_screen_widget> screen_widgets;
    s_tag_reference texture_render_list;
    ulong unknown;
};
static_assert(sizeof(s_user_interface_globals_definition) == 0x50);

struct s_user_interface_globals_definition_screen_widget
{
    s_tag_reference widget;
};
static_assert(sizeof(s_user_interface_globals_definition_screen_widget) == 0x10);

