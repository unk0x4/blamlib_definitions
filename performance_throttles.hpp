/* ---------- enums */


/* ---------- structures */

struct s_performance_throttles_performance_block;
struct s_performance_throttles;

struct s_performance_throttles
{
    c_tag_block<s_performance_throttles_performance_block> performance;
};
static_assert(sizeof(s_performance_throttles) == 0xC);

struct s_performance_throttles_performance_block
{
    ulong flags;
    float water;
    float decorators;
    float effects;
    float instanced_geometry;
    float object_fade;
    float object_lod;
    float decals;
    long cpu_light_count;
    float cpu_light_quality;
    long gpu_light_count;
    float gpu_light_quality;
    long shadow_count;
    float shadow_quality;
};
static_assert(sizeof(s_performance_throttles_performance_block) == 0x38);

