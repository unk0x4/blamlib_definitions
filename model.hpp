/* ---------- enums */

enum e_model_variant_region_permutation_flags
{
    _model_variant_region_permutation_flags_copy_states_to_all_permutations_bit = 0
};

enum e_model_variant_region_permutation_state_property_flags
{
    _model_variant_region_permutation_state_property_flags_blurred_bit = 0,
    _model_variant_region_permutation_state_property_flags_hella_blurred_bit = 1,
    _model_variant_region_permutation_state_property_flags_shielded_bit = 2,
    _model_variant_region_permutation_state_property_flags_bit3_bit = 3,
    _model_variant_region_permutation_state_property_flags_bit4_bit = 4,
    _model_variant_region_permutation_state_property_flags_bit5_bit = 5,
    _model_variant_region_permutation_state_property_flags_bit6_bit = 6,
    _model_variant_region_permutation_state_property_flags_bit7_bit = 7
};

enum e_model_variant_region_permutation_state_state
{
    _model_variant_region_permutation_state_state_default = 0,
    _model_variant_region_permutation_state_state_minor_damage = 1,
    _model_variant_region_permutation_state_state_medium_damage = 2,
    _model_variant_region_permutation_state_state_major_damage = 3,
    _model_variant_region_permutation_state_state_destroyed = 4
};

enum e_model_variant_region_sort_order
{
    _model_variant_region_sort_order_no_sorting = 0,
    _model_variant_region_sort_order_5_closest = 1,
    _model_variant_region_sort_order_4 = 2,
    _model_variant_region_sort_order_3 = 3,
    _model_variant_region_sort_order_2 = 4,
    _model_variant_region_sort_order_1 = 5,
    _model_variant_region_sort_order_0_same_as_model = 6,
    _model_variant_region_sort_order_1_2 = 7,
    _model_variant_region_sort_order_2_2 = 8,
    _model_variant_region_sort_order_3_2 = 9,
    _model_variant_region_sort_order_4_2 = 10,
    _model_variant_region_sort_order_5_farthest = 11
};

enum e_model_instance_group_choice
{
    _model_instance_group_choice_choose_one_member = 0,
    _model_instance_group_choice_choose_all_members = 1
};

enum e_model_material_material_type
{
    _model_material_material_type_dirt = 0,
    _model_material_material_type_sand = 1,
    _model_material_material_type_stone = 2,
    _model_material_material_type_snow = 3,
    _model_material_material_type_wood = 4,
    _model_material_material_type_metalhollow = 5,
    _model_material_material_type_metalthin = 6,
    _model_material_material_type_metalthick = 7,
    _model_material_material_type_rubber = 8,
    _model_material_material_type_glass = 9,
    _model_material_material_type_force_field = 10,
    _model_material_material_type_grunt = 11,
    _model_material_material_type_hunter_armor = 12,
    _model_material_material_type_hunter_skin = 13,
    _model_material_material_type_elite = 14,
    _model_material_material_type_jackal = 15,
    _model_material_material_type_jackal_energy_shield = 16,
    _model_material_material_type_engineer_skin = 17,
    _model_material_material_type_engineer_force_field = 18,
    _model_material_material_type_flood_combat_form = 19,
    _model_material_material_type_flood_carrier_form = 20,
    _model_material_material_type_cyborg_armor = 21,
    _model_material_material_type_cyborg_energy_shield = 22,
    _model_material_material_type_human_armor = 23,
    _model_material_material_type_human_skin = 24,
    _model_material_material_type_sentinel = 25,
    _model_material_material_type_monitor = 26,
    _model_material_material_type_plastic = 27,
    _model_material_material_type_water = 28,
    _model_material_material_type_leaves = 29,
    _model_material_material_type_elite_energy_shield = 30,
    _model_material_material_type_ice = 31,
    _model_material_material_type_hunter_shield = 32
};

enum e_model_global_damage_info_block_flags
{
    _model_global_damage_info_block_flags_takes_shield_damage_for_children_bit = 0,
    _model_global_damage_info_block_flags_takes_body_damage_for_children_bit = 1,
    _model_global_damage_info_block_flags_always_shields_friendly_damage_bit = 2,
    _model_global_damage_info_block_flags_passes_area_damage_to_children_bit = 3,
    _model_global_damage_info_block_flags_parent_never_takes_body_damage_for_children_bit = 4,
    _model_global_damage_info_block_flags_only_damaged_by_explosives_bit = 5,
    _model_global_damage_info_block_flags_parent_never_takes_shield_damage_for_children_bit = 6,
    _model_global_damage_info_block_flags_cannot_die_from_damage_bit = 7,
    _model_global_damage_info_block_flags_passes_attached_damage_to_riders_bit = 8,
    _model_global_damage_info_block_flags_shield_depletion_is_permanent_bit = 9,
    _model_global_damage_info_block_flags_shield_depletion_force_hard_ping_bit = 10,
    _model_global_damage_info_block_flags_ai_do_not_damage_without_player_bit = 11,
    _model_global_damage_info_block_flags_health_regrows_while_dead_bit = 12,
    _model_global_damage_info_block_flags_shield_recharge_plays_only_when_empty_bit = 13,
    _model_global_damage_info_block_flags_ignore_force_minimum_transfer_bit = 14,
    _model_global_damage_info_block_flags_orphan_from_postprocess_autogen_bit = 15,
    _model_global_damage_info_block_flags_only_damaged_by_boarding_damage_bit = 16
};

enum e_damage_reporting_type
{
    _damage_reporting_type_guardians_unknown = 0,
    _damage_reporting_type_guardians = 1,
    _damage_reporting_type_falling_damage = 2,
    _damage_reporting_type_generic_collision = 3,
    _damage_reporting_type_armor_lock_crush = 4,
    _damage_reporting_type_generic_melee = 5,
    _damage_reporting_type_generic_explosion = 6,
    _damage_reporting_type_magnum = 7,
    _damage_reporting_type_plasma_pistol = 8,
    _damage_reporting_type_needler = 9,
    _damage_reporting_type_mauler = 10,
    _damage_reporting_type_smg = 11,
    _damage_reporting_type_plasma_rifle = 12,
    _damage_reporting_type_battle_rifle = 13,
    _damage_reporting_type_carbine = 14,
    _damage_reporting_type_shotgun = 15,
    _damage_reporting_type_sniper_rifle = 16,
    _damage_reporting_type_beam_rifle = 17,
    _damage_reporting_type_assault_rifle = 18,
    _damage_reporting_type_spiker = 19,
    _damage_reporting_type_fuel_rod_cannon = 20,
    _damage_reporting_type_missile_pod = 21,
    _damage_reporting_type_rocket_launcher = 22,
    _damage_reporting_type_spartan_laser = 23,
    _damage_reporting_type_brute_shot = 24,
    _damage_reporting_type_flamethrower = 25,
    _damage_reporting_type_sentinel_gun = 26,
    _damage_reporting_type_energy_sword = 27,
    _damage_reporting_type_gravity_hammer = 28,
    _damage_reporting_type_frag_grenade = 29,
    _damage_reporting_type_plasma_grenade = 30,
    _damage_reporting_type_spike_grenade = 31,
    _damage_reporting_type_firebomb_grenade = 32,
    _damage_reporting_type_flag = 33,
    _damage_reporting_type_bomb = 34,
    _damage_reporting_type_bomb_explosion = 35,
    _damage_reporting_type_ball = 36,
    _damage_reporting_type_machinegun_turret = 37,
    _damage_reporting_type_plasma_cannon = 38,
    _damage_reporting_type_plasma_mortar = 39,
    _damage_reporting_type_plasma_turret = 40,
    _damage_reporting_type_shade_turret = 41,
    _damage_reporting_type_banshee = 42,
    _damage_reporting_type_ghost = 43,
    _damage_reporting_type_mongoose = 44,
    _damage_reporting_type_scorpion = 45,
    _damage_reporting_type_scorpion_gunner = 46,
    _damage_reporting_type_spectre = 47,
    _damage_reporting_type_spectre_gunner = 48,
    _damage_reporting_type_warthog = 49,
    _damage_reporting_type_warthog_gunner = 50,
    _damage_reporting_type_warthog_gauss_turret = 51,
    _damage_reporting_type_wraith = 52,
    _damage_reporting_type_wraith_gunner = 53,
    _damage_reporting_type_tank = 54,
    _damage_reporting_type_chopper = 55,
    _damage_reporting_type_hornet = 56,
    _damage_reporting_type_mantis = 57,
    _damage_reporting_type_prowler = 58,
    _damage_reporting_type_sentinel_beam = 59,
    _damage_reporting_type_sentinel_rpg = 60,
    _damage_reporting_type_teleporter = 61,
    _damage_reporting_type_tripmine = 62,
    _damage_reporting_type_dmr = 63
};

enum e_model_global_damage_info_block_damage_section_flags
{
    _model_global_damage_info_block_damage_section_flags_absorbs_body_damage_bit = 0,
    _model_global_damage_info_block_damage_section_flags_takes_full_damage_when_object_dies_bit = 1,
    _model_global_damage_info_block_damage_section_flags_cannot_die_with_riders_bit = 2,
    _model_global_damage_info_block_damage_section_flags_takes_full_damage_when_object_destroyed_bit = 3,
    _model_global_damage_info_block_damage_section_flags_restored_on_ressurection_bit = 4,
    _model_global_damage_info_block_damage_section_flags_unused5_bit = 5,
    _model_global_damage_info_block_damage_section_flags_unused6_bit = 6,
    _model_global_damage_info_block_damage_section_flags_headshotable_bit = 7,
    _model_global_damage_info_block_damage_section_flags_ignores_shields_bit = 8,
    _model_global_damage_info_block_damage_section_flags_takes_full_damage_when_shield_depleted_bit = 9,
    _model_global_damage_info_block_damage_section_flags_networked_bit = 10,
    _model_global_damage_info_block_damage_section_flags_allow_damage_response_overflow_bit = 11
};

enum e_model_global_damage_info_block_damage_section_instant_response_response_type
{
    _model_global_damage_info_block_damage_section_instant_response_response_type_recieves_all_damage = 0,
    _model_global_damage_info_block_damage_section_instant_response_response_type_recieves_area_effect_damage = 1,
    _model_global_damage_info_block_damage_section_instant_response_response_type_recieves_local_damage = 2
};

enum e_model_global_damage_info_block_damage_section_instant_response_constraint_damage_type
{
    _model_global_damage_info_block_damage_section_instant_response_constraint_damage_type_none = 0,
    _model_global_damage_info_block_damage_section_instant_response_constraint_damage_type_destroy_one_of_group = 1,
    _model_global_damage_info_block_damage_section_instant_response_constraint_damage_type_destroy_entire_group = 2,
    _model_global_damage_info_block_damage_section_instant_response_constraint_damage_type_loosen_one_of_group = 3,
    _model_global_damage_info_block_damage_section_instant_response_constraint_damage_type_loosen_entire_group = 4
};

enum e_model_global_damage_info_block_damage_section_instant_response_flags
{
    _model_global_damage_info_block_damage_section_instant_response_flags_kills_object_bit = 0,
    _model_global_damage_info_block_damage_section_instant_response_flags_inhibits_melee_attack_bit = 1,
    _model_global_damage_info_block_damage_section_instant_response_flags_inhibits_weapon_attack_bit = 2,
    _model_global_damage_info_block_damage_section_instant_response_flags_inhibits_walking_bit = 3,
    _model_global_damage_info_block_damage_section_instant_response_flags_forces_drop_weapon_bit = 4,
    _model_global_damage_info_block_damage_section_instant_response_flags_kills_weapon_primary_trigger_bit = 5,
    _model_global_damage_info_block_damage_section_instant_response_flags_kills_weapon_secondary_trigger_bit = 6,
    _model_global_damage_info_block_damage_section_instant_response_flags_destroys_object_bit = 7,
    _model_global_damage_info_block_damage_section_instant_response_flags_damages_weapon_primary_trigger_bit = 8,
    _model_global_damage_info_block_damage_section_instant_response_flags_damages_weapon_secondary_trigger_bit = 9,
    _model_global_damage_info_block_damage_section_instant_response_flags_light_damage_left_turn_bit = 10,
    _model_global_damage_info_block_damage_section_instant_response_flags_major_damage_left_turn_bit = 11,
    _model_global_damage_info_block_damage_section_instant_response_flags_light_damage_right_turn_bit = 12,
    _model_global_damage_info_block_damage_section_instant_response_flags_major_damage_right_turn_bit = 13,
    _model_global_damage_info_block_damage_section_instant_response_flags_light_damage_engine_bit = 14,
    _model_global_damage_info_block_damage_section_instant_response_flags_major_damage_engine_bit = 15,
    _model_global_damage_info_block_damage_section_instant_response_flags_kills_object_no_player_solo_bit = 16,
    _model_global_damage_info_block_damage_section_instant_response_flags_causes_detonation_bit = 17,
    _model_global_damage_info_block_damage_section_instant_response_flags_fires_on_creation_bit = 18,
    _model_global_damage_info_block_damage_section_instant_response_flags_kills_variant_objects_bit = 19,
    _model_global_damage_info_block_damage_section_instant_response_flags_force_unattached_effects_bit = 20,
    _model_global_damage_info_block_damage_section_instant_response_flags_fires_under_threshold_bit = 21,
    _model_global_damage_info_block_damage_section_instant_response_flags_triggers_special_death_bit = 22,
    _model_global_damage_info_block_damage_section_instant_response_flags_only_on_special_death_bit = 23,
    _model_global_damage_info_block_damage_section_instant_response_flags_only_not_on_special_death_bit = 24,
    _model_global_damage_info_block_damage_section_instant_response_flags_buckles_giants_bit = 25,
    _model_global_damage_info_block_damage_section_instant_response_flags_causes_sp_detonation_bit = 26,
    _model_global_damage_info_block_damage_section_instant_response_flags_skip_sounds_on_generic_effect_bit = 27,
    _model_global_damage_info_block_damage_section_instant_response_flags_kills_giants_bit = 28,
    _model_global_damage_info_block_damage_section_instant_response_flags_skip_sounds_on_special_death_bit = 29,
    _model_global_damage_info_block_damage_section_instant_response_flags_cause_head_dismemberment_bit = 30,
    _model_global_damage_info_block_damage_section_instant_response_flags_cause_left_leg_dismemberment_bit = 31
};

enum e_model_global_damage_info_block_damage_section_instant_response_new_state
{
    _model_global_damage_info_block_damage_section_instant_response_new_state_default = 0,
    _model_global_damage_info_block_damage_section_instant_response_new_state_minor_damage = 1,
    _model_global_damage_info_block_damage_section_instant_response_new_state_medium_damage = 2,
    _model_global_damage_info_block_damage_section_instant_response_new_state_major_damage = 3,
    _model_global_damage_info_block_damage_section_instant_response_new_state_destroyed = 4
};

enum e_model_global_damage_info_block_damage_section_instant_response_custom_response_behavior
{
    _model_global_damage_info_block_damage_section_instant_response_custom_response_behavior_plays_always = 0,
    _model_global_damage_info_block_damage_section_instant_response_custom_response_behavior_plays_if_labels_match = 1,
    _model_global_damage_info_block_damage_section_instant_response_custom_response_behavior_plays_if_labels_differ = 2
};

enum e_model_global_damage_info_block_damage_constraint_type
{
    _model_global_damage_info_block_damage_constraint_type_hinge = 0,
    _model_global_damage_info_block_damage_constraint_type_limited_hinge = 1,
    _model_global_damage_info_block_damage_constraint_type_ragdoll = 2,
    _model_global_damage_info_block_damage_constraint_type_stiff_spring = 3,
    _model_global_damage_info_block_damage_constraint_type_ball_and_socket = 4,
    _model_global_damage_info_block_damage_constraint_type_prismatic = 5
};

enum e_model_target_byte_flags
{
    _model_target_byte_flags_aoe_top_level_bit = 0,
    _model_target_byte_flags_aoe_test_obstruction_bit = 1,
    _model_target_byte_flags_shows_tracking_reticle_bit = 2
};

enum e_model_target_flags
{
    _model_target_flags_locked_by_human_tracking_bit = 0,
    _model_target_flags_locked_by_plasma_tracking_bit = 1,
    _model_target_flags_headshot_bit = 2,
    _model_target_flags_bit3_bit = 3,
    _model_target_flags_vulnerable_bit = 4,
    _model_target_flags_bit5_bit = 5,
    _model_target_flags_always_locked_by_plasma_tracking_bit = 6,
    _model_target_flags_bit7_bit = 7,
    _model_target_flags_bit8_bit = 8,
    _model_target_flags_bit9_bit = 9,
    _model_target_flags_bit10_bit = 10,
    _model_target_flags_bit11_bit = 11,
    _model_target_flags_bit12_bit = 12,
    _model_target_flags_bit13_bit = 13,
    _model_target_flags_bit14_bit = 14,
    _model_target_flags_bit15_bit = 15,
    _model_target_flags_bit16_bit = 16,
    _model_target_flags_bit17_bit = 17,
    _model_target_flags_bit18_bit = 18,
    _model_target_flags_bit19_bit = 19,
    _model_target_flags_bit20_bit = 20,
    _model_target_flags_bit21_bit = 21,
    _model_target_flags_bit22_bit = 22,
    _model_target_flags_bit23_bit = 23,
    _model_target_flags_bit24_bit = 24,
    _model_target_flags_bit25_bit = 25,
    _model_target_flags_bit26_bit = 26,
    _model_target_flags_bit27_bit = 27,
    _model_target_flags_bit28_bit = 28,
    _model_target_flags_bit29_bit = 29,
    _model_target_flags_bit30_bit = 30,
    _model_target_flags_bit31_bit = 31
};

enum e_model_collision_region_permutation_flags
{
    _model_collision_region_permutation_flags_cannot_be_chosen_randomly_bit = 0,
    _model_collision_region_permutation_flags_bit1_bit = 1,
    _model_collision_region_permutation_flags_bit2_bit = 2,
    _model_collision_region_permutation_flags_bit3_bit = 3,
    _model_collision_region_permutation_flags_bit4_bit = 4,
    _model_collision_region_permutation_flags_bit5_bit = 5,
    _model_collision_region_permutation_flags_bit6_bit = 6,
    _model_collision_region_permutation_flags_bit7_bit = 7
};

enum e_model_model_object_datum_type
{
    _model_model_object_datum_type_not_set = 0,
    _model_model_object_datum_type_user_defined = 1,
    _model_model_object_datum_type_auto_generated = 2
};

enum e_model_flags
{
    _model_flags_active_camo_always_on_bit = 0,
    _model_flags_active_camo_always_merge_bit = 1,
    _model_flags_active_camo_never_merge_bit = 2,
    _model_flags_inconsequential_target_bit = 3,
    _model_flags_locked_precomputed_probes_bit = 4,
    _model_flags_model_is_big_battle_object_bit = 5,
    _model_flags_model_never_uses_compressed_vertex_positions_bit = 6,
    _model_flags_model_is_invisible_even_attachments_bit = 7,
    _model_flags_bit8_bit = 8,
    _model_flags_bit9_bit = 9,
    _model_flags_bit10_bit = 10,
    _model_flags_bit11_bit = 11,
    _model_flags_bit12_bit = 12,
    _model_flags_bit13_bit = 13,
    _model_flags_bit14_bit = 14,
    _model_flags_bit15_bit = 15,
    _model_flags_bit16_bit = 16,
    _model_flags_bit17_bit = 17,
    _model_flags_bit18_bit = 18,
    _model_flags_bit19_bit = 19,
    _model_flags_bit20_bit = 20,
    _model_flags_bit21_bit = 21,
    _model_flags_bit22_bit = 22,
    _model_flags_bit23_bit = 23,
    _model_flags_bit24_bit = 24,
    _model_flags_bit25_bit = 25,
    _model_flags_bit26_bit = 26,
    _model_flags_bit27_bit = 27,
    _model_flags_bit28_bit = 28,
    _model_flags_bit29_bit = 29,
    _model_flags_bit30_bit = 30,
    _model_flags_bit31_bit = 31
};

enum e_model_runtime_flags
{
    _model_runtime_flags_contains_runtime_nodes_bit = 0,
    _model_runtime_flags_bit1_bit = 1,
    _model_runtime_flags_bit2_bit = 2,
    _model_runtime_flags_bit3_bit = 3,
    _model_runtime_flags_bit4_bit = 4,
    _model_runtime_flags_bit5_bit = 5,
    _model_runtime_flags_bit6_bit = 6,
    _model_runtime_flags_bit7_bit = 7,
    _model_runtime_flags_bit8_bit = 8,
    _model_runtime_flags_bit9_bit = 9,
    _model_runtime_flags_bit10_bit = 10,
    _model_runtime_flags_bit11_bit = 11,
    _model_runtime_flags_bit12_bit = 12,
    _model_runtime_flags_bit13_bit = 13,
    _model_runtime_flags_bit14_bit = 14,
    _model_runtime_flags_bit15_bit = 15,
    _model_runtime_flags_bit16_bit = 16,
    _model_runtime_flags_bit17_bit = 17,
    _model_runtime_flags_bit18_bit = 18,
    _model_runtime_flags_bit19_bit = 19,
    _model_runtime_flags_bit20_bit = 20,
    _model_runtime_flags_bit21_bit = 21,
    _model_runtime_flags_bit22_bit = 22,
    _model_runtime_flags_bit23_bit = 23,
    _model_runtime_flags_bit24_bit = 24,
    _model_runtime_flags_bit25_bit = 25,
    _model_runtime_flags_bit26_bit = 26,
    _model_runtime_flags_bit27_bit = 27,
    _model_runtime_flags_bit28_bit = 28,
    _model_runtime_flags_bit29_bit = 29,
    _model_runtime_flags_bit30_bit = 30,
    _model_runtime_flags_bit31_bit = 31
};

enum e_model_prt_shadow_detail
{
    _model_prt_shadow_detail_ambient_occlusion = 0,
    _model_prt_shadow_detail_linear = 1,
    _model_prt_shadow_detail_quadratic = 2
};

enum e_model_prt_shadow_bounces
{
    _model_prt_shadow_bounces_zero_bounces = 0,
    _model_prt_shadow_bounces_one_bounce = 1,
    _model_prt_shadow_bounces_two_bounces = 2,
    _model_prt_shadow_bounces_three_bounces = 3
};

enum e_model_shadow_receive_override_shadow_types
{
    _model_shadow_receive_override_shadow_types_prt_shadows_from_all_regions = 0,
    _model_shadow_receive_override_shadow_types_prt_self_shadow_only = 1,
    _model_shadow_receive_override_shadow_types_no_prt_shadows_at_all = 2
};


/* ---------- structures */

struct s_model_variant_region_permutation_state;
struct s_model_variant_region_permutation;
struct s_model_variant_region;
struct s_model_variant_object;
struct s_model_variant;
struct s_model_region_name;
struct s_model_instance_group_instance_member;
struct s_model_instance_group;
struct s_model_material;
struct s_damage_reporting_type;
struct s_model_global_damage_info_block_damage_section_instant_response;
struct s_model_global_damage_info_block_damage_section;
struct s_model_global_damage_info_block_node;
struct s_model_global_damage_info_block_damage_seat_region_specific_damage_block;
struct s_model_global_damage_info_block_damage_seat;
struct s_model_global_damage_info_block_damage_constraint;
struct s_model_global_damage_info_block;
struct s_model_target;
struct s_model_collision_region_permutation;
struct s_model_collision_region;
struct s_model_node;
struct s_model_model_object_datum;
struct s_model_scenario_load_parameters_block;
struct s_model_shadow_cast_override;
struct s_model_shadow_receive_override;
struct s_model_occlusion_sphere;
struct s_model;

struct s_model
{
    s_tag_reference render_model;
    s_tag_reference collision_model;
    s_tag_reference animation;
    s_tag_reference physics_model;
    float reduce_to_l_1_super_low;
    float reduce_to_l_2_low;
    float reduce_to_l_3_medium;
    float reduce_to_l_4_high;
    float reduce_to_l_5_super_high;
    s_tag_reference lod_model;
    c_tag_block<s_model_variant> variants;
    c_tag_block<s_model_region_name> region_sort;
    c_tag_block<s_model_instance_group> instance_groups;
    c_tag_block<s_model_material> materials;
    c_tag_block<s_model_global_damage_info_block> new_damage_info;
    c_tag_block<s_model_target> targets;
    c_tag_block<s_model_collision_region> collision_regions;
    c_tag_block<s_model_node> nodes;
    long runtime_node_list_checksum;
    c_tag_block<s_model_model_object_datum> model_object_data;
    s_tag_reference primary_dialogue;
    s_tag_reference secondary_dialogue;
    c_flags<e_model_flags, long> flags;
    string_id default_dialogue_effect;
    int32 render_only_node_flags[8];
    int32 render_only_section_flags[8];
    c_flags<e_model_runtime_flags, long> runtime_flags;
    c_tag_block<s_model_scenario_load_parameters_block> scenario_load_parameters;
    c_enum<e_model_prt_shadow_detail, uchar> shadow_detail;
    c_enum<e_model_prt_shadow_bounces, uchar> shadow_bounces;
    byte pad[2];
    c_tag_block<s_model_shadow_cast_override> shadow_cast_overrides;
    c_tag_block<s_model_shadow_receive_override> shadow_receive_overrides;
    c_tag_block<s_model_occlusion_sphere> occlusion_spheres;
    s_tag_reference shield_impact_third_person;
    s_tag_reference shield_impact_first_person;
    s_tag_reference overshield_third_person;
    s_tag_reference overshield_first_person;
};
static_assert(sizeof(s_model) == 0x1B4);

struct s_model_occlusion_sphere
{
    string_id marker1_name;
    ulong marker1_index;
    string_id marker2_name;
    ulong marker2_index;
    float radius;
};
static_assert(sizeof(s_model_occlusion_sphere) == 0x14);

struct s_model_shadow_receive_override
{
    string_id region;
    c_enum<e_model_shadow_receive_override_shadow_types, long> shadow_type;
};
static_assert(sizeof(s_model_shadow_receive_override) == 0x8);

struct s_model_shadow_cast_override
{
    string_id region;
    string_id permutation;
};
static_assert(sizeof(s_model_shadow_cast_override) == 0x8);

struct s_model_scenario_load_parameters_block
{
    s_tag_reference scenario;
    s_tag_data data;
    byte unused[32];
};
static_assert(sizeof(s_model_scenario_load_parameters_block) == 0x44);

struct s_model_model_object_datum
{
    c_enum<e_model_model_object_datum_type, short> type;
    byte padding[2];
    real_point3d offset;
    float radius;
};
static_assert(sizeof(s_model_model_object_datum) == 0x14);

struct s_model_node
{
    string_id name;
    short parent_node;
    short first_child_node;
    short next_sibling_node;
    short import_node_index;
    real_point3d default_translation;
    real_quaternion default_rotation;
    float default_scale;
    real_matrix4_x3 inverse;
};
static_assert(sizeof(s_model_node) == 0x5C);

struct s_model_collision_region
{
    string_id name;
    char collision_region_index;
    char physics_region_index;
    byte pad1[2];
    c_tag_block<s_model_collision_region_permutation> permutations;
};
static_assert(sizeof(s_model_collision_region) == 0x14);

struct s_model_collision_region_permutation
{
    string_id name;
    c_flags<e_model_collision_region_permutation_flags, uchar> flags;
    char collision_permutation_index;
    char physics_permutation_index;
    byte pad[1];
};
static_assert(sizeof(s_model_collision_region_permutation) == 0x8);

struct s_model_target
{
    c_flags<e_model_target_byte_flags, uchar> flags1;
    byte pad[3];
    string_id marker_name;
    float size;
    real cone_angle;
    short damage_section;
    short variant;
    float targeting_relevance;
    float aoe_exclusion_radius;
    c_flags<e_model_target_flags, long> flags;
    float lock_on_distance;
    string_id target_filter;
};
static_assert(sizeof(s_model_target) == 0x28);

struct s_model_global_damage_info_block
{
    c_flags<e_model_global_damage_info_block_flags, long> flags;
    string_id global_indirect_material_name;
    short indirect_damage_section;
    byte unused1[6];
    s_damage_reporting_type collision_damage_reporting_type;
    s_damage_reporting_type response_damage_reporting_type;
    short unused2;
    byte unused5[20];
    float max_vitality;
    float min_stun_damage;
    float stun_time;
    float recharge_time;
    float recharge_fraction;
    byte unused6[64];
    float max_shield_vitality;
    string_id global_shield_material_name;
    float min_stun_damage2;
    float shield_stun_time;
    float shield_recharge_time;
    float shield_damaged_threshold;
    s_tag_reference shield_damaged_effect;
    s_tag_reference shield_depleted_effect;
    s_tag_reference shield_recharging_effect;
    c_tag_block<s_model_global_damage_info_block_damage_section> damage_sections;
    c_tag_block<s_model_global_damage_info_block_node> nodes;
    short global_shield_material_index;
    short global_indirect_material_index;
    float runtime_shield_recharge_velocity;
    float runtime_health_recharge_velocity;
    c_tag_block<s_model_global_damage_info_block_damage_seat> damage_seats;
    c_tag_block<s_model_global_damage_info_block_damage_constraint> damage_constraints;
};
static_assert(sizeof(s_model_global_damage_info_block) == 0x100);

struct s_model_global_damage_info_block_damage_constraint
{
    string_id physics_model_constraint_name;
    string_id damage_constraint_name;
    string_id damage_constraint_group_name;
    float group_probability_scale;
    c_enum<e_model_global_damage_info_block_damage_constraint_type, short> type;
    short index;
};
static_assert(sizeof(s_model_global_damage_info_block_damage_constraint) == 0x14);

struct s_model_global_damage_info_block_damage_seat
{
    string_id seat_label;
    float direct_damage_scale;
    float damage_transfer_fall_off_radius;
    float maximum_transfer_damage_scale;
    float minimum_transfer_damage_scale;
    c_tag_block<s_model_global_damage_info_block_damage_seat_region_specific_damage_block> region_specific_damage;
};
static_assert(sizeof(s_model_global_damage_info_block_damage_seat) == 0x20);

struct s_model_global_damage_info_block_damage_seat_region_specific_damage_block
{
    string_id damage_region_name;
    short runtime_damage_region_index;
    short unused;
    float direct_damage_scale_minor;
    float max_transfer_scale_minor;
    float min_transfer_scale_minor;
    float direct_damage_scale_medium;
    float max_transfer_scale_medium;
    float min_transfer_scale_medium;
    float direct_damage_scale_major;
    float max_transfer_scale_major;
    float min_transfer_scale_major;
};
static_assert(sizeof(s_model_global_damage_info_block_damage_seat_region_specific_damage_block) == 0x2C);

struct s_model_global_damage_info_block_node
{
    short runtime_damage_part;
    byte unused1[14];
};
static_assert(sizeof(s_model_global_damage_info_block_node) == 0x10);

struct s_model_global_damage_info_block_damage_section
{
    string_id name;
    c_flags<e_model_global_damage_info_block_damage_section_flags, long> flags;
    float vitality_percentage;
    c_tag_block<s_model_global_damage_info_block_damage_section_instant_response> instant_responses;
    byte unused1[24];
    float stun_time;
    float recharge_time;
    float runtime_recharge_velocity;
    string_id resurrection_region_name;
    short ressurection_region_runtime_index;
    byte unused2[2];
};
static_assert(sizeof(s_model_global_damage_info_block_damage_section) == 0x44);

struct s_model_global_damage_info_block_damage_section_instant_response
{
    c_enum<e_model_global_damage_info_block_damage_section_instant_response_response_type, short> response_type;
    c_enum<e_model_global_damage_info_block_damage_section_instant_response_constraint_damage_type, short> constraint_damage_type;
    string_id trigger;
    c_flags<e_model_global_damage_info_block_damage_section_instant_response_flags, long> flags;
    float damage_threshold;
    s_tag_reference primary_transition_effect;
    s_tag_reference secondary_transition_effect;
    s_tag_reference transition_damage_effect;
    string_id region;
    c_enum<e_model_global_damage_info_block_damage_section_instant_response_new_state, short> new_state;
    short runtime_region_index;
    string_id secondary_region;
    c_enum<e_model_global_damage_info_block_damage_section_instant_response_new_state, short> secondary_new_state;
    short secondary_runtime_region_index;
    short destroy_instance_group;
    c_enum<e_model_global_damage_info_block_damage_section_instant_response_custom_response_behavior, short> custom_response_behavior;
    string_id custom_response_label;
    string_id effect_marker_name;
    string_id damage_effect_marker_name;
    float response_delay;
    s_tag_reference delay_effect;
    string_id delay_effect_marker_name;
    string_id ejecting_seat_label;
    float skip_fraction;
    string_id destroyed_child_object_marker_name;
    float total_damage_threshold;
};
static_assert(sizeof(s_model_global_damage_info_block_damage_section_instant_response) == 0x88);

struct s_damage_reporting_type
{
    c_enum<e_damage_reporting_type, char> ;
};
static_assert(sizeof(s_damage_reporting_type) == 0x1);

struct s_model_material
{
    string_id name;
    c_enum<e_model_material_material_type, short> material_type;
    short damage_section_index;
    short runtime_collision_material_index;
    short runtime_damager_material_index;
    string_id material_name;
    short global_material_index;
    byte unused[2];
};
static_assert(sizeof(s_model_material) == 0x14);

struct s_model_instance_group
{
    string_id name;
    c_enum<e_model_instance_group_choice, long> choice;
    c_tag_block<s_model_instance_group_instance_member> instance_members;
    float probability;
};
static_assert(sizeof(s_model_instance_group) == 0x18);

struct s_model_instance_group_instance_member
{
    long subgroup_index;
    string_id instance_name;
    float probability;
    long instance_placement_mask1;
    long instance_placement_mask2;
    long instance_placement_mask3;
    long instance_placement_mask4;
};
static_assert(sizeof(s_model_instance_group_instance_member) == 0x1C);

struct s_model_region_name
{
    string_id name;
};
static_assert(sizeof(s_model_region_name) == 0x4);

struct s_model_variant
{
    string_id name;
    s_tag_reference variant_dialogue;
    string_id default_dialog_effect;
    char unknown;
    char unknown2;
    char unknown3;
    char unknown4;
    s_byte model_region_indices[16];
    c_tag_block<s_model_variant_region> regions;
    c_tag_block<s_model_variant_object> objects;
    long instance_group_index;
    ulong unknown6;
    ulong unknown7;
};
static_assert(sizeof(s_model_variant) == 0x50);

struct s_model_variant_object
{
    string_id parent_marker;
    string_id child_marker;
    string_id child_variant;
    s_tag_reference child_object;
};
static_assert(sizeof(s_model_variant_object) == 0x1C);

struct s_model_variant_region
{
    string_id name;
    char render_model_region_index;
    char runtime_flags;
    short parent_variant;
    c_tag_block<s_model_variant_region_permutation> permutations;
    c_enum<e_model_variant_region_sort_order, long> sort_order;
};
static_assert(sizeof(s_model_variant_region) == 0x18);

struct s_model_variant_region_permutation
{
    string_id name;
    char render_model_permutation_index;
    c_flags<e_model_variant_region_permutation_flags, uchar> flags;
    byte pad[2];
    float probability;
    c_tag_block<s_model_variant_region_permutation_state> states;
    s_byte runtime_state_permutation_indices[12];
};
static_assert(sizeof(s_model_variant_region_permutation) == 0x24);

struct s_model_variant_region_permutation_state
{
    string_id name;
    uchar model_permutation_index;
    c_flags<e_model_variant_region_permutation_state_property_flags, uchar> property_flags;
    c_enum<e_model_variant_region_permutation_state_state, short> state2;
    s_tag_reference looping_effect;
    string_id looping_effect_marker_name;
    float initial_probability;
};
static_assert(sizeof(s_model_variant_region_permutation_state) == 0x20);

