/* ---------- enums */

enum e_formation_primitive_flag_bits
{
    _formation_primitive_flag_bits_radial_bit = 0,
    _formation_primitive_flag_bits_leader_bit = 1
};


/* ---------- structures */

struct s_formation_primitive_point;
struct s_formation_primitive;
struct s_formation;

struct s_formation
{
    string_id name;
    c_tag_block<s_formation_primitive> primitives;
    byte unused[8];
};
static_assert(sizeof(s_formation) == 0x18);

struct s_formation_primitive
{
    c_flags<e_formation_primitive_flag_bits, ushort> flags;
    short priority;
    short capacity;
    byte unused[2];
    float distance_forwards;
    float distance_backwards;
    float rank_spacing;
    float file_spacing;
    c_tag_block<s_formation_primitive_point> points;
};
static_assert(sizeof(s_formation_primitive) == 0x24);

struct s_formation_primitive_point
{
    real angle;
    float offset;
};
static_assert(sizeof(s_formation_primitive_point) == 0x8);

