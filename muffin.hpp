/* ---------- enums */


/* ---------- structures */

struct s_muffin_locations_block;
struct s_tag_function;
struct s_muffin_unknown_block;
struct s_muffin;

struct s_muffin
{
    s_tag_reference render_model;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    long unknown4;
    c_tag_block<s_muffin_locations_block> locations;
    c_tag_block<s_muffin_unknown_block> unknown5;
};
static_assert(sizeof(s_muffin) == 0x38);

struct s_muffin_unknown_block
{
    short unknown;
    short unknown2;
    ulong unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    s_tag_function unknown7;
    float unknown8;
    s_tag_function unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    s_tag_reference effect;
};
static_assert(sizeof(s_muffin_unknown_block) == 0x70);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_muffin_locations_block
{
    string_id name;
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_muffin_locations_block) == 0x8);

