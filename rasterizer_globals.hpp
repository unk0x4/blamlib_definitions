/* ---------- enums */

enum e_rasterizer_globals_default_bitmap_default_bitmap_flags
{
    _rasterizer_globals_default_bitmap_default_bitmap_flags_do_not_load_bit = 0
};


/* ---------- structures */

struct s_rasterizer_globals_default_bitmap;
struct s_rasterizer_globals_default_rasterizer_bitmap;
struct s_rasterizer_globals_default_shader;
struct s_rasterizer_globals;

struct s_rasterizer_globals
{
    c_tag_block<s_rasterizer_globals_default_bitmap> default_bitmaps;
    c_tag_block<s_rasterizer_globals_default_rasterizer_bitmap> default_rasterizer_bitmaps;
    s_tag_reference vertex_shader_simple;
    s_tag_reference pixel_shader_simple;
    c_tag_block<s_rasterizer_globals_default_shader> default_shaders;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    long unknown4;
    long unknown5;
    s_tag_reference active_camo_distortion;
    s_tag_reference default_performance_template;
    s_tag_reference default_shield_impact;
    s_tag_reference default_vision_mode;
    long unknown6_ho;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
};
static_assert(sizeof(s_rasterizer_globals) == 0xBC);

struct s_rasterizer_globals_default_shader
{
    s_tag_reference vertex_shader;
    s_tag_reference pixel_shader;
};
static_assert(sizeof(s_rasterizer_globals_default_shader) == 0x20);

struct s_rasterizer_globals_default_rasterizer_bitmap
{
    s_tag_reference bitmap;
};
static_assert(sizeof(s_rasterizer_globals_default_rasterizer_bitmap) == 0x10);

struct s_rasterizer_globals_default_bitmap
{
    c_flags<e_rasterizer_globals_default_bitmap_default_bitmap_flags, long> flags;
    s_tag_reference bitmap;
};
static_assert(sizeof(s_rasterizer_globals_default_bitmap) == 0x14);

