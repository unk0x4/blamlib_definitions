/* ---------- enums */


/* ---------- structures */

struct s_gui_widget_animation_definition;

struct s_gui_widget_animation_definition
{
    string_id stringid;
    ulong unknown2;
    s_tag_reference widget_color;
    s_tag_reference widget_position;
    s_tag_reference widget_rotation;
    s_tag_reference widget_scale;
    s_tag_reference widget_texture_coordinate;
    s_tag_reference widget_sprite;
    s_tag_reference widget_font;
    ulong unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_gui_widget_animation_definition) == 0x80);

