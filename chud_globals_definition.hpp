/* ---------- enums */

enum e_chud_globals_definition_hud_global_biped
{
    _chud_globals_definition_hud_global_biped_spartan = 0,
    _chud_globals_definition_hud_global_biped_elite = 1,
    _chud_globals_definition_hud_global_biped_monitor = 2
};

enum e_chud_globals_definition_hud_global_hud_attribute_resolution_flag
{
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_wide_full_bit = 0,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_wide_half_bit = 1,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_native_full_bit = 2,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_standard_full_bit = 3,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_wide_quarter_bit = 4,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_standard_half_bit = 5,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_native_quarter_bit = 6,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_standard_quarter_bit = 7,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit8_bit = 8,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit9_bit = 9,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit10_bit = 10,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit11_bit = 11,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit12_bit = 12,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit13_bit = 13,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit14_bit = 14,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit15_bit = 15,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit16_bit = 16,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit17_bit = 17,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit18_bit = 18,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit19_bit = 19,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit20_bit = 20,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit21_bit = 21,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit22_bit = 22,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit23_bit = 23,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit24_bit = 24,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit25_bit = 25,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit26_bit = 26,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit27_bit = 27,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit28_bit = 28,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit29_bit = 29,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit30_bit = 30,
    _chud_globals_definition_hud_global_hud_attribute_resolution_flag_bit31_bit = 31
};

enum e_chud_globals_definition_hud_global_hud_sound_latched_to_values
{
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_recharging_bit = 0,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_minor_damage_bit = 1,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_major_damage_bit = 2,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_critical_damage_bit = 3,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_minor_state_bit = 4,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_low_bit = 5,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_health_empty_bit = 6,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_recharging_bit = 7,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_minor_damage_bit = 8,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_major_damage_bit = 9,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_critical_damage_bit = 10,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_minor_state_bit = 11,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_low_bit = 12,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_shield_empty_bit = 13,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_rocket_locking_bit = 14,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_rocket_locked_bit = 15,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_missile_locking_bit = 16,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_missile_locked_bit = 17,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit18_bit = 18,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit19_bit = 19,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit20_bit = 20,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit21_bit = 21,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_stamina_full_bit = 22,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_stamina_warning_bit = 23,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_stamina_recharge_bit = 24,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit25_bit = 25,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit26_bit = 26,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_bit27_bit = 27,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_tactical_package_error_bit = 28,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_tactical_package_used_bit = 29,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_gain_medal_bit = 30,
    _chud_globals_definition_hud_global_hud_sound_latched_to_values_winning_points_bit = 31
};

enum e_chud_globals_definition_hud_global_hud_sound_biped_data_biped_type_value_ho
{
    _chud_globals_definition_hud_global_hud_sound_biped_data_biped_type_value_ho_spartan = 0,
    _chud_globals_definition_hud_global_hud_sound_biped_data_biped_type_value_ho_elite = 1,
    _chud_globals_definition_hud_global_hud_sound_biped_data_biped_type_value_ho_monitor = 2
};

enum e_chud_definition_hud_widget_placement_datum_anchor
{
    _chud_definition_hud_widget_placement_datum_anchor_top_left = 0,
    _chud_definition_hud_widget_placement_datum_anchor_top_right = 1,
    _chud_definition_hud_widget_placement_datum_anchor_bottom_right = 2,
    _chud_definition_hud_widget_placement_datum_anchor_bottom_left = 3,
    _chud_definition_hud_widget_placement_datum_anchor_center = 4,
    _chud_definition_hud_widget_placement_datum_anchor_top_edge = 5,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_a = 6,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_b = 7,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_c = 8,
    _chud_definition_hud_widget_placement_datum_anchor_grenade_d = 9,
    _chud_definition_hud_widget_placement_datum_anchor_scoreboard_friendly = 10,
    _chud_definition_hud_widget_placement_datum_anchor_scoreboard_enemy = 11,
    _chud_definition_hud_widget_placement_datum_anchor_health_and_shield = 12,
    _chud_definition_hud_widget_placement_datum_anchor_bottom_edge = 13,
    _chud_definition_hud_widget_placement_datum_anchor_equipment_xy = 14,
    _chud_definition_hud_widget_placement_datum_anchor_equipment_y = 15,
    _chud_definition_hud_widget_placement_datum_anchor_unknown2 = 16,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated = 17,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated2 = 18,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated3 = 19,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated4 = 20,
    _chud_definition_hud_widget_placement_datum_anchor_depreciated5 = 21,
    _chud_definition_hud_widget_placement_datum_anchor_notification = 22,
    _chud_definition_hud_widget_placement_datum_anchor_gametype = 23,
    _chud_definition_hud_widget_placement_datum_anchor_unknown4 = 24,
    _chud_definition_hud_widget_placement_datum_anchor_state_right = 25,
    _chud_definition_hud_widget_placement_datum_anchor_state_left = 26,
    _chud_definition_hud_widget_placement_datum_anchor_state_center = 27,
    _chud_definition_hud_widget_placement_datum_anchor_state_center2 = 28,
    _chud_definition_hud_widget_placement_datum_anchor_gametype_friendly = 29,
    _chud_definition_hud_widget_placement_datum_anchor_gametype_enemy = 30,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_top = 31,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player1 = 32,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player2 = 33,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player3 = 34,
    _chud_definition_hud_widget_placement_datum_anchor_metagame_player4 = 35,
    _chud_definition_hud_widget_placement_datum_anchor_theater = 36,
    _chud_definition_hud_widget_placement_datum_anchor_prompt = 37,
    _chud_definition_hud_widget_placement_datum_anchor_hologram_target = 38,
    _chud_definition_hud_widget_placement_datum_anchor_medals = 39,
    _chud_definition_hud_widget_placement_datum_anchor_survival_medals = 40,
    _chud_definition_hud_widget_placement_datum_anchor_unknown_h_o_offset3 = 41,
    _chud_definition_hud_widget_placement_datum_anchor_motion_sensor = 42
};


/* ---------- structures */

struct s_chud_globals_definition_hud_global_hud_attribute;
struct s_chud_globals_definition_hud_global_hud_sound_biped_data;
struct s_chud_globals_definition_hud_global_hud_sound;
struct s_chud_globals_definition_hud_global_multiplayer_medal;
struct s_chud_globals_definition_hud_global;
struct s_chud_globals_definition_hud_shader;
struct s_chud_globals_definition_unknown_block;
struct s_chud_globals_definition_cortana_suck_block_levels_block;
struct s_chud_globals_definition_cortana_suck_block;
struct s_chud_globals_definition_player_training_datum;
struct s_tag_function;
struct s_chud_globals_definition;

struct s_chud_globals_definition
{
    c_tag_block<s_chud_globals_definition_hud_global> hud_globals;
    c_tag_block<s_chud_globals_definition_hud_shader> hud_shaders;
    c_tag_block<s_chud_globals_definition_unknown_block> unknown_block40;
    c_tag_block<s_chud_globals_definition_cortana_suck_block> cortana_suck;
    c_tag_block<s_chud_globals_definition_player_training_datum> player_training_data;
    s_tag_reference start_menu_emblems;
    s_tag_reference campaign_medals;
    s_tag_reference campaign_medal_hud_animation;
    c_enum<e_chud_definition_hud_widget_placement_datum_anchor, short> campaign_medal_chud_anchor;
    byte post_anchor_padding[2];
    float campaign_medal_scale;
    float campaign_medal_spacing;
    float campaign_medal_offset_x;
    float campaign_medal_offset_y;
    float metagame_scoreboard_top_y;
    float metagame_scoreboard_spacing;
    s_tag_reference unit_damage_grid;
    float micro_texture_tile_amount;
    float medium_sensor_blip_scale;
    float small_sensor_blip_scale;
    float large_sensor_blip_scale;
    float sensor_blip_glow_amount;
    float sensor_blip_glow_radius;
    float sensor_blip_glow_opacity;
    s_tag_reference motion_sensor_blip;
    s_tag_reference birthday_party_effect;
    s_tag_reference campaign_flood_mask;
    s_tag_reference campaign_flood_mask_tile;
    float motion_sensor_blip_height_modifier;
    float shield_minor_threshold;
    float shield_major_threshold;
    float shield_critical_threshold;
    float health_minor_threshold;
    float health_major_threshold;
    float health_critical_threshold;
    float unknown12;
    float unknown13;
    float unknown13_2;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
    float unknown25;
    float unknown26;
    s_tag_function damage_mask_function1;
    real_rgb_color damage_mask_color_overlay;
    float damage_mask_color_overlay_alpha;
    float damage_mask_opacity_red;
    float damage_mask_opacity_green;
    float damage_mask_opacity_blue;
    float damage_mask_opacity_alpha;
    real_rgb_color damage_mask_color_floor;
    float damage_mask_color_floor_alpha;
    real_rgb_color damage_mask_bitmap_tint;
    float damage_mask_bitmap_tint_alpha;
    s_tag_function damage_mask_function2;
    float sprint_fov_multiplier;
    float sprint_fov_transition_in_time;
    float sprint_fox_transition_out_time;
    s_tag_reference parallax_data;
    float unknown49;
    s_tag_function unknown60;
    s_tag_function unknown61;
    s_tag_function unknown62;
    s_tag_function unknown63;
    s_tag_function unknown64;
    float unknown65;
    s_tag_function unknown66;
    s_tag_function unknown67;
    s_tag_function unknown68;
    s_tag_function unknown69;
    s_tag_function unknown70;
    s_tag_reference survival_hud_unknown;
    float achievement_display_time;
    float unknown73;
    float unknown74;
};
static_assert(sizeof(s_chud_globals_definition) == 0x2C0);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_chud_globals_definition_player_training_datum
{
    string_id display_string;
    short max_display_time;
    short display_count;
    short disappear_delay;
    short redisplay_delay;
    float display_delay;
    ushort flags;
    short unknown;
};
static_assert(sizeof(s_chud_globals_definition_player_training_datum) == 0x14);

struct s_chud_globals_definition_cortana_suck_block
{
    ulong unknown;
    c_tag_block<s_chud_globals_definition_cortana_suck_block_levels_block> levels;
};
static_assert(sizeof(s_chud_globals_definition_cortana_suck_block) == 0x10);

struct s_chud_globals_definition_cortana_suck_block_levels_block
{
    float scale;
    float noisea_velocitymin;
    float noisea_velocitymax;
    float noisea_scalexmin;
    float noisea_scalexmax;
    float noisea_scaleymin;
    float noisea_scaleymax;
    float noiseb_velocitymin;
    float noiseb_velocitymax;
    float noiseb_scalexmin;
    float noiseb_scalexmax;
    float noiseb_scaleymin;
    float noiseb_scaleymax;
    float noise_pixel_thresholdmin;
    float noise_pixel_thresholdmax;
    float noise_pixel_persistencemin;
    float noise_pixel_persistencemax;
    float noise_pixel_velocitymin;
    float noise_pixel_velocitymax;
    float noise_pixel_turbulencemin;
    float noise_pixel_turbulencemax;
    float noise_translation_scalexmin;
    float noise_translation_scalexmax;
    float noise_translation_scaleymin;
    float noise_translation_scaleymax;
    s_tag_reference message;
    float noisea_velocitymin_b;
    float noisea_velocitymax_b;
    float noisea_scalexmin_b;
    float noisea_scalexmax_b;
    float noisea_scaleymin_b;
    float noisea_scaleymax_b;
    float noiseb_velocitymin_b;
    float noiseb_velocitymax_b;
    float noiseb_scalexmin_b;
    float noiseb_scalexmax_b;
    float noiseb_scaleymin_b;
    float noiseb_scaleymax_b;
    float noise_pixel_thresholdmin_b;
    float noise_pixel_thresholdmax_b;
    float noise_pixel_persistencemin_b;
    float noise_pixel_persistencemax_b;
    float noise_pixel_velocitymin_b;
    float noise_pixel_velocitymax_b;
    float noise_pixel_turbulencemin_b;
    float noise_pixel_turbulencemax_b;
    float noise_translation_scalexmin_b;
    float noise_translation_scalexmax_b;
    float noise_translation_scaleymin_b;
    float noise_translation_scaleymax_b;
    s_tag_reference message_b;
};
static_assert(sizeof(s_chud_globals_definition_cortana_suck_block_levels_block) == 0xE4);

struct s_chud_globals_definition_unknown_block
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
};
static_assert(sizeof(s_chud_globals_definition_unknown_block) == 0x40);

struct s_chud_globals_definition_hud_shader
{
    s_tag_reference vertex_shader;
    s_tag_reference pixel_shader;
};
static_assert(sizeof(s_chud_globals_definition_hud_shader) == 0x20);

struct s_chud_globals_definition_hud_global
{
    c_enum<e_chud_globals_definition_hud_global_biped, long> biped;
    argb_color hud_disabled;
    argb_color hud_primary;
    argb_color hud_foreground;
    argb_color hud_warning;
    argb_color neutral_reticule;
    argb_color hostile_reticule;
    argb_color friendly_reticule;
    argb_color global_dynamic7_unknown_blip;
    argb_color neutral_blip;
    argb_color hostile_blip;
    argb_color friendly_player_blip;
    argb_color friendly_ai_blip;
    argb_color global_dynamic12;
    argb_color waypoint_blip;
    argb_color distant_waypoint_blip;
    argb_color friendly_waypoint;
    argb_color global_dynamic16;
    argb_color hostile_waypoint;
    argb_color global_dynamic18;
    argb_color blue_waypoint_ho;
    argb_color global_dynamic19;
    argb_color global_dynamic20;
    argb_color global_dynamic21;
    argb_color global_dynamic22;
    argb_color global_dynamic23;
    argb_color global_dynamic24;
    argb_color global_dynamic25;
    argb_color global_dynamic26;
    argb_color global_dynamic27;
    argb_color global_dynamic29_ho;
    argb_color default_item_outline;
    argb_color mag_item_outline;
    argb_color dmg_item_outline;
    argb_color acc_item_outline;
    argb_color rof_item_outline;
    argb_color rng_item_outline;
    argb_color pwr_item_outline;
    c_tag_block<s_chud_globals_definition_hud_global_hud_attribute> hud_attributes;
    c_tag_block<s_chud_globals_definition_hud_global_hud_sound> hud_sounds;
    s_tag_reference unknown;
    s_tag_reference frag_grenade_swap_sound;
    s_tag_reference plasma_grenade_swap_sound;
    s_tag_reference spike_grenade_swap_sound;
    s_tag_reference firebomb_grenade_swap_sound;
    s_tag_reference damage_microtexture;
    s_tag_reference damage_noise;
    s_tag_reference directional_arrow;
    s_tag_reference grenade_waypoint;
    s_tag_reference pink_gradient;
    float directional_arrow_display_time;
    float directional_cutoff_angle;
    float directional_arrow_scale;
    float unknown10;
    float unknown11;
    float unknown12;
    s_tag_reference objective_waypoints;
    s_tag_reference player_waypoints;
    s_tag_reference scoreboard_hud;
    s_tag_reference metagame_scoreboard_hud;
    s_tag_reference survival_hud;
    s_tag_reference metagame_scoreboard_hud2;
    s_tag_reference theater_hud;
    s_tag_reference forge_hud;
    s_tag_reference hud_strings;
    s_tag_reference medals;
    c_tag_block<s_chud_globals_definition_hud_global_multiplayer_medal> multiplayer_medals;
    s_tag_reference medal_hud_animation2;
    s_tag_reference medal_hud_animation;
    s_tag_reference cortana_channel;
    s_tag_reference unknown20;
    s_tag_reference unknown21;
    s_tag_reference jammer_response;
    s_tag_reference jammer_shield_hit;
    real_point2d grenade_schematics_offset;
    float grenade_scematics_spacing;
    float equipment_scematic_offset_y;
    float dual_equipment_scematic_offset_y;
    float unknown_scematic_offset_y;
    float unknown_scematic_offset_y_2;
    float scoreboard_leader_offset_y;
    float waypoint_scale_min;
    float waypoint_scale_max;
    float unknown29;
};
static_assert(sizeof(s_chud_globals_definition_hud_global) == 0x2B0);

struct s_chud_globals_definition_hud_global_multiplayer_medal
{
    string_id medal;
};
static_assert(sizeof(s_chud_globals_definition_hud_global_multiplayer_medal) == 0x4);

struct s_chud_globals_definition_hud_global_hud_sound
{
    c_flags<e_chud_globals_definition_hud_global_hud_sound_latched_to_values, long> latched_to;
    float scale;
    c_tag_block<s_chud_globals_definition_hud_global_hud_sound_biped_data> bipeds;
};
static_assert(sizeof(s_chud_globals_definition_hud_global_hud_sound) == 0x14);

struct s_chud_globals_definition_hud_global_hud_sound_biped_data
{
    c_enum<e_chud_globals_definition_hud_global_hud_sound_biped_data_biped_type_value_ho, long> biped_type_ho;
    s_tag_reference sound;
};
static_assert(sizeof(s_chud_globals_definition_hud_global_hud_sound_biped_data) == 0x14);

struct s_chud_globals_definition_hud_global_hud_attribute
{
    c_flags<e_chud_globals_definition_hud_global_hud_attribute_resolution_flag, long> resolution_flags;
    real warp_angle;
    float warp_amount;
    float warp_direction;
    ulong resolution_width;
    ulong resolution_height;
    real_point2d motion_sensor_offset;
    float motion_sensor_radius;
    float motion_sensor_scale;
    float horizontal_scale;
    float vertical_scale;
    float horizontal_stretch;
    float vertical_stretch;
    s_tag_reference first_person_unknown_bitmap;
    s_tag_reference third_person_unknown_bitmap;
    s_tag_reference first_person_damage_border;
    s_tag_reference third_person_damage_border;
    float state_scale_ho;
    real_point2d state_left_right_offset_ho;
    float scale_unknown1;
    float spacing_unknown1;
    float scale_unknown2;
    float spacing_unknown2;
    float state_center_offset_y;
    float state_center_offset_y_2;
    float medal_scale;
    float medal_spacing;
    real_point2d survival_medals_offset;
    float unknown32;
    real_point2d multiplayer_medals_offset;
    float notification_scale;
    float notification_line_spacing;
    long notification_line_count_modifier;
    float notification_offset_x_ho;
    float notification_offset_y_ho;
    float scale_unknown;
    float spacing_unknown;
    float null_unknown;
    real_point2d unknown_offset3;
    float prompt_offset_y;
    float prompt_offset_x;
};
static_assert(sizeof(s_chud_globals_definition_hud_global_hud_attribute) == 0xE8);

