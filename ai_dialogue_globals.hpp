/* ---------- enums */

enum e_ai_vocalization_priority
{
    _ai_vocalization_priority_none = 0,
    _ai_vocalization_priority_automatic = 1,
    _ai_vocalization_priority_recall = 2,
    _ai_vocalization_priority_idle = 3,
    _ai_vocalization_priority_comment = 4,
    _ai_vocalization_priority_idle_response = 5,
    _ai_vocalization_priority_postcombat = 6,
    _ai_vocalization_priority_combat = 7,
    _ai_vocalization_priority_status = 8,
    _ai_vocalization_priority_respond = 9,
    _ai_vocalization_priority_warn = 10,
    _ai_vocalization_priority_act = 11,
    _ai_vocalization_priority_react = 12,
    _ai_vocalization_priority_involuntary = 13,
    _ai_vocalization_priority_scream = 14,
    _ai_vocalization_priority_scripted = 15,
    _ai_vocalization_priority_death = 16
};

enum e_ai_vocalization_flags
{
    _ai_vocalization_flags_immediate_bit = 0,
    _ai_vocalization_flags_interrupt_bit = 1,
    _ai_vocalization_flags_cancel_low_priority_bit = 2,
    _ai_vocalization_flags_disable_dialogue_effect_bit = 3,
    _ai_vocalization_flags_predict_facial_animations_bit = 4,
    _ai_vocalization_flags_no_distance_check_exceptions_bit = 5
};

enum e_ai_glance_behavior
{
    _ai_glance_behavior_none = 0,
    _ai_glance_behavior_glance_subject_short = 1,
    _ai_glance_behavior_glance_subject_long = 2,
    _ai_glance_behavior_glance_cause_short = 3,
    _ai_glance_behavior_glance_cause_long = 4,
    _ai_glance_behavior_glance_friend_short = 5,
    _ai_glance_behavior_glance_friend_long = 6
};

enum e_ai_dialogue_perception
{
    _ai_dialogue_perception_none = 0,
    _ai_dialogue_perception_speaker = 1,
    _ai_dialogue_perception_listener = 2
};

enum e_ai_combat_status
{
    _ai_combat_status_asleep = 0,
    _ai_combat_status_idle = 1,
    _ai_combat_status_alert = 2,
    _ai_combat_status_active = 3,
    _ai_combat_status_uninspected = 4,
    _ai_combat_status_definite = 5,
    _ai_combat_status_certain = 6,
    _ai_combat_status_visible = 7,
    _ai_combat_status_clear_los = 8,
    _ai_combat_status_dangerous = 9
};

enum e_ai_animation_impulse
{
    _ai_animation_impulse_none = 0,
    _ai_animation_impulse_shakefist = 1,
    _ai_animation_impulse_cheer = 2,
    _ai_animation_impulse_surprise_front = 3,
    _ai_animation_impulse_surprise_back = 4,
    _ai_animation_impulse_taunt = 5,
    _ai_animation_impulse_brace = 6,
    _ai_animation_impulse_point = 7,
    _ai_animation_impulse_hold = 8,
    _ai_animation_impulse_wave = 9,
    _ai_animation_impulse_advance = 10,
    _ai_animation_impulse_fallback = 11
};

enum e_ai_emotion
{
    _ai_emotion_none = 0,
    _ai_emotion_happy = 1,
    _ai_emotion_sad = 2,
    _ai_emotion_angry = 3,
    _ai_emotion_disgusted = 4,
    _ai_emotion_scared = 5,
    _ai_emotion_surprised = 6,
    _ai_emotion_pain = 7,
    _ai_emotion_shout = 8
};

enum e_ai_vocalization_response_flags
{
    _ai_vocalization_response_flags_nonexclusive_bit = 0,
    _ai_vocalization_response_flags_trigger_response_bit = 1
};

enum e_ai_vocalization_response_type
{
    _ai_vocalization_response_type_friend = 0,
    _ai_vocalization_response_type_enemy = 1,
    _ai_vocalization_response_type_listener = 2,
    _ai_vocalization_response_type_joint = 3,
    _ai_vocalization_response_type_peer = 4,
    _ai_vocalization_response_type_leader = 5,
    _ai_vocalization_response_type_friend_infantry = 6
};

enum e_ai_dialogue_type
{
    _ai_dialogue_type_none = 0,
    _ai_dialogue_type_death = 1,
    _ai_dialogue_type_death_headshot = 2,
    _ai_dialogue_type_death_assassination = 3,
    _ai_dialogue_type_damage = 4,
    _ai_dialogue_type_damage_weakspot = 5,
    _ai_dialogue_type_announce = 6,
    _ai_dialogue_type_sighted_interest = 7,
    _ai_dialogue_type_sighted_new = 8,
    _ai_dialogue_type_sighted_new_major = 9,
    _ai_dialogue_type_sighted_first = 10,
    _ai_dialogue_type_sighted_special = 11,
    _ai_dialogue_type_heard_new = 12,
    _ai_dialogue_type_heard_old = 13,
    _ai_dialogue_type_found_unit = 14,
    _ai_dialogue_type_found_unit_pursuit = 15,
    _ai_dialogue_type_throwing_grenade = 16,
    _ai_dialogue_type_throwing_grenade_all = 17,
    _ai_dialogue_type_stuck_grenade = 18,
    _ai_dialogue_type_fighting = 19,
    _ai_dialogue_type_suppressing_fire = 20,
    _ai_dialogue_type_grenade_uncover = 21,
    _ai_dialogue_type_jump = 22,
    _ai_dialogue_type_reload = 23,
    _ai_dialogue_type_reload_low_ammo = 24,
    _ai_dialogue_type_ready_spartan_laser = 25,
    _ai_dialogue_type_ready_rocket_launcher = 26,
    _ai_dialogue_type_ready_flak_cannon = 27,
    _ai_dialogue_type_ready_plasma_launcher = 28,
    _ai_dialogue_type_weapon_pickup = 29,
    _ai_dialogue_type_surprised = 30,
    _ai_dialogue_type_lost_contact = 31,
    _ai_dialogue_type_investigate_failed = 32,
    _ai_dialogue_type_pursuit_failed = 33,
    _ai_dialogue_type_investigate_start = 34,
    _ai_dialogue_type_investigate_interest = 35,
    _ai_dialogue_type_searching = 36,
    _ai_dialogue_type_abandoned_search_space = 37,
    _ai_dialogue_type_abandoned_search_time = 38,
    _ai_dialogue_type_presearch_failed = 39,
    _ai_dialogue_type_abandoned_search_restricted = 40,
    _ai_dialogue_type_pursuit_start = 41,
    _ai_dialogue_type_postcombat_inspect_body = 42,
    _ai_dialogue_type_vehicle_slow_down = 43,
    _ai_dialogue_type_vehicle_get_in = 44,
    _ai_dialogue_type_idle = 45,
    _ai_dialogue_type_combat_idle = 46,
    _ai_dialogue_type_taunt = 47,
    _ai_dialogue_type_taunt_reply = 48,
    _ai_dialogue_type_retreat = 49,
    _ai_dialogue_type_retreat_from_scary_target = 50,
    _ai_dialogue_type_retreat_from_dead_leader = 51,
    _ai_dialogue_type_retreat_from_proximity = 52,
    _ai_dialogue_type_retreat_from_low_shield = 53,
    _ai_dialogue_type_flee = 54,
    _ai_dialogue_type_melee_charge = 55,
    _ai_dialogue_type_melee_attack_melee = 56,
    _ai_dialogue_type_assassination_attack = 57,
    _ai_dialogue_type_assassination_interrupted = 58,
    _ai_dialogue_type_vehicle_falling = 59,
    _ai_dialogue_type_vehicle_woohoo = 60,
    _ai_dialogue_type_vehicle_scared = 61,
    _ai_dialogue_type_vehicle_crazy = 62,
    _ai_dialogue_type_leap = 63,
    _ai_dialogue_type_postcombat_win = 64,
    _ai_dialogue_type_postcombat_lose = 65,
    _ai_dialogue_type_postcombat_neutral = 66,
    _ai_dialogue_type_shoot_corpse = 67,
    _ai_dialogue_type_postcombat_start = 68,
    _ai_dialogue_type_inspect_body_start = 69,
    _ai_dialogue_type_postcombat_status = 70,
    _ai_dialogue_type_postcombat_last_standing = 71,
    _ai_dialogue_type_vehicle_entry_start_driver = 72,
    _ai_dialogue_type_vehicle_enter = 73,
    _ai_dialogue_type_vehicle_entry_start_gun = 74,
    _ai_dialogue_type_vehicle_entry_start_passenger = 75,
    _ai_dialogue_type_vehicle_exit = 76,
    _ai_dialogue_type_evict_driver = 77,
    _ai_dialogue_type_evict_gunner = 78,
    _ai_dialogue_type_evict_passenger = 79,
    _ai_dialogue_type_vehicle_boarded = 80,
    _ai_dialogue_type_new_order_enemy_advancing = 81,
    _ai_dialogue_type_new_order_enemy_charging = 82,
    _ai_dialogue_type_new_order_enemy_fallingback = 83,
    _ai_dialogue_type_new_order_advance = 84,
    _ai_dialogue_type_new_order_charge = 85,
    _ai_dialogue_type_new_order_fallback = 86,
    _ai_dialogue_type_new_order_moveon = 87,
    _ai_dialogue_type_new_order_fllplr = 88,
    _ai_dialogue_type_new_order_arrive_combat = 89,
    _ai_dialogue_type_new_order_end_combat = 90,
    _ai_dialogue_type_new_order_investigate = 91,
    _ai_dialogue_type_new_order_spread = 92,
    _ai_dialogue_type_new_order_hold = 93,
    _ai_dialogue_type_new_order_find_cover = 94,
    _ai_dialogue_type_new_order_covering_fire = 95,
    _ai_dialogue_type_order_ack_positive = 96,
    _ai_dialogue_type_order_ack_negative = 97,
    _ai_dialogue_type_order_ack_canceled = 98,
    _ai_dialogue_type_order_ack_completed = 99,
    _ai_dialogue_type_order_ack_regroup = 100,
    _ai_dialogue_type_order_ack_disband = 101,
    _ai_dialogue_type_order_ack_weapon_change = 102,
    _ai_dialogue_type_order_ack_attack_vehicle = 103,
    _ai_dialogue_type_order_ack_attack_infantry = 104,
    _ai_dialogue_type_order_ack_interact = 105,
    _ai_dialogue_type_order_ack_pinned_down = 106,
    _ai_dialogue_type_fireteam_member_join = 107,
    _ai_dialogue_type_fireteam_member_danger = 108,
    _ai_dialogue_type_fireteam_member_died = 109,
    _ai_dialogue_type_emerge = 110,
    _ai_dialogue_type_curse = 111,
    _ai_dialogue_type_threaten = 112,
    _ai_dialogue_type_cover_friend = 113,
    _ai_dialogue_type_move_cover = 114,
    _ai_dialogue_type_in_cover = 115,
    _ai_dialogue_type_pinned_down = 116,
    _ai_dialogue_type_strike = 117,
    _ai_dialogue_type_open_fire = 118,
    _ai_dialogue_type_shoot = 119,
    _ai_dialogue_type_shoot_multiple = 120,
    _ai_dialogue_type_shoot_gunner = 121,
    _ai_dialogue_type_gloat = 122,
    _ai_dialogue_type_greet = 123,
    _ai_dialogue_type_player_look = 124,
    _ai_dialogue_type_player_look_longtime = 125,
    _ai_dialogue_type_panic_grenade_attached = 126,
    _ai_dialogue_type_panic_vehicle_destroyed = 127,
    _ai_dialogue_type_help_response = 128,
    _ai_dialogue_type_remind = 129,
    _ai_dialogue_type_overheated = 130,
    _ai_dialogue_type_weapon_trade_better = 131,
    _ai_dialogue_type_weapon_trade_worse = 132,
    _ai_dialogue_type_weapon_trade_equal = 133,
    _ai_dialogue_type_betray = 134,
    _ai_dialogue_type_forgive = 135,
    _ai_dialogue_type_warn_target = 136,
    _ai_dialogue_type_warn_pursuit = 137,
    _ai_dialogue_type_use_equipment = 138,
    _ai_dialogue_type_ambush = 139,
    _ai_dialogue_type_undr_fire = 140,
    _ai_dialogue_type_undr_fire_trrt = 141,
    _ai_dialogue_type_flood_boom = 142,
    _ai_dialogue_type_vehicle_boom = 143,
    _ai_dialogue_type_berserk = 144,
    _ai_dialogue_type_stealth = 145,
    _ai_dialogue_type_infection = 146,
    _ai_dialogue_type_reanimate = 147,
    _ai_dialogue_type_scold = 148,
    _ai_dialogue_type_praise = 149,
    _ai_dialogue_type_scorn = 150,
    _ai_dialogue_type_plead = 151,
    _ai_dialogue_type_thank = 152,
    _ai_dialogue_type_ok = 153,
    _ai_dialogue_type_cheer = 154,
    _ai_dialogue_type_invite_vehicle = 155,
    _ai_dialogue_type_invite_vehicle_driver = 156,
    _ai_dialogue_type_invite_vehicle_gunner = 157,
    _ai_dialogue_type_player_blocking = 158,
    _ai_dialogue_type_player_multi_kill = 159,
    _ai_dialogue_type_advance_start = 160,
    _ai_dialogue_type_hamstring_charge = 161,
    _ai_dialogue_type_gangfire = 162,
    _ai_dialogue_type_lioncharge_begin = 163,
    _ai_dialogue_type_lioncharge_shoot = 164,
    _ai_dialogue_type_lioncharge_abandon = 165,
    _ai_dialogue_type_lioncharge_runner = 166,
    _ai_dialogue_type_pack_charge = 167,
    _ai_dialogue_type_tracking_ping = 168,
    _ai_dialogue_type_tracking_reflection = 169,
    _ai_dialogue_type_catch_up_to_player = 170,
    _ai_dialogue_type_blocking_player = 171,
    _ai_dialogue_type_attraction_area_on_player = 172,
    _ai_dialogue_type_attraction_area_general = 173,
    _ai_dialogue_type_attraction_area_sniper = 174,
    _ai_dialogue_type_attraction_area_suppress = 175,
    _ai_dialogue_type_attraction_area_cqb = 176,
    _ai_dialogue_type_player_order_occupy_ground = 177,
    _ai_dialogue_type_player_order_prosecute_target = 178,
    _ai_dialogue_type_player_order_enter_vehicle = 179,
    _ai_dialogue_type_player_order_get_weapon = 180,
    _ai_dialogue_type_player_order_regroup = 181,
    _ai_dialogue_type_player_order_revive = 182,
    _ai_dialogue_type_actor_acknowledge_order = 183,
    _ai_dialogue_type_actor_order_occupy_ground_completed = 184,
    _ai_dialogue_type_actor_order_prosecute_target_completed = 185,
    _ai_dialogue_type_actor_order_enter_vehicle_completed = 186,
    _ai_dialogue_type_actor_order_get_weapon_completed = 187,
    _ai_dialogue_type_actor_order_get_weapon_denied = 188,
    _ai_dialogue_type_actor_order_target_change_delayed = 189,
    _ai_dialogue_type_actor_order_unreachable = 190,
    _ai_dialogue_type_actor_notice_something = 191,
    _ai_dialogue_type_musk_report_idle = 192,
    _ai_dialogue_type_musk_report_scanning = 193,
    _ai_dialogue_type_musk_report_imminent_threat = 194,
    _ai_dialogue_type_musk_report_troops_in_combat = 195,
    _ai_dialogue_type_bounding_providing_cover = 196,
    _ai_dialogue_type_bounding_advancing = 197,
    _ai_dialogue_type_shield_wall_invite = 198,
    _ai_dialogue_type_shield_wall_join = 199,
    _ai_dialogue_type_cavalier_gate_spawn = 200,
    _ai_dialogue_type_cavalier_close_threat = 201,
    _ai_dialogue_type_cavalier_reveal_player = 202,
    _ai_dialogue_type_cavalier_block_player = 203,
    _ai_dialogue_type_cavalier_transition_mode = 204,
    _ai_dialogue_type_soldier_warp = 205,
    _ai_dialogue_type_firing_wall_create = 206,
    _ai_dialogue_type_firing_wall_join = 207,
    _ai_dialogue_type_firebuddy_follower_death = 208,
    _ai_dialogue_type_firebuddy_leader_death = 209,
    _ai_dialogue_type_downed_start = 210,
    _ai_dialogue_type_downed_player_order_help = 211,
    _ai_dialogue_type_downed_call_for_help = 212,
    _ai_dialogue_type_downed_call_for_help_reminder = 213,
    _ai_dialogue_type_moving_to_revive = 214,
    _ai_dialogue_type_unit_revives_unit = 215,
    _ai_dialogue_type_unit_revived_unit = 216,
    _ai_dialogue_type_clamber = 217,
    _ai_dialogue_type_vault = 218,
    _ai_dialogue_type_spawn_gate_spawning = 219,
    _ai_dialogue_type_spawn_gate_destroyed = 220,
    _ai_dialogue_type_spawn_gate_exhausted = 221,
    _ai_dialogue_type_shield_depleted = 222,
    _ai_dialogue_type_shield_recharge_started = 223,
    _ai_dialogue_type_shield_recharge_finished = 224,
    _ai_dialogue_type_overshield_depleted = 225,
    _ai_dialogue_type_dropped_weapon = 226,
    _ai_dialogue_type_spot_character = 227,
    _ai_dialogue_type_death_assist = 228,
    _ai_dialogue_type_first_strike = 229,
    _ai_dialogue_type_rocket_roll = 230,
    _ai_dialogue_type_extermination = 231,
    _ai_dialogue_type_weapon_pad_warning = 232,
    _ai_dialogue_type_weapon_pad_spawn = 233,
    _ai_dialogue_type_halfway_to_win = 234,
    _ai_dialogue_type_ten_points_to_win = 235,
    _ai_dialogue_type_five_points_to_win = 236,
    _ai_dialogue_type_one_point_to_win = 237,
    _ai_dialogue_type_ten_minutes_remaining = 238,
    _ai_dialogue_type_five_minutes_remaining = 239,
    _ai_dialogue_type_one_minute_remaining = 240,
    _ai_dialogue_type_thirty_seconds_remaining = 241,
    _ai_dialogue_type_player_spawn = 242,
    _ai_dialogue_type_captured_first = 243,
    _ai_dialogue_type_captured = 244,
    _ai_dialogue_type_captured_last = 245,
    _ai_dialogue_type_capturing_started = 246,
    _ai_dialogue_type_capturing_joined = 247,
    _ai_dialogue_type_capturing_contested = 248,
    _ai_dialogue_type_breakout_heartbeat = 249,
    _ai_dialogue_type_breakout_death = 250,
    _ai_dialogue_type_round_start = 251,
    _ai_dialogue_type_round_start_after_loss = 252,
    _ai_dialogue_type_round_start_after_win = 253,
    _ai_dialogue_type_round_start_elimination = 254,
    _ai_dialogue_type_cower = 255,
    _ai_dialogue_type_kamikaze = 256,
    _ai_dialogue_type_evade = 257,
    _ai_dialogue_type_special_fire = 258,
    _ai_dialogue_type_shoulder_bash = 259,
    _ai_dialogue_type_ground_pound = 260,
    _ai_dialogue_type_ground_pound_started = 261,
    _ai_dialogue_type_armor_break = 262,
    _ai_dialogue_type_projectile_deflect_try = 263,
    _ai_dialogue_type_vehicle_critical_damage = 264,
    _ai_dialogue_type_vehicle_critical_damage_occupied = 265,
    _ai_dialogue_type_vehicle_boarding_started = 266,
    _ai_dialogue_type_dropship_spawned = 267,
    _ai_dialogue_type_knight_hover_shoot = 268,
    _ai_dialogue_type_cavalier_death_ball_fire = 269,
    _ai_dialogue_type_cavalier_death_ball_charge = 270,
    _ai_dialogue_type_cavalier_death_ball_charge_suppress = 271,
    _ai_dialogue_type_watcher_heal = 272,
    _ai_dialogue_type_watcher_deflect_grenade = 273,
    _ai_dialogue_type_watcher_shield = 274,
    _ai_dialogue_type_collectible_pickup = 275,
    _ai_dialogue_type_bump_into = 276
};

enum e_ai_dialogue_speaker_type
{
    _ai_dialogue_speaker_type_subject = 0,
    _ai_dialogue_speaker_type_cause = 1,
    _ai_dialogue_speaker_type_friend = 2,
    _ai_dialogue_speaker_type_friend_of_cause = 3,
    _ai_dialogue_speaker_type_target = 4,
    _ai_dialogue_speaker_type_enemy = 5,
    _ai_dialogue_speaker_type_enemy_of_cause = 6,
    _ai_dialogue_speaker_type_vehicle = 7,
    _ai_dialogue_speaker_type_joint = 8,
    _ai_dialogue_speaker_type_task = 9,
    _ai_dialogue_speaker_type_leader = 10,
    _ai_dialogue_speaker_type_joint_leader = 11,
    _ai_dialogue_speaker_type_clump = 12,
    _ai_dialogue_speaker_type_peer = 13,
    _ai_dialogue_speaker_type_none = 14
};

enum e_ai_dialogue_pattern_hostility
{
    _ai_dialogue_pattern_hostility_none = 0,
    _ai_dialogue_pattern_hostility_self = 1,
    _ai_dialogue_pattern_hostility_neutral = 2,
    _ai_dialogue_pattern_hostility_friend = 3,
    _ai_dialogue_pattern_hostility_enemy = 4,
    _ai_dialogue_pattern_hostility_traitor = 5
};

enum e_ai_dialogue_pattern_flags
{
    _ai_dialogue_pattern_flags_subject_visible_bit = 0,
    _ai_dialogue_pattern_flags_cause_visible_bit = 1,
    _ai_dialogue_pattern_flags_friends_present_bit = 2,
    _ai_dialogue_pattern_flags_subject_is_speaker_s_target_bit = 3,
    _ai_dialogue_pattern_flags_cause_is_speaker_s_target_bit = 4,
    _ai_dialogue_pattern_flags_cause_is_player_bit = 5,
    _ai_dialogue_pattern_flags_cause_is_primary_player_ally_bit = 6,
    _ai_dialogue_pattern_flags_cause_is_infantry_bit = 7,
    _ai_dialogue_pattern_flags_subject_is_infantry_bit = 8,
    _ai_dialogue_pattern_flags_speaker_is_downed_bit = 9,
    _ai_dialogue_pattern_flags_speaker_is_infantry_bit = 10,
    _ai_dialogue_pattern_flags_speaker_has_low_health_bit = 11,
    _ai_dialogue_pattern_flags_cause_is_targeting_player_bit = 12,
    _ai_dialogue_pattern_flags_subject_dead_bit = 13,
    _ai_dialogue_pattern_flags_cause_dead_bit = 14
};

enum e_ai_actor_type
{
    _ai_actor_type_elite = 0,
    _ai_actor_type_jackal = 1,
    _ai_actor_type_grunt = 2,
    _ai_actor_type_hunter = 3,
    _ai_actor_type_engineer = 4,
    _ai_actor_type_assassin = 5,
    _ai_actor_type_player = 6,
    _ai_actor_type_marine = 7,
    _ai_actor_type_crew = 8,
    _ai_actor_type_combat_form = 9,
    _ai_actor_type_infection_form = 10,
    _ai_actor_type_carrier_form = 11,
    _ai_actor_type_monitor = 12,
    _ai_actor_type_sentinel = 13,
    _ai_actor_type_none = 14,
    _ai_actor_type_mounted_weapon = 15,
    _ai_actor_type_brute = 16,
    _ai_actor_type_prophet = 17,
    _ai_actor_type_bugger = 18,
    _ai_actor_type_juggernaut = 19,
    _ai_actor_type_pure_form_stealth = 20,
    _ai_actor_type_pure_form_tank = 21,
    _ai_actor_type_pure_form_ranged = 22,
    _ai_actor_type_scarab = 23,
    _ai_actor_type_guardian = 24
};


/* ---------- structures */

struct s_ai_vocalization_response;
struct s_ai_vocalization;
struct s_ai_dialogue_pattern;
struct s_ai_dialogue_globals_dialog_datum;
struct s_ai_dialogue_globals_involuntary_datum;
struct s_ai_dialogue_globals;

struct s_ai_dialogue_globals
{
    real_bounds strike_delay_bounds;
    float remind_delay;
    float cover_curse_chance;
    float face_friendly_player_distance;
    c_tag_block<s_ai_vocalization> vocalizations;
    c_tag_block<s_ai_dialogue_pattern> patterns;
    byte unused1[12];
    c_tag_block<s_ai_dialogue_globals_dialog_datum> dialog_data;
    c_tag_block<s_ai_dialogue_globals_involuntary_datum> involuntary_data;
    byte unused2[12];
};
static_assert(sizeof(s_ai_dialogue_globals) == 0x5C);

struct s_ai_dialogue_globals_involuntary_datum
{
    short involuntary_vocalization_index;
    byte unused[2];
};
static_assert(sizeof(s_ai_dialogue_globals_involuntary_datum) == 0x4);

struct s_ai_dialogue_globals_dialog_datum
{
    short start_index;
    short length;
};
static_assert(sizeof(s_ai_dialogue_globals_dialog_datum) == 0x4);

struct s_ai_dialogue_pattern
{
    c_enum<e_ai_dialogue_type, short> dialogue_type;
    short vocalizations_index;
    string_id vocalization_name;
    c_enum<e_ai_dialogue_speaker_type, short> speaker_type;
    c_enum<e_ai_dialogue_speaker_type, short> listener_type;
    c_enum<e_ai_dialogue_pattern_hostility, short> hostility;
    c_flags<e_ai_dialogue_pattern_flags, ushort> flags;
    c_enum<e_ai_actor_type, short> cause_actor_type;
    short cause_type;
    string_id cause_ai_type_name;
    ulong unknown3;
    short unknown4;
    short unknown5;
    short attitude;
    short unknown6;
    ulong conditions;
    short spacial_relationship;
    short damage_type;
    short unknown7;
    short subject_type;
    string_id subject_ai_type_name;
};
static_assert(sizeof(s_ai_dialogue_pattern) == 0x34);

struct s_ai_vocalization
{
    string_id name;
    short parent_index;
    c_enum<e_ai_vocalization_priority, short> priority;
    c_flags<e_ai_vocalization_flags, long> flags;
    c_enum<e_ai_glance_behavior, short> glance_behavior;
    c_enum<e_ai_glance_behavior, short> glance_recipient;
    c_enum<e_ai_dialogue_perception, short> perception_type;
    c_enum<e_ai_combat_status, short> max_combat_status;
    c_enum<e_ai_animation_impulse, short> animation_impulse;
    short proxy_dialogue_index;
    float sound_repetition_delay;
    float allowable_queue_delay;
    float pre_vocalization_delay;
    float notification_delay;
    float post_vocalization_delay;
    float repeat_delay;
    float weight;
    float speaker_freeze_time;
    float listener_freeze_time;
    c_enum<e_ai_emotion, short> speaker_emotion;
    c_enum<e_ai_emotion, short> listener_emotion;
    float player_speaker_skip_fraction;
    float player_skip_fraction;
    float flood_skip_fraction;
    float skip_fraction;
    string_id sample_line;
    c_tag_block<s_ai_vocalization_response> responses;
};
static_assert(sizeof(s_ai_vocalization) == 0x60);

struct s_ai_vocalization_response
{
    string_id vocalization_name;
    c_flags<e_ai_vocalization_response_flags, ushort> flags;
    short vocalization_index;
    c_enum<e_ai_vocalization_response_type, short> response_type;
    short import_dialogue_index;
};
static_assert(sizeof(s_ai_vocalization_response) == 0xC);

