/* ---------- enums */

enum e_scenario_map_type
{
    _scenario_map_type_single_player = 0,
    _scenario_map_type_multiplayer = 1,
    _scenario_map_type_main_menu = 2
};

enum e_scenario_map_sub_type
{
    _scenario_map_sub_type_none = 0,
    _scenario_map_sub_type_hub = 1,
    _scenario_map_sub_type_level = 2,
    _scenario_map_sub_type_scene = 3,
    _scenario_map_sub_type_cinematic = 4
};

enum e_scenario_flags
{
    _scenario_flags_bit0_bit = 0,
    _scenario_flags_bit1_bit = 1,
    _scenario_flags_bit2_bit = 2,
    _scenario_flags_bit3_bit = 3,
    _scenario_flags_bit4_bit = 4,
    _scenario_flags_characters_use_previous_mission_weapons_bit = 5
};

enum e_scenario_bsp_short_flags
{
    _scenario_bsp_short_flags_bsp0_bit = 0,
    _scenario_bsp_short_flags_bsp1_bit = 1,
    _scenario_bsp_short_flags_bsp2_bit = 2,
    _scenario_bsp_short_flags_bsp3_bit = 3,
    _scenario_bsp_short_flags_bsp4_bit = 4,
    _scenario_bsp_short_flags_bsp5_bit = 5,
    _scenario_bsp_short_flags_bsp6_bit = 6,
    _scenario_bsp_short_flags_bsp7_bit = 7,
    _scenario_bsp_short_flags_bsp8_bit = 8,
    _scenario_bsp_short_flags_bsp9_bit = 9,
    _scenario_bsp_short_flags_bsp10_bit = 10,
    _scenario_bsp_short_flags_bsp11_bit = 11,
    _scenario_bsp_short_flags_bsp12_bit = 12,
    _scenario_bsp_short_flags_bsp13_bit = 13,
    _scenario_bsp_short_flags_bsp14_bit = 14,
    _scenario_bsp_short_flags_bsp15_bit = 15
};

enum e_scenario_bsp_flags
{
    _scenario_bsp_flags_bsp0_bit = 0,
    _scenario_bsp_flags_bsp1_bit = 1,
    _scenario_bsp_flags_bsp2_bit = 2,
    _scenario_bsp_flags_bsp3_bit = 3,
    _scenario_bsp_flags_bsp4_bit = 4,
    _scenario_bsp_flags_bsp5_bit = 5,
    _scenario_bsp_flags_bsp6_bit = 6,
    _scenario_bsp_flags_bsp7_bit = 7,
    _scenario_bsp_flags_bsp8_bit = 8,
    _scenario_bsp_flags_bsp9_bit = 9,
    _scenario_bsp_flags_bsp10_bit = 10,
    _scenario_bsp_flags_bsp11_bit = 11,
    _scenario_bsp_flags_bsp12_bit = 12,
    _scenario_bsp_flags_bsp13_bit = 13,
    _scenario_bsp_flags_bsp14_bit = 14,
    _scenario_bsp_flags_bsp15_bit = 15,
    _scenario_bsp_flags_bsp16_bit = 16,
    _scenario_bsp_flags_bsp17_bit = 17,
    _scenario_bsp_flags_bsp18_bit = 18,
    _scenario_bsp_flags_bsp19_bit = 19,
    _scenario_bsp_flags_bsp20_bit = 20,
    _scenario_bsp_flags_bsp21_bit = 21,
    _scenario_bsp_flags_bsp22_bit = 22,
    _scenario_bsp_flags_bsp23_bit = 23,
    _scenario_bsp_flags_bsp24_bit = 24,
    _scenario_bsp_flags_bsp25_bit = 25,
    _scenario_bsp_flags_bsp26_bit = 26,
    _scenario_bsp_flags_bsp27_bit = 27,
    _scenario_bsp_flags_bsp28_bit = 28,
    _scenario_bsp_flags_bsp29_bit = 29,
    _scenario_bsp_flags_bsp30_bit = 30,
    _scenario_bsp_flags_bsp31_bit = 31
};

enum e_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags
{
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit0_bit = 0,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit1_bit = 1,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit2_bit = 2,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit3_bit = 3,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit4_bit = 4,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_effects_bit = 5,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit6_bit = 6,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit7_bit = 7,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit8_bit = 8,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit9_bit = 9,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit10_bit = 10,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit11_bit = 11,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit12_bit = 12,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit13_bit = 13,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit14_bit = 14,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit15_bit = 15,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_firing_effects_bit = 16,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit17_bit = 17,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit18_bit = 18,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit19_bit = 19,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit20_bit = 20,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit21_bit = 21,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit22_bit = 22,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit23_bit = 23,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit24_bit = 24,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit25_bit = 25,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit26_bit = 26,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit27_bit = 27,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit28_bit = 28,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit29_bit = 29,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit30_bit = 30,
    _scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags_bit31_bit = 31
};

enum e_game_object_type_halo3_odst
{
    _game_object_type_halo3_odst_none = -1,
    _game_object_type_halo3_odst_biped = 0,
    _game_object_type_halo3_odst_vehicle = 1,
    _game_object_type_halo3_odst_weapon = 2,
    _game_object_type_halo3_odst_equipment = 3,
    _game_object_type_halo3_odst_alternate_reality_device = 4,
    _game_object_type_halo3_odst_terminal = 5,
    _game_object_type_halo3_odst_projectile = 6,
    _game_object_type_halo3_odst_scenery = 7,
    _game_object_type_halo3_odst_machine = 8,
    _game_object_type_halo3_odst_control = 9,
    _game_object_type_halo3_odst_sound_scenery = 10,
    _game_object_type_halo3_odst_crate = 11,
    _game_object_type_halo3_odst_creature = 12,
    _game_object_type_halo3_odst_giant = 13,
    _game_object_type_halo3_odst_effect_scenery = 14
};

enum e_scenario_object_source
{
    _scenario_object_source_structure = 0,
    _scenario_object_source_editor = 1,
    _scenario_object_source_dynamic = 2,
    _scenario_object_source_legacy = 3,
    _scenario_object_source_sky = 4,
    _scenario_object_source_parent = 5
};

enum e_scenario_zone_set_flags
{
    _scenario_zone_set_flags_set0_bit = 0,
    _scenario_zone_set_flags_set1_bit = 1,
    _scenario_zone_set_flags_set2_bit = 2,
    _scenario_zone_set_flags_set3_bit = 3,
    _scenario_zone_set_flags_set4_bit = 4,
    _scenario_zone_set_flags_set5_bit = 5,
    _scenario_zone_set_flags_set6_bit = 6,
    _scenario_zone_set_flags_set7_bit = 7,
    _scenario_zone_set_flags_set8_bit = 8,
    _scenario_zone_set_flags_set9_bit = 9,
    _scenario_zone_set_flags_set10_bit = 10,
    _scenario_zone_set_flags_set11_bit = 11,
    _scenario_zone_set_flags_set12_bit = 12,
    _scenario_zone_set_flags_set13_bit = 13,
    _scenario_zone_set_flags_set14_bit = 14,
    _scenario_zone_set_flags_set15_bit = 15,
    _scenario_zone_set_flags_set16_bit = 16,
    _scenario_zone_set_flags_set17_bit = 17,
    _scenario_zone_set_flags_set18_bit = 18,
    _scenario_zone_set_flags_set19_bit = 19,
    _scenario_zone_set_flags_set20_bit = 20,
    _scenario_zone_set_flags_set21_bit = 21,
    _scenario_zone_set_flags_set22_bit = 22,
    _scenario_zone_set_flags_set23_bit = 23,
    _scenario_zone_set_flags_set24_bit = 24,
    _scenario_zone_set_flags_set25_bit = 25,
    _scenario_zone_set_flags_set26_bit = 26,
    _scenario_zone_set_flags_set27_bit = 27,
    _scenario_zone_set_flags_set28_bit = 28,
    _scenario_zone_set_flags_set29_bit = 29,
    _scenario_zone_set_flags_set30_bit = 30,
    _scenario_zone_set_flags_set31_bit = 31
};

enum e_scenario_comment_type
{
    _scenario_comment_type_generic = 0
};

enum e_scenario_object_placement_flags
{
    _scenario_object_placement_flags_not_automatically_bit = 0,
    _scenario_object_placement_flags_not_on_easy_bit = 1,
    _scenario_object_placement_flags_not_on_normal_bit = 2,
    _scenario_object_placement_flags_not_on_hard_bit = 3,
    _scenario_object_placement_flags_lock_type_to_env_object_bit = 4,
    _scenario_object_placement_flags_lock_transform_to_env_object_bit = 5,
    _scenario_object_placement_flags_never_placed_bit = 6,
    _scenario_object_placement_flags_lock_name_to_env_object_bit = 7,
    _scenario_object_placement_flags_create_at_rest_bit = 8,
    _scenario_object_placement_flags_store_orientations_bit = 9,
    _scenario_object_placement_flags_startup_bit = 10,
    _scenario_object_placement_flags_attach_physically_bit = 11,
    _scenario_object_placement_flags_attach_with_scale_bit = 12,
    _scenario_object_placement_flags_no_parent_lighting_bit = 13
};

enum e_scenario_scenario_instance_source
{
    _scenario_scenario_instance_source_structure = 0,
    _scenario_scenario_instance_source_editor = 1,
    _scenario_scenario_instance_source_dynamic = 2,
    _scenario_scenario_instance_source_legacy = 3
};

enum e_scenario_scenario_instance_bsp_policy
{
    _scenario_scenario_instance_bsp_policy_default = 0,
    _scenario_scenario_instance_bsp_policy_always_placed = 1,
    _scenario_scenario_instance_bsp_policy_manual_bsp_index = 2
};

enum e_scenario_pathfinding_policy
{
    _scenario_pathfinding_policy_tag_default = 0,
    _scenario_pathfinding_policy_dynamic = 1,
    _scenario_pathfinding_policy_cut_out = 2,
    _scenario_pathfinding_policy_standard = 3,
    _scenario_pathfinding_policy_none = 4
};

enum e_scenario_lightmapping_policy
{
    _scenario_lightmapping_policy_tag_default = 0,
    _scenario_lightmapping_policy_dynamic = 1,
    _scenario_lightmapping_policy_per_vertex = 2
};

enum e_scenario_multiplayer_object_properties_symmetry
{
    _scenario_multiplayer_object_properties_symmetry_both = 0,
    _scenario_multiplayer_object_properties_symmetry_symmetric = 1,
    _scenario_multiplayer_object_properties_symmetry_asymmetric = 2
};

enum e_scenario_multiplayer_object_properties_team
{
    _scenario_multiplayer_object_properties_team_red = 0,
    _scenario_multiplayer_object_properties_team_blue = 1,
    _scenario_multiplayer_object_properties_team_green = 2,
    _scenario_multiplayer_object_properties_team_orange = 3,
    _scenario_multiplayer_object_properties_team_purple = 4,
    _scenario_multiplayer_object_properties_team_yellow = 5,
    _scenario_multiplayer_object_properties_team_brown = 6,
    _scenario_multiplayer_object_properties_team_pink = 7,
    _scenario_multiplayer_object_properties_team_neutral = 8
};

enum e_scenario_multiplayer_object_properties_shape
{
    _scenario_multiplayer_object_properties_shape_none = 0,
    _scenario_multiplayer_object_properties_shape_sphere = 1,
    _scenario_multiplayer_object_properties_shape_cylinder = 2,
    _scenario_multiplayer_object_properties_shape_box = 3
};

enum e_scenario_device_group_flags
{
    _scenario_device_group_flags_only_use_once_bit = 0
};

enum e_scenario_machine_instance_pathfinding_policy
{
    _scenario_machine_instance_pathfinding_policy_tag_default = 0,
    _scenario_machine_instance_pathfinding_policy_cut_out = 1,
    _scenario_machine_instance_pathfinding_policy_sectors = 2,
    _scenario_machine_instance_pathfinding_policy_discs = 3,
    _scenario_machine_instance_pathfinding_policy_none = 4
};

enum e_scenario_light_volume_instance_type_value2
{
    _scenario_light_volume_instance_type_value2_sphere = 0,
    _scenario_light_volume_instance_type_value2_projective = 1
};

enum e_scenario_light_volume_instance_lightmap_type
{
    _scenario_light_volume_instance_lightmap_type_use_light_tag_setting = 0,
    _scenario_light_volume_instance_lightmap_type_dynamic_only = 1,
    _scenario_light_volume_instance_lightmap_type_dynamic_with_lightmaps = 2,
    _scenario_light_volume_instance_lightmap_type_lightmaps_only = 3
};

enum e_scenario_soft_ceiling_flags
{
    _scenario_soft_ceiling_flags_ignore_bipeds_bit = 0,
    _scenario_soft_ceiling_flags_ignore_vehicles_bit = 1,
    _scenario_soft_ceiling_flags_ignore_camera_bit = 2,
    _scenario_soft_ceiling_flags_ignore_huge_vehicles_bit = 3
};

enum e_scenario_soft_ceiling_type
{
    _scenario_soft_ceiling_type_acceleration = 0,
    _scenario_soft_ceiling_type_soft_kill = 1,
    _scenario_soft_ceiling_type_slip_surface = 2
};

enum e_scenario_player_starting_location_flags
{
    _scenario_player_starting_location_flags_survival_mode_bit = 0,
    _scenario_player_starting_location_flags_survival_mode_elite_bit = 1
};

enum e_scenario_trigger_volume_type
{
    _scenario_trigger_volume_type_bounding_box = 0,
    _scenario_trigger_volume_type_sector = 1
};

enum e_scenario_zone_set_switch_trigger_volume_flag_bits
{
    _scenario_zone_set_switch_trigger_volume_flag_bits_teleport_vehicles_bit = 0
};

enum e_scenario_decal_flag_bits
{
    _scenario_decal_flag_bits_force_planer_bit = 0,
    _scenario_decal_flag_bits_project_u_vs_bit = 1
};

enum e_scenario_squad_flags
{
    _scenario_squad_flags_bit0_bit = 0,
    _scenario_squad_flags_blind_bit = 1,
    _scenario_squad_flags_deaf_bit = 2,
    _scenario_squad_flags_braindead_bit = 3,
    _scenario_squad_flags_initially_placed_bit = 4,
    _scenario_squad_flags_units_not_enterable_by_player_bit = 5,
    _scenario_squad_flags_fireteam_absorber_bit = 6,
    _scenario_squad_flags_squad_is_runtime_bit = 7,
    _scenario_squad_flags_no_wave_spawn_bit = 8,
    _scenario_squad_flags_squad_is_musketeer_bit = 9
};

enum e_game_team
{
    _game_team_default = 0,
    _game_team_player = 1,
    _game_team_human = 2,
    _game_team_covenant = 3,
    _game_team_flood = 4,
    _game_team_sentinel = 5,
    _game_team_heretic = 6,
    _game_team_prophet = 7,
    _game_team_guilty = 8,
    _game_team_unused9 = 9,
    _game_team_unused10 = 10,
    _game_team_unused11 = 11,
    _game_team_unused12 = 12,
    _game_team_unused13 = 13,
    _game_team_unused14 = 14,
    _game_team_unused15 = 15
};

enum e_scenario_squad_difficulty_flags
{
    _scenario_squad_difficulty_flags_easy_bit = 0,
    _scenario_squad_difficulty_flags_normal_bit = 1,
    _scenario_squad_difficulty_flags_heroic_bit = 2,
    _scenario_squad_difficulty_flags_legendary_bit = 3
};

enum e_scenario_squad_movement_mode
{
    _scenario_squad_movement_mode_default = 0,
    _scenario_squad_movement_mode_climbing = 1,
    _scenario_squad_movement_mode_flying = 2
};

enum e_scenario_squad_patrol_mode
{
    _scenario_squad_patrol_mode_ping_pong = 0,
    _scenario_squad_patrol_mode_loop = 1,
    _scenario_squad_patrol_mode_random = 2
};

enum e_scenario_squad_point_flags
{
    _scenario_squad_point_flags_single_use_bit = 0
};

enum e_scenario_squad_activity
{
    _scenario_squad_activity_none = 0,
    _scenario_squad_activity_patrol = 1,
    _scenario_squad_activity_stand = 2,
    _scenario_squad_activity_crouch = 3,
    _scenario_squad_activity_stand_drawn = 4,
    _scenario_squad_activity_crouch_drawn = 5,
    _scenario_squad_activity_combat = 6,
    _scenario_squad_activity_backup = 7,
    _scenario_squad_activity_guard = 8,
    _scenario_squad_activity_guard_crouch = 9,
    _scenario_squad_activity_guard_wall = 10,
    _scenario_squad_activity_typing = 11,
    _scenario_squad_activity_kneel = 12,
    _scenario_squad_activity_gaze = 13,
    _scenario_squad_activity_poke = 14,
    _scenario_squad_activity_sniff = 15,
    _scenario_squad_activity_track = 16,
    _scenario_squad_activity_watch = 17,
    _scenario_squad_activity_examine = 18,
    _scenario_squad_activity_sleep = 19,
    _scenario_squad_activity_at_ease = 20,
    _scenario_squad_activity_cower = 21,
    _scenario_squad_activity_tai_chi = 22,
    _scenario_squad_activity_pee = 23,
    _scenario_squad_activity_doze = 24,
    _scenario_squad_activity_eat = 25,
    _scenario_squad_activity_medic = 26,
    _scenario_squad_activity_work = 27,
    _scenario_squad_activity_cheering = 28,
    _scenario_squad_activity_injured = 29,
    _scenario_squad_activity_captured = 30
};

enum e_scenario_squad_spawn_point_flags
{
    _scenario_squad_spawn_point_flags_none = 0,
    _scenario_squad_spawn_point_flags_infection_form_explode = 1,
    _scenario_squad_spawn_point_flags_nothing = 4,
    _scenario_squad_spawn_point_flags_always_place = 8,
    _scenario_squad_spawn_point_flags_initially_hidden = 16,
    _scenario_squad_spawn_point_flags_vehicle_destroyed_when_no_driver = 32,
    _scenario_squad_spawn_point_flags_vehicle_open = 64,
    _scenario_squad_spawn_point_flags_actor_surface_emerge = 128,
    _scenario_squad_spawn_point_flags_actor_surface_emerge_auto = 256,
    _scenario_squad_spawn_point_flags_actor_surface_emerge_upwards = 512
};

enum e_scenario_squad_seat_type
{
    _scenario_squad_seat_type_default = 0,
    _scenario_squad_seat_type_passenger = 1,
    _scenario_squad_seat_type_gunner = 2,
    _scenario_squad_seat_type_driver = 3,
    _scenario_squad_seat_type_out_of_vehicle = 4,
    _scenario_squad_seat_type_vehicle_only = 6,
    _scenario_squad_seat_type_passenger2 = 7
};

enum e_scenario_squad_grenade_type
{
    _scenario_squad_grenade_type_none = 0,
    _scenario_squad_grenade_type_frag = 1,
    _scenario_squad_grenade_type_plasma = 2,
    _scenario_squad_grenade_type_spike = 3,
    _scenario_squad_grenade_type_fire = 4
};

enum e_scenario_zone_zone_flags_new
{
    _scenario_zone_zone_flags_new_giants_zone_bit = 0
};

enum e_scenario_zone_firing_position_flags
{
    _scenario_zone_firing_position_flags_open_bit = 0,
    _scenario_zone_firing_position_flags_partial_bit = 1,
    _scenario_zone_firing_position_flags_closed_bit = 2,
    _scenario_zone_firing_position_flags_mobile_bit = 3,
    _scenario_zone_firing_position_flags_wall_lean_bit = 4,
    _scenario_zone_firing_position_flags_perch_bit = 5,
    _scenario_zone_firing_position_flags_ground_point_bit = 6,
    _scenario_zone_firing_position_flags_dynamic_cover_point_bit = 7,
    _scenario_zone_firing_position_flags_automatically_generated_bit = 8,
    _scenario_zone_firing_position_flags_nav_volume_bit = 9,
    _scenario_zone_firing_position_flags_center_bunkering_bit = 10
};

enum e_scenario_zone_firing_position_posture_flags
{
    _scenario_zone_firing_position_posture_flags_corner_left_bit = 0,
    _scenario_zone_firing_position_posture_flags_corner_right_bit = 1,
    _scenario_zone_firing_position_posture_flags_bunker_bit = 2,
    _scenario_zone_firing_position_posture_flags_bunker_high_bit = 3,
    _scenario_zone_firing_position_posture_flags_bunker_low_bit = 4
};

enum e_scenario_zone_area_area_flags
{
    _scenario_zone_area_area_flags_vehicle_area_bit = 0,
    _scenario_zone_area_area_flags_perch_bit = 1,
    _scenario_zone_area_area_flags_manual_reference_frame_bit = 2,
    _scenario_zone_area_area_flags_turret_deployment_area_bit = 3,
    _scenario_zone_area_area_flags_invalid_sector_def_bit = 4
};

enum e_scenario_zone_area_area_generation_flags
{
    _scenario_zone_area_area_generation_flags_exclude_cover_bit = 0,
    _scenario_zone_area_area_generation_flags_ignore_existing_bit = 1,
    _scenario_zone_area_area_generation_flags_generate_radial_bit = 2,
    _scenario_zone_area_area_generation_flags_do_not_stagger_bit = 3,
    _scenario_zone_area_area_generation_flags_airborne_bit = 4,
    _scenario_zone_area_area_generation_flags_airborne_stagger_bit = 5,
    _scenario_zone_area_area_generation_flags_continue_casting_bit = 6
};

enum e_scenario_mission_scene_flag_bits
{
    _scenario_mission_scene_flag_bits_scene_can_play_multiple_times_bit = 0,
    _scenario_mission_scene_flag_bits_enable_combat_dialogue_bit = 1
};

enum e_scenario_mission_scene_trigger_condition_rule
{
    _scenario_mission_scene_trigger_condition_rule_or = 0,
    _scenario_mission_scene_trigger_condition_rule_and = 1
};

enum e_scenario_mission_scene_role_group
{
    _scenario_mission_scene_role_group_group1 = 0,
    _scenario_mission_scene_role_group_group2 = 1,
    _scenario_mission_scene_role_group_group3 = 2
};

enum e_scenario_ai_pathfinding_datum_user_hint_long_flags
{
    _scenario_ai_pathfinding_datum_user_hint_long_flags_bidirectional_bit = 0,
    _scenario_ai_pathfinding_datum_user_hint_long_flags_closed_bit = 1
};

enum e_scenario_ai_pathfinding_datum_user_hint_short_flags
{
    _scenario_ai_pathfinding_datum_user_hint_short_flags_bidirectional_bit = 0,
    _scenario_ai_pathfinding_datum_user_hint_short_flags_closed_bit = 1
};

enum e_game_object_ai_distance
{
    _game_object_ai_distance_none = 0,
    _game_object_ai_distance_down = 1,
    _game_object_ai_distance_step = 2,
    _game_object_ai_distance_crouch = 3,
    _game_object_ai_distance_stand = 4,
    _game_object_ai_distance_storey = 5,
    _game_object_ai_distance_tower = 6,
    _game_object_ai_distance_infinite = 7
};

enum e_scenario_ai_pathfinding_datum_jump_hint_control_flags
{
    _scenario_ai_pathfinding_datum_jump_hint_control_flags_magic_lift_bit = 0
};

enum e_scenario_ai_pathfinding_datum_well_hint_flags
{
    _scenario_ai_pathfinding_datum_well_hint_flags_bidirectional_bit = 0
};

enum e_scenario_ai_pathfinding_datum_well_hint_well_point_type
{
    _scenario_ai_pathfinding_datum_well_hint_well_point_type_jump = 0,
    _scenario_ai_pathfinding_datum_well_hint_well_point_type_climb = 1,
    _scenario_ai_pathfinding_datum_well_hint_well_point_type_hoist = 2
};

enum e_hs_script_type
{
    _hs_script_type_startup = 0,
    _hs_script_type_dormant = 1,
    _hs_script_type_continuous = 2,
    _hs_script_type_static = 3,
    _hs_script_type_command_script = 4,
    _hs_script_type_stub = 5
};

enum e_hs_type
{
    _hs_type_invalid = 47802,
    _hs_type_unparsed = 0,
    _hs_type_special_form = 1,
    _hs_type_function_name = 2,
    _hs_type_passthrough = 3,
    _hs_type_void = 4,
    _hs_type_boolean = 5,
    _hs_type_real = 6,
    _hs_type_short = 7,
    _hs_type_long = 8,
    _hs_type_string = 9,
    _hs_type_script = 10,
    _hs_type_string_id = 11,
    _hs_type_unit_seat_mapping = 12,
    _hs_type_trigger_volume = 13,
    _hs_type_cutscene_flag = 14,
    _hs_type_cutscene_camera_point = 15,
    _hs_type_cutscene_title = 16,
    _hs_type_cutscene_recording = 17,
    _hs_type_device_group = 18,
    _hs_type_ai = 19,
    _hs_type_ai_command_list = 20,
    _hs_type_ai_command_script = 21,
    _hs_type_ai_behavior = 22,
    _hs_type_ai_orders = 23,
    _hs_type_ai_line = 24,
    _hs_type_starting_profile = 25,
    _hs_type_conversation = 26,
    _hs_type_zone_set = 27,
    _hs_type_designer_zone = 28,
    _hs_type_point_reference = 29,
    _hs_type_style = 30,
    _hs_type_object_list = 31,
    _hs_type_folder = 32,
    _hs_type_sound = 33,
    _hs_type_effect = 34,
    _hs_type_damage = 35,
    _hs_type_looping_sound = 36,
    _hs_type_animation_graph = 37,
    _hs_type_damage_effect = 38,
    _hs_type_object_definition = 39,
    _hs_type_bitmap = 40,
    _hs_type_shader = 41,
    _hs_type_render_model = 42,
    _hs_type_structure_definition = 43,
    _hs_type_lightmap_definition = 44,
    _hs_type_cinematic_definition = 45,
    _hs_type_cinematic_scene_definition = 46,
    _hs_type_bink_definition = 47,
    _hs_type_any_tag = 48,
    _hs_type_any_tag_not_resolving = 49,
    _hs_type_game_difficulty = 50,
    _hs_type_team = 51,
    _hs_type_mp_team = 52,
    _hs_type_controller = 53,
    _hs_type_button_preset = 54,
    _hs_type_joystick_preset = 55,
    _hs_type_player_character_type = 56,
    _hs_type_voice_output_setting = 57,
    _hs_type_voice_mask = 58,
    _hs_type_subtitle_setting = 59,
    _hs_type_actor_type = 60,
    _hs_type_model_state = 61,
    _hs_type_event = 62,
    _hs_type_character_physics = 63,
    _hs_type_primary_skull = 64,
    _hs_type_secondary_skull = 65,
    _hs_type_object = 66,
    _hs_type_unit = 67,
    _hs_type_vehicle = 68,
    _hs_type_weapon = 69,
    _hs_type_device = 70,
    _hs_type_scenery = 71,
    _hs_type_effect_scenery = 72,
    _hs_type_object_name = 73,
    _hs_type_unit_name = 74,
    _hs_type_vehicle_name = 75,
    _hs_type_weapon_name = 76,
    _hs_type_device_name = 77,
    _hs_type_scenery_name = 78,
    _hs_type_effect_scenery_name = 79,
    _hs_type_cinematic_lightprobe = 80,
    _hs_type_animation_budget_reference = 81,
    _hs_type_looping_sound_budget_reference = 82,
    _hs_type_sound_budget_reference = 83
};

enum e_scenario_scripting_datum_point_set_flags
{
    _scenario_scripting_datum_point_set_flags_manual_reference_frame_bit = 0,
    _scenario_scripting_datum_point_set_flags_turret_deployment_bit = 1,
    _scenario_scripting_datum_point_set_flags_giant_set_bit = 2,
    _scenario_scripting_datum_point_set_flags_invalid_sector_refs_bit = 3
};

enum e_scenario_cutscene_camera_point_flags
{
    _scenario_cutscene_camera_point_flags_bit0_bit = 0,
    _scenario_cutscene_camera_point_flags_prematch_camera_hack_bit = 1,
    _scenario_cutscene_camera_point_flags_podium_camera_hack_bit = 2,
    _scenario_cutscene_camera_point_flags_bit3_bit = 3,
    _scenario_cutscene_camera_point_flags_bit4_bit = 4,
    _scenario_cutscene_camera_point_flags_bit5_bit = 5,
    _scenario_cutscene_camera_point_flags_bit6_bit = 6,
    _scenario_cutscene_camera_point_flags_bit7_bit = 7,
    _scenario_cutscene_camera_point_flags_bit8_bit = 8,
    _scenario_cutscene_camera_point_flags_bit9_bit = 9,
    _scenario_cutscene_camera_point_flags_bit10_bit = 10,
    _scenario_cutscene_camera_point_flags_bit11_bit = 11,
    _scenario_cutscene_camera_point_flags_bit12_bit = 12,
    _scenario_cutscene_camera_point_flags_bit13_bit = 13,
    _scenario_cutscene_camera_point_flags_bit14_bit = 14,
    _scenario_cutscene_camera_point_flags_bit15_bit = 15
};

enum e_scenario_cutscene_camera_point_type
{
    _scenario_cutscene_camera_point_type_normal = 0,
    _scenario_cutscene_camera_point_type_ignore_target_orientation = 1,
    _scenario_cutscene_camera_point_type_dolly = 2,
    _scenario_cutscene_camera_point_type_ignore_target_updates = 3
};

enum e_scenario_cutscene_title_horizontal_justification
{
    _scenario_cutscene_title_horizontal_justification_left = 0,
    _scenario_cutscene_title_horizontal_justification_right = 1,
    _scenario_cutscene_title_horizontal_justification_center = 2
};

enum e_scenario_cutscene_title_vertical_justification
{
    _scenario_cutscene_title_vertical_justification_bottom = 0,
    _scenario_cutscene_title_vertical_justification_top = 1,
    _scenario_cutscene_title_vertical_justification_middle = 2,
    _scenario_cutscene_title_vertical_justification_bottom2 = 3,
    _scenario_cutscene_title_vertical_justification_top2 = 4
};

enum e_scenario_cutscene_title_font
{
    _scenario_cutscene_title_font_terminal_font = 0,
    _scenario_cutscene_title_font_subtitle_font = 1
};

enum e_scenario_unit_seat_flags
{
    _scenario_unit_seat_flags_seat0_bit = 0,
    _scenario_unit_seat_flags_seat1_bit = 1,
    _scenario_unit_seat_flags_seat2_bit = 2,
    _scenario_unit_seat_flags_seat3_bit = 3,
    _scenario_unit_seat_flags_seat4_bit = 4,
    _scenario_unit_seat_flags_seat5_bit = 5,
    _scenario_unit_seat_flags_seat6_bit = 6,
    _scenario_unit_seat_flags_seat7_bit = 7,
    _scenario_unit_seat_flags_seat8_bit = 8,
    _scenario_unit_seat_flags_seat9_bit = 9,
    _scenario_unit_seat_flags_seat10_bit = 10,
    _scenario_unit_seat_flags_seat11_bit = 11,
    _scenario_unit_seat_flags_seat12_bit = 12,
    _scenario_unit_seat_flags_seat13_bit = 13,
    _scenario_unit_seat_flags_seat14_bit = 14,
    _scenario_unit_seat_flags_seat15_bit = 15,
    _scenario_unit_seat_flags_seat16_bit = 16,
    _scenario_unit_seat_flags_seat17_bit = 17,
    _scenario_unit_seat_flags_seat18_bit = 18,
    _scenario_unit_seat_flags_seat19_bit = 19,
    _scenario_unit_seat_flags_seat20_bit = 20,
    _scenario_unit_seat_flags_seat21_bit = 21,
    _scenario_unit_seat_flags_seat22_bit = 22,
    _scenario_unit_seat_flags_seat23_bit = 23,
    _scenario_unit_seat_flags_seat24_bit = 24,
    _scenario_unit_seat_flags_seat25_bit = 25,
    _scenario_unit_seat_flags_seat26_bit = 26,
    _scenario_unit_seat_flags_seat27_bit = 27,
    _scenario_unit_seat_flags_seat28_bit = 28,
    _scenario_unit_seat_flags_seat29_bit = 29,
    _scenario_unit_seat_flags_seat30_bit = 30,
    _scenario_unit_seat_flags_seat31_bit = 31
};

enum e_hs_syntax_node_flags
{
    _hs_syntax_node_flags_invalid_bit = 1,
    _hs_syntax_node_flags_primitive_bit = 0,
    _hs_syntax_node_flags_script_index_bit = 1,
    _hs_syntax_node_flags_global_index_bit = 2,
    _hs_syntax_node_flags_do_not_gc_bit = 3,
    _hs_syntax_node_flags_parameter_index_bit = 4,
    _hs_syntax_node_flags_group_bit = 3,
    _hs_syntax_node_flags_expression_bit = 0,
    _hs_syntax_node_flags_script_reference_bit = 1,
    _hs_syntax_node_flags_globals_reference_bit = 0,
    _hs_syntax_node_flags_parameter_reference_bit = 0
};

enum e_scenario_structure_bsp_sound_environment_type
{
    _scenario_structure_bsp_sound_environment_type_default = 0,
    _scenario_structure_bsp_sound_environment_type_interior_narrow = 1,
    _scenario_structure_bsp_sound_environment_type_interior_small = 2,
    _scenario_structure_bsp_sound_environment_type_interior_medium = 3,
    _scenario_structure_bsp_sound_environment_type_interior_large = 4,
    _scenario_structure_bsp_sound_environment_type_exterior_small = 5,
    _scenario_structure_bsp_sound_environment_type_exterior_medium = 6,
    _scenario_structure_bsp_sound_environment_type_exterior_large = 7,
    _scenario_structure_bsp_sound_environment_type_exterior_half_open = 8,
    _scenario_structure_bsp_sound_environment_type_exterior_open = 9
};

enum e_scenario_structure_bsp_background_sound_scale_flags
{
    _scenario_structure_bsp_background_sound_scale_flags_override_default_scale_bit = 0,
    _scenario_structure_bsp_background_sound_scale_flags_use_adjacent_cluster_as_portal_scale_bit = 1,
    _scenario_structure_bsp_background_sound_scale_flags_use_adjacent_cluster_as_exterior_scale_bit = 2,
    _scenario_structure_bsp_background_sound_scale_flags_scale_with_weather_intensity_bit = 3
};

enum e_scenario_spawn_datum_relevant_team_flags
{
    _scenario_spawn_datum_relevant_team_flags_red_bit = 0,
    _scenario_spawn_datum_relevant_team_flags_blue_bit = 1,
    _scenario_spawn_datum_relevant_team_flags_green_bit = 2,
    _scenario_spawn_datum_relevant_team_flags_orange_bit = 3,
    _scenario_spawn_datum_relevant_team_flags_purple_bit = 4,
    _scenario_spawn_datum_relevant_team_flags_yellow_bit = 5,
    _scenario_spawn_datum_relevant_team_flags_brown_bit = 6,
    _scenario_spawn_datum_relevant_team_flags_pink_bit = 7,
    _scenario_spawn_datum_relevant_team_flags_neutral_bit = 8
};

enum e_scenario_ai_objective_flags
{
    _scenario_ai_objective_flags_use_front_area_selection_bit = 0,
    _scenario_ai_objective_flags_use_players_as_front_bit = 1,
    _scenario_ai_objective_flags_inhibit_vehicle_entry_bit = 2
};

enum e_scenario_ai_objective_task_flags
{
    _scenario_ai_objective_task_flags_latch_on_bit = 0,
    _scenario_ai_objective_task_flags_latch_off_bit = 1,
    _scenario_ai_objective_task_flags_gate_bit = 2,
    _scenario_ai_objective_task_flags_single_use_bit = 3,
    _scenario_ai_objective_task_flags_suppress_combat_bit = 4,
    _scenario_ai_objective_task_flags_suppress_active_camo_bit = 5,
    _scenario_ai_objective_task_flags_blind_bit = 6,
    _scenario_ai_objective_task_flags_deaf_bit = 7,
    _scenario_ai_objective_task_flags_braindead_bit = 8,
    _scenario_ai_objective_task_flags_magic_player_sight_bit = 9,
    _scenario_ai_objective_task_flags_disable_bit = 10,
    _scenario_ai_objective_task_flags_ignore_fronts_bit = 11,
    _scenario_ai_objective_task_flags_dont_generate_front_bit = 12,
    _scenario_ai_objective_task_flags_reverse_direction_bit = 13,
    _scenario_ai_objective_task_flags_invert_filter_logic_bit = 14
};

enum e_scenario_ai_objective_task_task_inhibit_groups
{
    _scenario_ai_objective_task_task_inhibit_groups_cover_bit = 0,
    _scenario_ai_objective_task_task_inhibit_groups_retreat_bit = 1,
    _scenario_ai_objective_task_task_inhibit_groups_vehicles_all_bit = 2,
    _scenario_ai_objective_task_task_inhibit_groups_grenades_bit = 3,
    _scenario_ai_objective_task_task_inhibit_groups_berserk_bit = 4,
    _scenario_ai_objective_task_task_inhibit_groups_equipment_bit = 5,
    _scenario_ai_objective_task_task_inhibit_groups_object_interaction_bit = 6,
    _scenario_ai_objective_task_task_inhibit_groups_turrets_bit = 7,
    _scenario_ai_objective_task_task_inhibit_groups_vehicles_non_turrets_bit = 8
};

enum e_scenario_ai_objective_task_movement
{
    _scenario_ai_objective_task_movement_run = 0,
    _scenario_ai_objective_task_movement_walk = 1,
    _scenario_ai_objective_task_movement_crouch = 2
};

enum e_scenario_ai_objective_task_follow
{
    _scenario_ai_objective_task_follow_none = 0,
    _scenario_ai_objective_task_follow_player = 1,
    _scenario_ai_objective_task_follow_squad = 2
};

enum e_scenario_ai_objective_task_follow_player_flags
{
    _scenario_ai_objective_task_follow_player_flags_player0_bit = 0,
    _scenario_ai_objective_task_follow_player_flags_player1_bit = 1,
    _scenario_ai_objective_task_follow_player_flags_player2_bit = 2,
    _scenario_ai_objective_task_follow_player_flags_player3_bit = 3
};

enum e_scenario_ai_objective_task_dialogue_type
{
    _scenario_ai_objective_task_dialogue_type_none = 0,
    _scenario_ai_objective_task_dialogue_type_enemy_is_advancing = 1,
    _scenario_ai_objective_task_dialogue_type_enemy_is_charging = 2,
    _scenario_ai_objective_task_dialogue_type_enemy_is_falling_back = 3,
    _scenario_ai_objective_task_dialogue_type_advance = 4,
    _scenario_ai_objective_task_dialogue_type_charge = 5,
    _scenario_ai_objective_task_dialogue_type_fall_back = 6,
    _scenario_ai_objective_task_dialogue_type_move_on_moveone = 7,
    _scenario_ai_objective_task_dialogue_type_follow_player = 8,
    _scenario_ai_objective_task_dialogue_type_arriving_into_combat = 9,
    _scenario_ai_objective_task_dialogue_type_end_combat = 10,
    _scenario_ai_objective_task_dialogue_type_investigate = 11,
    _scenario_ai_objective_task_dialogue_type_spread_out = 12,
    _scenario_ai_objective_task_dialogue_type_hold_position_hold = 13,
    _scenario_ai_objective_task_dialogue_type_find_cover = 14,
    _scenario_ai_objective_task_dialogue_type_covering_fire = 15
};

enum e_scenario_ai_objective_task_runtime_flags
{
    _scenario_ai_objective_task_runtime_flags_area_connectivity_valid_bit = 0
};

enum e_scenario_ai_objective_task_activation_script_block_compile_state
{
    _scenario_ai_objective_task_activation_script_block_compile_state_edited = 0,
    _scenario_ai_objective_task_activation_script_block_compile_state_success = 1,
    _scenario_ai_objective_task_activation_script_block_compile_state_error = 2
};

enum e_scenario_ai_objective_task_filter_flags
{
    _scenario_ai_objective_task_filter_flags_exclusive_bit = 0
};

enum e_scenario_ai_objective_task_task_filter_halo3_odst
{
    _scenario_ai_objective_task_task_filter_halo3_odst_none = 0,
    _scenario_ai_objective_task_task_filter_halo3_odst_leader = 1,
    _scenario_ai_objective_task_task_filter_halo3_odst_no_leader = 2,
    _scenario_ai_objective_task_task_filter_halo3_odst_arbiter = 3,
    _scenario_ai_objective_task_task_filter_halo3_odst_player = 4,
    _scenario_ai_objective_task_task_filter_halo3_odst_in_combat = 5,
    _scenario_ai_objective_task_task_filter_halo3_odst_sighted_player = 6,
    _scenario_ai_objective_task_task_filter_halo3_odst_sighted_enemy = 7,
    _scenario_ai_objective_task_task_filter_halo3_odst_disengaged = 8,
    _scenario_ai_objective_task_task_filter_halo3_odst_infantry = 9,
    _scenario_ai_objective_task_task_filter_halo3_odst_has_an_engineer = 10,
    _scenario_ai_objective_task_task_filter_halo3_odst_strength_greater_than_one_quarter = 11,
    _scenario_ai_objective_task_task_filter_halo3_odst_strength_greater_than_one_half = 12,
    _scenario_ai_objective_task_task_filter_halo3_odst_strength_greater_than_three_quarters = 13,
    _scenario_ai_objective_task_task_filter_halo3_odst_strength_less_than_one_quarter = 14,
    _scenario_ai_objective_task_task_filter_halo3_odst_strength_less_than_one_half = 15,
    _scenario_ai_objective_task_task_filter_halo3_odst_strength_less_than_three_quarters = 16,
    _scenario_ai_objective_task_task_filter_halo3_odst_human_team = 17,
    _scenario_ai_objective_task_task_filter_halo3_odst_covenant_team = 18,
    _scenario_ai_objective_task_task_filter_halo3_odst_flood_team = 19,
    _scenario_ai_objective_task_task_filter_halo3_odst_sentinel_team = 20,
    _scenario_ai_objective_task_task_filter_halo3_odst_prophet_team = 21,
    _scenario_ai_objective_task_task_filter_halo3_odst_guilty_team = 22,
    _scenario_ai_objective_task_task_filter_halo3_odst_elite = 23,
    _scenario_ai_objective_task_task_filter_halo3_odst_jackal = 24,
    _scenario_ai_objective_task_task_filter_halo3_odst_grunt = 25,
    _scenario_ai_objective_task_task_filter_halo3_odst_hunter = 26,
    _scenario_ai_objective_task_task_filter_halo3_odst_marine = 27,
    _scenario_ai_objective_task_task_filter_halo3_odst_flood_combat = 28,
    _scenario_ai_objective_task_task_filter_halo3_odst_flood_carrier = 29,
    _scenario_ai_objective_task_task_filter_halo3_odst_sentinel = 30,
    _scenario_ai_objective_task_task_filter_halo3_odst_brute = 31,
    _scenario_ai_objective_task_task_filter_halo3_odst_prophet = 32,
    _scenario_ai_objective_task_task_filter_halo3_odst_bugger = 33,
    _scenario_ai_objective_task_task_filter_halo3_odst_flood_pureform = 34,
    _scenario_ai_objective_task_task_filter_halo3_odst_guardian = 35,
    _scenario_ai_objective_task_task_filter_halo3_odst_engineer = 36,
    _scenario_ai_objective_task_task_filter_halo3_odst_sniper = 37,
    _scenario_ai_objective_task_task_filter_halo3_odst_rifle = 38,
    _scenario_ai_objective_task_task_filter_halo3_odst_vehicle = 39,
    _scenario_ai_objective_task_task_filter_halo3_odst_scorpion = 40,
    _scenario_ai_objective_task_task_filter_halo3_odst_ghost = 41,
    _scenario_ai_objective_task_task_filter_halo3_odst_warthog = 42,
    _scenario_ai_objective_task_task_filter_halo3_odst_spectre = 43,
    _scenario_ai_objective_task_task_filter_halo3_odst_wraith = 44,
    _scenario_ai_objective_task_task_filter_halo3_odst_phantom = 45,
    _scenario_ai_objective_task_task_filter_halo3_odst_pelican = 46,
    _scenario_ai_objective_task_task_filter_halo3_odst_banshee = 47,
    _scenario_ai_objective_task_task_filter_halo3_odst_hornet = 48,
    _scenario_ai_objective_task_task_filter_halo3_odst_brute_chopper = 49,
    _scenario_ai_objective_task_task_filter_halo3_odst_mauler = 50,
    _scenario_ai_objective_task_task_filter_halo3_odst_mongoose = 51
};

enum e_scenario_ai_objective_task_attitude
{
    _scenario_ai_objective_task_attitude_normal = 0,
    _scenario_ai_objective_task_attitude_defensive = 1,
    _scenario_ai_objective_task_attitude_aggressive = 2,
    _scenario_ai_objective_task_attitude_playfighting = 3,
    _scenario_ai_objective_task_attitude_patrol = 4,
    _scenario_ai_objective_task_attitude_chckn_shit_recon = 5,
    _scenario_ai_objective_task_attitude_spread_out = 6
};

enum e_scenario_ai_objective_task_area_type
{
    _scenario_ai_objective_task_area_type_normal = 0,
    _scenario_ai_objective_task_area_type_search = 1,
    _scenario_ai_objective_task_area_type_core = 2
};

enum e_scenario_ai_objective_task_area_flags
{
    _scenario_ai_objective_task_area_flags_goal_bit = 0,
    _scenario_ai_objective_task_area_flags_direction_valid_bit = 1
};


/* ---------- structures */

struct s_scenario_structure_bsp_block;
struct s_scenario_sky_reference;
struct s_scenario_zone_set_pvs_block_bsp_checksum;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_sky;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_unknown_block;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping_cluster_reference;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping;
struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set;
struct s_scenario_object_type;
struct s_scenario_zone_set_pvs_block_portal_to_device_mapping_device_portal_association;
struct s_scenario_zone_set_pvs_block_portal_to_device_mapping_game_portal_to_portal_mapping;
struct s_scenario_zone_set_pvs_block_portal_to_device_mapping;
struct s_scenario_zone_set_pvs_block;
struct s_scenario_zone_set_audibility_block_encoded_door_pa;
struct s_scenario_zone_set_audibility_block_room_door_portal_encoded_pa;
struct s_scenario_zone_set_audibility_block_ai_deafening_pa;
struct s_scenario_zone_set_audibility_block_room_distance;
struct s_scenario_zone_set_audibility_block_game_portal_to_door_occluder_mapping;
struct s_scenario_zone_set_audibility_block_bsp_cluster_to_room_bounds_mapping;
struct s_scenario_zone_set_audibility_block_bsp_cluster_to_room_index;
struct s_scenario_zone_set_audibility_block;
struct s_scenario_zone_set;
struct s_scenario_bsp_atlas_block;
struct s_scenario_campaign_player;
struct s_scenario_comment;
struct s_game_object_type;
struct s_scenario_object_name;
struct s_scenario_object_node_orientation_bit_vector;
struct s_scenario_object_node_orientation_orientation;
struct s_scenario_object_node_orientation;
struct s_scenario_scenario_instance;
struct s_scenario_permutation_instance;
struct s_scenario_pathfinding_reference;
struct s_scenario_multiplayer_object_properties;
struct s_scenario_scenery_instance;
struct s_scenario_scenario_palette_entry;
struct s_scenario_biped_instance;
struct s_scenario_vehicle_instance;
struct s_scenario_equipment_instance;
struct s_scenario_weapon_instance;
struct s_scenario_device_group;
struct s_scenario_machine_instance_pathfinding_reference;
struct s_scenario_machine_instance;
struct s_scenario_terminal_instance;
struct s_scenario_alternate_reality_device_instance;
struct s_scenario_control_instance;
struct s_scenario_sound_scenery_instance;
struct s_scenario_giant_instance_pathfinding_reference;
struct s_scenario_giant_instance;
struct s_scenario_effect_scenery_instance;
struct s_scenario_light_volume_instance;
struct s_scenario_sandbox_object;
struct s_scenario_soft_ceiling;
struct s_scenario_player_starting_profile_block;
struct s_scenario_player_starting_location;
struct s_scenario_trigger_volume_sector_point;
struct s_scenario_trigger_volume_runtime_triangle;
struct s_scenario_trigger_volume;
struct s_scenario_recorded_animation;
struct s_scenario_zone_set_switch_trigger_volume;
struct s_scenario_unknown_block;
struct s_scenario_decal;
struct s_tag_reference_block;
struct s_scenario_squad_group;
struct s_scenario_ai_conditions;
struct s_scenario_ai_point3_d;
struct s_scenario_squad_point;
struct s_scenario_squad_spawn_formation;
struct s_scenario_squad_spawn_point;
struct s_scenario_squad_fireteam_character_type_block;
struct s_scenario_squad_fireteam_item_type_block;
struct s_scenario_squad_fireteam;
struct s_scenario_squad;
struct s_scenario_zone_firing_position;
struct s_scenario_zone_area_flight_hint;
struct s_scenario_zone_area_point;
struct s_scenario_zone_area;
struct s_scenario_zone;
struct s_scenario_squad_patrol_squad;
struct s_scenario_squad_patrol_point;
struct s_scenario_squad_patrol_transition_waypoint;
struct s_scenario_squad_patrol_transition;
struct s_scenario_squad_patrol;
struct s_scenario_mission_scene_trigger_condition;
struct s_scenario_mission_scene_role_variant;
struct s_scenario_mission_scene_role;
struct s_scenario_mission_scene;
struct s_scenario_ai_pathfinding_datum_line_segment;
struct s_scenario_ai_pathfinding_datum_parallelogram;
struct s_scenario_ai_pathfinding_datum_jump_hint;
struct s_scenario_ai_pathfinding_datum_climb_hint;
struct s_scenario_ai_pathfinding_datum_well_hint_well_point;
struct s_scenario_ai_pathfinding_datum_well_hint;
struct s_scenario_ai_pathfinding_datum_flight_hint_flight_point;
struct s_scenario_ai_pathfinding_datum_flight_hint;
struct s_scenario_ai_pathfinding_datum_unknown_block8_unknown_block;
struct s_scenario_ai_pathfinding_datum_unknown_block8;
struct s_scenario_ai_pathfinding_datum_unknown_block9;
struct s_scenario_ai_pathfinding_datum;
struct s_hs_type;
struct s_hs_script_parameter;
struct s_hs_script;
struct s_hs_global;
struct s_scenario_scripting_datum_point_set_point;
struct s_scenario_scripting_datum_point_set;
struct s_scenario_scripting_datum;
struct s_scenario_cutscene_flag;
struct s_scenario_cutscene_camera_point;
struct s_scenario_cutscene_title;
struct s_scenario_scenario_resource_reference;
struct s_scenario_scenario_resource;
struct s_scenario_unit_seats_mapping_block;
struct s_scenario_scenario_kill_trigger;
struct s_scenario_scenario_safe_trigger;
struct s_hs_syntax_node;
struct s_scenario_structure_bsp_background_sound_environment_palette_block;
struct s_scenario_unknown_block3;
struct s_scenario_fog_block;
struct s_scenario_camera_fx_block;
struct s_scenario_scenario_cluster_datum_background_sound_environment;
struct s_scenario_scenario_cluster_datum_unknown_block;
struct s_scenario_scenario_cluster_datum_unknown_block2;
struct s_scenario_scenario_cluster_datum_cluster_centroid;
struct s_scenario_scenario_cluster_datum_weather_property;
struct s_scenario_scenario_cluster_datum_fog_block;
struct s_scenario_scenario_cluster_datum_camera_effect;
struct s_scenario_scenario_cluster_datum;
struct s_scenario_spawn_datum_dynamic_spawn_overload;
struct s_scenario_spawn_datum_spawn_zone;
struct s_scenario_spawn_datum;
struct s_scenario_crate_instance;
struct s_scenario_flock_source;
struct s_scenario_flock_sink;
struct s_scenario_flock;
struct s_scenario_creature_instance;
struct s_scenario_editor_folder;
struct s_scenario_interpolator;
struct s_scenario_simulation_definition_table_block;
struct s_scenario_reference_frame;
struct s_scenario_ai_objective_opposing_objective;
struct s_scenario_ai_objective_task_pureform_distribution_block;
struct s_scenario_ai_objective_task_activation_script_block;
struct s_scenario_ai_objective_task_task_filter;
struct s_scenario_ai_objective_task_area;
struct s_scenario_ai_objective_task_direction_block_point;
struct s_scenario_ai_objective_task_direction_block;
struct s_scenario_ai_objective_task;
struct s_scenario_ai_objective;
struct s_scenario_object_reference;
struct s_scenario_designer_zone_set;
struct s_scenario_unknown_block5;
struct s_scenario_cinematic_lighting_block;
struct s_scenario_scenario_metagame_block_time_multiplier;
struct s_scenario_scenario_metagame_block_survival_block;
struct s_scenario_scenario_metagame_block;
struct s_scenario_collision_unknown_block;
struct s_scenario_unknown_block7;
struct s_scenario_lightmap_airprobe;
struct s_scenario;

struct s_scenario
{
    c_enum<e_scenario_map_type, char> map_type;
    c_enum<e_scenario_map_sub_type, char> map_sub_type;
    c_flags<e_scenario_flags, ushort> flags;
    long campaign_id;
    long map_id;
    real local_north;
    float sandbox_budget;
    c_tag_block<s_scenario_structure_bsp_block> structure_bsps;
    s_tag_reference unknown;
    c_tag_block<s_scenario_sky_reference> sky_references;
    c_tag_block<s_scenario_zone_set_pvs_block> zone_set_pvs;
    c_tag_block<s_scenario_zone_set_audibility_block> zone_set_audibility;
    c_tag_block<s_scenario_zone_set> zone_sets;
    c_tag_block<s_scenario_bsp_atlas_block> bsp_atlas;
    c_tag_block<s_scenario_campaign_player> campaign_players;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    s_tag_data editor_scenario_data;
    c_tag_block<s_scenario_comment> comments;
    c_tag_block<s_scenario_object_name> object_names;
    c_tag_block<s_scenario_scenery_instance> scenery;
    c_tag_block<s_scenario_scenario_palette_entry> scenery_palette;
    c_tag_block<s_scenario_biped_instance> bipeds;
    c_tag_block<s_scenario_scenario_palette_entry> biped_palette;
    c_tag_block<s_scenario_vehicle_instance> vehicles;
    c_tag_block<s_scenario_scenario_palette_entry> vehicle_palette;
    c_tag_block<s_scenario_equipment_instance> equipment;
    c_tag_block<s_scenario_scenario_palette_entry> equipment_palette;
    c_tag_block<s_scenario_weapon_instance> weapons;
    c_tag_block<s_scenario_scenario_palette_entry> weapon_palette;
    c_tag_block<s_scenario_device_group> device_groups;
    c_tag_block<s_scenario_machine_instance> machines;
    c_tag_block<s_scenario_scenario_palette_entry> machine_palette;
    c_tag_block<s_scenario_terminal_instance> terminals;
    c_tag_block<s_scenario_scenario_palette_entry> terminal_palette;
    c_tag_block<s_scenario_alternate_reality_device_instance> alternate_reality_devices;
    c_tag_block<s_scenario_scenario_palette_entry> alternate_reality_device_palette;
    c_tag_block<s_scenario_control_instance> controls;
    c_tag_block<s_scenario_scenario_palette_entry> control_palette;
    c_tag_block<s_scenario_sound_scenery_instance> sound_scenery;
    c_tag_block<s_scenario_scenario_palette_entry> sound_scenery_palette;
    c_tag_block<s_scenario_giant_instance> giants;
    c_tag_block<s_scenario_scenario_palette_entry> giant_palette;
    c_tag_block<s_scenario_effect_scenery_instance> effect_scenery;
    c_tag_block<s_scenario_scenario_palette_entry> effect_scenery_palette;
    c_tag_block<s_scenario_light_volume_instance> light_volumes;
    c_tag_block<s_scenario_scenario_palette_entry> light_volume_palette;
    c_tag_block<s_scenario_sandbox_object> sandbox_vehicles;
    c_tag_block<s_scenario_sandbox_object> sandbox_weapons;
    c_tag_block<s_scenario_sandbox_object> sandbox_equipment;
    c_tag_block<s_scenario_sandbox_object> sandbox_scenery;
    c_tag_block<s_scenario_sandbox_object> sandbox_teleporters;
    c_tag_block<s_scenario_sandbox_object> sandbox_goal_objects;
    c_tag_block<s_scenario_sandbox_object> sandbox_spawning;
    c_tag_block<s_scenario_soft_ceiling> soft_ceilings;
    c_tag_block<s_scenario_player_starting_profile_block> player_starting_profile;
    c_tag_block<s_scenario_player_starting_location> player_starting_locations;
    c_tag_block<s_scenario_trigger_volume> trigger_volumes;
    c_tag_block<s_scenario_recorded_animation> recorded_animations;
    c_tag_block<s_scenario_zone_set_switch_trigger_volume> zoneset_switch_trigger_volumes;
    c_tag_block<s_scenario_unknown_block> unknown32;
    c_tag_block<s_scenario_unknown_block> unknown33;
    c_tag_block<s_scenario_unknown_block> unknown34;
    c_tag_block<s_scenario_unknown_block> unknown35;
    c_tag_block<s_scenario_unknown_block> unknown36;
    ulong unknown45;
    ulong unknown46;
    ulong unknown47;
    ulong unknown48;
    ulong unknown49;
    ulong unknown50;
    ulong unknown51;
    ulong unknown52;
    ulong unknown53;
    ulong unknown54;
    ulong unknown55;
    ulong unknown56;
    ulong unknown57;
    ulong unknown58;
    ulong unknown59;
    ulong unknown60;
    ulong unknown61;
    ulong unknown62;
    ulong unknown63;
    ulong unknown64;
    ulong unknown65;
    ulong unknown66;
    ulong unknown67;
    ulong unknown68;
    ulong unknown69;
    ulong unknown70;
    ulong unknown71;
    ulong unknown72;
    ulong unknown73;
    ulong unknown74;
    ulong unknown75;
    ulong unknown76;
    ulong unknown77;
    ulong unknown78;
    ulong unknown79;
    ulong unknown80;
    c_tag_block<s_scenario_decal> decals;
    c_tag_block<s_tag_reference_block> decal_palette;
    c_tag_block<s_tag_reference_block> detail_object_collection_palette;
    c_tag_block<s_tag_reference_block> style_palette;
    c_tag_block<s_scenario_squad_group> squad_groups;
    c_tag_block<s_scenario_squad> squads;
    c_tag_block<s_scenario_zone> zones;
    c_tag_block<s_scenario_squad_patrol> squad_patrols;
    c_tag_block<s_scenario_mission_scene> mission_scenes;
    c_tag_block<s_tag_reference_block> character_palette;
    ulong unknown88;
    ulong unknown89;
    ulong unknown90;
    c_tag_block<s_scenario_ai_pathfinding_datum> ai_pathfinding_data;
    ulong unknown91;
    ulong unknown92;
    ulong unknown93;
    s_tag_data script_strings;
    c_tag_block<s_hs_script> scripts;
    c_tag_block<s_hs_global> globals;
    c_tag_block<s_tag_reference_block> script_source_file_references;
    c_tag_block<s_tag_reference_block> script_external_file_references;
    c_tag_block<s_scenario_scripting_datum> scripting_data;
    c_tag_block<s_scenario_cutscene_flag> cutscene_flags;
    c_tag_block<s_scenario_cutscene_camera_point> cutscene_camera_points;
    c_tag_block<s_scenario_cutscene_title> cutscene_titles;
    s_tag_reference custom_object_name_strings;
    s_tag_reference chapter_title_strings;
    c_tag_block<s_scenario_scenario_resource> scenario_resources;
    c_tag_block<s_scenario_unit_seats_mapping_block> unit_seats_mapping;
    c_tag_block<s_scenario_scenario_kill_trigger> scenario_kill_triggers;
    c_tag_block<s_scenario_scenario_safe_trigger> scenario_safe_triggers;
    c_tag_block<s_hs_syntax_node> script_expressions;
    ulong unknown97;
    ulong unknown98;
    ulong unknown99;
    ulong unknown100;
    ulong unknown101;
    ulong unknown102;
    c_tag_block<s_scenario_structure_bsp_background_sound_environment_palette_block> background_sound_environment_palette;
    ulong unknown103;
    ulong unknown104;
    ulong unknown105;
    ulong unknown106;
    ulong unknown107;
    ulong unknown108;
    c_tag_block<s_scenario_unknown_block3> unknown109;
    c_tag_block<s_scenario_fog_block> fog;
    c_tag_block<s_scenario_camera_fx_block> camera_fx;
    ulong unknown110;
    ulong unknown111;
    ulong unknown112;
    ulong unknown113;
    ulong unknown114;
    ulong unknown115;
    ulong unknown116;
    ulong unknown117;
    ulong unknown118;
    c_tag_block<s_scenario_scenario_cluster_datum> scenario_cluster_data;
    ulong unknown119;
    ulong unknown120;
    ulong unknown121;
    int32 object_salts[32];
    c_tag_block<s_scenario_spawn_datum> spawn_data;
    s_tag_reference sound_effects_collection;
    c_tag_block<s_scenario_crate_instance> crates;
    c_tag_block<s_scenario_scenario_palette_entry> crate_palette;
    c_tag_block<s_tag_reference_block> flock_palette;
    c_tag_block<s_scenario_flock> flocks;
    s_tag_reference subtitle_strings;
    c_tag_block<s_scenario_creature_instance> creatures;
    c_tag_block<s_scenario_scenario_palette_entry> creature_palette;
    c_tag_block<s_scenario_editor_folder> editor_folders;
    s_tag_reference territory_location_name_strings;
    ulong unknown125;
    ulong unknown126;
    c_tag_block<s_tag_reference_block> mission_dialogue;
    s_tag_reference objective_strings;
    c_tag_block<s_scenario_interpolator> interpolators;
    ulong unknown127;
    ulong unknown128;
    ulong unknown129;
    ulong unknown130;
    ulong unknown131;
    ulong unknown132;
    c_tag_block<s_scenario_simulation_definition_table_block> simulation_definition_table;
    s_tag_reference default_camera_fx;
    s_tag_reference default_screen_fx;
    s_tag_reference unknown133;
    s_tag_reference sky_parameters;
    s_tag_reference global_lighting;
    s_tag_reference lightmap;
    s_tag_reference performance_throttles;
    c_tag_block<s_scenario_reference_frame> object_reference_frames;
    c_tag_block<s_scenario_ai_objective> ai_objectives;
    c_tag_block<s_scenario_designer_zone_set> designer_zone_sets;
    c_tag_block<s_scenario_unknown_block5> unknown135;
    ulong unknown136;
    ulong unknown137;
    ulong unknown138;
    c_tag_block<s_tag_reference_block> cinematics;
    c_tag_block<s_scenario_cinematic_lighting_block> cinematic_lighting;
    ulong unknown139;
    ulong unknown140;
    ulong unknown141;
    c_tag_block<s_scenario_scenario_metagame_block> scenario_metagame;
    c_tag_block<s_scenario_collision_unknown_block> collision_unknown;
    c_tag_block<s_scenario_unknown_block7> unknown143;
    c_tag_block<s_tag_reference_block> cortana_effects;
    c_tag_block<s_scenario_lightmap_airprobe> lightmap_airprobes;
    byte unused[12];
    s_tag_reference mission_vision_mode_effect;
    s_tag_reference mission_vision_mode_theater_effect;
    c_tag_block<s_tag_reference_block> unknown155;
};
static_assert(sizeof(s_scenario) == 0x824);

struct s_scenario_lightmap_airprobe
{
    real_point3d position;
    string_id name;
    short unknown5;
    short unknown6;
};
static_assert(sizeof(s_scenario_lightmap_airprobe) == 0x14);

struct s_scenario_unknown_block7
{
    real_point3d position;
    short unknown4;
    short unknown5;
};
static_assert(sizeof(s_scenario_unknown_block7) == 0x10);

struct s_scenario_collision_unknown_block
{
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
};
static_assert(sizeof(s_scenario_collision_unknown_block) == 0x18);

struct s_scenario_scenario_metagame_block
{
    c_tag_block<s_scenario_scenario_metagame_block_time_multiplier> time_multipliers;
    float par_score;
    c_tag_block<s_scenario_scenario_metagame_block_survival_block> survival;
};
static_assert(sizeof(s_scenario_scenario_metagame_block) == 0x1C);

struct s_scenario_scenario_metagame_block_survival_block
{
    short insertion_index;
    short unknown;
    float par_score;
};
static_assert(sizeof(s_scenario_scenario_metagame_block_survival_block) == 0x8);

struct s_scenario_scenario_metagame_block_time_multiplier
{
    float time;
    float multiplier;
};
static_assert(sizeof(s_scenario_scenario_metagame_block_time_multiplier) == 0x8);

struct s_scenario_cinematic_lighting_block
{
    string_id name;
    s_tag_reference cinematic_light;
};
static_assert(sizeof(s_scenario_cinematic_lighting_block) == 0x14);

struct s_scenario_unknown_block5
{
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_scenario_unknown_block5) == 0x4);

struct s_scenario_designer_zone_set
{
    string_id name;
    ulong unknown;
    c_tag_block<s_scenario_object_reference> bipeds;
    c_tag_block<s_scenario_object_reference> vehicles;
    c_tag_block<s_scenario_object_reference> weapons;
    c_tag_block<s_scenario_object_reference> equipment;
    c_tag_block<s_scenario_object_reference> scenery;
    c_tag_block<s_scenario_object_reference> machines;
    c_tag_block<s_scenario_object_reference> terminals;
    c_tag_block<s_scenario_object_reference> controls;
    c_tag_block<s_scenario_object_reference> unknown2;
    c_tag_block<s_scenario_object_reference> crates;
    c_tag_block<s_scenario_object_reference> creatures;
    c_tag_block<s_scenario_object_reference> giants;
    c_tag_block<s_scenario_object_reference> unknown4;
    c_tag_block<s_scenario_object_reference> characters;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
};
static_assert(sizeof(s_scenario_designer_zone_set) == 0xBC);

struct s_scenario_object_reference
{
    short index;
};
static_assert(sizeof(s_scenario_object_reference) == 0x2);

struct s_scenario_ai_objective
{
    string_id name;
    c_tag_block<s_scenario_ai_objective_opposing_objective> opposing_objectives;
    c_flags<e_scenario_ai_objective_flags, ushort> flags;
    short zone;
    short first_task_index;
    short editor_folder_index;
    c_tag_block<s_scenario_ai_objective_task> tasks;
};
static_assert(sizeof(s_scenario_ai_objective) == 0x24);

struct s_scenario_ai_objective_task
{
    c_flags<e_scenario_ai_objective_task_flags, ushort> flags;
    c_flags<e_scenario_ai_objective_task_task_inhibit_groups, ushort> inhibit_groups;
    ulong unknown1;
    ulong unknown2;
    c_flags<e_scenario_squad_difficulty_flags, ushort> inhibit_on_difficulty;
    c_enum<e_scenario_ai_objective_task_movement, short> movement;
    c_enum<e_scenario_ai_objective_task_follow, short> follow;
    short follow_squad_index;
    float follow_radius;
    float follow_z_clamp;
    c_flags<e_scenario_ai_objective_task_follow_player_flags, ushort> follow_players;
    s_tag_data unused;
    float maximum_duration;
    float exhaustion_delay;
    char entry_script_name[32];
    char command_script_name[32];
    char exhaustion_script_name[32];
    short entry_script_index;
    short command_script_index;
    short exhaustion_script_index;
    short squad_group_filter;
    c_enum<e_scenario_ai_objective_task_dialogue_type, short> dialogue_type;
    c_flags<e_scenario_ai_objective_task_runtime_flags, ushort> runtime_flags;
    c_tag_block<s_scenario_ai_objective_task_pureform_distribution_block> pureform_distribution;
    short unknown20;
    short unknown21;
    string_id name;
    short hierarchy_level_from100;
    short previous_role;
    short next_role;
    short parent_role;
    c_tag_block<s_scenario_ai_objective_task_activation_script_block> activation_script;
    short script_index;
    short lifetime_count;
    c_flags<e_scenario_ai_objective_task_filter_flags, ushort> filter_flags;
    s_scenario_ai_objective_task_task_filter filter;
    short_bounds capacity;
    short max_body_count;
    c_enum<e_scenario_ai_objective_task_attitude, short> attitude;
    float min_strength;
    c_tag_block<s_scenario_ai_objective_task_area> areas;
    c_tag_block<s_scenario_ai_objective_task_direction_block> direction;
};
static_assert(sizeof(s_scenario_ai_objective_task) == 0xE8);

struct s_scenario_ai_objective_task_direction_block
{
    c_tag_block<s_scenario_ai_objective_task_direction_block_point> points;
};
static_assert(sizeof(s_scenario_ai_objective_task_direction_block) == 0xC);

struct s_scenario_ai_objective_task_direction_block_point
{
    real_point3d position;
    short manual_reference_frame;
    short bsp_index;
};
static_assert(sizeof(s_scenario_ai_objective_task_direction_block_point) == 0x10);

struct s_scenario_ai_objective_task_area
{
    c_enum<e_scenario_ai_objective_task_area_type, short> type;
    c_flags<e_scenario_ai_objective_task_area_flags, uchar> flags;
    uchar character_flags;
    short zone_index;
    short area_index;
    real yaw;
    long connectivity_bit_vector;
};
static_assert(sizeof(s_scenario_ai_objective_task_area) == 0x10);

struct s_scenario_ai_objective_task_task_filter
{
    c_enum<e_scenario_ai_objective_task_task_filter_halo3_odst, short> halo3_odst;
};
static_assert(sizeof(s_scenario_ai_objective_task_task_filter) == 0x2);

struct s_scenario_ai_objective_task_activation_script_block
{
    char script_name[32];
    char script_source[256];
    c_enum<e_scenario_ai_objective_task_activation_script_block_compile_state, short> compile_state;
    byte unused[2];
};
static_assert(sizeof(s_scenario_ai_objective_task_activation_script_block) == 0x124);

struct s_scenario_ai_objective_task_pureform_distribution_block
{
    short unknown1;
    short unknown2;
    short unknown3;
    byte unused[2];
};
static_assert(sizeof(s_scenario_ai_objective_task_pureform_distribution_block) == 0x8);

struct s_scenario_ai_objective_opposing_objective
{
    short objective_index;
    byte unused[2];
};
static_assert(sizeof(s_scenario_ai_objective_opposing_objective) == 0x4);

struct s_scenario_reference_frame
{
    datum_handle object_handle;
    short origin_bsp_index;
    s_scenario_object_type object_type;
    c_enum<e_scenario_scenario_instance_source, char> source;
    short unknown3;
    short unknown4;
    short unknown5;
    short unknown6;
};
static_assert(sizeof(s_scenario_reference_frame) == 0x10);

struct s_scenario_simulation_definition_table_block
{
    s_tag_reference_short tag;
};
static_assert(sizeof(s_scenario_simulation_definition_table_block) == 0x4);

struct s_scenario_interpolator
{
    string_id name;
    string_id accelerator_name;
    string_id multiplier_name;
    s_tag_data function;
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_scenario_interpolator) == 0x24);

struct s_scenario_editor_folder
{
    long parent_folder;
    char name[256];
};
static_assert(sizeof(s_scenario_editor_folder) == 0x104);

struct s_scenario_creature_instance : s_scenario_permutation_instance
{
};
static_assert(sizeof(s_scenario_creature_instance) == 0x70);

struct s_scenario_flock
{
    string_id name;
    short flock_palette_index;
    short bsp_index;
    short bounding_trigger_volume;
    ushort flags;
    float ecology_margin;
    c_tag_block<s_scenario_flock_source> sources;
    c_tag_block<s_scenario_flock_sink> sinks;
    real_bounds production_frequency_bounds;
    real_bounds scale_bounds;
    float source_scaleto0;
    float sink_scaleto0;
    short creature_palette_index;
    short_bounds boid_count_bounds;
    short enemy_flock_index;
};
static_assert(sizeof(s_scenario_flock) == 0x48);

struct s_scenario_flock_sink
{
    real_point3d position;
    float radius;
};
static_assert(sizeof(s_scenario_flock_sink) == 0x10);

struct s_scenario_flock_source
{
    long unknown;
    real_point3d position;
    real_euler_angles2d starting;
    float radius;
    float weight;
    char unknown2;
    char unknown3;
    char unknown4;
    char unknown5;
};
static_assert(sizeof(s_scenario_flock_source) == 0x24);

struct s_scenario_crate_instance : s_scenario_permutation_instance
{
    c_enum<e_scenario_pathfinding_policy, short> pathfinding_policy;
    c_enum<e_scenario_lightmapping_policy, short> lightmapping_policy;
    c_tag_block<s_scenario_pathfinding_reference> pathfinding_references;
    s_scenario_multiplayer_object_properties multiplayer;
};
static_assert(sizeof(s_scenario_crate_instance) == 0xB4);

struct s_scenario_spawn_datum
{
    real_bounds dynamic_spawn_height_bounds;
    float game_object_reset_height;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    c_tag_block<s_scenario_spawn_datum_dynamic_spawn_overload> dynamic_spawn_overloads;
    c_tag_block<s_scenario_spawn_datum_spawn_zone> static_respawn_zones;
    c_tag_block<s_scenario_spawn_datum_spawn_zone> static_initial_spawn_zones;
};
static_assert(sizeof(s_scenario_spawn_datum) == 0x6C);

struct s_scenario_spawn_datum_spawn_zone
{
    string_id name;
    c_flags<e_scenario_spawn_datum_relevant_team_flags, long> relevant_teams;
    ulong relevant_games;
    ulong flags;
    real_point3d position;
    real_bounds height_bounds;
    real_bounds radius_bounds;
    float weight;
};
static_assert(sizeof(s_scenario_spawn_datum_spawn_zone) == 0x30);

struct s_scenario_spawn_datum_dynamic_spawn_overload
{
    short overload_type;
    short unknown;
    float inner_radius;
    float outer_radius;
    float weight;
};
static_assert(sizeof(s_scenario_spawn_datum_dynamic_spawn_overload) == 0x10);

struct s_scenario_scenario_cluster_datum
{
    s_tag_reference bsp;
    c_tag_block<s_scenario_scenario_cluster_datum_background_sound_environment> background_sound_environments;
    c_tag_block<s_scenario_scenario_cluster_datum_unknown_block> unknown;
    c_tag_block<s_scenario_scenario_cluster_datum_unknown_block2> unknown2;
    long bsp_checksum;
    c_tag_block<s_scenario_scenario_cluster_datum_cluster_centroid> cluster_centroids;
    c_tag_block<s_scenario_scenario_cluster_datum_weather_property> weather_properties;
    c_tag_block<s_scenario_scenario_cluster_datum_fog_block> fog;
    c_tag_block<s_scenario_scenario_cluster_datum_camera_effect> camera_effects;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum) == 0x68);

struct s_scenario_scenario_cluster_datum_camera_effect
{
    short camera_effect_index;
    short unknown;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_camera_effect) == 0x4);

struct s_scenario_scenario_cluster_datum_fog_block
{
    short fog_index;
    short unknown;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_fog_block) == 0x4);

struct s_scenario_scenario_cluster_datum_weather_property
{
    short unknown;
    short unused;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_weather_property) == 0x4);

struct s_scenario_scenario_cluster_datum_cluster_centroid
{
    real_point3d centroid;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_cluster_centroid) == 0xC);

struct s_scenario_scenario_cluster_datum_unknown_block2
{
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_unknown_block2) == 0x4);

struct s_scenario_scenario_cluster_datum_unknown_block
{
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_unknown_block) == 0x4);

struct s_scenario_scenario_cluster_datum_background_sound_environment
{
    short background_sound_environment_index;
    short unknown;
};
static_assert(sizeof(s_scenario_scenario_cluster_datum_background_sound_environment) == 0x4);

struct s_scenario_camera_fx_block
{
    string_id name;
    s_tag_reference camera_fx;
    uchar unknown;
    uchar unknown2;
    uchar unknown3;
    uchar unknown4;
    ulong unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    ulong unknown9;
    ulong unknown10;
};
static_assert(sizeof(s_scenario_camera_fx_block) == 0x30);

struct s_scenario_fog_block
{
    string_id name;
    short unknown;
    short unknown2;
};
static_assert(sizeof(s_scenario_fog_block) == 0x8);

struct s_scenario_unknown_block3
{
    char name[32];
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
};
static_assert(sizeof(s_scenario_unknown_block3) == 0x78);

struct s_scenario_structure_bsp_background_sound_environment_palette_block
{
    string_id name;
    s_tag_reference sound_environment;
    c_enum<e_scenario_structure_bsp_sound_environment_type, long> type;
    float reverb_cutoff_distance;
    float reverb_interpolation_speed;
    s_tag_reference ambience_background_sound;
    s_tag_reference ambience_inside_cluster_sound;
    float ambience_cutoff_distance;
    c_flags<e_scenario_structure_bsp_background_sound_scale_flags, long> ambience_scale_flags;
    float ambience_interior_scale;
    float ambience_portal_scale;
    float ambience_exterior_scale;
    float ambience_interpolation_speed;
};
static_assert(sizeof(s_scenario_structure_bsp_background_sound_environment_palette_block) == 0x58);

struct s_hs_syntax_node
{
    ushort identifier;
    ushort opcode;
    s_hs_type value_type;
    c_flags<e_hs_syntax_node_flags, ushort> flags;
    datum_handle next_expression_handle;
    ulong string_address;
    s_tag_data data;
    short line_number;
    short unknown;
};
static_assert(sizeof(s_hs_syntax_node) == 0x18);

struct s_scenario_scenario_safe_trigger
{
    short trigger_volume;
};
static_assert(sizeof(s_scenario_scenario_safe_trigger) == 0x2);

struct s_scenario_scenario_kill_trigger
{
    short trigger_volume;
};
static_assert(sizeof(s_scenario_scenario_kill_trigger) == 0x2);

struct s_scenario_unit_seats_mapping_block
{
    s_tag_reference_short unit;
    c_flags<e_scenario_unit_seat_flags, long> seats1;
    c_flags<e_scenario_unit_seat_flags, long> seats2;
};
static_assert(sizeof(s_scenario_unit_seats_mapping_block) == 0xC);

struct s_scenario_scenario_resource
{
    long unknown;
    c_tag_block<s_tag_reference_block> script_source;
    c_tag_block<s_tag_reference_block> ai_resources;
    c_tag_block<s_scenario_scenario_resource_reference> references;
};
static_assert(sizeof(s_scenario_scenario_resource) == 0x28);

struct s_scenario_scenario_resource_reference
{
    s_tag_reference scenery_resource;
    c_tag_block<s_tag_reference_block> other_scenery;
    s_tag_reference bipeds_resource;
    c_tag_block<s_tag_reference_block> other_bipeds;
    s_tag_reference vehicles_resource;
    s_tag_reference equipment_resource;
    s_tag_reference weapons_resource;
    s_tag_reference sound_scenery_resource;
    s_tag_reference lights_resource;
    s_tag_reference devices_resource;
    c_tag_block<s_tag_reference_block> other_devices;
    s_tag_reference effect_scenery_resource;
    s_tag_reference decals_resource;
    c_tag_block<s_tag_reference_block> other_decals;
    s_tag_reference cinematics_resource;
    s_tag_reference trigger_volumes_resource;
    s_tag_reference cluster_data_resource;
    s_tag_reference comments_resource;
    s_tag_reference creature_resource;
    s_tag_reference structure_lighting_resource;
    s_tag_reference decorators_resource;
    c_tag_block<s_tag_reference_block> other_decorators;
    s_tag_reference sky_references_resource;
    s_tag_reference cubemap_resource;
};
static_assert(sizeof(s_scenario_scenario_resource_reference) == 0x16C);

struct s_scenario_cutscene_title
{
    string_id name;
    rectangle2d text_bounds;
    c_enum<e_scenario_cutscene_title_horizontal_justification, short> horizontal_justification;
    c_enum<e_scenario_cutscene_title_vertical_justification, short> vertical_justification;
    c_enum<e_scenario_cutscene_title_font, short> font;
    byte unused[2];
    argb_color text_color;
    argb_color shadow_color;
    float fade_in_time;
    float uptime;
    float fade_out_time;
};
static_assert(sizeof(s_scenario_cutscene_title) == 0x28);

struct s_scenario_cutscene_camera_point
{
    c_flags<e_scenario_cutscene_camera_point_flags, ushort> flags;
    c_enum<e_scenario_cutscene_camera_point_type, short> type;
    char name[32];
    byte unused[4];
    real_point3d position;
    real_euler_angles3d orientation;
};
static_assert(sizeof(s_scenario_cutscene_camera_point) == 0x40);

struct s_scenario_cutscene_flag
{
    byte unused[4];
    string_id name;
    real_point3d position;
    real_euler_angles2d facing;
    short editor_folder_index;
    short source_bsp_index;
};
static_assert(sizeof(s_scenario_cutscene_flag) == 0x20);

struct s_scenario_scripting_datum
{
    c_tag_block<s_scenario_scripting_datum_point_set> point_sets;
    byte unused[120];
};
static_assert(sizeof(s_scenario_scripting_datum) == 0x84);

struct s_scenario_scripting_datum_point_set
{
    char name[32];
    c_tag_block<s_scenario_scripting_datum_point_set_point> points;
    short bsp_index;
    short manual_reference_frame;
    c_flags<e_scenario_scripting_datum_point_set_flags, long> flags;
    short editor_folder_index;
    short unknown;
};
static_assert(sizeof(s_scenario_scripting_datum_point_set) == 0x38);

struct s_scenario_scripting_datum_point_set_point
{
    char name[32];
    real_point3d position;
    short reference_frame;
    short bsp_index;
    short zone_index;
    short surface_index;
    real_euler_angles2d facing_direction;
};
static_assert(sizeof(s_scenario_scripting_datum_point_set_point) == 0x3C);

struct s_hs_global
{
    char name[32];
    s_hs_type type;
    short unknown;
    datum_handle initialization_expression_handle;
};
static_assert(sizeof(s_hs_global) == 0x28);

struct s_hs_script
{
    char script_name[32];
    c_enum<e_hs_script_type, short> type;
    s_hs_type return_type;
    datum_handle root_expression_handle;
    c_tag_block<s_hs_script_parameter> parameters;
};
static_assert(sizeof(s_hs_script) == 0x34);

struct s_hs_script_parameter
{
    char name[32];
    s_hs_type type;
    short unknown;
};
static_assert(sizeof(s_hs_script_parameter) == 0x24);

struct s_hs_type
{
    c_enum<e_hs_type, ushort> ;
};
static_assert(sizeof(s_hs_type) == 0x2);

struct s_scenario_ai_pathfinding_datum
{
    c_tag_block<s_scenario_ai_pathfinding_datum_line_segment> line_segments;
    c_tag_block<s_scenario_ai_pathfinding_datum_parallelogram> parallelograms;
    c_tag_block<s_scenario_ai_pathfinding_datum_jump_hint> jump_hints;
    c_tag_block<s_scenario_ai_pathfinding_datum_climb_hint> climb_hints;
    c_tag_block<s_scenario_ai_pathfinding_datum_well_hint> well_hints;
    c_tag_block<s_scenario_ai_pathfinding_datum_flight_hint> flight_hints;
    c_tag_block<s_scenario_trigger_volume> cookie_cutters;
    c_tag_block<s_scenario_ai_pathfinding_datum_unknown_block8> unknown8;
    c_tag_block<s_scenario_ai_pathfinding_datum_unknown_block9> unknown9;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum) == 0x6C);

struct s_scenario_ai_pathfinding_datum_unknown_block9
{
    long unknown1;
    long unknown2;
    long unknown3;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_unknown_block9) == 0xC);

struct s_scenario_ai_pathfinding_datum_unknown_block8
{
    long unknown;
    long unknown2;
    long unknown3;
    c_tag_block<s_scenario_ai_pathfinding_datum_unknown_block8_unknown_block> unknown4;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_unknown_block8) == 0x18);

struct s_scenario_ai_pathfinding_datum_unknown_block8_unknown_block
{
    long unknown2;
    long unknown3;
    long unknown4;
    long unknown5;
    long unknown6;
    long unknown7;
    long unknown8;
    long unknown9;
    long unknown10;
    long unknown11;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_unknown_block8_unknown_block) == 0x28);

struct s_scenario_ai_pathfinding_datum_flight_hint
{
    c_tag_block<s_scenario_ai_pathfinding_datum_flight_hint_flight_point> flight_points;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_flight_hint) == 0xC);

struct s_scenario_ai_pathfinding_datum_flight_hint_flight_point
{
    real_point3d point;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_flight_hint_flight_point) == 0xC);

struct s_scenario_ai_pathfinding_datum_well_hint
{
    c_flags<e_scenario_ai_pathfinding_datum_well_hint_flags, long> flags;
    c_tag_block<s_scenario_ai_pathfinding_datum_well_hint_well_point> points;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_well_hint) == 0x10);

struct s_scenario_ai_pathfinding_datum_well_hint_well_point
{
    c_enum<e_scenario_ai_pathfinding_datum_well_hint_well_point_type, short> type;
    byte unused1[2];
    real_point3d position;
    short reference_frame;
    short sector_index;
    real_vector2d normal;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_well_hint_well_point) == 0x1C);

struct s_scenario_ai_pathfinding_datum_climb_hint
{
    c_flags<e_scenario_ai_pathfinding_datum_user_hint_short_flags, short> flags;
    short line_segment_index;
    short unknown1;
    short unknown2;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_climb_hint) == 0x8);

struct s_scenario_ai_pathfinding_datum_jump_hint
{
    c_flags<e_scenario_ai_pathfinding_datum_user_hint_short_flags, short> flags;
    short parallelogram_index;
    c_enum<e_game_object_ai_distance, short> force_jump_height;
    c_flags<e_scenario_ai_pathfinding_datum_jump_hint_control_flags, ushort> control_flags;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_jump_hint) == 0x8);

struct s_scenario_ai_pathfinding_datum_parallelogram
{
    c_flags<e_scenario_ai_pathfinding_datum_user_hint_long_flags, long> flags;
    real_point3d point0;
    short reference_unknown0;
    short reference_frame0;
    real_point3d point1;
    short reference_unknown1;
    short reference_frame1;
    real_point3d point2;
    short reference_unknown2;
    short reference_frame2;
    real_point3d point3;
    short reference_unknown3;
    short reference_frame3;
    short unknown1;
    byte unused[2];
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_parallelogram) == 0x48);

struct s_scenario_ai_pathfinding_datum_line_segment
{
    c_flags<e_scenario_ai_pathfinding_datum_user_hint_long_flags, long> flags;
    real_point3d point0;
    short reference_unknown0;
    short reference_frame0;
    real_point3d point1;
    short reference_unknown1;
    short reference_frame1;
};
static_assert(sizeof(s_scenario_ai_pathfinding_datum_line_segment) == 0x24);

struct s_scenario_mission_scene
{
    string_id name;
    c_flags<e_scenario_mission_scene_flag_bits, long> flags;
    c_tag_block<s_scenario_mission_scene_trigger_condition> trigger_conditions;
    c_tag_block<s_scenario_mission_scene_role> roles;
};
static_assert(sizeof(s_scenario_mission_scene) == 0x20);

struct s_scenario_mission_scene_role
{
    string_id name;
    c_enum<e_scenario_mission_scene_role_group, short> group;
    byte unused[2];
    c_tag_block<s_scenario_mission_scene_role_variant> variants;
};
static_assert(sizeof(s_scenario_mission_scene_role) == 0x14);

struct s_scenario_mission_scene_role_variant
{
    string_id name;
};
static_assert(sizeof(s_scenario_mission_scene_role_variant) == 0x4);

struct s_scenario_mission_scene_trigger_condition
{
    c_enum<e_scenario_mission_scene_trigger_condition_rule, short> combination_rule;
    byte unused[2];
};
static_assert(sizeof(s_scenario_mission_scene_trigger_condition) == 0x4);

struct s_scenario_squad_patrol
{
    string_id name;
    c_tag_block<s_scenario_squad_patrol_squad> squads;
    c_tag_block<s_scenario_squad_patrol_point> points;
    c_tag_block<s_scenario_squad_patrol_transition> transitions;
    short editor_folder_index;
    byte unused[2];
};
static_assert(sizeof(s_scenario_squad_patrol) == 0x2C);

struct s_scenario_squad_patrol_transition
{
    short point1_index;
    short point2_index;
    c_tag_block<s_scenario_squad_patrol_transition_waypoint> waypoints;
};
static_assert(sizeof(s_scenario_squad_patrol_transition) == 0x10);

struct s_scenario_squad_patrol_transition_waypoint
{
    real_point3d position;
    short manual_reference_frame;
    short bsp_index;
    long surface_index;
};
static_assert(sizeof(s_scenario_squad_patrol_transition_waypoint) == 0x14);

struct s_scenario_squad_patrol_point
{
    short objective_index;
    byte unused[2];
    float hold_time;
    float search_time;
    float pause_time;
    float cooldown_time;
};
static_assert(sizeof(s_scenario_squad_patrol_point) == 0x14);

struct s_scenario_squad_patrol_squad
{
    short squad_index;
    byte unused[2];
};
static_assert(sizeof(s_scenario_squad_patrol_squad) == 0x4);

struct s_scenario_zone
{
    char name[32];
    c_flags<e_scenario_zone_zone_flags_new, ushort> flags_new;
    ushort bsp_flags;
    c_tag_block<s_scenario_zone_firing_position> firing_positions;
    c_tag_block<s_scenario_zone_area> areas;
};
static_assert(sizeof(s_scenario_zone) == 0x3C);

struct s_scenario_zone_area
{
    char name[32];
    c_flags<e_scenario_zone_area_area_flags, long> flags;
    real_point3d runtime_relative_mean_point;
    short runtime_relative_reference_frame;
    short runtime_relative_bsp_index;
    float runtime_standard_deviation;
    short firing_position_start_index;
    short firing_position_count;
    short manual_reference_frame_new;
    short bsp_index;
    int32 cluster_occupancy_bit_vector[8];
    c_tag_block<s_scenario_zone_area_flight_hint> flight_hints;
    c_tag_block<s_scenario_zone_area_point> points;
    short generation_preset;
    byte unused[2];
    c_flags<e_scenario_zone_area_area_generation_flags, long> generation_flags;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
    float unknown25;
};
static_assert(sizeof(s_scenario_zone_area) == 0xA8);

struct s_scenario_zone_area_point
{
    real_point3d position;
    short reference_frame;
    short bsp_index;
    real_euler_angles2d facing;
};
static_assert(sizeof(s_scenario_zone_area_point) == 0x18);

struct s_scenario_zone_area_flight_hint
{
    short flight_hint_index;
    short point_index;
    short bsp_index;
    short unknown2;
};
static_assert(sizeof(s_scenario_zone_area_flight_hint) == 0x8);

struct s_scenario_zone_firing_position
{
    real_point3d position;
    short reference_frame;
    short bsp_index;
    c_flags<e_scenario_zone_firing_position_flags, ushort> flags;
    c_flags<e_scenario_zone_firing_position_posture_flags, ushort> posture_flags;
    short area_index;
    short cluster_index;
    short sector_bsp_index;
    short sector_index;
    real_euler_angles2d normal;
    real yaw;
};
static_assert(sizeof(s_scenario_zone_firing_position) == 0x28);

struct s_scenario_squad
{
    char name[32];
    c_flags<e_scenario_squad_flags, long> flags;
    c_enum<e_game_team, short> team;
    short parent_squad_group_index;
    short initial_zone_index;
    short objective_index;
    short objective_role_index;
    short editor_folder_index_new;
    c_tag_block<s_scenario_squad_spawn_formation> spawn_formations;
    c_tag_block<s_scenario_squad_spawn_point> spawn_points;
    string_id module_id;
    s_tag_reference_short squad_template;
    c_tag_block<s_scenario_squad_fireteam> designer_fireteams;
    c_tag_block<s_scenario_squad_fireteam> templated_fireteams;
};
static_assert(sizeof(s_scenario_squad) == 0x68);

struct s_scenario_squad_fireteam
{
    string_id name;
    s_scenario_ai_conditions spawn_conditions;
    short spawn_count;
    short major_upgrade;
    c_tag_block<s_scenario_squad_fireteam_character_type_block> character_type;
    c_tag_block<s_scenario_squad_fireteam_item_type_block> initial_primary_weapon;
    c_tag_block<s_scenario_squad_fireteam_item_type_block> initial_secondary_weapon;
    c_tag_block<s_scenario_squad_fireteam_item_type_block> initial_equipment;
    c_enum<e_scenario_squad_grenade_type, short> grenade_type;
    short vehicle_type_index;
    string_id vehicle_variant;
    char command_script_name[32];
    short command_script_index;
    byte unused1[2];
    string_id activity_name;
    short point_set_index;
    c_enum<e_scenario_squad_patrol_mode, short> patrol_mode;
    c_tag_block<s_scenario_squad_point> patrol_points;
};
static_assert(sizeof(s_scenario_squad_fireteam) == 0x84);

struct s_scenario_squad_fireteam_item_type_block
{
    s_scenario_ai_conditions spawn_conditions;
    short item_type_index;
    short probability;
};
static_assert(sizeof(s_scenario_squad_fireteam_item_type_block) == 0x10);

struct s_scenario_squad_fireteam_character_type_block
{
    s_scenario_ai_conditions spawn_conditions;
    short character_type_index;
    short chance;
};
static_assert(sizeof(s_scenario_squad_fireteam_character_type_block) == 0x10);

struct s_scenario_squad_spawn_point
{
    s_scenario_ai_conditions condition;
    string_id name;
    short fireteam_index;
    byte unused1[2];
    s_scenario_ai_point3_d point;
    c_enum<e_scenario_squad_spawn_point_flags, ushort> flags;
    short character_type_index;
    short initial_primary_weapon_index;
    short initial_secondary_weapon_index;
    short initial_equipment_index_new;
    short vehicle_type_index;
    c_enum<e_scenario_squad_seat_type, short> seat_type;
    c_enum<e_scenario_squad_grenade_type, short> initial_grenades;
    short swarm_count;
    byte unused3[2];
    string_id actor_variant;
    string_id vehicle_variant;
    float initial_movement_distance;
    c_enum<e_scenario_squad_movement_mode, short> initial_movement_mode_new;
    short emitter_vehicle_index;
    short emitter_giant_index;
    short emitter_biped_index;
    char placement_script_name[32];
    short placement_script_index;
    byte unused4[2];
    string_id activity_name;
    short point_set_index;
    c_enum<e_scenario_squad_patrol_mode, short> patrol_mode;
    c_tag_block<s_scenario_squad_point> patrol_points;
};
static_assert(sizeof(s_scenario_squad_spawn_point) == 0x90);

struct s_scenario_squad_spawn_formation
{
    s_scenario_ai_conditions spawn_conditions;
    string_id name;
    s_scenario_ai_point3_d point;
    string_id formation_type;
    ulong initial_movement_distance;
    c_enum<e_scenario_squad_movement_mode, short> initial_movement_mode;
    short placement_script_index;
    char placement_script_name[32];
    string_id initial_state;
    short point_set_index;
    c_enum<e_scenario_squad_patrol_mode, short> patrol_mode;
    c_tag_block<s_scenario_squad_point> patrol_points;
};
static_assert(sizeof(s_scenario_squad_spawn_formation) == 0x6C);

struct s_scenario_squad_point
{
    short point_index;
    c_flags<e_scenario_squad_point_flags, ushort> flags;
    float delay;
    float angle_degrees;
    string_id activity_name;
    c_enum<e_scenario_squad_activity, short> activity;
    short activity_variant;
    char command_script_name[32];
    short command_script_index;
    byte unused[2];
};
static_assert(sizeof(s_scenario_squad_point) == 0x38);

struct s_scenario_ai_point3_d
{
    real_point3d position;
    short reference_frame;
    short bsp_index;
    real_euler_angles3d facing;
};
static_assert(sizeof(s_scenario_ai_point3_d) == 0x1C);

struct s_scenario_ai_conditions
{
    c_flags<e_scenario_squad_difficulty_flags, ushort> difficulty_flags;
    byte unused[2];
    int16 round_range[2];
    int16 set_range[2];
};
static_assert(sizeof(s_scenario_ai_conditions) == 0xC);

struct s_scenario_squad_group
{
    char name[32];
    short parent_index;
    short initial_objective;
    byte unused2[2];
    short editor_folder_index;
};
static_assert(sizeof(s_scenario_squad_group) == 0x28);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

struct s_scenario_decal
{
    short decal_palette_index;
    c_flags<e_scenario_decal_flag_bits, uchar> flags;
    byte unused[1];
    real_quaternion rotation;
    real_point3d position;
    float scale;
};
static_assert(sizeof(s_scenario_decal) == 0x24);

struct s_scenario_unknown_block
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
};
static_assert(sizeof(s_scenario_unknown_block) == 0x14);

struct s_scenario_zone_set_switch_trigger_volume
{
    c_flags<e_scenario_zone_set_switch_trigger_volume_flag_bits, ushort> flags;
    short begin_zone_set;
    short trigger_volume;
    short commit_zone_set;
    short unknown2;
    short unknown3;
};
static_assert(sizeof(s_scenario_zone_set_switch_trigger_volume) == 0xC);

struct s_scenario_recorded_animation
{
    char name[32];
    char version;
    char raw_animation_data;
    char unit_control_data_version;
    byte unused1[1];
    short length_of_animation;
    byte unused2[2];
    byte unused3[4];
    s_tag_data recorded_animation_event_stream;
};
static_assert(sizeof(s_scenario_recorded_animation) == 0x40);

struct s_scenario_trigger_volume
{
    string_id name;
    short object_name;
    short runtime_node_index;
    string_id node_name;
    c_enum<e_scenario_trigger_volume_type, short> type;
    byte unused[2];
    real_vector3d forward;
    real_vector3d up;
    real_point3d position;
    real_point3d extents;
    float z_sink;
    c_tag_block<s_scenario_trigger_volume_sector_point> sector_points;
    c_tag_block<s_scenario_trigger_volume_runtime_triangle> runtime_triangles;
    real_bounds runtime_sector_x_bounds;
    real_bounds runtime_sector_y_bounds;
    real_bounds runtime_sector_z_bounds;
    float c;
    short kill_volume;
    short editor_folder_index;
};
static_assert(sizeof(s_scenario_trigger_volume) == 0x7C);

struct s_scenario_trigger_volume_runtime_triangle
{
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    ulong unknown19;
    ulong unknown20;
};
static_assert(sizeof(s_scenario_trigger_volume_runtime_triangle) == 0x50);

struct s_scenario_trigger_volume_sector_point
{
    real_point3d position;
    real_euler_angles2d normal;
};
static_assert(sizeof(s_scenario_trigger_volume_sector_point) == 0x14);

struct s_scenario_player_starting_location
{
    real_point3d position;
    real_euler_angles2d facing;
    short insertion_point_index;
    c_flags<e_scenario_player_starting_location_flags, ushort> flags;
    short editor_folder_index;
    byte unused[2];
};
static_assert(sizeof(s_scenario_player_starting_location) == 0x1C);

struct s_scenario_player_starting_profile_block
{
    char name[32];
    float starting_health_damage;
    float starting_shield_damage;
    s_tag_reference primary_weapon;
    short primary_rounds_loaded;
    short primary_rounds_total;
    s_tag_reference secondary_weapon;
    short secondary_rounds_loaded;
    short secondary_rounds_total;
    ulong unknown;
    ulong unknown2;
    uchar starting_frag_grenade_count;
    uchar starting_plasma_grenade_count;
    uchar starting_spike_grenade_count;
    uchar starting_firebomb_grenade_count;
    short unknown3;
    short unknown4;
};
static_assert(sizeof(s_scenario_player_starting_profile_block) == 0x60);

struct s_scenario_soft_ceiling
{
    c_flags<e_scenario_soft_ceiling_flags, ushort> flags;
    c_flags<e_scenario_soft_ceiling_flags, ushort> runtime_flags;
    string_id name;
    c_enum<e_scenario_soft_ceiling_type, short> type;
    byte unused[2];
};
static_assert(sizeof(s_scenario_soft_ceiling) == 0xC);

struct s_scenario_sandbox_object
{
    s_tag_reference object;
    string_id name;
    long max_allowed;
    float cost;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
};
static_assert(sizeof(s_scenario_sandbox_object) == 0x30);

struct s_scenario_light_volume_instance : s_scenario_scenario_instance
{
    short power_group;
    short position_group;
    ulong device_flags;
    c_enum<e_scenario_light_volume_instance_type_value2, short> type2;
    ushort flags;
    c_enum<e_scenario_light_volume_instance_lightmap_type, short> lightmap_type;
    ushort lightmap_flags;
    float lightmap_half_life;
    float lightmap_light_scale;
    float x;
    float y;
    float z;
    float width;
    float height_scale;
    real field_of_view;
    float falloff_distance;
    float cutoff_distance;
};
static_assert(sizeof(s_scenario_light_volume_instance) == 0x8C);

struct s_scenario_effect_scenery_instance : s_scenario_scenario_instance
{
};
static_assert(sizeof(s_scenario_effect_scenery_instance) == 0x54);

struct s_scenario_giant_instance : s_scenario_permutation_instance
{
    float body_vitality_percentage;
    ulong flags;
    short unknown11;
    short unknown12;
    c_tag_block<s_scenario_giant_instance_pathfinding_reference> pathfinding_references;
};
static_assert(sizeof(s_scenario_giant_instance) == 0x88);

struct s_scenario_giant_instance_pathfinding_reference
{
    short bsp_index;
    short pathfinding_object_index;
};
static_assert(sizeof(s_scenario_giant_instance_pathfinding_reference) == 0x0);

struct s_scenario_sound_scenery_instance : s_scenario_scenario_instance
{
    long volume_type;
    float height;
    real_bounds override_distance;
    real_bounds override_cone_angle;
    float override_outer_cone_gain;
};
static_assert(sizeof(s_scenario_sound_scenery_instance) == 0x70);

struct s_scenario_control_instance : s_scenario_scenario_instance
{
    string_id variant;
    uchar active_change_colors;
    char unknown7;
    char unknown8;
    char unknown9;
    argb_color primary_color;
    argb_color secondary_color;
    argb_color tertiary_color;
    argb_color quaternary_color;
    ulong unknown10;
    short power_group;
    short position_group;
    ulong device_flags;
    ulong control_flags;
    short unknown11;
    short unknown12;
};
static_assert(sizeof(s_scenario_control_instance) == 0x80);

struct s_scenario_alternate_reality_device_instance : s_scenario_permutation_instance
{
    short power_group;
    short position_group;
    ulong device_flags;
    char tap_script_name[32];
    char hold_script_name[32];
    short tap_script_index;
    short hold_script_index;
};
static_assert(sizeof(s_scenario_alternate_reality_device_instance) == 0xBC);

struct s_scenario_terminal_instance : s_scenario_scenario_instance
{
    string_id variant;
    uchar active_change_colors;
    char unknown7;
    char unknown8;
    char unknown9;
    argb_color primary_color;
    argb_color secondary_color;
    argb_color tertiary_color;
    argb_color quaternary_color;
    ulong unknown10;
    short power_group;
    short position_group;
    ulong device_flags;
    ulong machine_flags;
};
static_assert(sizeof(s_scenario_terminal_instance) == 0x7C);

struct s_scenario_machine_instance : s_scenario_scenario_instance
{
    string_id variant;
    uchar active_change_colors;
    char unknown7;
    char unknown8;
    char unknown9;
    argb_color primary_color;
    argb_color secondary_color;
    argb_color tertiary_color;
    argb_color quaternary_color;
    ulong unknown10;
    short power_group;
    short position_group;
    ulong device_flags;
    ulong machine_flags;
    c_tag_block<s_scenario_machine_instance_pathfinding_reference> pathfinding_references;
    c_enum<e_scenario_machine_instance_pathfinding_policy, short> pathfinding_policy;
    short unknown11;
};
static_assert(sizeof(s_scenario_machine_instance) == 0x8C);

struct s_scenario_machine_instance_pathfinding_reference
{
    short bsp_index;
    short pathfinding_object_index;
};
static_assert(sizeof(s_scenario_machine_instance_pathfinding_reference) == 0x0);

struct s_scenario_device_group
{
    char name[32];
    float initial_value;
    c_flags<e_scenario_device_group_flags, long> flags;
    short editor_folder_index;
    short unknown;
};
static_assert(sizeof(s_scenario_device_group) == 0x2C);

struct s_scenario_weapon_instance : s_scenario_permutation_instance
{
    short rounds_left;
    short rounds_loaded;
    ulong weapon_flags;
    s_scenario_multiplayer_object_properties multiplayer;
};
static_assert(sizeof(s_scenario_weapon_instance) == 0xAC);

struct s_scenario_equipment_instance : s_scenario_scenario_instance
{
    ulong equipment_flags;
    s_scenario_multiplayer_object_properties multiplayer;
};
static_assert(sizeof(s_scenario_equipment_instance) == 0x8C);

struct s_scenario_vehicle_instance : s_scenario_permutation_instance
{
    float body_vitality_percentage;
    ulong flags;
    s_scenario_multiplayer_object_properties multiplayer;
};
static_assert(sizeof(s_scenario_vehicle_instance) == 0xAC);

struct s_scenario_biped_instance : s_scenario_permutation_instance
{
    float body_vitality_percentage;
    ulong flags;
};
static_assert(sizeof(s_scenario_biped_instance) == 0x78);

struct s_scenario_scenario_palette_entry
{
    s_tag_reference object;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
};
static_assert(sizeof(s_scenario_scenario_palette_entry) == 0x30);

struct s_scenario_scenery_instance : s_scenario_permutation_instance
{
    c_enum<e_scenario_pathfinding_policy, short> pathfinding_policy;
    c_enum<e_scenario_lightmapping_policy, short> lightmapping_policy;
    c_tag_block<s_scenario_pathfinding_reference> pathfinding_references;
    short unknown11;
    short unknown12;
    s_scenario_multiplayer_object_properties multiplayer;
};
static_assert(sizeof(s_scenario_scenery_instance) == 0xB8);

struct s_scenario_multiplayer_object_properties
{
    c_enum<e_scenario_multiplayer_object_properties_symmetry, long> symmetry;
    ushort engine_flags;
    c_enum<e_scenario_multiplayer_object_properties_team, short> team;
    char spawn_sequence;
    char runtime_minimum;
    char runtime_maximum;
    uchar multiplayer_flags;
    short spawn_time;
    short abandon_time;
    char teleporter_flags;
    c_enum<e_scenario_multiplayer_object_properties_shape, char> shape;
    char teleporter_channel;
    char unknown14;
    short unknown15;
    short attached_name_index;
    ulong unknown16;
    ulong unknown17;
    float width_radius;
    float depth;
    float top;
    float bottom;
    ulong unknown18;
};
static_assert(sizeof(s_scenario_multiplayer_object_properties) == 0x34);

struct s_scenario_pathfinding_reference
{
    short bsp_index;
    short pathfinding_object_index;
};
static_assert(sizeof(s_scenario_pathfinding_reference) == 0x0);

struct s_scenario_permutation_instance : s_scenario_scenario_instance
{
    string_id variant;
    uchar active_change_colors;
    char unknown7;
    char unknown8;
    char unknown9;
    argb_color primary_color;
    argb_color secondary_color;
    argb_color tertiary_color;
    argb_color quaternary_color;
    ulong unknown10;
};
static_assert(sizeof(s_scenario_permutation_instance) == 0x70);

struct s_scenario_scenario_instance
{
    short palette_index;
    short name_index;
    c_flags<e_scenario_object_placement_flags, long> placement_flags;
    real_point3d position;
    real_euler_angles3d rotation;
    float scale;
    c_tag_block<s_scenario_object_node_orientation> node_orientations;
    short unknown2;
    ushort old_manual_bsp_flags_now_zone_sets;
    string_id unique_name;
    datum_handle unique_handle;
    short origin_bsp_index;
    s_scenario_object_type object_type;
    c_enum<e_scenario_scenario_instance_source, char> source;
    c_enum<e_scenario_scenario_instance_bsp_policy, char> bsp_policy;
    char unknown3;
    short editor_folder_index;
    short unknown4;
    short parent_name_index;
    string_id child_name;
    string_id unknown5;
    ushort allowed_zone_sets;
    short unknown6;
};
static_assert(sizeof(s_scenario_scenario_instance) == 0x54);

struct s_scenario_object_node_orientation
{
    short node_count;
    byte unused[2];
    c_tag_block<s_scenario_object_node_orientation_bit_vector> bit_vectors;
    c_tag_block<s_scenario_object_node_orientation_orientation> orientations;
};
static_assert(sizeof(s_scenario_object_node_orientation) == 0x1C);

struct s_scenario_object_node_orientation_orientation
{
    short number;
};
static_assert(sizeof(s_scenario_object_node_orientation_orientation) == 0x2);

struct s_scenario_object_node_orientation_bit_vector
{
    uchar data;
};
static_assert(sizeof(s_scenario_object_node_orientation_bit_vector) == 0x1);

struct s_scenario_object_name
{
    char name[32];
    s_game_object_type object_type;
    short placement_index;
};
static_assert(sizeof(s_scenario_object_name) == 0x24);

struct s_game_object_type
{
    c_enum<e_game_object_type_halo3_odst, char> halo3_odst;
    char unknown2;
};
static_assert(sizeof(s_game_object_type) == 0x2);

struct s_scenario_comment
{
    real_point3d position;
    c_enum<e_scenario_comment_type, long> type;
    char name[32];
    char text[256];
};
static_assert(sizeof(s_scenario_comment) == 0x130);

struct s_scenario_campaign_player
{
    string_id player_representation_name;
};
static_assert(sizeof(s_scenario_campaign_player) == 0x4);

struct s_scenario_bsp_atlas_block
{
    string_id name;
    c_flags<e_scenario_bsp_flags, long> bsp;
    c_flags<e_scenario_bsp_flags, long> connected_bsps;
};
static_assert(sizeof(s_scenario_bsp_atlas_block) == 0xC);

struct s_scenario_zone_set
{
    string_id name;
    long potentially_visible_set_index;
    long import_loaded_bsps;
    c_flags<e_scenario_bsp_flags, long> loaded_bsps;
    c_flags<e_scenario_zone_set_flags, long> loaded_designer_zone_sets;
    c_flags<e_scenario_zone_set_flags, long> unloaded_designer_zone_sets;
    c_flags<e_scenario_zone_set_flags, long> loaded_cinematic_zone_sets;
    long bsp_atlas_index;
    long scenario_bsp_audibility_index;
};
static_assert(sizeof(s_scenario_zone_set) == 0x24);

struct s_scenario_zone_set_audibility_block
{
    long door_portal_count;
    long unique_cluster_count;
    real_bounds cluster_distance_bounds;
    c_tag_block<s_scenario_zone_set_audibility_block_encoded_door_pa> encoded_door_pas;
    c_tag_block<s_scenario_zone_set_audibility_block_room_door_portal_encoded_pa> cluster_door_portal_encoded_pas;
    c_tag_block<s_scenario_zone_set_audibility_block_ai_deafening_pa> ai_deafening_pas;
    c_tag_block<s_scenario_zone_set_audibility_block_room_distance> room_distances;
    c_tag_block<s_scenario_zone_set_audibility_block_game_portal_to_door_occluder_mapping> game_portal_to_door_occluder_mappings;
    c_tag_block<s_scenario_zone_set_audibility_block_bsp_cluster_to_room_bounds_mapping> bsp_cluster_to_room_bounds_mappings;
    c_tag_block<s_scenario_zone_set_audibility_block_bsp_cluster_to_room_index> bsp_cluster_to_room_indices;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block) == 0x64);

struct s_scenario_zone_set_audibility_block_bsp_cluster_to_room_index
{
    short room_index;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_bsp_cluster_to_room_index) == 0x2);

struct s_scenario_zone_set_audibility_block_bsp_cluster_to_room_bounds_mapping
{
    long first_room_index;
    long room_index_count;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_bsp_cluster_to_room_bounds_mapping) == 0x8);

struct s_scenario_zone_set_audibility_block_game_portal_to_door_occluder_mapping
{
    long first_door_occluder_index;
    long door_occluder_count;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_game_portal_to_door_occluder_mapping) == 0x8);

struct s_scenario_zone_set_audibility_block_room_distance
{
    char encoded_data;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_room_distance) == 0x1);

struct s_scenario_zone_set_audibility_block_ai_deafening_pa
{
    long encoded_data;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_ai_deafening_pa) == 0x4);

struct s_scenario_zone_set_audibility_block_room_door_portal_encoded_pa
{
    long encoded_data;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_room_door_portal_encoded_pa) == 0x4);

struct s_scenario_zone_set_audibility_block_encoded_door_pa
{
    long encoded_data;
};
static_assert(sizeof(s_scenario_zone_set_audibility_block_encoded_door_pa) == 0x4);

struct s_scenario_zone_set_pvs_block
{
    c_flags<e_scenario_bsp_flags, long> structure_bsp_mask;
    long version;
    c_tag_block<s_scenario_zone_set_pvs_block_bsp_checksum> bsp_checksums;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set> structure_bsp_potentially_visible_sets;
    c_tag_block<s_scenario_zone_set_pvs_block_portal_to_device_mapping> portal_to_device_mappings;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block) == 0x2C);

struct s_scenario_zone_set_pvs_block_portal_to_device_mapping
{
    c_tag_block<s_scenario_zone_set_pvs_block_portal_to_device_mapping_device_portal_association> device_portal_associations;
    c_tag_block<s_scenario_zone_set_pvs_block_portal_to_device_mapping_game_portal_to_portal_mapping> game_portal_to_portal_mappings;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_portal_to_device_mapping) == 0x18);

struct s_scenario_zone_set_pvs_block_portal_to_device_mapping_game_portal_to_portal_mapping
{
    short portal_index;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_portal_to_device_mapping_game_portal_to_portal_mapping) == 0x2);

struct s_scenario_zone_set_pvs_block_portal_to_device_mapping_device_portal_association
{
    long unique_id;
    short origin_bsp_index;
    s_scenario_object_type object_type;
    c_enum<e_scenario_object_source, char> source;
    short first_game_portal_index;
    ushort game_portal_count;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_portal_to_device_mapping_device_portal_association) == 0xC);

struct s_scenario_object_type
{
    c_enum<e_game_object_type_halo3_odst, char> halo3_odst;
};
static_assert(sizeof(s_scenario_object_type) == 0x1);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set
{
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster> clusters;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster> clusters_doors_closed;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_sky> cluster_skies;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_sky> cluster_visible_skies;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_unknown_block> unknown;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_unknown_block> unknown2;
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping> cluster_mappings;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set) == 0x54);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping
{
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping_cluster_reference> clusters;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping) == 0xC);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping_cluster_reference
{
    char cluster_index;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_bsp_seam_cluster_mapping_cluster_reference) == 0x1);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_unknown_block
{
    ulong unknown;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_unknown_block) == 0x4);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_sky
{
    char sky_index;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_sky) == 0x1);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster
{
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector> bit_vectors;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster) == 0xC);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector
{
    c_tag_block<s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit> bits;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector) == 0xC);

struct s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit
{
    c_flags<e_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit_allow_flags, long> allow;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_structure_bsp_potentially_visible_set_cluster_bit_vector_bit) == 0x4);

struct s_scenario_zone_set_pvs_block_bsp_checksum
{
    ulong checksum;
};
static_assert(sizeof(s_scenario_zone_set_pvs_block_bsp_checksum) == 0x4);

struct s_scenario_sky_reference
{
    s_tag_reference sky_object;
    short name_index;
    c_flags<e_scenario_bsp_short_flags, ushort> active_bsps;
};
static_assert(sizeof(s_scenario_sky_reference) == 0x14);

struct s_scenario_structure_bsp_block
{
    s_tag_reference structure_bsp;
    s_tag_reference design;
    s_tag_reference lighting;
    long unknown;
    float unknown2;
    ulong unknown3;
    ulong unknown4;
    short unknown5;
    short unknown6;
    short unknown7;
    short unknown8;
    s_tag_reference cubemap;
    s_tag_reference wind;
    long unknown9;
};
static_assert(sizeof(s_scenario_structure_bsp_block) == 0x6C);

