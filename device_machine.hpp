/* ---------- enums */

enum e_device_machine_type
{
    _device_machine_type_door = 0,
    _device_machine_type_platform = 1,
    _device_machine_type_gear = 2
};

enum e_device_machine_collision_response
{
    _device_machine_collision_response_pause_until_crushed = 0,
    _device_machine_collision_response_reverse_directions = 1,
    _device_machine_collision_response_discs = 2
};

enum e_device_machine_pathfinding_policy
{
    _device_machine_pathfinding_policy_discs = 0,
    _device_machine_pathfinding_policy_sectors = 1,
    _device_machine_pathfinding_policy_cut_out = 2,
    _device_machine_pathfinding_policy_none = 3
};


/* ---------- structures */

struct s_device_machine;

struct s_device_machine : s_device
{
    c_enum<e_device_machine_type, short> type;
    ushort flags3;
    float door_open_time;
    real_bounds occlusion_bounds;
    c_enum<e_device_machine_collision_response, short> collision_response;
    short elevator_node;
    c_enum<e_device_machine_pathfinding_policy, short> pathfinding_policy;
    short unknown6;
    byte unknown7[12];
};
static_assert(sizeof(s_device_machine) == 0x1DC);

