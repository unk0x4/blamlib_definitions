/* ---------- enums */


/* ---------- structures */

struct s_shader_custom;

struct s_shader_custom : s_render_method
{
    string_id material;
};
static_assert(sizeof(s_shader_custom) == 0x44);

