/* ---------- enums */


/* ---------- structures */

struct s_shader_black;

struct s_shader_black : s_render_method
{
    string_id material;
};
static_assert(sizeof(s_shader_black) == 0x44);

