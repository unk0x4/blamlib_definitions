/* ---------- enums */


/* ---------- structures */

struct s_color_table_color_table_block;
struct s_color_table;

struct s_color_table
{
    c_tag_block<s_color_table_color_table_block> color_tables;
};
static_assert(sizeof(s_color_table) == 0xC);

struct s_color_table_color_table_block
{
    char string[32];
    real_argb_color color;
};
static_assert(sizeof(s_color_table_color_table_block) == 0x30);

