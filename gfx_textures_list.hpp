/* ---------- enums */


/* ---------- structures */

struct s_gfx_textures_list_texture;
struct s_gfx_textures_list;

struct s_gfx_textures_list
{
    c_tag_block<s_gfx_textures_list_texture> textures;
    ulong unknown;
};
static_assert(sizeof(s_gfx_textures_list) == 0x10);

struct s_gfx_textures_list_texture
{
    char file_name[256];
    s_tag_reference bitmap;
};
static_assert(sizeof(s_gfx_textures_list_texture) == 0x110);

