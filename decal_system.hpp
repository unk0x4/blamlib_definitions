/* ---------- enums */

enum e_decal_system_flags
{
    _decal_system_flags_bit0_bit = 0,
    _decal_system_flags_bit1_bit = 1,
    _decal_system_flags_bit2_bit = 2,
    _decal_system_flags_bit3_bit = 3,
    _decal_system_flags_bit4_bit = 4,
    _decal_system_flags_bit5_bit = 5,
    _decal_system_flags_bit6_bit = 6,
    _decal_system_flags_bit7_bit = 7,
    _decal_system_flags_bit8_bit = 8,
    _decal_system_flags_bit9_bit = 9,
    _decal_system_flags_bit10_bit = 10,
    _decal_system_flags_bit11_bit = 11,
    _decal_system_flags_bit12_bit = 12,
    _decal_system_flags_bit13_bit = 13,
    _decal_system_flags_bit14_bit = 14,
    _decal_system_flags_bit15_bit = 15,
    _decal_system_flags_bit16_bit = 16,
    _decal_system_flags_bit17_bit = 17,
    _decal_system_flags_bit18_bit = 18,
    _decal_system_flags_bit19_bit = 19,
    _decal_system_flags_bit20_bit = 20,
    _decal_system_flags_bit21_bit = 21,
    _decal_system_flags_bit22_bit = 22,
    _decal_system_flags_bit23_bit = 23,
    _decal_system_flags_bit24_bit = 24,
    _decal_system_flags_bit25_bit = 25,
    _decal_system_flags_bit26_bit = 26,
    _decal_system_flags_bit27_bit = 27,
    _decal_system_flags_bit28_bit = 28,
    _decal_system_flags_bit29_bit = 29,
    _decal_system_flags_bit30_bit = 30,
    _decal_system_flags_bit31_bit = 31
};

enum e_decal_system_decal_definition_block_flags
{
    _decal_system_decal_definition_block_flags_custom_blend_factor_bit = 0,
    _decal_system_decal_definition_block_flags_bit1_bit = 1,
    _decal_system_decal_definition_block_flags_bit2_bit = 2,
    _decal_system_decal_definition_block_flags_bit3_bit = 3,
    _decal_system_decal_definition_block_flags_bit4_bit = 4,
    _decal_system_decal_definition_block_flags_bit5_bit = 5,
    _decal_system_decal_definition_block_flags_bit6_bit = 6,
    _decal_system_decal_definition_block_flags_bit7_bit = 7,
    _decal_system_decal_definition_block_flags_bit8_bit = 8,
    _decal_system_decal_definition_block_flags_bit9_bit = 9,
    _decal_system_decal_definition_block_flags_bit10_bit = 10,
    _decal_system_decal_definition_block_flags_bit11_bit = 11,
    _decal_system_decal_definition_block_flags_bit12_bit = 12,
    _decal_system_decal_definition_block_flags_bit13_bit = 13,
    _decal_system_decal_definition_block_flags_bit14_bit = 14,
    _decal_system_decal_definition_block_flags_bit15_bit = 15,
    _decal_system_decal_definition_block_flags_bit16_bit = 16,
    _decal_system_decal_definition_block_flags_bit17_bit = 17,
    _decal_system_decal_definition_block_flags_bit18_bit = 18,
    _decal_system_decal_definition_block_flags_bit19_bit = 19,
    _decal_system_decal_definition_block_flags_bit20_bit = 20,
    _decal_system_decal_definition_block_flags_bit21_bit = 21,
    _decal_system_decal_definition_block_flags_bit22_bit = 22,
    _decal_system_decal_definition_block_flags_bit23_bit = 23,
    _decal_system_decal_definition_block_flags_bit24_bit = 24,
    _decal_system_decal_definition_block_flags_bit25_bit = 25,
    _decal_system_decal_definition_block_flags_bit26_bit = 26,
    _decal_system_decal_definition_block_flags_bit27_bit = 27,
    _decal_system_decal_definition_block_flags_bit28_bit = 28,
    _decal_system_decal_definition_block_flags_bit29_bit = 29,
    _decal_system_decal_definition_block_flags_bit30_bit = 30,
    _decal_system_decal_definition_block_flags_bit31_bit = 31
};

enum e_render_method_option_option_block_option_data_type
{
    _render_method_option_option_block_option_data_type_sampler = 0,
    _render_method_option_option_block_option_data_type_float4 = 1,
    _render_method_option_option_block_option_data_type_float = 2,
    _render_method_option_option_block_option_data_type_integer = 3,
    _render_method_option_option_block_option_data_type_boolean = 4,
    _render_method_option_option_block_option_data_type_integer_color = 5
};

enum e_render_method_shader_function_function_type
{
    _render_method_shader_function_function_type_value = 0,
    _render_method_shader_function_function_type_color = 1,
    _render_method_shader_function_function_type_scale_uniform = 2,
    _render_method_shader_function_function_type_scale_x = 3,
    _render_method_shader_function_function_type_scale_y = 4,
    _render_method_shader_function_function_type_translation_x = 5,
    _render_method_shader_function_function_type_translation_y = 6,
    _render_method_shader_function_function_type_frame_index = 7,
    _render_method_shader_function_function_type_alpha = 8,
    _render_method_shader_function_function_type_change_color_primary = 9,
    _render_method_shader_function_function_type_change_color_secondary = 10,
    _render_method_shader_function_function_type_change_color_tertiary = 11,
    _render_method_shader_function_function_type_change_color_quaternary = 12,
    _render_method_shader_function_function_type_change_color_quinary = 13
};

enum e_render_method_shader_property_texture_constant_sampler_filter_mode
{
    _render_method_shader_property_texture_constant_sampler_filter_mode_trilinear = 0,
    _render_method_shader_property_texture_constant_sampler_filter_mode_point = 1,
    _render_method_shader_property_texture_constant_sampler_filter_mode_bilinear = 2,
    _render_method_shader_property_texture_constant_sampler_filter_mode_unused_00 = 3,
    _render_method_shader_property_texture_constant_sampler_filter_mode_anisotropic_2_x = 4,
    _render_method_shader_property_texture_constant_sampler_filter_mode_unused_01 = 5,
    _render_method_shader_property_texture_constant_sampler_filter_mode_anisotropic_4_x = 6,
    _render_method_shader_property_texture_constant_sampler_filter_mode_lightprobe_texture_array = 7,
    _render_method_shader_property_texture_constant_sampler_filter_mode_texture_array_quadlinear = 8,
    _render_method_shader_property_texture_constant_sampler_filter_mode_texture_array_quadanisotropic_2_x = 9
};

enum e_render_method_shader_property_alpha_blend_mode
{
    _render_method_shader_property_alpha_blend_mode_opaque = 0,
    _render_method_shader_property_alpha_blend_mode_additive = 1,
    _render_method_shader_property_alpha_blend_mode_multiply = 2,
    _render_method_shader_property_alpha_blend_mode_alpha_blend = 3,
    _render_method_shader_property_alpha_blend_mode_double_multiply = 4,
    _render_method_shader_property_alpha_blend_mode_pre_multiplied_alpha = 5,
    _render_method_shader_property_alpha_blend_mode_maximum = 6,
    _render_method_shader_property_alpha_blend_mode_multiply_add = 7,
    _render_method_shader_property_alpha_blend_mode_add_src_times_dstalpha = 8,
    _render_method_shader_property_alpha_blend_mode_add_src_times_srcalpha = 9,
    _render_method_shader_property_alpha_blend_mode_inv_alpha_blend = 10,
    _render_method_shader_property_alpha_blend_mode_separate_alpha_blend = 11,
    _render_method_shader_property_alpha_blend_mode_separate_alpha_blend_additive = 12
};

enum e_render_method_shader_property_blend_mode_flags
{
    _render_method_shader_property_blend_mode_flags_bit0_bit = 0,
    _render_method_shader_property_blend_mode_flags_enable_alpha_test_bit = 1,
    _render_method_shader_property_blend_mode_flags_sfx_distort_force_alpha_blend_bit = 2
};

enum e_render_method_render_method_render_flags
{
    _render_method_render_method_render_flags_ignore_fog_bit = 0,
    _render_method_render_method_render_flags_use_sky_atmosphere_properties_bit = 1,
    _render_method_render_method_render_flags_uses_depth_camera_bit = 2,
    _render_method_render_method_render_flags_disable_with_shields_bit = 3,
    _render_method_render_method_render_flags_enable_with_shields_bit = 4
};

enum e_sorting_layer
{
    _sorting_layer_invalid = 0,
    _sorting_layer_pre_pass = 1,
    _sorting_layer_normal = 2,
    _sorting_layer_post_pass = 3
};


/* ---------- structures */

struct s_render_method_render_method_definition_option_index;
struct s_tag_function;
struct s_render_method_shader_function;
struct s_render_method_import_datum;
struct s_render_method_shader_property_texture_constant_packed_sampler_address_mode;
struct s_render_method_template_tag_block_index;
struct s_render_method_shader_property_texture_constant;
struct s_render_method_shader_property_real_constant;
struct s_render_method_shader_property_parameter_table;
struct s_render_method_shader_property_parameter_mapping;
struct s_render_method_shader_property;
struct s_render_method;
struct s_decal_system_decal_definition_block;
struct s_decal_system;

struct s_decal_system
{
    c_flags<e_decal_system_flags, long> flags;
    short ring_buffer_size;
    short ring_buffer_size_single_player;
    float material_shader_fade_time;
    real_point2d decal_scale_override;
    c_tag_block<s_decal_system_decal_definition_block> decal;
    float runtime_max_radius;
};
static_assert(sizeof(s_decal_system) == 0x24);

struct s_decal_system_decal_definition_block
{
    string_id decal_name;
    c_flags<e_decal_system_decal_definition_block_flags, long> flags;
    s_render_method render_method;
    real_bounds radius;
    real_bounds decay_time;
    real_bounds lifespan;
    float clamp_angle;
    float cull_angle;
    long unknown2;
    float depth_bias;
    float runtime_bitmap_aspect;
};
static_assert(sizeof(s_decal_system_decal_definition_block) == 0x74);

struct s_render_method
{
    s_tag_reference base_render_method;
    c_tag_block<s_render_method_render_method_definition_option_index> render_method_definition_option_indices;
    c_tag_block<s_render_method_import_datum> import_data;
    c_tag_block<s_render_method_shader_property> shader_properties;
    c_flags<e_render_method_render_method_render_flags, ushort> render_flags;
    c_enum<e_sorting_layer, uchar> sorting_layer;
    uchar version;
    long sky_atmosphere_properties_index;
    long unknown2;
};
static_assert(sizeof(s_render_method) == 0x40);

struct s_render_method_shader_property
{
    s_tag_reference template;
    c_tag_block<s_render_method_shader_property_texture_constant> texture_constants;
    c_tag_block<s_render_method_shader_property_real_constant> real_constants;
    c_tag_block<ulong> integer_constants;
    ulong boolean_constants;
    c_tag_block<s_render_method_template_tag_block_index> entry_points;
    c_tag_block<s_render_method_shader_property_parameter_table> parameter_tables;
    c_tag_block<s_render_method_shader_property_parameter_mapping> parameters;
    c_tag_block<s_render_method_shader_function> functions;
    c_enum<e_render_method_shader_property_alpha_blend_mode, ulong> alpha_blend_mode;
    c_flags<e_render_method_shader_property_blend_mode_flags, ulong> blend_flags;
    ulong unknown8;
    int16 queryable_properties[8];
};
static_assert(sizeof(s_render_method_shader_property) == 0x84);

struct s_render_method_shader_property_parameter_mapping
{
    short register_index;
    uchar function_index;
    uchar source_index;
};
static_assert(sizeof(s_render_method_shader_property_parameter_mapping) == 0x4);

struct s_render_method_shader_property_parameter_table
{
    s_render_method_template_tag_block_index texture;
    s_render_method_template_tag_block_index real_vertex;
    s_render_method_template_tag_block_index real_pixel;
};
static_assert(sizeof(s_render_method_shader_property_parameter_table) == 0x6);

struct s_render_method_shader_property_real_constant
{
    single values[4];
};
static_assert(sizeof(s_render_method_shader_property_real_constant) == 0x10);

struct s_render_method_shader_property_texture_constant
{
    s_tag_reference bitmap;
    short bitmap_index;
    s_render_method_shader_property_texture_constant_packed_sampler_address_mode sampler_address_mode;
    c_enum<e_render_method_shader_property_texture_constant_sampler_filter_mode, uchar> filter_mode;
    char extern_mode;
    char x_form_argument_index;
    s_render_method_template_tag_block_index functions;
};
static_assert(sizeof(s_render_method_shader_property_texture_constant) == 0x18);

struct s_render_method_template_tag_block_index
{
    ushort integer;
};
static_assert(sizeof(s_render_method_template_tag_block_index) == 0x2);

struct s_render_method_shader_property_texture_constant_packed_sampler_address_mode
{
    uchar sampler_address_uv;
};
static_assert(sizeof(s_render_method_shader_property_texture_constant_packed_sampler_address_mode) == 0x1);

struct s_render_method_import_datum
{
    string_id name;
    c_enum<e_render_method_option_option_block_option_data_type, ulong> type;
    s_tag_reference bitmap;
    float default_real_value;
    long default_int_bool_value;
    short sampler_flags;
    short sampler_filter_mode;
    short default_address_mode;
    short address_u;
    short address_v;
    short unknown9;
    ulong unknown10;
    c_tag_block<s_render_method_shader_function> functions;
};
static_assert(sizeof(s_render_method_import_datum) == 0x3C);

struct s_render_method_shader_function
{
    c_enum<e_render_method_shader_function_function_type, long> type;
    string_id input_name;
    string_id range_name;
    float time_period;
    s_tag_function function;
};
static_assert(sizeof(s_render_method_shader_function) == 0x24);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_render_method_render_method_definition_option_index
{
    short option_index;
};
static_assert(sizeof(s_render_method_render_method_definition_option_index) == 0x2);

