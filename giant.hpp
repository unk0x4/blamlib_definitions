/* ---------- enums */

enum e_giant_giant_flags
{
};

enum e_giant_buckle_setting_curve
{
    _giant_buckle_setting_curve_linear = 0,
    _giant_buckle_setting_curve_light_ease_in = 1,
    _giant_buckle_setting_curve_full_ease_in = 2,
    _giant_buckle_setting_curve_light_ease_out = 3,
    _giant_buckle_setting_curve_full_ease_out = 4,
    _giant_buckle_setting_curve_light_ease_in_and_out = 5,
    _giant_buckle_setting_curve_full_ease_in_and_out = 6
};


/* ---------- structures */

struct s_giant_buckle_setting;
struct s_giant;

struct s_giant : s_unit
{
    c_flags<e_giant_giant_flags, long> giant_flags;
    float acceleration_time;
    float deceleration_time;
    float speed_scale;
    float elevation_change_rate;
    float foot_target_radius;
    c_tag_block<s_giant_buckle_setting> buckle_settings;
    float ankle_ik_scale;
};
static_assert(sizeof(s_giant) == 0x410);

struct s_giant_buckle_setting
{
    float lower_time;
    c_enum<e_giant_buckle_setting_curve, long> lower_curve;
    float raise_time;
    c_enum<e_giant_buckle_setting_curve, long> raise_curve;
    float pause_time_easy;
    float pause_time_normal;
    float pause_time_heroic;
    float pause_time_legendary;
    float buckle_gravity_scale;
    string_id buckling_marker;
    float forward_rear_scan;
    float left_right_scan;
    long forward_rear_steps;
    long left_right_steps;
    real_bounds pitch_bounds;
    real_bounds roll_bounds;
    string_id buckle_animation;
    string_id descent_overlay;
    string_id pause_overlay;
    float descent_overlay_scale;
    float paused_overlay_scale;
};
static_assert(sizeof(s_giant_buckle_setting) == 0x5C);

