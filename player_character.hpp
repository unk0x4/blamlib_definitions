/* ---------- enums */

enum e_player_character_character_flags
{
    _player_character_character_flags_can_sprint_bit = 0,
    _player_character_character_flags_first_person_bit = 1,
    _player_character_character_flags_third_person_bit = 2
};

enum e_player_character_model_customization_block_region_permutation_flags
{
    _player_character_model_customization_block_region_permutation_flags_has_requirement_bit = 0,
    _player_character_model_customization_block_region_permutation_flags_has_special_requirement_bit = 1,
    _player_character_model_customization_block_region_permutation_flags_bit2_bit = 2,
    _player_character_model_customization_block_region_permutation_flags_bit3_bit = 3,
    _player_character_model_customization_block_region_permutation_flags_bit4_bit = 4,
    _player_character_model_customization_block_region_permutation_flags_bit5_bit = 5,
    _player_character_model_customization_block_region_permutation_flags_bit6_bit = 6,
    _player_character_model_customization_block_region_permutation_flags_bit7_bit = 7,
    _player_character_model_customization_block_region_permutation_flags_bit8_bit = 8,
    _player_character_model_customization_block_region_permutation_flags_bit9_bit = 9,
    _player_character_model_customization_block_region_permutation_flags_bit10_bit = 10,
    _player_character_model_customization_block_region_permutation_flags_bit11_bit = 11,
    _player_character_model_customization_block_region_permutation_flags_bit12_bit = 12,
    _player_character_model_customization_block_region_permutation_flags_bit13_bit = 13,
    _player_character_model_customization_block_region_permutation_flags_bit14_bit = 14,
    _player_character_model_customization_block_region_permutation_flags_bit15_bit = 15
};

enum e_player_character_model_customization_block_change_color_block_change_color_flags
{
    _player_character_model_customization_block_change_color_block_change_color_flags_primary_color_bit = 0,
    _player_character_model_customization_block_change_color_block_change_color_flags_secondary_color_bit = 1,
    _player_character_model_customization_block_change_color_block_change_color_flags_tertiary_color_bit = 2,
    _player_character_model_customization_block_change_color_block_change_color_flags_quaternary_color_bit = 3,
    _player_character_model_customization_block_change_color_block_change_color_flags_quinary_color_bit = 4
};

enum e_player_character_character_position_data_block_flags
{
    _player_character_character_position_data_block_flags_place_biped_relative_to_camera_bit = 0,
    _player_character_character_position_data_block_flags_can_rotate_on_main_menu_bit = 1,
    _player_character_character_position_data_block_flags_has_platform_bit = 2
};


/* ---------- structures */

struct s_player_character_model_customization_block_region_permutation_variant_block;
struct s_player_character_model_customization_block_region_permutation;
struct s_player_character_model_customization_block_region;
struct s_player_character_model_customization_block_change_color_block_change_color;
struct s_player_character_model_customization_block_change_color_block;
struct s_player_character_model_customization_block;
struct s_globals_player_representation_block;
struct s_globals_player_information_block;
struct s_globals_player_control_block_look_function_block;
struct s_globals_player_control_block;
struct s_player_character_first_person_weapon_interface_block;
struct s_player_character_character_position_data_block;
struct s_player_character;

struct s_player_character
{
    c_flags<e_player_character_character_flags, ulong> flags;
    string_id character_name;
    string_id character_description;
    s_tag_reference hud_globals;
    s_tag_reference vision_mode_globals;
    c_tag_block<s_player_character_model_customization_block> model_customization;
    c_tag_block<s_globals_player_representation_block> player_representation;
    c_tag_block<s_globals_player_information_block> player_information;
    c_tag_block<s_globals_player_control_block> player_control;
    c_tag_block<s_player_character_first_person_weapon_interface_block> first_person_weapon_overrides;
    c_tag_block<s_player_character_character_position_data_block> character_position_data;
};
static_assert(sizeof(s_player_character) == 0x74);

struct s_player_character_character_position_data_block
{
    c_flags<e_player_character_character_position_data_block_flags, long> flags;
    char settings_camera_name[32];
    string_id platform_name;
    real_point3d biped_position;
    float biped_rotation;
};
static_assert(sizeof(s_player_character_character_position_data_block) == 0x38);

struct s_player_character_first_person_weapon_interface_block
{
    s_tag_reference weapon;
    s_tag_reference first_person_model;
    s_tag_reference first_person_animations;
};
static_assert(sizeof(s_player_character_first_person_weapon_interface_block) == 0x30);

struct s_globals_player_control_block
{
    float magnetism_friction;
    float magnetism_adhesion;
    float inconsequential_target_scale;
    real_point2d crosshair_location;
    float seconds_to_start;
    float seconds_to_full_speed;
    float decay_rate;
    float full_speed_multiplier;
    float pegged_magnitude;
    float pegged_angular_threshold;
    float unknown4;
    float unknown5;
    float look_default_pitch_rate;
    float look_default_yaw_rate;
    float look_peg_threshold;
    float look_yaw_acceleration_time;
    float look_yaw_acceleration_scale;
    float look_pitch_acceleration_time;
    float look_pitch_acceleration_scale;
    float look_autoleveling_scale;
    float unknown8;
    float unknown9;
    float gravity_scale;
    short unknown10;
    short minimum_autoleveling_ticks;
    c_tag_block<s_globals_player_control_block_look_function_block> look_function;
};
static_assert(sizeof(s_globals_player_control_block) == 0x70);

struct s_globals_player_control_block_look_function_block
{
    float scale;
};
static_assert(sizeof(s_globals_player_control_block_look_function_block) == 0x0);

struct s_globals_player_information_block
{
    float walking_speed;
    float run_forward;
    float run_backward;
    float run_sideways;
    float run_acceleration;
    float sneak_forward;
    float sneak_backward;
    float sneak_sideways;
    float sneak_acceleration;
    float airborne_acceleration;
    real_point3d grenade_origin;
    float stun_movement_penalty;
    float stun_turning_penalty;
    float stun_jumping_penalty;
    real_bounds stun_time_range;
    real_bounds first_person_idle_time_range;
    float first_person_skip_fraction;
    float unknown1;
    s_tag_reference unknown2;
    s_tag_reference unknown3;
    s_tag_reference unknown4;
    long binoculars_zoom_count;
    real_bounds binocular_zoom_range;
    ulong unknown5;
    ulong unknown6;
    s_tag_reference flashlight_on;
    s_tag_reference flashlight_off;
    s_tag_reference default_damage_response;
};
static_assert(sizeof(s_globals_player_information_block) == 0xCC);

struct s_globals_player_representation_block
{
    string_id name;
    ulong flags;
    s_tag_reference first_person_hands;
    s_tag_reference first_person_body;
    s_tag_reference third_person_unit;
    string_id third_person_variant;
    s_tag_reference binoculars_zoom_in_sound;
    s_tag_reference binoculars_zoom_out_sound;
    s_tag_reference combat_dialogue;
};
static_assert(sizeof(s_globals_player_representation_block) == 0x6C);

struct s_player_character_model_customization_block
{
    c_tag_block<s_player_character_model_customization_block_region> regions;
    c_tag_block<s_player_character_model_customization_block_change_color_block> colors;
};
static_assert(sizeof(s_player_character_model_customization_block) == 0x18);

struct s_player_character_model_customization_block_change_color_block
{
    c_flags<e_player_character_model_customization_block_change_color_block_change_color_flags, uchar> valid_colors;
    c_flags<e_player_character_model_customization_block_change_color_block_change_color_flags, uchar> team_colors;
    short unused;
    change_color colors[5];
};
static_assert(sizeof(s_player_character_model_customization_block_change_color_block) == 0x40);

struct s_player_character_model_customization_block_change_color_block_change_color
{
    argb_color default;
    string_id name;
    string_id description;
};
static_assert(sizeof(s_player_character_model_customization_block_change_color_block_change_color) == 0xC);

struct s_player_character_model_customization_block_region
{
    string_id name;
    c_tag_block<s_player_character_model_customization_block_region_permutation> permutations;
};
static_assert(sizeof(s_player_character_model_customization_block_region) == 0x10);

struct s_player_character_model_customization_block_region_permutation
{
    string_id name;
    string_id description;
    c_flags<e_player_character_model_customization_block_region_permutation_flags, ushort> flags;
    short unknown;
    string_id achievement_requirement;
    c_tag_block<s_player_character_model_customization_block_region_permutation_variant_block> variant;
};
static_assert(sizeof(s_player_character_model_customization_block_region_permutation) == 0x1C);

struct s_player_character_model_customization_block_region_permutation_variant_block
{
    string_id region;
    string_id permutation;
};
static_assert(sizeof(s_player_character_model_customization_block_region_permutation_variant_block) == 0x8);

