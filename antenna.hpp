/* ---------- enums */


/* ---------- structures */

struct s_antenna_vertex;
struct s_antenna;

struct s_antenna
{
    string_id attachment_marker_name;
    s_tag_reference bitmaps;
    s_tag_reference physics;
    float spring_strength_coefficient;
    float falloff_pixels;
    float cutoff_pixels;
    float point_of_bend;
    float starting_bend;
    float ending_bend;
    float runtime_total_length;
    c_tag_block<s_antenna_vertex> vertices;
};
static_assert(sizeof(s_antenna) == 0x4C);

struct s_antenna_vertex
{
    real_euler_angles2d angles;
    float length;
    short sequence_index;
    byte unused[2];
    real_argb_color color;
    real_argb_color lod_color;
    float hermite_t;
    real_vector3d vector_to_next;
};
static_assert(sizeof(s_antenna_vertex) == 0x40);

