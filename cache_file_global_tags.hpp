/* ---------- enums */


/* ---------- structures */

struct s_tag_reference_block;
struct s_cache_file_global_tags;

struct s_cache_file_global_tags
{
    c_tag_block<s_tag_reference_block> global_tags;
    byte unused[4];
};
static_assert(sizeof(s_cache_file_global_tags) == 0x10);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

