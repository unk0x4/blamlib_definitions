/* ---------- enums */

enum e_old_raw_page_flags
{
    _old_raw_page_flags_use_checksum_bit = 0,
    _old_raw_page_flags_in_resources_bit = 1,
    _old_raw_page_flags_in_textures_bit = 2,
    _old_raw_page_flags_in_textures_b_bit = 3,
    _old_raw_page_flags_in_audio_bit = 4,
    _old_raw_page_flags_in_resources_b_bit = 5,
    _old_raw_page_flags_in_mods_bit = 6,
    _old_raw_page_flags_use_checksum2_bit = 7,
    _old_raw_page_flags_location_mask_bit = 1
};


/* ---------- structures */

struct s_codec_definition;
struct s_resource_shared_file;
struct s_resource_page;
struct s_resource_subpage;
struct s_resource_subpage_table;
struct s_resource_section;
struct s_resource_layout_table;

struct s_resource_layout_table
{
    c_tag_block<s_codec_definition> codec_definitions;
    c_tag_block<s_resource_shared_file> shared_files;
    c_tag_block<s_resource_page> pages;
    c_tag_block<s_resource_subpage_table> subpage_tables;
    c_tag_block<s_resource_section> sections;
};
static_assert(sizeof(s_resource_layout_table) == 0x3C);

struct s_resource_section
{
    short required_page_index;
    short optional_page_index;
    long required_segment_offset;
    long optional_segment_offset;
    short required_size_index;
    short optional_size_index;
};
static_assert(sizeof(s_resource_section) == 0x10);

struct s_resource_subpage_table
{
    long total_size;
    c_tag_block<s_resource_subpage> subpages;
};
static_assert(sizeof(s_resource_subpage_table) == 0x10);

struct s_resource_subpage
{
    long offset;
    long size;
};
static_assert(sizeof(s_resource_subpage) == 0x8);

struct s_resource_page
{
    short salt;
    c_flags<e_old_raw_page_flags, uchar> old_flags;
    char compression_codec_index;
    long index;
    ulong compressed_block_size;
    ulong uncompressed_block_size;
    long crc_checksum;
    ulong unknown_size;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_resource_page) == 0x24);

struct s_resource_shared_file
{
    char map_path[256];
    short unknown;
    ushort flags;
    ulong block_offset;
};
static_assert(sizeof(s_resource_shared_file) == 0x108);

struct s_codec_definition
{
    s_tag_data guid;
};
static_assert(sizeof(s_codec_definition) == 0x10);

