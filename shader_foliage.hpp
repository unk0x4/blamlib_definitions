/* ---------- enums */


/* ---------- structures */

struct s_shader_foliage;

struct s_shader_foliage : s_render_method
{
    string_id material;
};
static_assert(sizeof(s_shader_foliage) == 0x44);

