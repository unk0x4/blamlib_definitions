/* ---------- enums */

enum e_multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category
{
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_none = 0,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_options = 1,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_social_options = 2,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_map_overrides = 3,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_base_traits_main = 4,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_base_traits_health = 5,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_base_traits_weapons = 6,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_base_traits_movement = 7,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_base_traits_sensors = 8,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_base_traits_appearance = 9,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown = 10,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_traits_main = 11,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_traits_health = 12,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_traits_weapons = 13,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_traits_movement = 14,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_traits_sensors = 15,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_traits_appearance = 16,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown2 = 17,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_red_traits_main = 18,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_red_traits_health = 19,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_red_traits_weapons = 20,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_red_traits_movement = 21,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_red_traits_sensors = 22,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_red_traits_appearance = 23,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown3 = 24,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_blue_traits_main = 25,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_blue_traits_health = 26,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_blue_traits_weapons = 27,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_blue_traits_movement = 28,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_blue_traits_sensors = 29,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_blue_traits_appearance = 30,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown4 = 31,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_yellow_traits_main = 32,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_yellow_traits_health = 33,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_yellow_traits_weapons = 34,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_yellow_traits_movement = 35,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_yellow_traits_sensors = 36,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups_yellow_traits_appearance = 37,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown5 = 38,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown6 = 39,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_leader_traits_main = 40,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_leader_traits_health = 41,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_leader_traits_weapons = 42,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_leader_traits_movement = 43,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_leader_traits_sensors = 44,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_leader_traits_appearance = 45,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown7 = 46,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_carrier_traits_main = 47,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_carrier_traits_health = 48,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_carrier_traits_weapons = 49,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_carrier_traits_movement = 50,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_carrier_traits_sensors = 51,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_carrier_traits_appearance = 52,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown8 = 53,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_carrier_traits_main = 54,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_carrier_traits_health = 55,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_carrier_traits_weapons = 56,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_carrier_traits_movement = 57,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_carrier_traits_sensors = 58,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_carrier_traits_appearance = 59,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown9 = 60,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_carrier_traits_main = 61,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_carrier_traits_health = 62,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_carrier_traits_weapons = 63,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_carrier_traits_movement = 64,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_carrier_traits_sensors = 65,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_carrier_traits_appearance = 66,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown10 = 67,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_juggy_traits_main = 68,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_juggy_traits_health = 69,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_juggy_traits_weapons = 70,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_juggy_traits_movement = 71,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_juggy_traits_sensors = 72,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_juggy_traits_appearance = 73,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown11 = 74,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_hill_traits_main = 75,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_hill_traits_health = 76,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_hill_traits_weapons = 77,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_hill_traits_movement = 78,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_hill_traits_sensors = 79,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_hill_traits_appearance = 80,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown12 = 81,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_zombie_traits_main = 82,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_zombie_traits_health = 83,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_zombie_traits_weapons = 84,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_zombie_traits_movement = 85,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_zombie_traits_sensors = 86,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_zombie_traits_appearance = 87,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown13 = 88,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_alpha_zombie_traits_main = 89,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_alpha_zombie_traits_health = 90,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_alpha_zombie_traits_weapons = 91,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_alpha_zombie_traits_movement = 92,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_alpha_zombie_traits_sensors = 93,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_alpha_zombie_traits_appearance = 94,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown14 = 95,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_safe_haven_traits_main = 96,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_safe_haven_traits_health = 97,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_safe_haven_traits_weapons = 98,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_safe_haven_traits_movement = 99,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_safe_haven_traits_sensors = 100,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_safe_haven_traits_appearance = 101,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown15 = 102,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_last_man_traits_main = 103,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_last_man_traits_health = 104,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_last_man_traits_weapons = 105,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_last_man_traits_movement = 106,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_last_man_traits_sensors = 107,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_last_man_traits_appearance = 108,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown16 = 109,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown_unknown_traits_main = 110,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown_unknown_traits_health = 111,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown_unknown_traits_weapons = 112,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown_unknown_traits_movement = 113,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown_unknown_traits_sensors = 114,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown_unknown_traits_appearance = 115,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown17 = 116,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_defender_traits_main = 117,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_defender_traits_health = 118,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_defender_traits_weapons = 119,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_defender_traits_movement = 120,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_defender_traits_sensors = 121,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_defender_traits_appearance = 122,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown18 = 123,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_attacker_traits_main = 124,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_attacker_traits_health = 125,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_attacker_traits_weapons = 126,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_attacker_traits_movement = 127,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_attacker_traits_sensors = 128,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_attacker_traits_appearance = 129,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown19 = 130,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_team_traits_main = 131,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_team_traits_health = 132,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_team_traits_weapons = 133,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_team_traits_movement = 134,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_team_traits_sensors = 135,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_team_traits_appearance = 136,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown20 = 137,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_influence_traits_main = 138,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_influence_traits_health = 139,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_influence_traits_weapons = 140,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_influence_traits_movement = 141,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_influence_traits_sensors = 142,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_influence_traits_appearance = 143,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown21 = 144,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_vip_traits_main = 145,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_vip_traits_health = 146,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_vip_traits_weapons = 147,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_vip_traits_movement = 148,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_vip_traits_sensors = 149,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_vip_traits_appearance = 150,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown22 = 151,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_editor_traits_main = 152,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_editor_traits_health = 153,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_editor_traits_weapons = 154,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_editor_traits_movement = 155,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_editor_traits_sensors = 156,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_editor_traits_appearance = 157,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown23 = 158,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_top = 159,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_top = 160,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_top = 161,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_top = 162,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_editor_top = 163,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_top = 164,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_top = 165,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_top = 166,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_top = 167,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_top = 168,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown24 = 169,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_powerups = 170,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_advanced = 171,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_global_respawn_modifiers = 172,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_main = 173,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown25 = 174,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_slayer_scoring = 175,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_main = 176,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_oddball_scoring = 177,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_main = 178,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_assault_scoring = 179,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_main = 180,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_ctf_scoring = 181,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_main = 182,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_juggernaut_scoring = 183,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_main = 184,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_koth_scoring = 185,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_main = 186,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_advanced = 187,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_infection_scoring = 188,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown26 = 189,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown27 = 190,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown28 = 191,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown29 = 192,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown30 = 193,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown31 = 194,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown32 = 195,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown33 = 196,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown34 = 197,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown35 = 198,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown36 = 199,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_unknown37 = 200,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_main = 201,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_vip_scoring = 202,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_main = 203,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_territories_scoring = 204,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_traits_main = 205,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_traits_health = 206,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_traits_weapons = 207,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_traits_movement = 208,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_traits_sensors = 209,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_traits_appearance = 210,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_dynamic_traits_main = 211,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_dynamic_traits_health = 212,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_dynamic_traits_weapons = 213,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_dynamic_traits_movement = 214,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_dynamic_traits_sensors = 215,
    _multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category_templates_dynamic_traits_appearance = 216
};


/* ---------- structures */

struct s_multiplayer_variant_settings_interface_definition_game_engine_setting_option;
struct s_multiplayer_variant_settings_interface_definition_game_engine_setting;
struct s_multiplayer_variant_settings_interface_definition;

struct s_multiplayer_variant_settings_interface_definition
{
    long unknown;
    c_tag_block<s_multiplayer_variant_settings_interface_definition_game_engine_setting> game_engine_settings;
};
static_assert(sizeof(s_multiplayer_variant_settings_interface_definition) == 0x10);

struct s_multiplayer_variant_settings_interface_definition_game_engine_setting
{
    string_id name;
    c_enum<e_multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category, long> setting_category;
    c_tag_block<s_multiplayer_variant_settings_interface_definition_game_engine_setting_option> options;
};
static_assert(sizeof(s_multiplayer_variant_settings_interface_definition_game_engine_setting) == 0x14);

struct s_multiplayer_variant_settings_interface_definition_game_engine_setting_option
{
    s_tag_reference explicit_submenu;
    s_tag_reference template_based_submenu;
    c_enum<e_multiplayer_variant_settings_interface_definition_game_engine_setting_setting_category, long> submenu_setting_category;
    string_id submenu_name;
    string_id submenu_description;
    s_tag_reference value_pairs;
};
static_assert(sizeof(s_multiplayer_variant_settings_interface_definition_game_engine_setting_option) == 0x3C);

