/* ---------- enums */

enum e_damage_effect_flags
{
    _damage_effect_flags_don_t_scale_damage_by_distance_bit = 0,
    _damage_effect_flags_area_damage_players_only_bit = 1,
    _damage_effect_flags_affects_model_targets_bit = 2
};

enum e_damage_effect_side_effect
{
    _damage_effect_side_effect_none = 0,
    _damage_effect_side_effect_harmless = 1,
    _damage_effect_side_effect_lethal_to_the_unsuspecting = 2,
    _damage_effect_side_effect_emp = 3
};

enum e_damage_effect_category
{
    _damage_effect_category_none = 0,
    _damage_effect_category_falling = 1,
    _damage_effect_category_bullet = 2,
    _damage_effect_category_grenade = 3,
    _damage_effect_category_high_explosive = 4,
    _damage_effect_category_sniper = 5,
    _damage_effect_category_melee = 6,
    _damage_effect_category_flame = 7,
    _damage_effect_category_mounted_weapon = 8,
    _damage_effect_category_vehicle = 9,
    _damage_effect_category_plasma = 10,
    _damage_effect_category_needle = 11,
    _damage_effect_category_shotgun = 12
};

enum e_damage_effect_flags_value1
{
    _damage_effect_flags_value1_does_not_hurt_owner_bit = 0,
    _damage_effect_flags_value1_can_cause_headshots_bit = 1,
    _damage_effect_flags_value1_pings_resistant_units_bit = 2,
    _damage_effect_flags_value1_does_not_hurt_friends_bit = 3,
    _damage_effect_flags_value1_does_not_ping_units_bit = 4,
    _damage_effect_flags_value1_detonates_explosives_bit = 5,
    _damage_effect_flags_value1_only_hurts_shields_bit = 6,
    _damage_effect_flags_value1_causes_flaming_death_bit = 7,
    _damage_effect_flags_value1_damage_indicators_always_point_down_bit = 8,
    _damage_effect_flags_value1_skips_shields_bit = 9,
    _damage_effect_flags_value1_only_hurts_one_infection_form_bit = 10,
    _damage_effect_flags_value1_transfer_damage_always_uses_minimum_bit = 11,
    _damage_effect_flags_value1_infection_form_pop_bit = 12,
    _damage_effect_flags_value1_ignore_seat_scale_for_dir_dmg_bit = 13,
    _damage_effect_flags_value1_forces_hard_ping_bit = 14,
    _damage_effect_flags_value1_does_not_hurt_players_bit = 15
};

enum e_damage_effect_player_response_block_response_type
{
    _damage_effect_player_response_block_response_type_shielded = 0,
    _damage_effect_player_response_block_response_type_unshielded = 1,
    _damage_effect_player_response_block_response_type_all = 2
};

enum e_damage_effect_player_response_block_type
{
    _damage_effect_player_response_block_type_none = 0,
    _damage_effect_player_response_block_type_lighten = 1,
    _damage_effect_player_response_block_type_darken = 2,
    _damage_effect_player_response_block_type_max = 3,
    _damage_effect_player_response_block_type_min = 4,
    _damage_effect_player_response_block_type_invert = 5,
    _damage_effect_player_response_block_type_tint = 6
};

enum e_damage_effect_player_response_block_priority
{
    _damage_effect_player_response_block_priority_low = 0,
    _damage_effect_player_response_block_priority_medium = 1,
    _damage_effect_player_response_block_priority_high = 2
};

enum e_function_type
{
    _function_type_linear = 0,
    _function_type_late = 1,
    _function_type_very_late = 2,
    _function_type_early = 3,
    _function_type_very_early = 4,
    _function_type_cosine = 5,
    _function_type_zero = 6,
    _function_type_one = 7
};

enum e_damage_effect_wobble_function
{
    _damage_effect_wobble_function_one = 0,
    _damage_effect_wobble_function_zero = 1,
    _damage_effect_wobble_function_cosine = 2,
    _damage_effect_wobble_function_cosine_variable_period = 3,
    _damage_effect_wobble_function_diagonal_wave = 4,
    _damage_effect_wobble_function_diagonal_wave_variable_period = 5,
    _damage_effect_wobble_function_slide = 6,
    _damage_effect_wobble_function_slide_variable_period = 7,
    _damage_effect_wobble_function_noise = 8,
    _damage_effect_wobble_function_jitter = 9,
    _damage_effect_wobble_function_wander = 10,
    _damage_effect_wobble_function_spark = 11
};


/* ---------- structures */

struct s_tag_function;
struct s_damage_effect_player_response_block;
struct s_damage_effect;

struct s_damage_effect
{
    real_bounds radius;
    float cutoff_scale;
    c_flags<e_damage_effect_flags, ulong> flags;
    c_enum<e_damage_effect_side_effect, short> side_effect;
    c_enum<e_damage_effect_category, short> category;
    c_flags<e_damage_effect_flags_value1, ulong> flags2;
    float area_of_effect_core_radius;
    float damage_lower_bound;
    real_bounds damage_upper_bound;
    real damage_inner_cone_angle;
    real damage_outer_cone_angle;
    float active_camoflage_damage;
    float stun;
    float max_stun;
    float stun_time;
    float instantaneous_acceleration;
    float rider_direct_damage_scale;
    float rider_max_transfer_damage_scale;
    float rider_min_transfer_damage_scale;
    string_id general_damage;
    string_id specific_damage;
    string_id special_damage;
    float ai_stun_radius;
    real_bounds ai_stun_bounds;
    float shake_radius;
    float emp_radius;
    float aoe_spike_radius;
    float aoe_spike_damage_bump;
    float unknown_ho;
    c_tag_block<s_damage_effect_player_response_block> player_responses;
    s_tag_reference damage_response;
    float camera_impulse_duration;
    c_enum<e_function_type, short> camera_impulse_fade_function;
    short unknown;
    real camera_impulse_rotation;
    float camera_impulse_pushback;
    real_bounds camera_impulse_jitter;
    float camera_shake_duration;
    c_enum<e_function_type, short> camera_shake_falloff_function;
    short unknown2;
    float camera_shake_random_translation;
    real camera_shake_random_rotation;
    c_enum<e_damage_effect_wobble_function, short> camera_shake_wobble_function;
    short unknown3;
    float camera_shake_wobble_function_period;
    float camera_shake_wobble_weight;
    s_tag_reference sound;
    float breaking_effect_forward_velocity;
    float breaking_effect_forward_radius;
    float breaking_effect_forward_exponent;
    float breaking_effect_outward_velocity;
    float breaking_effect_outward_radius;
    float breaking_effect_outward_exponent;
};
static_assert(sizeof(s_damage_effect) == 0xF4);

struct s_damage_effect_player_response_block
{
    c_enum<e_damage_effect_player_response_block_response_type, short> response_type;
    short unknown;
    c_enum<e_damage_effect_player_response_block_type, short> type;
    c_enum<e_damage_effect_player_response_block_priority, short> priority;
    float duration;
    c_enum<e_function_type, short> fade_function;
    short unknown2;
    float maximum_intensity;
    real_argb_color color;
    float low_frequency_vibration_duration;
    s_tag_function low_frequency_vibration_function;
    float high_frequency_vibration_duration;
    s_tag_function high_frequency_vibration_function;
    string_id effect_name;
    float duration2;
    s_tag_function effect_scale_function;
};
static_assert(sizeof(s_damage_effect_player_response_block) == 0x70);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

