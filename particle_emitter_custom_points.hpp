/* ---------- enums */


/* ---------- structures */

struct s_particle_emitter_custom_points_point;
struct s_particle_emitter_custom_points;

struct s_particle_emitter_custom_points
{
    s_tag_reference particle_model;
    real_vector3d compression_scale;
    real_vector3d compression_offset;
    c_tag_block<s_particle_emitter_custom_points_point> points;
};
static_assert(sizeof(s_particle_emitter_custom_points) == 0x34);

struct s_particle_emitter_custom_points_point
{
    short position_x;
    short position_y;
    short position_z;
    uchar normal_x;
    uchar normal_y;
    uchar normal_z;
    uchar correlation;
};
static_assert(sizeof(s_particle_emitter_custom_points_point) == 0xA);

