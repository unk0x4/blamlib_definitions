/* ---------- enums */

enum e_test_definition_test_enum_definition
{
};

enum e_test_definition_test_flags_definition
{
};

enum e_old_raw_page_flags
{
    _old_raw_page_flags_use_checksum_bit = 0,
    _old_raw_page_flags_in_resources_bit = 1,
    _old_raw_page_flags_in_textures_bit = 2,
    _old_raw_page_flags_in_textures_b_bit = 3,
    _old_raw_page_flags_in_audio_bit = 4,
    _old_raw_page_flags_in_resources_b_bit = 5,
    _old_raw_page_flags_in_mods_bit = 6,
    _old_raw_page_flags_use_checksum2_bit = 7,
    _old_raw_page_flags_location_mask_bit = 1
};

enum e_tag_resource_type_gen3
{
    _tag_resource_type_gen3_none = -1,
    _tag_resource_type_gen3_collision = 0,
    _tag_resource_type_gen3_bitmap = 1,
    _tag_resource_type_gen3_bitmap_interleaved = 2,
    _tag_resource_type_gen3_sound = 3,
    _tag_resource_type_gen3_animation = 4,
    _tag_resource_type_gen3_render_geometry = 5,
    _tag_resource_type_gen3_bink = 6,
    _tag_resource_type_gen3_pathfinding = 7
};


/* ---------- structures */

struct s_resource_page;
struct s_resource_fixup_location;
struct s_resource_interop_location;
struct s_resource_data;
struct s_pageable_resource;
struct s_test_definition_test_block_definition;
struct s_test_definition;

struct s_test_definition
{
    c_tag_block<s_test_definition_test_block_definition> test_block_field;
};
static_assert(sizeof(s_test_definition) == 0xC);

struct s_test_definition_test_block_definition
{
    uchar byte;
    char s_byte;
    ushort u_short;
    short short;
    ulong u_int;
    long int;
    u_int64 u_long;
    long_long long;
    float float;
    c_enum<e_test_definition_test_enum_definition, long> enum;
    c_flags<e_test_definition_test_flags_definition, long> flags;
    real angle;
    argb_color argb_color;
    s_tag_reference cached_tag_instance;
    cache_address cache_address;
    datum_handle datum_index;
    s_pageable_resource pageable_resource;
    point2d point2_d;
    real_argb_color real_argb_color;
    real_bounding_box real_bounding_box;
    real_euler_angles2d real_euler_angles2_d;
    real_euler_angles3d real_euler_angles3_d;
    real_matrix4_x3 real_matrix4_x3;
    real_plane2d real_plane2_d;
    real_plane3d real_plane3_d;
    real_point2d real_point2_d;
    real_point3d real_point3_d;
    real_quaternion real_quaternion;
    real_rgb_color real_rgb_color;
    real_vector2d real_vector2_d;
    real_vector3d real_vector3_d;
    rectangle2d rectangle2_d;
    string_id string_id;
    tag tag;
    real_bounds bounds_angle;
    uchar_bounds bounds_byte;
    char_bounds bounds_s_byte;
    ushort_bounds bounds_u_short;
    short_bounds bounds_short;
    ulong_bounds bounds_u_int;
    long_bounds bounds_int;
    u_int64_bounds bounds_u_long;
    long_long_bounds bounds_long;
    real_bounds bounds_float;
};
static_assert(sizeof(s_test_definition_test_block_definition) == 0x1E0);

struct s_pageable_resource
{
    s_resource_page page;
    s_resource_data resource;
};
static_assert(sizeof(s_pageable_resource) == 0x6C);

struct s_resource_data
{
    s_tag_reference parent_tag;
    ushort salt;
    c_enum<e_tag_resource_type_gen3, char> resource_type;
    uchar flags;
    s_tag_data definition_data;
    cache_address definition_address;
    c_tag_block<s_resource_fixup_location> fixup_locations;
    c_tag_block<s_resource_interop_location> interop_locations;
    long unknown2;
};
static_assert(sizeof(s_resource_data) == 0x48);

struct s_resource_interop_location
{
    cache_address address;
    long resource_structure_type_index;
};
static_assert(sizeof(s_resource_interop_location) == 0x8);

struct s_resource_fixup_location
{
    ulong block_offset;
    cache_address address;
    long type;
    long offset;
    long raw_address;
};
static_assert(sizeof(s_resource_fixup_location) == 0x8);

struct s_resource_page
{
    short salt;
    c_flags<e_old_raw_page_flags, uchar> old_flags;
    char compression_codec_index;
    long index;
    ulong compressed_block_size;
    ulong uncompressed_block_size;
    long crc_checksum;
    ulong unknown_size;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_resource_page) == 0x24);

