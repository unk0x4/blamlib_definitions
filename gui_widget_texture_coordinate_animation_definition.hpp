/* ---------- enums */


/* ---------- structures */

struct s_gui_widget_texture_coordinate_animation_definition_animation_definition_block;
struct s_tag_function;
struct s_gui_widget_texture_coordinate_animation_definition;

struct s_gui_widget_texture_coordinate_animation_definition
{
    ulong animation_flags;
    c_tag_block<s_gui_widget_texture_coordinate_animation_definition_animation_definition_block> animation_definition;
    s_tag_function function;
    ulong unknown;
    ulong unknown2;
};
static_assert(sizeof(s_gui_widget_texture_coordinate_animation_definition) == 0x2C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_gui_widget_texture_coordinate_animation_definition_animation_definition_block
{
    ulong frame;
    real_point2d coordinate;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
};
static_assert(sizeof(s_gui_widget_texture_coordinate_animation_definition_animation_definition_block) == 0x18);

