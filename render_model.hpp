/* ---------- enums */

enum e_render_model_flags
{
    _render_model_flags_force_third_person_bit = 0,
    _render_model_flags_force_carmack_reverse_bit = 1,
    _render_model_flags_force_node_maps_bit = 2,
    _render_model_flags_geometry_postprocessed_bit = 3,
    _render_model_flags_bit4_bit = 4,
    _render_model_flags_bit5_bit = 5,
    _render_model_flags_bit6_bit = 6,
    _render_model_flags_bit7_bit = 7,
    _render_model_flags_bit8_bit = 8,
    _render_model_flags_bit9_bit = 9,
    _render_model_flags_bit10_bit = 10,
    _render_model_flags_bit11_bit = 11,
    _render_model_flags_bit12_bit = 12,
    _render_model_flags_bit13_bit = 13,
    _render_model_flags_bit14_bit = 14,
    _render_model_flags_bit15_bit = 15
};

enum e_render_model_node_flags
{
    _render_model_node_flags_force_deterministic_bit = 0,
    _render_model_node_flags_force_render_thread_bit = 1
};

enum e_render_material_property_type_halo3
{
    _render_material_property_type_halo3_lightmap_resolution = 0,
    _render_material_property_type_halo3_lightmap_power = 1,
    _render_material_property_type_halo3_lightmap_half_life = 2,
    _render_material_property_type_halo3_lightmap_diffuse_scale = 3
};

enum e_render_geometry_runtime_flags
{
    _render_geometry_runtime_flags_processed_bit = 0,
    _render_geometry_runtime_flags_available_bit = 1,
    _render_geometry_runtime_flags_has_valid_budgets_bit = 2,
    _render_geometry_runtime_flags_manual_resource_calibration_bit = 3,
    _render_geometry_runtime_flags_keep_raw_geometry_bit = 4,
    _render_geometry_runtime_flags_do_not_use_compressed_vertex_positions_bit = 5,
    _render_geometry_runtime_flags_pca_animation_table_sorted_bit = 6,
    _render_geometry_runtime_flags_has_dual_quat_bit = 7
};

enum e_part_part_type_new
{
    _part_part_type_new_not_drawn = 0,
    _part_part_type_new_opaque_shadow_only = 1,
    _part_part_type_new_opaque_shadow_casting = 2,
    _part_part_type_new_opaque_nonshadowing = 3,
    _part_part_type_new_transparent = 4,
    _part_part_type_new_lightmap_only = 5
};

enum e_part_part_flags_new
{
    _part_part_flags_new_can_be_rendered_in_draw_bundles_bit = 0,
    _part_part_flags_new_per_vertex_lightmap_part_bit = 1,
    _part_part_flags_new_render_in_z_prepass_bit = 2,
    _part_part_flags_new_is_water_part_bit = 3,
    _part_part_flags_new_draw_cull_distance_medium_bit = 4,
    _part_part_flags_new_draw_cull_distance_close_bit = 5,
    _part_part_flags_new_draw_cull_rendering_shields_bit = 6,
    _part_part_flags_new_draw_cull_rendering_active_camo_bit = 7
};

enum e_vertex_buffer_format
{
    _vertex_buffer_format_invalid = 0,
    _vertex_buffer_format_world = 1,
    _vertex_buffer_format_rigid = 2,
    _vertex_buffer_format_skinned = 3,
    _vertex_buffer_format_static_per_pixel = 4,
    _vertex_buffer_format_unknown5 = 5,
    _vertex_buffer_format_static_per_vertex = 6,
    _vertex_buffer_format_unknown7 = 7,
    _vertex_buffer_format_unused8 = 8,
    _vertex_buffer_format_ambient_prt = 9,
    _vertex_buffer_format_linear_prt = 10,
    _vertex_buffer_format_quadratic_prt = 11,
    _vertex_buffer_format_unknown_c = 12,
    _vertex_buffer_format_unknown_d = 13,
    _vertex_buffer_format_static_per_vertex_color = 14,
    _vertex_buffer_format_unknown_f = 15,
    _vertex_buffer_format_unused10 = 16,
    _vertex_buffer_format_unused11 = 17,
    _vertex_buffer_format_unused12 = 18,
    _vertex_buffer_format_unused13 = 19,
    _vertex_buffer_format_tiny_position = 20,
    _vertex_buffer_format_unknown15 = 21,
    _vertex_buffer_format_unknown16 = 22,
    _vertex_buffer_format_unknown17 = 23,
    _vertex_buffer_format_decorator = 24,
    _vertex_buffer_format_particle_model = 25,
    _vertex_buffer_format_unknown1_a = 26,
    _vertex_buffer_format_unknown1_b = 27,
    _vertex_buffer_format_unknown1_c = 28,
    _vertex_buffer_format_unused1_d = 29,
    _vertex_buffer_format_world2 = 30,
    _vertex_buffer_format_rigid_compressed = 36,
    _vertex_buffer_format_skinned_compressed = 37
};

enum e_index_buffer_format
{
    _index_buffer_format_point_list = 0,
    _index_buffer_format_line_list = 1,
    _index_buffer_format_line_strip = 2,
    _index_buffer_format_triangle_list = 3,
    _index_buffer_format_triangle_fan = 4,
    _index_buffer_format_triangle_strip = 5
};

enum e_mesh_flags
{
    _mesh_flags_mesh_has_vertex_color_bit = 0,
    _mesh_flags_use_region_index_for_sorting_bit = 1,
    _mesh_flags_can_be_rendered_in_draw_bundles_bit = 2,
    _mesh_flags_mesh_is_custom_shadow_caster_bit = 3,
    _mesh_flags_mesh_is_unindexed_bit = 4,
    _mesh_flags_mash_should_render_in_z_prepass_bit = 5,
    _mesh_flags_mesh_has_water_bit = 6,
    _mesh_flags_mesh_has_decal_bit = 7
};

enum e_vertex_type
{
    _vertex_type_world = 0,
    _vertex_type_rigid = 1,
    _vertex_type_skinned = 2,
    _vertex_type_particle_model = 3,
    _vertex_type_flat_world = 4,
    _vertex_type_flat_rigid = 5,
    _vertex_type_flat_skinned = 6,
    _vertex_type_screen = 7,
    _vertex_type_debug = 8,
    _vertex_type_transparent = 9,
    _vertex_type_particle = 10,
    _vertex_type_contrail = 11,
    _vertex_type_light_volume = 12,
    _vertex_type_simple_chud = 13,
    _vertex_type_fancy_chud = 14,
    _vertex_type_decorator = 15,
    _vertex_type_tiny_position = 16,
    _vertex_type_patchy_fog = 17,
    _vertex_type_water = 18,
    _vertex_type_ripple = 19,
    _vertex_type_implicit = 20,
    _vertex_type_beam = 21,
    _vertex_type_dual_quat = 22
};

enum e_prt_sh_type
{
    _prt_sh_type_none = 0,
    _prt_sh_type_ambient = 1,
    _prt_sh_type_linear = 2,
    _prt_sh_type_quadratic = 3
};

enum e_primitive_type
{
    _primitive_type_point_list = 0,
    _primitive_type_line_list = 1,
    _primitive_type_line_strip = 2,
    _primitive_type_triangle_list = 3,
    _primitive_type_triangle_fan = 4,
    _primitive_type_triangle_strip = 5
};

enum e_render_geometry_compression_flags
{
    _render_geometry_compression_flags_compressed_position_bit = 0,
    _render_geometry_compression_flags_compressed_texcoord_bit = 1,
    _render_geometry_compression_flags_compression_optimized_bit = 2
};

enum e_old_raw_page_flags
{
    _old_raw_page_flags_use_checksum_bit = 0,
    _old_raw_page_flags_in_resources_bit = 1,
    _old_raw_page_flags_in_textures_bit = 2,
    _old_raw_page_flags_in_textures_b_bit = 3,
    _old_raw_page_flags_in_audio_bit = 4,
    _old_raw_page_flags_in_resources_b_bit = 5,
    _old_raw_page_flags_in_mods_bit = 6,
    _old_raw_page_flags_use_checksum2_bit = 7,
    _old_raw_page_flags_location_mask_bit = 1
};

enum e_tag_resource_type_gen3
{
    _tag_resource_type_gen3_none = -1,
    _tag_resource_type_gen3_collision = 0,
    _tag_resource_type_gen3_bitmap = 1,
    _tag_resource_type_gen3_bitmap_interleaved = 2,
    _tag_resource_type_gen3_sound = 3,
    _tag_resource_type_gen3_animation = 4,
    _tag_resource_type_gen3_render_geometry = 5,
    _tag_resource_type_gen3_bink = 6,
    _tag_resource_type_gen3_pathfinding = 7
};


/* ---------- structures */

struct s_render_model_region_permutation;
struct s_render_model_region;
struct s_render_model_instance_placement;
struct s_render_model_node;
struct s_render_model_marker_group_marker;
struct s_render_model_marker_group;
struct s_render_material_property_type;
struct s_render_material_property;
struct s_render_material;
struct s_part;
struct s_sub_part;
struct s_vertex_buffer_definition;
struct s_index_buffer_definition;
struct s_mesh_instanced_geometry_block_contents_block;
struct s_mesh_instanced_geometry_block;
struct s_mesh_water_block;
struct s_mesh;
struct s_render_geometry_compression;
struct s_render_geometry_bounding_sphere;
struct s_render_geometry_unknown_block;
struct s_render_geometry_geometry_tag_resource;
struct s_render_geometry_mopp_cluster_visiblity;
struct s_render_geometry_per_mesh_node_map_node_index;
struct s_render_geometry_per_mesh_node_map;
struct s_render_geometry_per_mesh_subpart_visibility_block;
struct s_render_geometry_static_per_pixel_lighting;
struct s_resource_page;
struct s_resource_fixup_location;
struct s_resource_interop_location;
struct s_resource_data;
struct s_pageable_resource;
struct s_tag_resource_reference;
struct s_render_geometry;
struct s_render_model_tag_block17;
struct s_render_model_unknown_sh_probe;
struct s_render_model_runtime_node_orientation;
struct s_render_model;

struct s_render_model
{
    string_id name;
    c_flags<e_render_model_flags, ushort> flags;
    short version;
    long checksum;
    c_tag_block<s_render_model_region> regions;
    long unknown18;
    long instance_starting_mesh_index;
    c_tag_block<s_render_model_instance_placement> instance_placements;
    long node_list_checksum;
    c_tag_block<s_render_model_node> nodes;
    c_tag_block<s_render_model_marker_group> marker_groups;
    c_tag_block<s_render_material> materials;
    byte unused[12];
    float dont_draw_over_camera_cosine_angle;
    s_render_geometry geometry;
    c_tag_block<s_render_model_tag_block17> unknown_e_8;
    single sh_red[16];
    single sh_green[16];
    single sh_blue[16];
    c_tag_block<s_render_model_unknown_sh_probe> unknown_sh_probes;
    c_tag_block<s_render_model_runtime_node_orientation> runtime_node_orientations;
};
static_assert(sizeof(s_render_model) == 0x1CC);

struct s_render_model_runtime_node_orientation
{
    real_quaternion rotation;
    real_point3d translation;
    float scale;
};
static_assert(sizeof(s_render_model_runtime_node_orientation) == 0x20);

struct s_render_model_unknown_sh_probe
{
    real_point3d position;
    single coefficients[81];
};
static_assert(sizeof(s_render_model_unknown_sh_probe) == 0x150);

struct s_render_model_tag_block17
{
    float unknown0;
    float unknown4;
    float unknown8;
    float unknown_c;
    float unknown10;
    float unknown14;
    float unknown18;
};
static_assert(sizeof(s_render_model_tag_block17) == 0x1C);

struct s_render_geometry
{
    c_flags<e_render_geometry_runtime_flags, long> runtime_flags;
    c_tag_block<s_mesh> meshes;
    c_tag_block<s_render_geometry_compression> compression;
    c_tag_block<s_render_geometry_bounding_sphere> bounding_spheres;
    c_tag_block<s_render_geometry_unknown_block> unknown2;
    c_tag_block<s_render_geometry_geometry_tag_resource> geometry_tag_resources;
    c_tag_block<s_render_geometry_mopp_cluster_visiblity> mesh_cluster_visibility;
    c_tag_block<s_render_geometry_per_mesh_node_map> per_mesh_node_maps;
    c_tag_block<s_render_geometry_per_mesh_subpart_visibility_block> per_mesh_subpart_visibility;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    c_tag_block<s_render_geometry_static_per_pixel_lighting> instanced_geometry_per_pixel_lighting;
    s_tag_resource_reference resource;
};
static_assert(sizeof(s_render_geometry) == 0x84);

struct s_tag_resource_reference
{
    s_pageable_resource pageable_resource;
    long unused;
};
static_assert(sizeof(s_tag_resource_reference) == 0x8);

struct s_pageable_resource
{
    s_resource_page page;
    s_resource_data resource;
};
static_assert(sizeof(s_pageable_resource) == 0x6C);

struct s_resource_data
{
    s_tag_reference parent_tag;
    ushort salt;
    c_enum<e_tag_resource_type_gen3, char> resource_type;
    uchar flags;
    s_tag_data definition_data;
    cache_address definition_address;
    c_tag_block<s_resource_fixup_location> fixup_locations;
    c_tag_block<s_resource_interop_location> interop_locations;
    long unknown2;
};
static_assert(sizeof(s_resource_data) == 0x48);

struct s_resource_interop_location
{
    cache_address address;
    long resource_structure_type_index;
};
static_assert(sizeof(s_resource_interop_location) == 0x8);

struct s_resource_fixup_location
{
    ulong block_offset;
    cache_address address;
    long type;
    long offset;
    long raw_address;
};
static_assert(sizeof(s_resource_fixup_location) == 0x8);

struct s_resource_page
{
    short salt;
    c_flags<e_old_raw_page_flags, uchar> old_flags;
    char compression_codec_index;
    long index;
    ulong compressed_block_size;
    ulong uncompressed_block_size;
    long crc_checksum;
    ulong unknown_size;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_resource_page) == 0x24);

struct s_render_geometry_static_per_pixel_lighting
{
    c_tag_block<long> unused_vertex_buffer;
    short vertex_buffer_index;
    byte unused[2];
    s_vertex_buffer_definition vertex_buffer;
};
static_assert(sizeof(s_render_geometry_static_per_pixel_lighting) == 0x10);

struct s_render_geometry_per_mesh_subpart_visibility_block
{
    c_tag_block<s_render_geometry_bounding_sphere> bounding_spheres;
};
static_assert(sizeof(s_render_geometry_per_mesh_subpart_visibility_block) == 0xC);

struct s_render_geometry_per_mesh_node_map
{
    c_tag_block<s_render_geometry_per_mesh_node_map_node_index> node_indices;
};
static_assert(sizeof(s_render_geometry_per_mesh_node_map) == 0xC);

struct s_render_geometry_per_mesh_node_map_node_index
{
    uchar node;
};
static_assert(sizeof(s_render_geometry_per_mesh_node_map_node_index) == 0x1);

struct s_render_geometry_mopp_cluster_visiblity
{
    s_tag_data mopp_data;
    c_tag_block<short> unknown_mesh_part_indices_count;
};
static_assert(sizeof(s_render_geometry_mopp_cluster_visiblity) == 0x20);

struct s_render_geometry_geometry_tag_resource
{
    c_tag_block<float> vertex_buffer;
    c_tag_block<short> index_buffer;
    byte unused[20];
};
static_assert(sizeof(s_render_geometry_geometry_tag_resource) == 0x2C);

struct s_render_geometry_unknown_block
{
    uchar unknown_byte1;
    uchar unknown_byte2;
    short unknown2;
    s_tag_data unknown3;
};
static_assert(sizeof(s_render_geometry_unknown_block) == 0x18);

struct s_render_geometry_bounding_sphere
{
    real_plane3d plane;
    real_point3d position;
    float radius;
    s_byte node_indices[4];
    single node_weights[3];
};
static_assert(sizeof(s_render_geometry_bounding_sphere) == 0x30);

struct s_render_geometry_compression
{
    c_flags<e_render_geometry_compression_flags, ushort> flags;
    byte unused[2];
    real_bounds x;
    real_bounds y;
    real_bounds z;
    real_bounds u;
    real_bounds v;
};
static_assert(sizeof(s_render_geometry_compression) == 0x2C);

struct s_mesh
{
    c_tag_block<s_part> parts;
    c_tag_block<s_sub_part> sub_parts;
    int16 vertex_buffer_indices[8];
    int16 index_buffer_indices[2];
    vertex_buffer_definition resource_vertex_buffers[0];
    index_buffer_definition resource_index_buffers[0];
    c_flags<e_mesh_flags, uchar> flags;
    char rigid_node_index;
    c_enum<e_vertex_type, uchar> type;
    c_enum<e_prt_sh_type, uchar> prt_type;
    c_enum<e_primitive_type, char> index_buffer_type;
    byte unused3[3];
    c_tag_block<s_mesh_instanced_geometry_block> instanced_geometry;
    c_tag_block<s_mesh_water_block> water;
};
static_assert(sizeof(s_mesh) == 0x4C);

struct s_mesh_water_block
{
    short value;
};
static_assert(sizeof(s_mesh_water_block) == 0x2);

struct s_mesh_instanced_geometry_block
{
    short section1;
    short section2;
    c_tag_block<s_mesh_instanced_geometry_block_contents_block> contents;
};
static_assert(sizeof(s_mesh_instanced_geometry_block) == 0x10);

struct s_mesh_instanced_geometry_block_contents_block
{
    short value;
};
static_assert(sizeof(s_mesh_instanced_geometry_block_contents_block) == 0x2);

struct s_index_buffer_definition
{
    c_enum<e_index_buffer_format, long> format;
    tag_data data;
    byte unused[8];
};
static_assert(sizeof(s_index_buffer_definition) == 0x20);

struct s_vertex_buffer_definition
{
    long count;
    c_enum<e_vertex_buffer_format, short> format;
    short vertex_size;
    tag_data data;
    byte unused[4];
};
static_assert(sizeof(s_vertex_buffer_definition) == 0x20);

struct s_sub_part
{
    ushort first_index;
    ushort index_count;
    short part_index;
    ushort vertex_count;
};
static_assert(sizeof(s_sub_part) == 0x8);

struct s_part
{
    short material_index;
    short transparent_sorting_index;
    ushort first_index_old;
    ushort index_count_old;
    short first_sub_part_index;
    short sub_part_count;
    c_enum<e_part_part_type_new, char> type_new;
    c_flags<e_part_part_flags_new, uchar> flags_new;
    ushort vertex_count;
};
static_assert(sizeof(s_part) == 0x10);

struct s_render_material
{
    s_tag_reference render_method;
    c_tag_block<s_render_material_property> properties;
    long unknown;
    char breakable_surface_index;
    char unknown2;
    char unknown3;
    char unknown4;
};
static_assert(sizeof(s_render_material) == 0x24);

struct s_render_material_property
{
    s_render_material_property_type type;
    long int_value;
    float real_value;
};
static_assert(sizeof(s_render_material_property) == 0xC);

struct s_render_material_property_type
{
    c_enum<e_render_material_property_type_halo3, long> halo3;
};
static_assert(sizeof(s_render_material_property_type) == 0x4);

struct s_render_model_marker_group
{
    string_id name;
    c_tag_block<s_render_model_marker_group_marker> markers;
};
static_assert(sizeof(s_render_model_marker_group) == 0x10);

struct s_render_model_marker_group_marker
{
    char region_index;
    char permutation_index;
    char node_index;
    char unknown3;
    real_point3d translation;
    real_quaternion rotation;
    float scale;
};
static_assert(sizeof(s_render_model_marker_group_marker) == 0x24);

struct s_render_model_node
{
    string_id name;
    short parent_node;
    short first_child_node;
    short next_sibling_node;
    c_flags<e_render_model_node_flags, ushort> flags;
    real_point3d default_translation;
    real_quaternion default_rotation;
    float default_scale;
    real_vector3d inverse_forward;
    real_vector3d inverse_left;
    real_vector3d inverse_up;
    real_point3d inverse_position;
    float distance_from_parent;
};
static_assert(sizeof(s_render_model_node) == 0x60);

struct s_render_model_instance_placement
{
    string_id name;
    long node_index;
    float scale;
    real_point3d forward;
    real_point3d left;
    real_point3d up;
    real_point3d position;
};
static_assert(sizeof(s_render_model_instance_placement) == 0x3C);

struct s_render_model_region
{
    string_id name;
    c_tag_block<s_render_model_region_permutation> permutations;
};
static_assert(sizeof(s_render_model_region) == 0x10);

struct s_render_model_region_permutation
{
    string_id name;
    short mesh_index;
    ushort mesh_count;
    long unknown8;
    long unknown_c;
    long unknown10;
    long unknown14;
};
static_assert(sizeof(s_render_model_region_permutation) == 0x18);

