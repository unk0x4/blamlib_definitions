/* ---------- enums */

enum e_render_water_ripple_type_flags
{
    _render_water_ripple_type_flags_ripple_drifted_by_flow_bit = 0,
    _render_water_ripple_type_flags_amplitude_changed_by_pendulum_function_bit = 1,
    _render_water_ripple_type_flags_display_flash_foam_bit = 2,
    _render_water_ripple_type_flags_foam_size_defined_in_game_unit_bit = 3
};

enum e_render_water_ripple_function
{
    _render_water_ripple_function_linear = 0,
    _render_water_ripple_function_early = 1,
    _render_water_ripple_function_very_early = 2,
    _render_water_ripple_function_late = 3,
    _render_water_ripple_function_very_late = 4,
    _render_water_ripple_function_cosine = 5,
    _render_water_ripple_function_one = 6,
    _render_water_ripple_function_zero = 7
};


/* ---------- structures */

struct s_render_water_ripple;

struct s_render_water_ripple
{
    c_flags<e_render_water_ripple_type_flags, long> type_flags;
    float radius;
    float amplitude;
    float spread_speed;
    float spread_bias;
    float position_random_range;
    float max_visibility_distance;
    float duration_max;
    float duration_min;
    float rise_period_ratio;
    c_enum<e_render_water_ripple_function, short> rise_function;
    c_enum<e_render_water_ripple_function, short> descend_function;
    float phase_revolution_speed;
    float phase_repeat_along_radius;
    float pattern_start_index;
    float pattern_end_index;
    c_enum<e_render_water_ripple_function, short> pattern_transition;
    byte unused1[2];
    float foam_out_radius;
    float foam_fade_radius;
    float foam_duration;
    c_enum<e_render_water_ripple_function, short> foam_fade_function;
    byte unused2[2];
};
static_assert(sizeof(s_render_water_ripple) == 0x50);

