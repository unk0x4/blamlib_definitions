/* ---------- enums */

enum e_render_method_option_option_block_option_data_type
{
    _render_method_option_option_block_option_data_type_sampler = 0,
    _render_method_option_option_block_option_data_type_float4 = 1,
    _render_method_option_option_block_option_data_type_float = 2,
    _render_method_option_option_block_option_data_type_integer = 3,
    _render_method_option_option_block_option_data_type_boolean = 4,
    _render_method_option_option_block_option_data_type_integer_color = 5
};

enum e_render_method_shader_function_function_type
{
    _render_method_shader_function_function_type_value = 0,
    _render_method_shader_function_function_type_color = 1,
    _render_method_shader_function_function_type_scale_uniform = 2,
    _render_method_shader_function_function_type_scale_x = 3,
    _render_method_shader_function_function_type_scale_y = 4,
    _render_method_shader_function_function_type_translation_x = 5,
    _render_method_shader_function_function_type_translation_y = 6,
    _render_method_shader_function_function_type_frame_index = 7,
    _render_method_shader_function_function_type_alpha = 8,
    _render_method_shader_function_function_type_change_color_primary = 9,
    _render_method_shader_function_function_type_change_color_secondary = 10,
    _render_method_shader_function_function_type_change_color_tertiary = 11,
    _render_method_shader_function_function_type_change_color_quaternary = 12,
    _render_method_shader_function_function_type_change_color_quinary = 13
};

enum e_render_method_shader_property_texture_constant_sampler_filter_mode
{
    _render_method_shader_property_texture_constant_sampler_filter_mode_trilinear = 0,
    _render_method_shader_property_texture_constant_sampler_filter_mode_point = 1,
    _render_method_shader_property_texture_constant_sampler_filter_mode_bilinear = 2,
    _render_method_shader_property_texture_constant_sampler_filter_mode_unused_00 = 3,
    _render_method_shader_property_texture_constant_sampler_filter_mode_anisotropic_2_x = 4,
    _render_method_shader_property_texture_constant_sampler_filter_mode_unused_01 = 5,
    _render_method_shader_property_texture_constant_sampler_filter_mode_anisotropic_4_x = 6,
    _render_method_shader_property_texture_constant_sampler_filter_mode_lightprobe_texture_array = 7,
    _render_method_shader_property_texture_constant_sampler_filter_mode_texture_array_quadlinear = 8,
    _render_method_shader_property_texture_constant_sampler_filter_mode_texture_array_quadanisotropic_2_x = 9
};

enum e_render_method_shader_property_alpha_blend_mode
{
    _render_method_shader_property_alpha_blend_mode_opaque = 0,
    _render_method_shader_property_alpha_blend_mode_additive = 1,
    _render_method_shader_property_alpha_blend_mode_multiply = 2,
    _render_method_shader_property_alpha_blend_mode_alpha_blend = 3,
    _render_method_shader_property_alpha_blend_mode_double_multiply = 4,
    _render_method_shader_property_alpha_blend_mode_pre_multiplied_alpha = 5,
    _render_method_shader_property_alpha_blend_mode_maximum = 6,
    _render_method_shader_property_alpha_blend_mode_multiply_add = 7,
    _render_method_shader_property_alpha_blend_mode_add_src_times_dstalpha = 8,
    _render_method_shader_property_alpha_blend_mode_add_src_times_srcalpha = 9,
    _render_method_shader_property_alpha_blend_mode_inv_alpha_blend = 10,
    _render_method_shader_property_alpha_blend_mode_separate_alpha_blend = 11,
    _render_method_shader_property_alpha_blend_mode_separate_alpha_blend_additive = 12
};

enum e_render_method_shader_property_blend_mode_flags
{
    _render_method_shader_property_blend_mode_flags_bit0_bit = 0,
    _render_method_shader_property_blend_mode_flags_enable_alpha_test_bit = 1,
    _render_method_shader_property_blend_mode_flags_sfx_distort_force_alpha_blend_bit = 2
};

enum e_render_method_render_method_render_flags
{
    _render_method_render_method_render_flags_ignore_fog_bit = 0,
    _render_method_render_method_render_flags_use_sky_atmosphere_properties_bit = 1,
    _render_method_render_method_render_flags_uses_depth_camera_bit = 2,
    _render_method_render_method_render_flags_disable_with_shields_bit = 3,
    _render_method_render_method_render_flags_enable_with_shields_bit = 4
};

enum e_sorting_layer
{
    _sorting_layer_invalid = 0,
    _sorting_layer_pre_pass = 1,
    _sorting_layer_normal = 2,
    _sorting_layer_post_pass = 3
};

enum e_light_volume_system_light_volume_flags
{
    _light_volume_system_light_volume_flags_bit0_bit = 0,
    _light_volume_system_light_volume_flags_bit1_bit = 1,
    _light_volume_system_light_volume_flags_bit2_bit = 2,
    _light_volume_system_light_volume_flags_bit3_bit = 3,
    _light_volume_system_light_volume_flags_bit4_bit = 4,
    _light_volume_system_light_volume_flags_bit5_bit = 5,
    _light_volume_system_light_volume_flags_bit6_bit = 6,
    _light_volume_system_light_volume_flags_bit7_bit = 7,
    _light_volume_system_light_volume_flags_bit8_bit = 8,
    _light_volume_system_light_volume_flags_bit9_bit = 9,
    _light_volume_system_light_volume_flags_bit10_bit = 10,
    _light_volume_system_light_volume_flags_bit11_bit = 12,
    _light_volume_system_light_volume_flags_bit13_bit = 13,
    _light_volume_system_light_volume_flags_bit14_bit = 14,
    _light_volume_system_light_volume_flags_bit15_bit = 15
};

enum e_tag_mapping_variable_type
{
    _tag_mapping_variable_type_particle_age = 0,
    _tag_mapping_variable_type_emitter_age = 1,
    _tag_mapping_variable_type_particle_random = 2,
    _tag_mapping_variable_type_emitter_random = 3,
    _tag_mapping_variable_type_particle_random1 = 4,
    _tag_mapping_variable_type_particle_random2 = 5,
    _tag_mapping_variable_type_particle_random3 = 6,
    _tag_mapping_variable_type_particle_random4 = 7,
    _tag_mapping_variable_type_emitter_random1 = 8,
    _tag_mapping_variable_type_emitter_random2 = 9,
    _tag_mapping_variable_type_emitter_time = 10,
    _tag_mapping_variable_type_system_lod = 11,
    _tag_mapping_variable_type_game_time = 12,
    _tag_mapping_variable_type_effect_a_scale = 13,
    _tag_mapping_variable_type_effect_b_scale = 14,
    _tag_mapping_variable_type_physics_rotation = 15,
    _tag_mapping_variable_type_location_random = 16,
    _tag_mapping_variable_type_distance_from_emitter = 17,
    _tag_mapping_variable_type_particle_simulation_a = 18,
    _tag_mapping_variable_type_particle_simulation_b = 19,
    _tag_mapping_variable_type_particle_velocity = 20,
    _tag_mapping_variable_type_invalid = 21
};

enum e_tag_mapping_output_modifier
{
    _tag_mapping_output_modifier_none = 0,
    _tag_mapping_output_modifier_plus = 1,
    _tag_mapping_output_modifier_times = 2
};


/* ---------- structures */

struct s_render_method_render_method_definition_option_index;
struct s_tag_function;
struct s_render_method_shader_function;
struct s_render_method_import_datum;
struct s_render_method_shader_property_texture_constant_packed_sampler_address_mode;
struct s_render_method_template_tag_block_index;
struct s_render_method_shader_property_texture_constant;
struct s_render_method_shader_property_real_constant;
struct s_render_method_shader_property_parameter_table;
struct s_render_method_shader_property_parameter_mapping;
struct s_render_method_shader_property;
struct s_render_method;
struct s_tag_mapping;
struct s_light_volume_system_light_volume_system_block_runtime_gpu_property;
struct s_light_volume_system_light_volume_system_block_runtime_gpu_function;
struct s_light_volume_system_light_volume_system_block_runtime_gpu_color;
struct s_light_volume_system_light_volume_system_block;
struct s_light_volume_system;

struct s_light_volume_system
{
    c_tag_block<s_light_volume_system_light_volume_system_block> light_volume;
    byte unused1[8];
};
static_assert(sizeof(s_light_volume_system) == 0x14);

struct s_light_volume_system_light_volume_system_block
{
    string_id light_volume_name;
    s_render_method render_method;
    c_flags<e_light_volume_system_light_volume_flags, ushort> flags;
    short unknown;
    float brightness_ratio;
    s_tag_mapping length;
    s_tag_mapping offset;
    s_tag_mapping profile_density;
    s_tag_mapping profile_length;
    s_tag_mapping profile_thickness;
    s_tag_mapping profile_color;
    s_tag_mapping profile_alpha;
    s_tag_mapping profile_intensity;
    ulong runtime_m_constant_per_profile_properties;
    ulong runtime_m_used_states;
    ulong runtime_m_max_profile_count;
    c_tag_block<s_light_volume_system_light_volume_system_block_runtime_gpu_property> runtime_gpu_properties;
    c_tag_block<s_light_volume_system_light_volume_system_block_runtime_gpu_function> runtime_gpu_functions;
    c_tag_block<s_light_volume_system_light_volume_system_block_runtime_gpu_color> runtime_gpu_colors;
};
static_assert(sizeof(s_light_volume_system_light_volume_system_block) == 0x17C);

struct s_light_volume_system_light_volume_system_block_runtime_gpu_color
{
    float red;
    float green;
    float blue;
    float magnitude;
};
static_assert(sizeof(s_light_volume_system_light_volume_system_block_runtime_gpu_color) == 0x10);

struct s_light_volume_system_light_volume_system_block_runtime_gpu_function
{
    single values[16];
};
static_assert(sizeof(s_light_volume_system_light_volume_system_block_runtime_gpu_function) == 0x40);

struct s_light_volume_system_light_volume_system_block_runtime_gpu_property
{
    single values[4];
};
static_assert(sizeof(s_light_volume_system_light_volume_system_block_runtime_gpu_property) == 0x10);

struct s_tag_mapping
{
    c_enum<e_tag_mapping_variable_type, char> input_variable;
    c_enum<e_tag_mapping_variable_type, char> range_variable;
    c_enum<e_tag_mapping_output_modifier, char> output_modifier;
    c_enum<e_tag_mapping_variable_type, char> output_modifier_input;
    s_tag_function function;
    float runtime_m_constant_value;
    uchar runtime_m_flags;
    byte unused[3];
};
static_assert(sizeof(s_tag_mapping) == 0x20);

struct s_render_method
{
    s_tag_reference base_render_method;
    c_tag_block<s_render_method_render_method_definition_option_index> render_method_definition_option_indices;
    c_tag_block<s_render_method_import_datum> import_data;
    c_tag_block<s_render_method_shader_property> shader_properties;
    c_flags<e_render_method_render_method_render_flags, ushort> render_flags;
    c_enum<e_sorting_layer, uchar> sorting_layer;
    uchar version;
    long sky_atmosphere_properties_index;
    long unknown2;
};
static_assert(sizeof(s_render_method) == 0x40);

struct s_render_method_shader_property
{
    s_tag_reference template;
    c_tag_block<s_render_method_shader_property_texture_constant> texture_constants;
    c_tag_block<s_render_method_shader_property_real_constant> real_constants;
    c_tag_block<ulong> integer_constants;
    ulong boolean_constants;
    c_tag_block<s_render_method_template_tag_block_index> entry_points;
    c_tag_block<s_render_method_shader_property_parameter_table> parameter_tables;
    c_tag_block<s_render_method_shader_property_parameter_mapping> parameters;
    c_tag_block<s_render_method_shader_function> functions;
    c_enum<e_render_method_shader_property_alpha_blend_mode, ulong> alpha_blend_mode;
    c_flags<e_render_method_shader_property_blend_mode_flags, ulong> blend_flags;
    ulong unknown8;
    int16 queryable_properties[8];
};
static_assert(sizeof(s_render_method_shader_property) == 0x84);

struct s_render_method_shader_property_parameter_mapping
{
    short register_index;
    uchar function_index;
    uchar source_index;
};
static_assert(sizeof(s_render_method_shader_property_parameter_mapping) == 0x4);

struct s_render_method_shader_property_parameter_table
{
    s_render_method_template_tag_block_index texture;
    s_render_method_template_tag_block_index real_vertex;
    s_render_method_template_tag_block_index real_pixel;
};
static_assert(sizeof(s_render_method_shader_property_parameter_table) == 0x6);

struct s_render_method_shader_property_real_constant
{
    single values[4];
};
static_assert(sizeof(s_render_method_shader_property_real_constant) == 0x10);

struct s_render_method_shader_property_texture_constant
{
    s_tag_reference bitmap;
    short bitmap_index;
    s_render_method_shader_property_texture_constant_packed_sampler_address_mode sampler_address_mode;
    c_enum<e_render_method_shader_property_texture_constant_sampler_filter_mode, uchar> filter_mode;
    char extern_mode;
    char x_form_argument_index;
    s_render_method_template_tag_block_index functions;
};
static_assert(sizeof(s_render_method_shader_property_texture_constant) == 0x18);

struct s_render_method_template_tag_block_index
{
    ushort integer;
};
static_assert(sizeof(s_render_method_template_tag_block_index) == 0x2);

struct s_render_method_shader_property_texture_constant_packed_sampler_address_mode
{
    uchar sampler_address_uv;
};
static_assert(sizeof(s_render_method_shader_property_texture_constant_packed_sampler_address_mode) == 0x1);

struct s_render_method_import_datum
{
    string_id name;
    c_enum<e_render_method_option_option_block_option_data_type, ulong> type;
    s_tag_reference bitmap;
    float default_real_value;
    long default_int_bool_value;
    short sampler_flags;
    short sampler_filter_mode;
    short default_address_mode;
    short address_u;
    short address_v;
    short unknown9;
    ulong unknown10;
    c_tag_block<s_render_method_shader_function> functions;
};
static_assert(sizeof(s_render_method_import_datum) == 0x3C);

struct s_render_method_shader_function
{
    c_enum<e_render_method_shader_function_function_type, long> type;
    string_id input_name;
    string_id range_name;
    float time_period;
    s_tag_function function;
};
static_assert(sizeof(s_render_method_shader_function) == 0x24);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_render_method_render_method_definition_option_index
{
    short option_index;
};
static_assert(sizeof(s_render_method_render_method_definition_option_index) == 0x2);

