/* ---------- enums */


/* ---------- structures */

struct s_map_list_map_image;
struct s_map_list;

struct s_map_list
{
    c_tag_block<s_map_list_map_image> map_images;
    s_tag_reference default_map_image;
    s_tag_reference default_loading_screen;
};
static_assert(sizeof(s_map_list) == 0x2C);

struct s_map_list_map_image
{
    long map_id;
    s_tag_reference bitmap;
    c_tag_block<s_tag_reference> loading_screen_bitmaps;
};
static_assert(sizeof(s_map_list_map_image) == 0x20);

