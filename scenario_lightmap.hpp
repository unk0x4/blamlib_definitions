/* ---------- enums */

enum e_airprobe_flags
{
};


/* ---------- structures */

struct s_half_rgb_light_probe;
struct s_airprobe;
struct s_scenery_light_probe;
struct s_machine_light_probes_machine_light_probe;
struct s_machine_light_probes;
struct s_scenario_lightmap;

struct s_scenario_lightmap
{
    ulong unknown;
    c_tag_block<s_tag_reference> lightmap_data_references;
    c_tag_block<s_tag_reference> unknown2;
    c_tag_block<s_airprobe> airprobes;
    c_tag_block<s_scenery_light_probe> scenery_light_probes;
    c_tag_block<s_machine_light_probes> machine_light_probes;
    c_tag_block<long> unknown5;
};
static_assert(sizeof(s_scenario_lightmap) == 0x4C);

struct s_machine_light_probes
{
    ulong unknown;
    short unknown2;
    short unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    c_tag_block<s_machine_light_probes_machine_light_probe> light_probes;
};
static_assert(sizeof(s_machine_light_probes) == 0x2C);

struct s_machine_light_probes_machine_light_probe
{
    real_point3d position;
    s_half_rgb_light_probe light_probe;
};
static_assert(sizeof(s_machine_light_probes_machine_light_probe) == 0x54);

struct s_scenery_light_probe
{
    long unknown1;
    short unknown2;
    uchar unknown3;
    uchar unknown4;
    s_half_rgb_light_probe light_probe;
};
static_assert(sizeof(s_scenery_light_probe) == 0x50);

struct s_airprobe
{
    real_point3d position;
    string_id name;
    c_flags<e_airprobe_flags, long> flags;
    s_half_rgb_light_probe light_probe;
};
static_assert(sizeof(s_airprobe) == 0x5C);

struct s_half_rgb_light_probe
{
    int16 dominant_light_direction[3];
    short padding1;
    int16 dominant_light_intensity[3];
    short padding2;
    int16 sh_red[9];
    int16 sh_green[9];
    int16 sh_blue[9];
    short padding3;
};
static_assert(sizeof(s_half_rgb_light_probe) == 0x48);

