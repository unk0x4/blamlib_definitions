/* ---------- enums */

enum e_projectile_projectile_flag_bits
{
    _projectile_projectile_flag_bits_oriented_along_velocity_bit = 0,
    _projectile_projectile_flag_bits_ai_must_use_ballistic_aiming_bit = 1,
    _projectile_projectile_flag_bits_detonation_max_time_if_attached_bit = 2,
    _projectile_projectile_flag_bits_has_super_combining_explosion_bit = 3,
    _projectile_projectile_flag_bits_damage_scales_based_on_distance_bit = 4,
    _projectile_projectile_flag_bits_travels_instantaneously_bit = 5,
    _projectile_projectile_flag_bits_steering_adjusts_orientation_bit = 6,
    _projectile_projectile_flag_bits_do_not_noise_up_steering_bit = 7,
    _projectile_projectile_flag_bits_can_track_behind_itself_bit = 8,
    _projectile_projectile_flag_bits_robotron_steering_bit = 9,
    _projectile_projectile_flag_bits_faster_when_owned_by_player_bit = 10
};

enum e_projectile_detonation_timer_starts
{
    _projectile_detonation_timer_starts_immediately = 0,
    _projectile_detonation_timer_starts_after_first_bounce = 1,
    _projectile_detonation_timer_starts_when_at_rest = 2,
    _projectile_detonation_timer_starts_after_first_bounce_off_any_surface = 3
};

enum e_projectile_noise_level
{
    _projectile_noise_level_silent = 0,
    _projectile_noise_level_medium = 1,
    _projectile_noise_level_loud = 2,
    _projectile_noise_level_shout = 3,
    _projectile_noise_level_quiet = 4
};

enum e_damage_reporting_type
{
    _damage_reporting_type_guardians_unknown = 0,
    _damage_reporting_type_guardians = 1,
    _damage_reporting_type_falling_damage = 2,
    _damage_reporting_type_generic_collision = 3,
    _damage_reporting_type_armor_lock_crush = 4,
    _damage_reporting_type_generic_melee = 5,
    _damage_reporting_type_generic_explosion = 6,
    _damage_reporting_type_magnum = 7,
    _damage_reporting_type_plasma_pistol = 8,
    _damage_reporting_type_needler = 9,
    _damage_reporting_type_mauler = 10,
    _damage_reporting_type_smg = 11,
    _damage_reporting_type_plasma_rifle = 12,
    _damage_reporting_type_battle_rifle = 13,
    _damage_reporting_type_carbine = 14,
    _damage_reporting_type_shotgun = 15,
    _damage_reporting_type_sniper_rifle = 16,
    _damage_reporting_type_beam_rifle = 17,
    _damage_reporting_type_assault_rifle = 18,
    _damage_reporting_type_spiker = 19,
    _damage_reporting_type_fuel_rod_cannon = 20,
    _damage_reporting_type_missile_pod = 21,
    _damage_reporting_type_rocket_launcher = 22,
    _damage_reporting_type_spartan_laser = 23,
    _damage_reporting_type_brute_shot = 24,
    _damage_reporting_type_flamethrower = 25,
    _damage_reporting_type_sentinel_gun = 26,
    _damage_reporting_type_energy_sword = 27,
    _damage_reporting_type_gravity_hammer = 28,
    _damage_reporting_type_frag_grenade = 29,
    _damage_reporting_type_plasma_grenade = 30,
    _damage_reporting_type_spike_grenade = 31,
    _damage_reporting_type_firebomb_grenade = 32,
    _damage_reporting_type_flag = 33,
    _damage_reporting_type_bomb = 34,
    _damage_reporting_type_bomb_explosion = 35,
    _damage_reporting_type_ball = 36,
    _damage_reporting_type_machinegun_turret = 37,
    _damage_reporting_type_plasma_cannon = 38,
    _damage_reporting_type_plasma_mortar = 39,
    _damage_reporting_type_plasma_turret = 40,
    _damage_reporting_type_shade_turret = 41,
    _damage_reporting_type_banshee = 42,
    _damage_reporting_type_ghost = 43,
    _damage_reporting_type_mongoose = 44,
    _damage_reporting_type_scorpion = 45,
    _damage_reporting_type_scorpion_gunner = 46,
    _damage_reporting_type_spectre = 47,
    _damage_reporting_type_spectre_gunner = 48,
    _damage_reporting_type_warthog = 49,
    _damage_reporting_type_warthog_gunner = 50,
    _damage_reporting_type_warthog_gauss_turret = 51,
    _damage_reporting_type_wraith = 52,
    _damage_reporting_type_wraith_gunner = 53,
    _damage_reporting_type_tank = 54,
    _damage_reporting_type_chopper = 55,
    _damage_reporting_type_hornet = 56,
    _damage_reporting_type_mantis = 57,
    _damage_reporting_type_prowler = 58,
    _damage_reporting_type_sentinel_beam = 59,
    _damage_reporting_type_sentinel_rpg = 60,
    _damage_reporting_type_teleporter = 61,
    _damage_reporting_type_tripmine = 62,
    _damage_reporting_type_dmr = 63
};

enum e_projectile_material_response_flag_bits
{
    _projectile_material_response_flag_bits_cannot_be_overpenetrated_bit = 0
};

enum e_projectile_material_response_response
{
    _projectile_material_response_response_impact_detonate = 0,
    _projectile_material_response_response_fizzle = 1,
    _projectile_material_response_response_overpenetrate = 2,
    _projectile_material_response_response_attach = 3,
    _projectile_material_response_response_bounce = 4,
    _projectile_material_response_response_bounce_dud = 5,
    _projectile_material_response_response_fizzle_ricochet = 6
};

enum e_projectile_material_response_response_flag_bits
{
    _projectile_material_response_response_flag_bits_only_against_units_bit = 0,
    _projectile_material_response_response_flag_bits_never_against_units_bit = 1,
    _projectile_material_response_response_flag_bits_only_against_bipeds_bit = 2,
    _projectile_material_response_response_flag_bits_only_against_vehicles_bit = 3,
    _projectile_material_response_response_flag_bits_never_against_wuss_players_bit = 4,
    _projectile_material_response_response_flag_bits_only_when_tethered_bit = 5,
    _projectile_material_response_response_flag_bits_only_when_not_tethered_bit = 6,
    _projectile_material_response_response_flag_bits_only_against_dead_bipeds_bit = 7,
    _projectile_material_response_response_flag_bits_never_against_dead_bipeds_bit = 8,
    _projectile_material_response_response_flag_bits_only_ai_projectiles_bit = 9,
    _projectile_material_response_response_flag_bits_never_ai_projectiles_bit = 10
};

enum e_projectile_material_response_scale_effects_by
{
    _projectile_material_response_scale_effects_by_damage = 0,
    _projectile_material_response_scale_effects_by_angle = 1
};


/* ---------- structures */

struct s_damage_reporting_type;
struct s_projectile_material_response;
struct s_projectile_brute_grenade_property;
struct s_projectile_fire_bomb_grenade_property;
struct s_projectile_shotgun_property;
struct s_projectile;

struct s_projectile : s_game_object
{
    c_flags<e_projectile_projectile_flag_bits, long> projectile_flags;
    c_enum<e_projectile_detonation_timer_starts, short> detonation_timer_starts;
    c_enum<e_projectile_noise_level, short> impact_noise;
    float collision_radius;
    float arming_time;
    float danger_radius;
    real_bounds timer;
    float minimum_velocity;
    float maximum_range;
    float detonation_charge_time;
    c_enum<e_projectile_noise_level, short> detonation_noise;
    short super_detonation_projectile_count;
    float super_detonation_delay;
    s_tag_reference detonation_started;
    s_tag_reference airborne_detonation_effect;
    s_tag_reference ground_detonation_effect;
    s_tag_reference detonation_damage;
    s_tag_reference attached_detonation_damage;
    s_tag_reference super_detonation;
    s_tag_reference super_detonation_damage;
    s_tag_reference detonation_sound;
    s_damage_reporting_type damage_reporting_type;
    uchar unknown3;
    uchar unknown4;
    uchar unknown5;
    s_tag_reference attached_super_detonation_damage;
    float material_effect_radius;
    s_tag_reference flyby_sound;
    s_tag_reference flyby_response;
    s_tag_reference impact_effect;
    s_tag_reference impact_damage;
    float boarding_detonation_time;
    s_tag_reference boarding_detonation_damage;
    s_tag_reference boarding_attached_detonation_damage;
    float air_gravity_scale;
    real_bounds air_damage_range;
    float water_gravity_scale;
    real_bounds water_damage_range;
    float initial_velocity;
    float final_velocity;
    float indirect_fire_velocity;
    float ai_velocity_scale;
    float ai_guided_angular_velocity_scale;
    real_bounds guided_angular_velocity;
    real guided_angular_velocity_at_rest;
    real_bounds acceleration_range;
    float ai_target_leading_scale;
    float targeted_leading_fraction;
    float guided_projectile_outer_range_error_radius;
    float autoaim_leading_max_lead_time;
    c_tag_block<s_projectile_material_response> material_responses;
    c_tag_block<s_projectile_brute_grenade_property> brute_grenade_properties;
    c_tag_block<s_projectile_fire_bomb_grenade_property> fire_bomb_grenade_properties;
    c_tag_block<s_projectile_shotgun_property> shotgun_properties;
};
static_assert(sizeof(s_projectile) == 0x2CC);

struct s_projectile_shotgun_property
{
    short amount;
    short distance;
    float accuracy;
    real spread_cone_angle;
};
static_assert(sizeof(s_projectile_shotgun_property) == 0xC);

struct s_projectile_fire_bomb_grenade_property
{
    float projection_offset;
};
static_assert(sizeof(s_projectile_fire_bomb_grenade_property) == 0x4);

struct s_projectile_brute_grenade_property
{
    real_bounds angular_velocity_range;
    real spin_angular_velocity;
    float angular_damping;
    float drag_angle_k;
    float drag_speed_k;
    float drag_exponent;
    float attach_sample_radius;
    float attach_acc_k;
    float attach_acc_s;
    float attach_acc_e;
    float attach_acc_damping;
};
static_assert(sizeof(s_projectile_brute_grenade_property) == 0x30);

struct s_projectile_material_response
{
    c_flags<e_projectile_material_response_flag_bits, ushort> flags;
    c_enum<e_projectile_material_response_response, short> default_response;
    string_id material_name;
    short global_material_index;
    byte unused1[2];
    c_enum<e_projectile_material_response_response, short> potential_response;
    c_flags<e_projectile_material_response_response_flag_bits, ushort> response_flags;
    float chance_fraction;
    real_bounds between_angle;
    real_bounds and_velocity;
    c_enum<e_projectile_material_response_scale_effects_by, short> scale_effects_by;
    byte unused2[2];
    real angular_noise;
    float velocity_noise;
    float initial_friction;
    float maximum_distance;
    float parallel_friction;
    float perpendicular_friction;
};
static_assert(sizeof(s_projectile_material_response) == 0x40);

struct s_damage_reporting_type
{
    c_enum<e_damage_reporting_type, char> ;
};
static_assert(sizeof(s_damage_reporting_type) == 0x1);

