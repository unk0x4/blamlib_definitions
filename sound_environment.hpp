/* ---------- enums */


/* ---------- structures */

struct s_sound_environment;

struct s_sound_environment
{
    ulong unknown1;
    short priority;
    short unknown2;
    float room_intensity;
    float room_intensity_high_frequency;
    float room_rolloff;
    float decay_time;
    float decay_high_frequency_ratio;
    float reflections_intensity;
    float reflections_delay;
    float reverb_intensity;
    float reverb_delay;
    float diffusion;
    float density;
    float high_frequency_refrence;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_sound_environment) == 0x48);

