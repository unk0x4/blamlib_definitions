/* ---------- enums */

enum e_render_method_option_option_block_option_data_type
{
    _render_method_option_option_block_option_data_type_sampler = 0,
    _render_method_option_option_block_option_data_type_float4 = 1,
    _render_method_option_option_block_option_data_type_float = 2,
    _render_method_option_option_block_option_data_type_integer = 3,
    _render_method_option_option_block_option_data_type_boolean = 4,
    _render_method_option_option_block_option_data_type_integer_color = 5
};

enum e_render_method_extern
{
    _render_method_extern_none = 0,
    _render_method_extern_texture_global_target_texaccum = 1,
    _render_method_extern_texture_global_target_normal = 2,
    _render_method_extern_texture_global_target_z = 3,
    _render_method_extern_texture_global_target_shadow_buffer1 = 4,
    _render_method_extern_texture_global_target_shadow_buffer2 = 5,
    _render_method_extern_texture_global_target_shadow_buffer3 = 6,
    _render_method_extern_texture_global_target_shadow_buffer4 = 7,
    _render_method_extern_texture_global_target_texture_camera = 8,
    _render_method_extern_texture_global_target_reflection = 9,
    _render_method_extern_texture_global_target_refraction = 10,
    _render_method_extern_texture_lightprobe_texture = 11,
    _render_method_extern_texture_dominant_light_intensity_map = 12,
    _render_method_extern_texture_unused1 = 13,
    _render_method_extern_texture_unused2 = 14,
    _render_method_extern_object_change_color_primary = 15,
    _render_method_extern_object_change_color_secondary = 16,
    _render_method_extern_object_change_color_tertiary = 17,
    _render_method_extern_object_change_color_quaternary = 18,
    _render_method_extern_object_change_color_quinary = 19,
    _render_method_extern_object_change_color_primary_anim = 20,
    _render_method_extern_object_change_color_secondary_anim = 21,
    _render_method_extern_texture_dynamic_environment_map_0 = 22,
    _render_method_extern_texture_dynamic_environment_map_1 = 23,
    _render_method_extern_texture_cook_torrance_cc0236 = 24,
    _render_method_extern_texture_cook_torrance_dd0236 = 25,
    _render_method_extern_texture_cook_torrance_c78_d78 = 26,
    _render_method_extern_light_dir_0 = 27,
    _render_method_extern_light_color_0 = 28,
    _render_method_extern_light_dir_1 = 29,
    _render_method_extern_light_color_1 = 30,
    _render_method_extern_light_dir_2 = 31,
    _render_method_extern_light_color_2 = 32,
    _render_method_extern_light_dir_3 = 33,
    _render_method_extern_light_color_3 = 34,
    _render_method_extern_texture_unused_3 = 35,
    _render_method_extern_texture_unused_4 = 36,
    _render_method_extern_texture_unused_5 = 37,
    _render_method_extern_texture_dynamic_light_gel_0 = 38,
    _render_method_extern_flat_envmap_matrix_x = 39,
    _render_method_extern_flat_envmap_matrix_y = 40,
    _render_method_extern_flat_envmap_matrix_z = 41,
    _render_method_extern_debug_tint = 42,
    _render_method_extern_screen_constants = 43,
    _render_method_extern_active_camo_distortion_texture = 44,
    _render_method_extern_scene_ldr_texture = 45,
    _render_method_extern_scene_hdr_texture = 46,
    _render_method_extern_water_memory_export_address = 47,
    _render_method_extern_tree_animation_timer = 48,
    _render_method_extern_emblem_player_shoulder_texture = 49,
    _render_method_extern_emblem_clan_chest_texture = 50,
    _render_method_extern_global_depth_constants = 51,
    _render_method_extern_global_camera_forward = 52
};

enum e_render_method_option_option_block_default_filter_mode
{
    _render_method_option_option_block_default_filter_mode_trilinear = 0,
    _render_method_option_option_block_default_filter_mode_point = 1,
    _render_method_option_option_block_default_filter_mode_bilinear = 2,
    _render_method_option_option_block_default_filter_mode_unused_00 = 3,
    _render_method_option_option_block_default_filter_mode_anisotropic_2_x = 4,
    _render_method_option_option_block_default_filter_mode_unused_01 = 5,
    _render_method_option_option_block_default_filter_mode_anisotropic_4_x = 6,
    _render_method_option_option_block_default_filter_mode_lightprobe_texture_array = 7,
    _render_method_option_option_block_default_filter_mode_texture_array_quadlinear = 8,
    _render_method_option_option_block_default_filter_mode_texture_array_quadanisotropic_2_x = 9
};

enum e_render_method_option_option_block_default_address_mode
{
    _render_method_option_option_block_default_address_mode_wrap = 0,
    _render_method_option_option_block_default_address_mode_clamp = 1,
    _render_method_option_option_block_default_address_mode_mirror = 2,
    _render_method_option_option_block_default_address_mode_black_border = 3,
    _render_method_option_option_block_default_address_mode_mirror_once = 4,
    _render_method_option_option_block_default_address_mode_mirror_once_border = 5
};


/* ---------- structures */

struct s_render_method_option_option_block;
struct s_render_method_option;

struct s_render_method_option
{
    c_tag_block<s_render_method_option_option_block> options;
};
static_assert(sizeof(s_render_method_option) == 0xC);

struct s_render_method_option_option_block
{
    string_id name;
    c_enum<e_render_method_option_option_block_option_data_type, ulong> type;
    c_enum<e_render_method_extern, long> render_method_extern;
    s_tag_reference default_sampler_bitmap;
    float default_float_argument;
    ulong default_int_bool_argument;
    short flags;
    c_enum<e_render_method_option_option_block_default_filter_mode, short> default_filter_mode;
    c_enum<e_render_method_option_option_block_default_address_mode, short> default_address_mode;
    short anisotropy_amount;
    argb_color default_color;
    float default_bitmap_scale;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
};
static_assert(sizeof(s_render_method_option_option_block) == 0x48);

