/* ---------- enums */

enum e_forge_globals_definition_palette_item_type
{
    _forge_globals_definition_palette_item_type_none = 0,
    _forge_globals_definition_palette_item_type_tool = 1,
    _forge_globals_definition_palette_item_type_prop = 2,
    _forge_globals_definition_palette_item_type_light = 3,
    _forge_globals_definition_palette_item_type_effects = 4,
    _forge_globals_definition_palette_item_type_structure = 5,
    _forge_globals_definition_palette_item_type_equipment = 6,
    _forge_globals_definition_palette_item_type_weapon = 7,
    _forge_globals_definition_palette_item_type_vehicle = 8,
    _forge_globals_definition_palette_item_type_teleporter = 9,
    _forge_globals_definition_palette_item_type_game = 10,
    _forge_globals_definition_palette_item_type_assault = 11,
    _forge_globals_definition_palette_item_type_capture_the_flag = 12,
    _forge_globals_definition_palette_item_type_infection = 13,
    _forge_globals_definition_palette_item_type_juggernaut = 14,
    _forge_globals_definition_palette_item_type_king_of_the_hill = 15,
    _forge_globals_definition_palette_item_type_territories = 16,
    _forge_globals_definition_palette_item_type_slayer = 17,
    _forge_globals_definition_palette_item_type_vip = 18
};

enum e_forge_globals_definition_palette_item_setter_target
{
    _forge_globals_definition_palette_item_setter_target_general_on_map_at_start = 0,
    _forge_globals_definition_palette_item_setter_target_general_symmetry = 1,
    _forge_globals_definition_palette_item_setter_target_general_respawn_rate = 2,
    _forge_globals_definition_palette_item_setter_target_general_spare_clips = 3,
    _forge_globals_definition_palette_item_setter_target_general_spawn_order = 4,
    _forge_globals_definition_palette_item_setter_target_general_team = 5,
    _forge_globals_definition_palette_item_setter_target_general_teleporter_channel = 6,
    _forge_globals_definition_palette_item_setter_target_general_shape_type = 7,
    _forge_globals_definition_palette_item_setter_target_general_shape_radius = 8,
    _forge_globals_definition_palette_item_setter_target_general_shape_width = 9,
    _forge_globals_definition_palette_item_setter_target_general_shape_top = 10,
    _forge_globals_definition_palette_item_setter_target_general_shape_bottom = 11,
    _forge_globals_definition_palette_item_setter_target_general_shape_depth = 12,
    _forge_globals_definition_palette_item_setter_target_general_physics = 13,
    _forge_globals_definition_palette_item_setter_target_general_engine_flags = 14,
    _forge_globals_definition_palette_item_setter_target_reforge_material = 15,
    _forge_globals_definition_palette_item_setter_target_reforge_material_color_r = 16,
    _forge_globals_definition_palette_item_setter_target_reforge_material_color_g = 17,
    _forge_globals_definition_palette_item_setter_target_reforge_material_color_b = 18,
    _forge_globals_definition_palette_item_setter_target_reforge_material_texture_override = 19,
    _forge_globals_definition_palette_item_setter_target_reforge_material_texture_scale = 20,
    _forge_globals_definition_palette_item_setter_target_reforge_material_texture_offset_x = 21,
    _forge_globals_definition_palette_item_setter_target_reforge_material_texture_offset_y = 22,
    _forge_globals_definition_palette_item_setter_target_reforge_material_allows_projectiles = 23,
    _forge_globals_definition_palette_item_setter_target_reforge_material_type = 24,
    _forge_globals_definition_palette_item_setter_target_light_type = 25,
    _forge_globals_definition_palette_item_setter_target_light_color_r = 26,
    _forge_globals_definition_palette_item_setter_target_light_color_g = 27,
    _forge_globals_definition_palette_item_setter_target_light_color_b = 28,
    _forge_globals_definition_palette_item_setter_target_light_intensity = 29,
    _forge_globals_definition_palette_item_setter_target_light_radius = 30,
    _forge_globals_definition_palette_item_setter_target_light_field_of_view = 31,
    _forge_globals_definition_palette_item_setter_target_light_near_width = 32,
    _forge_globals_definition_palette_item_setter_target_light_illumination_type = 33,
    _forge_globals_definition_palette_item_setter_target_light_illumination_base = 34,
    _forge_globals_definition_palette_item_setter_target_light_illumination_freq = 35,
    _forge_globals_definition_palette_item_setter_target_fx_range = 36,
    _forge_globals_definition_palette_item_setter_target_fx_light_intensity = 37,
    _forge_globals_definition_palette_item_setter_target_fx_hue = 38,
    _forge_globals_definition_palette_item_setter_target_fx_saturation = 39,
    _forge_globals_definition_palette_item_setter_target_fx_desaturation = 40,
    _forge_globals_definition_palette_item_setter_target_fx_gamma_increase = 41,
    _forge_globals_definition_palette_item_setter_target_fx_gamma_decrease = 42,
    _forge_globals_definition_palette_item_setter_target_fx_color_filter_r = 43,
    _forge_globals_definition_palette_item_setter_target_fx_color_filter_g = 44,
    _forge_globals_definition_palette_item_setter_target_fx_color_filter_b = 45,
    _forge_globals_definition_palette_item_setter_target_fx_color_floor_r = 46,
    _forge_globals_definition_palette_item_setter_target_fx_color_floor_g = 47,
    _forge_globals_definition_palette_item_setter_target_fx_color_floor_b = 48,
    _forge_globals_definition_palette_item_setter_target_fx_tracing = 49,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_collect_dead_biped = 50,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_collect_weapons = 51,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_collect_objectives = 52,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_collect_grenades = 53,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_collect_equipment = 54,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_collect_vehicles = 55,
    _forge_globals_definition_palette_item_setter_target_garbage_volume_interval = 56,
    _forge_globals_definition_palette_item_setter_target_kill_volume_always_visible = 57,
    _forge_globals_definition_palette_item_setter_target_kill_volume_destroy_vehicles = 58,
    _forge_globals_definition_palette_item_setter_target_kill_volume_damage_cause = 59,
    _forge_globals_definition_palette_item_setter_target_map_disable_push_barrier = 60,
    _forge_globals_definition_palette_item_setter_target_map_disable_death_barrier = 61,
    _forge_globals_definition_palette_item_setter_target_map_physics_gravity = 62,
    _forge_globals_definition_palette_item_setter_target_camera_fx_exposure = 63,
    _forge_globals_definition_palette_item_setter_target_camera_fx_light_intensity = 64,
    _forge_globals_definition_palette_item_setter_target_camera_fx_bloom = 65,
    _forge_globals_definition_palette_item_setter_target_camera_fx_light_bloom = 66,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_enabled = 67,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_weather = 68,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_brightness = 69,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_fog_density = 70,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_fog_visibility = 71,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_fog_color_r = 72,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_fog_color_g = 73,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_fog_color_b = 74,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_skybox = 75,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_skybox_offset_z = 76,
    _forge_globals_definition_palette_item_setter_target_atmosphere_properties_skybox_override_transform = 77,
    _forge_globals_definition_palette_item_setter_target_budget_minimum = 78,
    _forge_globals_definition_palette_item_setter_target_budget_maximum = 79
};

enum e_forge_globals_definition_palette_item_setter_type
{
    _forge_globals_definition_palette_item_setter_type_integer = 0,
    _forge_globals_definition_palette_item_setter_type_real = 1
};

enum e_forge_globals_definition_palette_item_setter_flags
{
    _forge_globals_definition_palette_item_setter_flags_hidden_bit = 0
};

enum e_forge_globals_definition_sky_flags
{
};


/* ---------- structures */

struct s_forge_globals_definition_re_forge_material;
struct s_forge_globals_definition_re_forge_material_type;
struct s_tag_reference_block;
struct s_forge_globals_definition_description;
struct s_forge_globals_definition_palette_category;
struct s_forge_globals_definition_palette_item_setter;
struct s_forge_globals_definition_palette_item;
struct s_forge_globals_definition_weather_effect;
struct s_forge_globals_definition_sky;
struct s_forge_globals_definition;

struct s_forge_globals_definition
{
    s_tag_reference invisible_render_method;
    s_tag_reference default_render_method;
    c_tag_block<s_forge_globals_definition_re_forge_material> re_forge_materials;
    c_tag_block<s_forge_globals_definition_re_forge_material_type> re_forge_material_types;
    c_tag_block<s_tag_reference_block> re_forge_objects;
    s_tag_reference prematch_camera_object;
    s_tag_reference modifier_object;
    s_tag_reference kill_volume_object;
    s_tag_reference garbage_volume_object;
    c_tag_block<s_forge_globals_definition_description> descriptions;
    c_tag_block<s_forge_globals_definition_palette_category> palette_categories;
    c_tag_block<s_forge_globals_definition_palette_item> palette;
    c_tag_block<s_forge_globals_definition_weather_effect> weather_effects;
    c_tag_block<s_forge_globals_definition_sky> skies;
};
static_assert(sizeof(s_forge_globals_definition) == 0xC0);

struct s_forge_globals_definition_sky
{
    char name[32];
    c_flags<e_forge_globals_definition_sky_flags, long> flags;
    real_point3d translation;
    real_euler_angles3d orientation;
    s_tag_reference object;
    s_tag_reference parameters;
    s_tag_reference wind;
    s_tag_reference camera_fx;
    s_tag_reference screen_fx;
    s_tag_reference global_lighting;
    s_tag_reference background_sound;
};
static_assert(sizeof(s_forge_globals_definition_sky) == 0xAC);

struct s_forge_globals_definition_weather_effect
{
    char name[32];
    s_tag_reference effect;
};
static_assert(sizeof(s_forge_globals_definition_weather_effect) == 0x30);

struct s_forge_globals_definition_palette_item
{
    char name[32];
    c_enum<e_forge_globals_definition_palette_item_type, ushort> type;
    short category_index;
    short description_index;
    ushort max_allowed;
    s_tag_reference object;
    c_tag_block<s_forge_globals_definition_palette_item_setter> setters;
};
static_assert(sizeof(s_forge_globals_definition_palette_item) == 0x44);

struct s_forge_globals_definition_palette_item_setter
{
    c_enum<e_forge_globals_definition_palette_item_setter_target, short> target;
    c_enum<e_forge_globals_definition_palette_item_setter_type, char> type;
    c_flags<e_forge_globals_definition_palette_item_setter_flags, uchar> flags;
    long integer_value;
    float real_value;
};
static_assert(sizeof(s_forge_globals_definition_palette_item_setter) == 0xC);

struct s_forge_globals_definition_palette_category
{
    char name[32];
    short description_index;
    short parent_category_index;
};
static_assert(sizeof(s_forge_globals_definition_palette_category) == 0x24);

struct s_forge_globals_definition_description
{
    char text[256];
};
static_assert(sizeof(s_forge_globals_definition_description) == 0x100);

struct s_tag_reference_block
{
    s_tag_reference instance;
};
static_assert(sizeof(s_tag_reference_block) == 0x10);

struct s_forge_globals_definition_re_forge_material_type
{
    char name[32];
    short collision_material_index;
    short physics_material_index;
};
static_assert(sizeof(s_forge_globals_definition_re_forge_material_type) == 0x24);

struct s_forge_globals_definition_re_forge_material
{
    char name[32];
    s_tag_reference render_method;
};
static_assert(sizeof(s_forge_globals_definition_re_forge_material) == 0x30);

