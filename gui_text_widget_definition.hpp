/* ---------- enums */

enum e_font
{
    _font_fixedsys9 = 0,
    _font_conduit16 = 1,
    _font_conduit32 = 2,
    _font_conduit23 = 3,
    _font_conduit18 = 4,
    _font_larabie_10 = 5,
    _font_pragmata_14 = 6
};


/* ---------- structures */

struct s_gui_definition;
struct s_gui_text_widget_definition;

struct s_gui_text_widget_definition
{
    ulong flags;
    s_gui_definition gui_render_block;
    string_id data_source_name;
    string_id text_string;
    string_id text_color;
    c_enum<e_font, short> text_font;
    short unknown2;
};
static_assert(sizeof(s_gui_text_widget_definition) == 0x3C);

struct s_gui_definition
{
    string_id name;
    short unknown;
    short layer;
    short widescreen_y_min;
    short widescreen_x_min;
    short widescreen_y_max;
    short widescreen_x_max;
    short standard_y_min;
    short standard_x_min;
    short standard_y_max;
    short standard_x_max;
    s_tag_reference animation;
};
static_assert(sizeof(s_gui_definition) == 0x28);

