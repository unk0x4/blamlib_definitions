/* ---------- enums */

enum e_physics_model_physics_model_flags
{
    _physics_model_physics_model_flags_serialized_havok_data_bit = 0,
    _physics_model_physics_model_flags_make_physical_children_keyframed_bit = 1,
    _physics_model_physics_model_flags_shrink_radius_by_havok_complex_radius_bit = 2,
    _physics_model_physics_model_flags_use_physics_for_collision_bit = 3
};

enum e_physics_model_phantom_type_flags_halo3_odst_bits
{
    _physics_model_phantom_type_flags_halo3_odst_bits_generates_effects_bit = 0,
    _physics_model_phantom_type_flags_halo3_odst_bits_use_acceleration_as_force_bit = 1,
    _physics_model_phantom_type_flags_halo3_odst_bits_negates_gravity_bit = 2,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_players_bit = 3,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_non_players_bit = 4,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_bipeds_bit = 5,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_vehicles_bit = 6,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_weapons_bit = 7,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_equipement_bit = 8,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_arg_devices_bit = 9,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_terminals_bit = 10,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_projectiles_bit = 11,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_scenery_bit = 12,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_machines_bit = 13,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_controls_bit = 14,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_sound_scenery_bit = 15,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_crates_bit = 16,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_creatures_bit = 17,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_giants_bit = 18,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_effect_scenery_bit = 19,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown1_bit = 20,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown2_bit = 21,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown3_bit = 22,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown4_bit = 23,
    _physics_model_phantom_type_flags_halo3_odst_bits_localizes_physics_bit = 24,
    _physics_model_phantom_type_flags_halo3_odst_bits_disable_linear_damping_bit = 25,
    _physics_model_phantom_type_flags_halo3_odst_bits_disable_angular_damping_bit = 26,
    _physics_model_phantom_type_flags_halo3_odst_bits_ignores_dead_bipeds_bit = 27,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown5_bit = 28,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown6_bit = 29,
    _physics_model_phantom_type_flags_halo3_odst_bits_unknown7_bit = 30,
    _physics_model_phantom_type_flags_halo3_odst_bits_accelerate_along_input_direction_bit = 31
};

enum e_physics_model_phantom_type_size
{
    _physics_model_phantom_type_size_default = 0,
    _physics_model_phantom_type_size_tiny = 1,
    _physics_model_phantom_type_size_small = 2,
    _physics_model_phantom_type_size_medium = 3,
    _physics_model_phantom_type_size_large = 4,
    _physics_model_phantom_type_size_huge = 5,
    _physics_model_phantom_type_size_extra_huge = 6
};

enum e_physics_model_constraint_type
{
    _physics_model_constraint_type_hinge = 0,
    _physics_model_constraint_type_limited_hinge = 1,
    _physics_model_constraint_type_ragdoll = 2,
    _physics_model_constraint_type_stiff_spring = 3,
    _physics_model_constraint_type_ball_and_socket = 4,
    _physics_model_constraint_type_prismatic = 5,
    _physics_model_constraint_type_powered_chain = 6
};

enum e_physics_model_motor_type
{
    _physics_model_motor_type_none = 0,
    _physics_model_motor_type_damped_sprint = 1,
    _physics_model_motor_type_strongest_force = 2
};

enum e_physics_model_node_edge_constraint_constraint_flags
{
    _physics_model_node_edge_constraint_constraint_flags_is_physical_child_bit = 0,
    _physics_model_node_edge_constraint_constraint_flags_is_rigid_bit = 1,
    _physics_model_node_edge_constraint_constraint_flags_disable_effects_bit = 2,
    _physics_model_node_edge_constraint_constraint_flags_not_created_automatically_bit = 3
};

enum e_physics_model_rigid_body_rigid_body_flags
{
    _physics_model_rigid_body_rigid_body_flags_no_collisions_with_self_bit = 0,
    _physics_model_rigid_body_rigid_body_flags_only_collide_with_environment_bit = 1,
    _physics_model_rigid_body_rigid_body_flags_disable_effects_bit = 2,
    _physics_model_rigid_body_rigid_body_flags_does_not_interact_with_environment_bit = 3,
    _physics_model_rigid_body_rigid_body_flags_best_early_mover_body_bit = 4,
    _physics_model_rigid_body_rigid_body_flags_has_no_phantom_power_version_bit = 5
};

enum e_physics_model_rigid_body_motion_type
{
    _physics_model_rigid_body_motion_type_sphere = 0,
    _physics_model_rigid_body_motion_type_stabilized_sphere = 1,
    _physics_model_rigid_body_motion_type_box = 2,
    _physics_model_rigid_body_motion_type_stabilized_box = 3,
    _physics_model_rigid_body_motion_type_keyframed = 4,
    _physics_model_rigid_body_motion_type_fixed = 5
};

enum e_physics_model_rigid_body_size
{
    _physics_model_rigid_body_size_default = 0,
    _physics_model_rigid_body_size_tiny = 1,
    _physics_model_rigid_body_size_small = 2,
    _physics_model_rigid_body_size_medium = 3,
    _physics_model_rigid_body_size_large = 4,
    _physics_model_rigid_body_size_huge = 5,
    _physics_model_rigid_body_size_extra_huge = 6
};

enum e_blam_shape_type
{
    _blam_shape_type_sphere = 0,
    _blam_shape_type_pill = 1,
    _blam_shape_type_box = 2,
    _blam_shape_type_triangle = 3,
    _blam_shape_type_polyhedron = 4,
    _blam_shape_type_multi_sphere = 5,
    _blam_shape_type_triangle_mesh = 6,
    _blam_shape_type_compound_shape = 7,
    _blam_shape_type_unused0 = 8,
    _blam_shape_type_unused1 = 9,
    _blam_shape_type_unused2 = 10,
    _blam_shape_type_unused3 = 11,
    _blam_shape_type_unused4 = 12,
    _blam_shape_type_unused5 = 13,
    _blam_shape_type_list = 14,
    _blam_shape_type_mopp = 15
};


/* ---------- structures */

struct s_physics_model_damped_spring_motor;
struct s_physics_model_position_motor;
struct s_physics_model_phantom_type_flags;
struct s_physics_model_phantom_type;
struct s_physics_model_powered_chain_node;
struct s_physics_model_motor;
struct s_physics_model_powered_chain_constraint;
struct s_physics_model_powered_chain;
struct s_physics_model_node_edge_constraint_ragdoll_motor;
struct s_physics_model_node_edge_constraint_limited_hinge_motor;
struct s_physics_model_node_edge_constraint;
struct s_physics_model_node_edge;
struct s_physics_model_rigid_body;
struct s_physics_model_material;
struct s_physics_model_havok_shape_base;
struct s_physics_model_shape;
struct s_physics_model_havok_shape_reference;
struct s_physics_model_sphere;
struct s_physics_model_pill;
struct s_physics_model_box;
struct s_physics_model_triangle;
struct s_physics_model_polyhedron;
struct s_physics_model_polyhedron_four_vector;
struct s_physics_model_polyhedron_plane_equation;
struct s_physics_model_list;
struct s_physics_model_list_shape;
struct s_hkp_referenced_object;
struct s_hkp_shape;
struct s_hkp_shape_container;
struct s_hkp_single_shape_container;
struct s_hkp_mopp_bv_tree_shape;
struct s_c_mopp_bv_tree_shape;
struct s_physics_model_hinge_constraint;
struct s_physics_model_ragdoll_constraint;
struct s_physics_model_region_permutation_rigid_body;
struct s_physics_model_region_permutation;
struct s_physics_model_region;
struct s_physics_model_node;
struct s_physics_model_limited_hinge_constraint;
struct s_physics_model_havok_shape_base_no_radius;
struct s_physics_model_phantom;
struct s_physics_model;

struct s_physics_model
{
    c_flags<e_physics_model_physics_model_flags, long> flags;
    float mass;
    float low_frequency_deactivation_scale;
    float high_frequency_deactivation_scale;
    float custom_shape_radius;
    float maximum_penetration_depth_scale;
    char import_version;
    byte unused[3];
    c_tag_block<s_physics_model_damped_spring_motor> damped_spring_motors;
    c_tag_block<s_physics_model_position_motor> position_motors;
    c_tag_block<s_physics_model_phantom_type> phantom_types;
    c_tag_block<s_physics_model_powered_chain> powered_chains;
    c_tag_block<s_physics_model_node_edge> node_edges;
    c_tag_block<s_physics_model_rigid_body> rigid_bodies;
    c_tag_block<s_physics_model_material> materials;
    c_tag_block<s_physics_model_sphere> spheres;
    byte unused_multi_spheres[12];
    c_tag_block<s_physics_model_pill> pills;
    c_tag_block<s_physics_model_box> boxes;
    c_tag_block<s_physics_model_triangle> triangles;
    c_tag_block<s_physics_model_polyhedron> polyhedra;
    c_tag_block<s_physics_model_polyhedron_four_vector> polyhedron_four_vectors;
    c_tag_block<s_physics_model_polyhedron_plane_equation> polyhedron_plane_equations;
    byte unused_mass_distributions[12];
    c_tag_block<s_physics_model_list> lists;
    c_tag_block<s_physics_model_list_shape> list_shapes;
    c_tag_block<s_c_mopp_bv_tree_shape> mopps;
    s_tag_data mopp_data;
    c_tag_block<s_physics_model_hinge_constraint> hinge_constraints;
    c_tag_block<s_physics_model_ragdoll_constraint> ragdoll_constraints;
    c_tag_block<s_physics_model_region> regions;
    c_tag_block<s_physics_model_node> nodes;
    byte unused_errors[12];
    byte unused_point_to_path_curves[12];
    c_tag_block<s_physics_model_limited_hinge_constraint> limited_hinge_constraints;
    byte unused_ball_and_socket_constraints[12];
    byte unused_stiff_spring_constraints[12];
    byte unused_prismatic_constraints[12];
    c_tag_block<s_physics_model_phantom> phantoms;
};
static_assert(sizeof(s_physics_model) == 0x198);

struct s_physics_model_phantom
{
    s_physics_model_havok_shape_base_no_radius shape_base;
    c_enum<e_blam_shape_type, short> shape_type;
    short shape_index;
    ulong unknown4;
    ulong unknown5;
    s_physics_model_havok_shape_base_no_radius phantom_shape;
};
static_assert(sizeof(s_physics_model_phantom) == 0x2C);

struct s_physics_model_havok_shape_base_no_radius
{
    long field_pointer_skip;
    short size;
    short count;
    long offset;
    long user_data;
};
static_assert(sizeof(s_physics_model_havok_shape_base_no_radius) == 0x10);

struct s_physics_model_limited_hinge_constraint
{
    string_id name;
    short node_a;
    short node_b;
    float a_scale;
    real_vector3d a_forward;
    real_vector3d a_left;
    real_vector3d a_up;
    real_point3d a_position;
    float b_scale;
    real_vector3d b_forward;
    real_vector3d b_left;
    real_vector3d b_up;
    real_point3d b_position;
    short edge_index;
    short unknown;
    ulong unknown2;
    float limit_friction;
    real_bounds limit_angle_bounds;
};
static_assert(sizeof(s_physics_model_limited_hinge_constraint) == 0x84);

struct s_physics_model_node
{
    string_id name;
    ushort flags;
    short parent;
    short sibling;
    short child;
};
static_assert(sizeof(s_physics_model_node) == 0xC);

struct s_physics_model_region
{
    string_id name;
    c_tag_block<s_physics_model_region_permutation> permutations;
};
static_assert(sizeof(s_physics_model_region) == 0x10);

struct s_physics_model_region_permutation
{
    string_id name;
    c_tag_block<s_physics_model_region_permutation_rigid_body> rigid_bodies;
};
static_assert(sizeof(s_physics_model_region_permutation) == 0x10);

struct s_physics_model_region_permutation_rigid_body
{
    short rigid_body_index;
};
static_assert(sizeof(s_physics_model_region_permutation_rigid_body) == 0x2);

struct s_physics_model_ragdoll_constraint
{
    string_id name;
    short node_a;
    short node_b;
    float a_scale;
    real_vector3d a_forward;
    real_vector3d a_left;
    real_vector3d a_up;
    real_point3d a_position;
    float b_scale;
    real_vector3d b_forward;
    real_vector3d b_left;
    real_vector3d b_up;
    real_point3d b_position;
    short edge_index;
    short unknown;
    ulong unknown2;
    real_bounds twist_range;
    real_bounds cone_range;
    real_bounds plane_range;
    float max_friction_torque;
};
static_assert(sizeof(s_physics_model_ragdoll_constraint) == 0x94);

struct s_physics_model_hinge_constraint
{
    string_id name;
    short node_a;
    short node_b;
    float a_scale;
    real_vector3d a_forward;
    real_vector3d a_left;
    real_vector3d a_up;
    real_point3d a_position;
    float b_scale;
    real_vector3d b_forward;
    real_vector3d b_left;
    real_vector3d b_up;
    real_point3d b_position;
    short edge_index;
    short unknown;
    ulong unknown2;
};
static_assert(sizeof(s_physics_model_hinge_constraint) == 0x78);

struct s_c_mopp_bv_tree_shape : s_hkp_mopp_bv_tree_shape
{
    float scale;
};
static_assert(sizeof(s_c_mopp_bv_tree_shape) == 0x20);

struct s_hkp_mopp_bv_tree_shape : s_hkp_shape
{
    s_hkp_single_shape_container child;
    ulong mopp_code_address;
};
static_assert(sizeof(s_hkp_mopp_bv_tree_shape) == 0x1C);

struct s_hkp_single_shape_container : s_hkp_shape_container
{
    c_enum<e_blam_shape_type, short> type;
    short index;
};
static_assert(sizeof(s_hkp_single_shape_container) == 0x8);

struct s_hkp_shape_container
{
    ulong vf_table_address;
};
static_assert(sizeof(s_hkp_shape_container) == 0x4);

struct s_hkp_shape
{
    ulong vf_table_address;
    s_hkp_referenced_object referenced_object;
    ulong user_data_address;
    ulong type;
};
static_assert(sizeof(s_hkp_shape) == 0x10);

struct s_hkp_referenced_object
{
    ushort size_and_flags;
    ushort reference_count;
};
static_assert(sizeof(s_hkp_referenced_object) == 0x4);

struct s_physics_model_list_shape
{
    c_enum<e_blam_shape_type, short> shape_type;
    short shape_index;
    ulong collision_filter;
    ulong shape_size;
    ulong num_child_shapes;
};
static_assert(sizeof(s_physics_model_list_shape) == 0x10);

struct s_physics_model_list
{
    long field_pointer_skip;
    short size;
    short count;
    long offset;
    long user_data;
    long field_pointer_skip0;
    uchar disable_welding;
    uchar collection_type;
    byte pad0[2];
    long field_pointer_skip1;
    long child_shapes_size;
    ulong child_shapes_capacity;
    byte nail_in_dick[12];
    real_vector3d aabb_half_extents;
    float aabb_half_extents_radius;
    real_vector3d aabb_center;
    float aabb_center_radius;
};
static_assert(sizeof(s_physics_model_list) == 0x50);

struct s_physics_model_polyhedron_plane_equation
{
    real_plane3d plane_equation;
};
static_assert(sizeof(s_physics_model_polyhedron_plane_equation) == 0x10);

struct s_physics_model_polyhedron_four_vector
{
    real_vector3d four_vectors_x;
    float four_vectors_x_radius;
    real_vector3d four_vectors_y;
    float four_vectors_y_radius;
    real_vector3d four_vectors_z;
    float four_vectors_z_radius;
};
static_assert(sizeof(s_physics_model_polyhedron_four_vector) == 0x30);

struct s_physics_model_polyhedron : s_physics_model_shape
{
    real_vector3d aabb_half_extents;
    float aabb_half_extents_radius;
    real_vector3d aabb_center;
    float aabb_center_radius;
    ulong field_pointer_skip;
    long four_vectors_size;
    ulong four_vectors_capacity;
    long num_vertices;
    ulong m_use_spu_buffer;
    long plane_equations_size;
    ulong plane_equations_capacity;
    ulong connectivity;
};
static_assert(sizeof(s_physics_model_polyhedron) == 0x80);

struct s_physics_model_triangle : s_physics_model_shape
{
    real_vector3d point_a;
    float havokw_point_a;
    real_vector3d point_b;
    float havokw_point_b;
    real_vector3d point_c;
    float havokw_point_c;
    real_vector3d extrusion;
    float havokw_extrusion;
};
static_assert(sizeof(s_physics_model_triangle) == 0x80);

struct s_physics_model_box : s_physics_model_shape
{
    real_vector3d half_extents;
    float half_extents_radius;
    s_physics_model_havok_shape_base convex_base;
    ulong field_pointer_skip;
    s_physics_model_havok_shape_reference shape_reference;
    real_vector3d rotation_i;
    float rotation_i_radius;
    real_vector3d rotation_j;
    float rotation_j_radius;
    real_vector3d rotation_k;
    float rotation_k_radius;
    real_vector3d translation;
    float translation_radius;
};
static_assert(sizeof(s_physics_model_box) == 0xB0);

struct s_physics_model_pill : s_physics_model_shape
{
    real_vector3d bottom;
    float bottom_radius;
    real_vector3d top;
    float top_radius;
};
static_assert(sizeof(s_physics_model_pill) == 0x60);

struct s_physics_model_sphere : s_physics_model_shape
{
    s_physics_model_havok_shape_base convex_base;
    ulong field_pointer_skip;
    s_physics_model_havok_shape_reference shape_reference;
    real_vector3d translation;
    float translation_radius;
};
static_assert(sizeof(s_physics_model_sphere) == 0x70);

struct s_physics_model_havok_shape_reference
{
    c_enum<e_blam_shape_type, short> shapetype;
    short shape_index;
    ulong child_shape_size;
};
static_assert(sizeof(s_physics_model_havok_shape_reference) == 0x8);

struct s_physics_model_shape
{
    string_id name;
    short material_index;
    short runtime_material_type;
    float relative_mass_scale;
    float friction;
    float restitution;
    float volume;
    float mass;
    short mass_distribution_index;
    char phantom_index;
    char proxy_collision_group;
    s_physics_model_havok_shape_base shape_base;
    byte pad[12];
};
static_assert(sizeof(s_physics_model_shape) == 0x40);

struct s_physics_model_havok_shape_base
{
    long field_pointer_skip;
    short size;
    short count;
    long offset;
    long user_data;
    float radius;
};
static_assert(sizeof(s_physics_model_havok_shape_base) == 0x14);

struct s_physics_model_material
{
    string_id name;
    string_id material_name;
    short phantom_type;
    uchar proxy_collision_group;
    uchar runtime_collision_group;
};
static_assert(sizeof(s_physics_model_material) == 0xC);

struct s_physics_model_rigid_body
{
    short node;
    short region;
    short permutation;
    short serialized_shapes;
    real_point3d bounding_sphere_offset;
    float bounding_sphere_radius;
    c_flags<e_physics_model_rigid_body_rigid_body_flags, short> flags;
    c_enum<e_physics_model_rigid_body_motion_type, short> motion_type;
    short no_phantom_power_alt_rigid_body;
    c_enum<e_physics_model_rigid_body_size, short> size;
    float inertia_tensor_scale;
    float linear_dampening;
    float angular_dampening;
    real_vector3d center_of_mass_offset;
    float water_physics_x_0;
    float water_physics_x_1;
    float water_physics_y_0;
    float water_physics_y_1;
    float water_physics_z_0;
    float water_physics_z_1;
    ulong runtime_shape_pointer;
    ulong unknown9;
    c_enum<e_blam_shape_type, short> shape_type;
    short shape_index;
    float mass;
    real_vector3d center_of_mass;
    float center_of_mass_radius;
    real_vector3d inertia_tensor_x;
    float inertia_tensor_x_radius;
    real_vector3d inertia_tensor_y;
    float inertia_tensor_y_radius;
    real_vector3d inertia_tensor_z;
    float inertia_tensor_z_radius;
    ulong runtime_havok_group_mask;
    ulong unknown10;
    float bounding_sphere_pad;
    uchar collision_quality_override_type;
    byte pad3[1];
    short runtime_flags;
};
static_assert(sizeof(s_physics_model_rigid_body) == 0xB0);

struct s_physics_model_node_edge
{
    short node_a_global_material_index;
    short node_b_global_material_index;
    short node_a;
    short node_b;
    c_tag_block<s_physics_model_node_edge_constraint> constraints;
    string_id node_a_material;
    string_id node_b_material;
};
static_assert(sizeof(s_physics_model_node_edge) == 0x1C);

struct s_physics_model_node_edge_constraint
{
    c_enum<e_physics_model_constraint_type, short> type;
    short index;
    c_flags<e_physics_model_node_edge_constraint_constraint_flags, long> flags;
    float friction;
    c_tag_block<s_physics_model_node_edge_constraint_ragdoll_motor> ragdoll_motors;
    c_tag_block<s_physics_model_node_edge_constraint_limited_hinge_motor> limited_hinge_motors;
};
static_assert(sizeof(s_physics_model_node_edge_constraint) == 0x24);

struct s_physics_model_node_edge_constraint_limited_hinge_motor
{
    s_physics_model_motor motor;
};
static_assert(sizeof(s_physics_model_node_edge_constraint_limited_hinge_motor) == 0x4);

struct s_physics_model_node_edge_constraint_ragdoll_motor
{
    s_physics_model_motor twist_motor;
    s_physics_model_motor cone_motor;
    s_physics_model_motor plane_motor;
};
static_assert(sizeof(s_physics_model_node_edge_constraint_ragdoll_motor) == 0xC);

struct s_physics_model_powered_chain
{
    c_tag_block<s_physics_model_powered_chain_node> nodes;
    c_tag_block<s_physics_model_powered_chain_constraint> constraints;
};
static_assert(sizeof(s_physics_model_powered_chain) == 0x18);

struct s_physics_model_powered_chain_constraint
{
    c_enum<e_physics_model_constraint_type, short> constraint_type;
    short constraint_index;
    s_physics_model_motor motor_x;
    s_physics_model_motor motor_y;
    s_physics_model_motor motor_z;
};
static_assert(sizeof(s_physics_model_powered_chain_constraint) == 0x10);

struct s_physics_model_motor
{
    c_enum<e_physics_model_motor_type, short> type;
    short index;
};
static_assert(sizeof(s_physics_model_motor) == 0x4);

struct s_physics_model_powered_chain_node
{
    short node_index;
};
static_assert(sizeof(s_physics_model_powered_chain_node) == 0x2);

struct s_physics_model_phantom_type
{
    s_physics_model_phantom_type_flags flags;
    c_enum<e_physics_model_phantom_type_size, char> minimum_size;
    c_enum<e_physics_model_phantom_type_size, char> maximum_size;
    byte unused1[2];
    string_id marker_name;
    string_id alignment_marker_name;
    byte unused2[8];
    float hookes_law_e;
    float linear_dead_radius;
    float center_acceleration;
    float center_max_level;
    float axis_acceleration;
    float axis_max_velocity;
    float direction_acceleration;
    float direction_max_velocity;
    byte unused3[28];
    float alignment_hookes_law_e;
    float alignment_acceleration;
    float alignment_max_velocity;
    byte unused4[8];
};
static_assert(sizeof(s_physics_model_phantom_type) == 0x68);

struct s_physics_model_phantom_type_flags
{
    c_flags<e_physics_model_phantom_type_flags_halo3_odst_bits, ulong> halo3_odst;
};
static_assert(sizeof(s_physics_model_phantom_type_flags) == 0x4);

struct s_physics_model_position_motor
{
    string_id name;
    float maximum_force;
    float minimum_force;
    float tau;
    float damping;
    float proportion_recover_velocity;
    float constant_recover_velocity;
    float initial_position;
};
static_assert(sizeof(s_physics_model_position_motor) == 0x20);

struct s_physics_model_damped_spring_motor
{
    string_id name;
    float maximum_force;
    float minimum_force;
    float spring_k;
    float damping;
    float initial_position;
};
static_assert(sizeof(s_physics_model_damped_spring_motor) == 0x18);

