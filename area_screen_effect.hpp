/* ---------- enums */

enum e_area_screen_effect_screen_effect_block_flag_bits_ho
{
    _area_screen_effect_screen_effect_block_flag_bits_ho_debug_disable_bit = 0,
    _area_screen_effect_screen_effect_block_flag_bits_ho_allow_effect_outside_radius_bit = 1,
    _area_screen_effect_screen_effect_block_flag_bits_ho_first_person_only_bit = 2,
    _area_screen_effect_screen_effect_block_flag_bits_ho_third_person_only_bit = 3,
    _area_screen_effect_screen_effect_block_flag_bits_ho_disable_cinematic_camera_falloffs_bit = 4,
    _area_screen_effect_screen_effect_block_flag_bits_ho_only_affects_attached_object_bit = 5,
    _area_screen_effect_screen_effect_block_flag_bits_ho_draw_precisely_one_bit = 6,
    _area_screen_effect_screen_effect_block_flag_bits_ho_use_player_travel_direction_bit = 7,
    _area_screen_effect_screen_effect_block_flag_bits_ho_bit8_bit = 8,
    _area_screen_effect_screen_effect_block_flag_bits_ho_use_name_as_string_id_input_bit = 9,
    _area_screen_effect_screen_effect_block_flag_bits_ho_invert_string_id_input_bit = 10
};

enum e_area_screen_effect_hidden_flag_bits
{
    _area_screen_effect_hidden_flag_bits_update_thread_bit = 0,
    _area_screen_effect_hidden_flag_bits_render_thread_bit = 1
};


/* ---------- structures */

struct s_tag_function;
struct s_area_screen_effect_screen_effect_block;
struct s_area_screen_effect;

struct s_area_screen_effect
{
    c_tag_block<s_area_screen_effect_screen_effect_block> screen_effects;
};
static_assert(sizeof(s_area_screen_effect) == 0xC);

struct s_area_screen_effect_screen_effect_block
{
    string_id name;
    c_flags<e_area_screen_effect_screen_effect_block_flag_bits_ho, ushort> flags_ho;
    c_flags<e_area_screen_effect_hidden_flag_bits, ushort> hidden_flags;
    float maximum_distance;
    s_tag_function distance_falloff_function;
    float duration;
    s_tag_function time_evolution_function;
    s_tag_function angle_falloff_function;
    float light_intensity;
    float primary_hue;
    float secondary_hue;
    float saturation;
    float desaturation;
    float gamma_increase;
    float gamma_decrease;
    float shadow_brightness;
    real_rgb_color color_filter;
    real_rgb_color color_floor;
    float vision;
    float screen_shake;
    s_tag_reference screen_shader;
};
static_assert(sizeof(s_area_screen_effect_screen_effect_block) == 0x9C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

