/* ---------- enums */


/* ---------- structures */

struct s_gui_definition;
struct s_gui_list_widget_definition_list_widget_item;
struct s_gui_list_widget_definition;

struct s_gui_list_widget_definition
{
    ulong flags;
    s_gui_definition gui_render_block;
    string_id data_source_name;
    s_tag_reference skin;
    long row_count;
    c_tag_block<s_gui_list_widget_definition_list_widget_item> list_widget_items;
    s_tag_reference up_arrow_bitmap;
    s_tag_reference down_arrow_bitmap;
};
static_assert(sizeof(s_gui_list_widget_definition) == 0x70);

struct s_gui_list_widget_definition_list_widget_item
{
    ulong flags;
    s_gui_definition gui_render_block;
    string_id target;
};
static_assert(sizeof(s_gui_list_widget_definition_list_widget_item) == 0x30);

struct s_gui_definition
{
    string_id name;
    short unknown;
    short layer;
    short widescreen_y_min;
    short widescreen_x_min;
    short widescreen_y_max;
    short widescreen_x_max;
    short standard_y_min;
    short standard_x_min;
    short standard_y_max;
    short standard_x_max;
    s_tag_reference animation;
};
static_assert(sizeof(s_gui_definition) == 0x28);

