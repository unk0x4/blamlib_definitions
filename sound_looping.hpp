/* ---------- enums */

enum e_sound_looping_sound_looping_flags
{
    _sound_looping_sound_looping_flags_deafening_to_a_is_bit = 0,
    _sound_looping_sound_looping_flags_not_a_loop_bit = 1,
    _sound_looping_sound_looping_flags_stops_music_bit = 2,
    _sound_looping_sound_looping_flags_always_spatialize_bit = 3,
    _sound_looping_sound_looping_flags_synchronize_playback_bit = 4,
    _sound_looping_sound_looping_flags_synchronize_tracks_bit = 5,
    _sound_looping_sound_looping_flags_fake_spatialization_with_distance_bit = 6,
    _sound_looping_sound_looping_flags_combine_all3_d_playback_bit = 7,
    _sound_looping_sound_looping_flags_persistent_flyby_bit = 8,
    _sound_looping_sound_looping_flags_dont_apply_random_spatialization_to_details_bit = 9,
    _sound_looping_sound_looping_flags_allow_marker_stitching_bit = 10,
    _sound_looping_sound_looping_flags_dont_delay_retries_bit = 11,
    _sound_looping_sound_looping_flags_use_vehicle_parent_for_playerness_bit = 12,
    _sound_looping_sound_looping_flags_implicit_speed_rptc_bit = 13,
    _sound_looping_sound_looping_flags_has_optional_player_sound_bit = 14,
    _sound_looping_sound_looping_flags_dont_occlude_bit = 15,
    _sound_looping_sound_looping_flags_negate_radius_based_focus_bit = 16
};

enum e_sound_looping_sound_class
{
    _sound_looping_sound_class_projectile_impact = 0,
    _sound_looping_sound_class_projectile_detonation = 1,
    _sound_looping_sound_class_projectile_flyby = 2,
    _sound_looping_sound_class_projectile_detonation_lod = 3,
    _sound_looping_sound_class_weapon_fire = 4,
    _sound_looping_sound_class_weapon_ready = 5,
    _sound_looping_sound_class_weapon_reload = 6,
    _sound_looping_sound_class_weapon_empty = 7,
    _sound_looping_sound_class_weapon_charge = 8,
    _sound_looping_sound_class_weapon_overheat = 9,
    _sound_looping_sound_class_weapon_idle = 10,
    _sound_looping_sound_class_weapon_melee = 11,
    _sound_looping_sound_class_weapon_animation = 12,
    _sound_looping_sound_class_object_impacts = 13,
    _sound_looping_sound_class_particle_impacts = 14,
    _sound_looping_sound_class_weapon_fire_lod = 15,
    _sound_looping_sound_class_weapon_fire_lod_far = 16,
    _sound_looping_sound_class_unused2_impacts = 17,
    _sound_looping_sound_class_unit_footsteps = 18,
    _sound_looping_sound_class_unit_dialog = 19,
    _sound_looping_sound_class_unit_animation = 20,
    _sound_looping_sound_class_unit_unused = 21,
    _sound_looping_sound_class_vehicle_collision = 22,
    _sound_looping_sound_class_vehicle_engine = 23,
    _sound_looping_sound_class_vehicle_animation = 24,
    _sound_looping_sound_class_vehicle_engine_lod = 25,
    _sound_looping_sound_class_device_door = 26,
    _sound_looping_sound_class_device_unused0 = 27,
    _sound_looping_sound_class_device_machinery = 28,
    _sound_looping_sound_class_device_stationary = 29,
    _sound_looping_sound_class_device_unused1 = 30,
    _sound_looping_sound_class_device_unused2 = 31,
    _sound_looping_sound_class_music = 32,
    _sound_looping_sound_class_ambient_nature = 33,
    _sound_looping_sound_class_ambient_machinery = 34,
    _sound_looping_sound_class_ambient_stationary = 35,
    _sound_looping_sound_class_huge_ass = 36,
    _sound_looping_sound_class_object_looping = 37,
    _sound_looping_sound_class_cinematic_music = 38,
    _sound_looping_sound_class_player_armor = 39,
    _sound_looping_sound_class_unknown_unused1 = 40,
    _sound_looping_sound_class_ambient_flock = 41,
    _sound_looping_sound_class_no_pad = 42,
    _sound_looping_sound_class_no_pad_stationary = 43,
    _sound_looping_sound_class_arg = 44,
    _sound_looping_sound_class_cortana_mission = 45,
    _sound_looping_sound_class_cortana_gravemind_channel = 46,
    _sound_looping_sound_class_mission_dialog = 47,
    _sound_looping_sound_class_cinematic_dialog = 48,
    _sound_looping_sound_class_scripted_cinematic_foley = 49,
    _sound_looping_sound_class_hud = 50,
    _sound_looping_sound_class_game_event = 51,
    _sound_looping_sound_class_ui = 52,
    _sound_looping_sound_class_test = 53,
    _sound_looping_sound_class_multilingual_test = 54,
    _sound_looping_sound_class_ambient_nature_details = 55,
    _sound_looping_sound_class_ambient_machinery_details = 56,
    _sound_looping_sound_class_inside_surround_tail = 57,
    _sound_looping_sound_class_outside_surround_tail = 58,
    _sound_looping_sound_class_vehicle_detonation = 59,
    _sound_looping_sound_class_ambient_detonation = 60,
    _sound_looping_sound_class_first_person_inside = 61,
    _sound_looping_sound_class_first_person_outside = 62,
    _sound_looping_sound_class_first_person_anywhere = 63,
    _sound_looping_sound_class_ui_pda = 64
};

enum e_sound_looping_track_sound_fade_mode
{
    _sound_looping_track_sound_fade_mode_none = 0,
    _sound_looping_track_sound_fade_mode_linear = 1,
    _sound_looping_track_sound_fade_mode_power = 2,
    _sound_looping_track_sound_fade_mode_inverse_power = 3,
    _sound_looping_track_sound_fade_mode_ease_in_out = 4
};

enum e_sound_looping_track_output_effect
{
    _sound_looping_track_output_effect_none = 0,
    _sound_looping_track_output_effect_output_front_speakers = 1,
    _sound_looping_track_output_effect_output_rear_speakers = 2,
    _sound_looping_track_output_effect_output_center_speakers = 3
};


/* ---------- structures */

struct s_sound_looping_track;
struct s_sound_looping_detail_sound;
struct s_sound_looping;

struct s_sound_looping
{
    c_flags<e_sound_looping_sound_looping_flags, long> flags;
    real_bounds marty_s_music_time;
    real_bounds distance_bounds;
    s_tag_reference unused;
    c_enum<e_sound_looping_sound_class, short> sound_class;
    short loop_type_ho;
    c_tag_block<s_sound_looping_track> tracks;
    c_tag_block<s_sound_looping_detail_sound> detail_sounds;
};
static_assert(sizeof(s_sound_looping) == 0x40);

struct s_sound_looping_detail_sound
{
    string_id name;
    s_tag_reference sound;
    real_bounds random_period_bounds;
    float unknown;
    ulong flags;
    real_bounds yaw_bounds;
    real_bounds pitch_bounds;
    real_bounds distance_bounds;
};
static_assert(sizeof(s_sound_looping_detail_sound) == 0x3C);

struct s_sound_looping_track
{
    string_id name;
    ulong flags;
    float gain;
    float fade_in_duration;
    c_enum<e_sound_looping_track_sound_fade_mode, short> fade_in_mode;
    byte padding1[2];
    float fade_out_duration;
    c_enum<e_sound_looping_track_sound_fade_mode, short> fade_out_mode;
    byte padding2[2];
    s_tag_reference in;
    s_tag_reference loop;
    s_tag_reference out;
    s_tag_reference alternate_loop;
    s_tag_reference alternate_out;
    c_enum<e_sound_looping_track_output_effect, short> output_effect;
    short unknown4;
    s_tag_reference alternate_transition_in;
    s_tag_reference alternate_transition_out;
    float alternate_crossfade_duration;
    c_enum<e_sound_looping_track_sound_fade_mode, short> alternate_crossfade_mode;
    byte padding3[2];
    float alternate_fade_out_duration;
    c_enum<e_sound_looping_track_sound_fade_mode, short> alternate_fade_out_mode;
    byte padding4[2];
};
static_assert(sizeof(s_sound_looping_track) == 0xA0);

