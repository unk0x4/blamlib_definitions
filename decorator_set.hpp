/* ---------- enums */

enum e_decorator_set_decorator_flags
{
    _decorator_set_decorator_flags_bit1_bit = 0,
    _decorator_set_decorator_flags_enabled_via_preferences_bit = 1
};

enum e_decorator_set_decorator_shader
{
    _decorator_set_decorator_shader_default = 0,
    _decorator_set_decorator_shader_no_wind = 1,
    _decorator_set_decorator_shader_static = 2,
    _decorator_set_decorator_shader_sun = 3,
    _decorator_set_decorator_shader_wavy = 4,
    _decorator_set_decorator_shader_shaded = 5
};

enum e_decorator_set_light_sampling_pattern
{
    _decorator_set_light_sampling_pattern_default = 0,
    _decorator_set_light_sampling_pattern_hanging = 1
};


/* ---------- structures */

struct s_decorator_set_decorator_lod_transition;
struct s_decorator_set;

struct s_decorator_set
{
    s_tag_reference model;
    c_tag_block<string_id> valid_render_model_instance_names;
    long valid_render_model_instances;
    s_tag_reference texture;
    c_flags<e_decorator_set_decorator_flags, uchar> render_flags;
    c_enum<e_decorator_set_decorator_shader, uchar> render_shader;
    c_enum<e_decorator_set_light_sampling_pattern, uchar> light_sampling_pattern;
    byte unused1[1];
    real_rgb_color translucency_abc;
    float material_translucency;
    real_bounds wavelength_xy;
    float wave_speed;
    float wave_frequency;
    float darkness;
    float brightness;
    byte unused2[8];
    s_decorator_set_decorator_lod_transition lod_settings;
    byte unused3[12];
};
static_assert(sizeof(s_decorator_set) == 0x80);

struct s_decorator_set_decorator_lod_transition
{
    float start_distance;
    float end_distance;
    float scale;
    float offset;
};
static_assert(sizeof(s_decorator_set_decorator_lod_transition) == 0x10);

