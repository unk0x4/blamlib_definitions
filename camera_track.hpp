/* ---------- enums */

enum e_camera_track_flags
{
    _camera_track_flags_lens_enabled_bit = 0
};


/* ---------- structures */

struct s_camera_track_camera_point;
struct s_camera_track;

struct s_camera_track
{
    c_flags<e_camera_track_flags, uchar> flags;
    byte unused1[3];
    c_tag_block<s_camera_track_camera_point> camera_points;
    byte unused2[4];
};
static_assert(sizeof(s_camera_track) == 0x14);

struct s_camera_track_camera_point
{
    real_vector3d position;
    real_quaternion orientation;
};
static_assert(sizeof(s_camera_track_camera_point) == 0x1C);

