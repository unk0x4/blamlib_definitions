/* ---------- enums */


/* ---------- structures */

struct s_v_file_info;
struct s_v_files_list;

struct s_v_files_list
{
    c_tag_block<s_v_file_info> files;
    s_tag_data data;
};
static_assert(sizeof(s_v_files_list) == 0x20);

struct s_v_file_info
{
    char name[256];
    char folder[256];
    long offset;
    long size;
};
static_assert(sizeof(s_v_file_info) == 0x208);

