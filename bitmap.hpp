/* ---------- enums */

enum e_bitmap_runtime_flags
{
    _bitmap_runtime_flags_bitmap_is_tiled_bit = 0,
    _bitmap_runtime_flags_use_less_blurry_bump_map_bit = 1,
    _bitmap_runtime_flags_dither_when_compressing_bit = 2,
    _bitmap_runtime_flags_generate_random_sprites_bit = 3,
    _bitmap_runtime_flags_using_tag_interop_and_tag_resource_bit = 4,
    _bitmap_runtime_flags_ignore_alpha_channel_bit = 5,
    _bitmap_runtime_flags_alpha_channel_stores_transparency_bit = 6,
    _bitmap_runtime_flags_preserve_alpha_channel_in_mipmaps_for_alpha_test_bit = 7,
    _bitmap_runtime_flags_only_use_on_demand_bit = 8,
    _bitmap_runtime_flags_generate_tight_bounds_bit = 9,
    _bitmap_runtime_flags_tight_bounds_from_alpha_channel_bit = 10,
    _bitmap_runtime_flags_do_not_generate_required_section_bit = 11,
    _bitmap_runtime_flags_apply_max_resolution_after_slicing_bit = 12,
    _bitmap_runtime_flags_generate_black_point_tight_bounds_bit = 13,
    _bitmap_runtime_flags_pre_filter_cubemaps_bit = 14
};

enum e_bitmap_curve_mode
{
    _bitmap_curve_mode_choose_best = 0,
    _bitmap_curve_mode_force_fast = 1,
    _bitmap_curve_mode_force_pretty = 2
};

enum e_bitmap_flags_xbox
{
    _bitmap_flags_xbox_medium_resolution_offset_is_valid_bit = 0,
    _bitmap_flags_xbox_xbox360_memory_spacing_bit = 1,
    _bitmap_flags_xbox_xbox360_byte_order_bit = 2,
    _bitmap_flags_xbox_tiled_texture_bit = 3,
    _bitmap_flags_xbox_created_correctly_bit = 4,
    _bitmap_flags_xbox_unknown_mip_flag_bit = 5,
    _bitmap_flags_xbox_use_interleaved_textures_bit = 6,
    _bitmap_flags_xbox_use_on_demand_only_bit = 7
};

enum e_bitmap_type
{
    _bitmap_type_texture2_d = 0,
    _bitmap_type_texture3_d = 1,
    _bitmap_type_cube_map = 2,
    _bitmap_type_array = 3
};

enum e_bitmap_format
{
    _bitmap_format_a_8 = 0,
    _bitmap_format_y_8 = 1,
    _bitmap_format_a_y_8 = 2,
    _bitmap_format_a_8_y_8 = 3,
    _bitmap_format_unused4 = 4,
    _bitmap_format_unused5 = 5,
    _bitmap_format_r_5_g_6_b_5 = 6,
    _bitmap_format_r_6_g_5_b_5 = 7,
    _bitmap_format_a_1_r_5_g_5_b_5 = 8,
    _bitmap_format_a_4_r_4_g_4_b_4 = 9,
    _bitmap_format_x_8_r_8_g_8_b_8 = 10,
    _bitmap_format_a_8_r_8_g_8_b_8 = 11,
    _bitmap_format_unused_c = 12,
    _bitmap_format_unused_d = 13,
    _bitmap_format_dxt1 = 14,
    _bitmap_format_dxt3 = 15,
    _bitmap_format_dxt5 = 16,
    _bitmap_format_a_4_r_4_g_4_b_4_font = 17,
    _bitmap_format_p_8 = 18,
    _bitmap_format_a_r_g_b_f_p_32 = 19,
    _bitmap_format_r_g_b_f_p_32 = 20,
    _bitmap_format_r_g_b_f_p_16 = 21,
    _bitmap_format_v_8_u_8 = 22,
    _bitmap_format_g_8_b_8 = 23,
    _bitmap_format_a_32_b_32_g_32_r_32_f = 24,
    _bitmap_format_a_16_b_16_g_16_r_16_f = 25,
    _bitmap_format_q_8_w_8_v_8_u_8 = 26,
    _bitmap_format_a_2_r_10_g_10_b_10 = 27,
    _bitmap_format_a_8_r_8_g_8_b_8_reach = 28,
    _bitmap_format_v_16_u_16 = 29,
    _bitmap_format_unused1_e = 30,
    _bitmap_format_dxt5_a = 31,
    _bitmap_format_y_16 = 32,
    _bitmap_format_dxn = 33,
    _bitmap_format_ctx1 = 34,
    _bitmap_format_dxt3_a_alpha = 35,
    _bitmap_format_dxt3_a_mono = 36,
    _bitmap_format_dxt5_a_alpha = 37,
    _bitmap_format_dxt5_a_mono = 38,
    _bitmap_format_dxn_mono_alpha = 39,
    _bitmap_format_reach_dxt3_a_mono = 40,
    _bitmap_format_reach_dxt3_a_alpha = 41,
    _bitmap_format_reach_dxt5_a_mono = 42,
    _bitmap_format_reach_dxt5_a_alpha = 43,
    _bitmap_format_reach_dxn_mono_alpha = 44
};

enum e_bitmap_flags
{
    _bitmap_flags_power_of_two_dimensions_bit = 0,
    _bitmap_flags_compressed_bit = 1,
    _bitmap_flags_unknown2_bit = 2,
    _bitmap_flags_unknown3_bit = 3,
    _bitmap_flags_unknown4_bit = 4,
    _bitmap_flags_unknown5_bit = 5,
    _bitmap_flags_unknown6_bit = 6,
    _bitmap_flags_prefer_low_detail_bit = 7,
    _bitmap_flags_unknown8_bit = 8,
    _bitmap_flags_unknown9_bit = 9,
    _bitmap_flags_unknown10_bit = 10,
    _bitmap_flags_unknown11_bit = 11,
    _bitmap_flags_unknown12_bit = 12,
    _bitmap_flags_unknown13_bit = 13,
    _bitmap_flags_unknown14_bit = 14,
    _bitmap_flags_unknown15_bit = 15
};

enum e_bitmap_image_curve
{
    _bitmap_image_curve_unknown = 0,
    _bitmap_image_curve_x_rgb = 1,
    _bitmap_image_curve_gamma2 = 2,
    _bitmap_image_curve_linear = 3,
    _bitmap_image_curve_offset_log = 4,
    _bitmap_image_curve_s_rgb = 5,
    _bitmap_image_curve_rec709 = 6
};

enum e_old_raw_page_flags
{
    _old_raw_page_flags_use_checksum_bit = 0,
    _old_raw_page_flags_in_resources_bit = 1,
    _old_raw_page_flags_in_textures_bit = 2,
    _old_raw_page_flags_in_textures_b_bit = 3,
    _old_raw_page_flags_in_audio_bit = 4,
    _old_raw_page_flags_in_resources_b_bit = 5,
    _old_raw_page_flags_in_mods_bit = 6,
    _old_raw_page_flags_use_checksum2_bit = 7,
    _old_raw_page_flags_location_mask_bit = 1
};

enum e_tag_resource_type_gen3
{
    _tag_resource_type_gen3_none = -1,
    _tag_resource_type_gen3_collision = 0,
    _tag_resource_type_gen3_bitmap = 1,
    _tag_resource_type_gen3_bitmap_interleaved = 2,
    _tag_resource_type_gen3_sound = 3,
    _tag_resource_type_gen3_animation = 4,
    _tag_resource_type_gen3_render_geometry = 5,
    _tag_resource_type_gen3_bink = 6,
    _tag_resource_type_gen3_pathfinding = 7
};


/* ---------- structures */

struct s_bitmap_tight_binding;
struct s_bitmap_usage_override;
struct s_bitmap_sequence_sprite;
struct s_bitmap_sequence;
struct s_bitmap_image;
struct s_resource_page;
struct s_resource_fixup_location;
struct s_resource_interop_location;
struct s_resource_data;
struct s_pageable_resource;
struct s_tag_resource_reference;
struct s_bitmap;

struct s_bitmap
{
    long usage;
    c_flags<e_bitmap_runtime_flags, short> flags;
    short sprite_spacing;
    float bump_map_height;
    float fade_factor;
    c_enum<e_bitmap_curve_mode, char> bitmap_curve_mode;
    uchar max_mip_map_level;
    short max_resolution;
    short atlas_index;
    short force_bitmap_format;
    c_tag_block<s_bitmap_tight_binding> tight_bounds_old;
    c_tag_block<s_bitmap_usage_override> usage_overrides;
    c_tag_block<s_bitmap_sequence> manual_sequences;
    s_tag_data source_data;
    s_tag_data processed_pixel_data;
    c_tag_block<s_bitmap_sequence> sequences;
    c_tag_block<s_bitmap_image> images;
    s_tag_data xenon_processed_pixel_data;
    c_tag_block<s_bitmap_image> xenon_images;
    c_tag_block<s_tag_resource_reference> resources;
    c_tag_block<s_tag_resource_reference> interleaved_resources;
    long unknown_b_4;
};
static_assert(sizeof(s_bitmap) == 0xB8);

struct s_tag_resource_reference
{
    s_pageable_resource pageable_resource;
    long unused;
};
static_assert(sizeof(s_tag_resource_reference) == 0x8);

struct s_pageable_resource
{
    s_resource_page page;
    s_resource_data resource;
};
static_assert(sizeof(s_pageable_resource) == 0x6C);

struct s_resource_data
{
    s_tag_reference parent_tag;
    ushort salt;
    c_enum<e_tag_resource_type_gen3, char> resource_type;
    uchar flags;
    s_tag_data definition_data;
    cache_address definition_address;
    c_tag_block<s_resource_fixup_location> fixup_locations;
    c_tag_block<s_resource_interop_location> interop_locations;
    long unknown2;
};
static_assert(sizeof(s_resource_data) == 0x48);

struct s_resource_interop_location
{
    cache_address address;
    long resource_structure_type_index;
};
static_assert(sizeof(s_resource_interop_location) == 0x8);

struct s_resource_fixup_location
{
    ulong block_offset;
    cache_address address;
    long type;
    long offset;
    long raw_address;
};
static_assert(sizeof(s_resource_fixup_location) == 0x8);

struct s_resource_page
{
    short salt;
    c_flags<e_old_raw_page_flags, uchar> old_flags;
    char compression_codec_index;
    long index;
    ulong compressed_block_size;
    ulong uncompressed_block_size;
    long crc_checksum;
    ulong unknown_size;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_resource_page) == 0x24);

struct s_bitmap_image
{
    tag signature;
    short width;
    short height;
    char depth;
    c_flags<e_bitmap_flags_xbox, uchar> xbox_flags;
    c_enum<e_bitmap_type, uchar> type;
    uchar unknown_flags;
    c_enum<e_bitmap_format, char> format;
    byte unused2_2[1];
    c_flags<e_bitmap_flags, ushort> flags;
    point2d registration_point;
    char mipmap_count;
    c_enum<e_bitmap_image_curve, char> curve;
    uchar interleaved_texture_index1;
    uchar interleaved_texture_index2;
    long data_offset;
    long data_size;
    long unknown20;
    long mip_map_offset;
    long rasterizer_texture_reference;
    long runtime_address;
};
static_assert(sizeof(s_bitmap_image) == 0x30);

struct s_bitmap_sequence
{
    char name[32];
    short first_bitmap_index;
    short bitmap_count;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    c_tag_block<s_bitmap_sequence_sprite> sprites;
};
static_assert(sizeof(s_bitmap_sequence) == 0x40);

struct s_bitmap_sequence_sprite
{
    short bitmap_index;
    short unknown;
    ulong unknown2;
    float left;
    float right;
    float top;
    float bottom;
    real_point2d registration_point;
};
static_assert(sizeof(s_bitmap_sequence_sprite) == 0x20);

struct s_bitmap_usage_override
{
    float source_gamma;
    long bitmap_curve_enum;
    uchar flags;
    uchar unknown9;
    uchar unknown_a;
    uchar unknown_b;
    uchar unknown_c;
    uchar unknown_d;
    short downsampling_flags;
    short unknown10;
    short unknown12;
    long unknown14;
    long unknown18;
    long unknown1_c;
    uchar swizzle_red_enum;
    uchar swizzle_blue_enum;
    uchar swizzle_green_enum;
    uchar swizzle_alpha_enum;
    long format_enum;
};
static_assert(sizeof(s_bitmap_usage_override) == 0x28);

struct s_bitmap_tight_binding
{
    real_point2d uv;
};
static_assert(sizeof(s_bitmap_tight_binding) == 0x8);

