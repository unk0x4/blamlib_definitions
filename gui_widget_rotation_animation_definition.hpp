/* ---------- enums */

enum e_gui_widget_rotation_animation_definition_animation_definition_block_anchor
{
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_custom = 0,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_center = 1,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_top_center = 2,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_bottom_center = 3,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_left_center = 4,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_right_center = 5,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_top_left = 6,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_top_right = 7,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_bottom_right = 8,
    _gui_widget_rotation_animation_definition_animation_definition_block_anchor_bottom_left = 9
};


/* ---------- structures */

struct s_gui_widget_rotation_animation_definition_animation_definition_block;
struct s_tag_function;
struct s_gui_widget_rotation_animation_definition;

struct s_gui_widget_rotation_animation_definition
{
    ulong animation_flags;
    c_tag_block<s_gui_widget_rotation_animation_definition_animation_definition_block> animation_definition;
    s_tag_function function;
    ulong unknown;
    ulong unknown2;
};
static_assert(sizeof(s_gui_widget_rotation_animation_definition) == 0x2C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_gui_widget_rotation_animation_definition_animation_definition_block
{
    ulong frame;
    c_enum<e_gui_widget_rotation_animation_definition_animation_definition_block_anchor, short> anchor;
    short unknown;
    float custom_anchor_x;
    float custom_anchor_y;
    float rotation_amount;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_gui_widget_rotation_animation_definition_animation_definition_block) == 0x20);

