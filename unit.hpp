/* ---------- enums */

enum e_unit_unit_flag_bits
{
    _unit_unit_flag_bits_circular_aiming_bit = 0,
    _unit_unit_flag_bits_destroyed_after_dying_bit = 1,
    _unit_unit_flag_bits_half_speed_interpolation_bit = 2,
    _unit_unit_flag_bits_fires_from_camera_bit = 3,
    _unit_unit_flag_bits_entrance_inside_bounding_sphere_bit = 4,
    _unit_unit_flag_bits_doesnt_show_readied_weapon_bit = 5,
    _unit_unit_flag_bits_causes_passenger_dialogue_bit = 6,
    _unit_unit_flag_bits_resists_pings_bit = 7,
    _unit_unit_flag_bits_melee_attack_is_fatal_bit = 8,
    _unit_unit_flag_bits_dont_reface_during_pings_bit = 9,
    _unit_unit_flag_bits_has_no_aiming_bit = 10,
    _unit_unit_flag_bits_simple_creature_bit = 11,
    _unit_unit_flag_bits_impact_melee_attaches_to_unit_bit = 12,
    _unit_unit_flag_bits_impact_melee_dies_on_shield_bit = 13,
    _unit_unit_flag_bits_cannot_open_doors_automatically_bit = 14,
    _unit_unit_flag_bits_melee_attackers_cannot_attach_bit = 15,
    _unit_unit_flag_bits_not_instantly_killed_by_melee_bit = 16,
    _unit_unit_flag_bits_shield_sapping_bit = 17,
    _unit_unit_flag_bits_runs_around_flaming_bit = 18,
    _unit_unit_flag_bits_inconsequential_bit = 19,
    _unit_unit_flag_bits_special_cinematic_unit_bit = 20,
    _unit_unit_flag_bits_ignored_by_autoaiming_bit = 21,
    _unit_unit_flag_bits_shields_fry_infection_forms_bit = 22,
    _unit_unit_flag_bits_can_dual_wield_bit = 23,
    _unit_unit_flag_bits_acts_as_gunner_for_parent_bit = 24,
    _unit_unit_flag_bits_controlled_by_parent_gunner_bit = 25,
    _unit_unit_flag_bits_parents_primary_weapon_bit = 26,
    _unit_unit_flag_bits_parents_secondary_weapon_bit = 27,
    _unit_unit_flag_bits_unit_has_boost_bit = 28
};

enum e_unit_default_team
{
    _unit_default_team_default = 0,
    _unit_default_team_player = 1,
    _unit_default_team_human = 2,
    _unit_default_team_covenant = 3,
    _unit_default_team_flood = 4,
    _unit_default_team_sentinel = 5,
    _unit_default_team_heretic = 6,
    _unit_default_team_prophet = 7,
    _unit_default_team_guilty = 8,
    _unit_default_team_unused9 = 9,
    _unit_default_team_unused10 = 10,
    _unit_default_team_unused11 = 11,
    _unit_default_team_unused12 = 12,
    _unit_default_team_unused13 = 13,
    _unit_default_team_unused14 = 14,
    _unit_default_team_unused15 = 15
};

enum e_unit_constant_sound_volume
{
    _unit_constant_sound_volume_silent = 0,
    _unit_constant_sound_volume_medium = 1,
    _unit_constant_sound_volume_loud = 2,
    _unit_constant_sound_volume_shout = 3,
    _unit_constant_sound_volume_quiet = 4
};

enum e_unit_metagame_property_unit_type
{
    _unit_metagame_property_unit_type_brute = 0,
    _unit_metagame_property_unit_type_grunt = 1,
    _unit_metagame_property_unit_type_jackal = 2,
    _unit_metagame_property_unit_type_marine = 3,
    _unit_metagame_property_unit_type_bugger = 4,
    _unit_metagame_property_unit_type_hunter = 5,
    _unit_metagame_property_unit_type_flood_infection = 6,
    _unit_metagame_property_unit_type_flood_carrier = 7,
    _unit_metagame_property_unit_type_flood_combat = 8,
    _unit_metagame_property_unit_type_flood_pureform = 9,
    _unit_metagame_property_unit_type_sentinel = 10,
    _unit_metagame_property_unit_type_elite = 11,
    _unit_metagame_property_unit_type_turret = 12,
    _unit_metagame_property_unit_type_mongoose = 13,
    _unit_metagame_property_unit_type_warthog = 14,
    _unit_metagame_property_unit_type_scorpion = 15,
    _unit_metagame_property_unit_type_hornet = 16,
    _unit_metagame_property_unit_type_pelican = 17,
    _unit_metagame_property_unit_type_shade = 18,
    _unit_metagame_property_unit_type_watchtower = 19,
    _unit_metagame_property_unit_type_ghost = 20,
    _unit_metagame_property_unit_type_chopper = 21,
    _unit_metagame_property_unit_type_mauler = 22,
    _unit_metagame_property_unit_type_wraith = 23,
    _unit_metagame_property_unit_type_banshee = 24,
    _unit_metagame_property_unit_type_phantom = 25,
    _unit_metagame_property_unit_type_scarab = 26,
    _unit_metagame_property_unit_type_guntower = 27,
    _unit_metagame_property_unit_type_engineer = 28,
    _unit_metagame_property_unit_type_engineer_recharge_station = 29
};

enum e_unit_metagame_property_unit_classification
{
    _unit_metagame_property_unit_classification_infantry = 0,
    _unit_metagame_property_unit_classification_leader = 1,
    _unit_metagame_property_unit_classification_hero = 2,
    _unit_metagame_property_unit_classification_specialist = 3,
    _unit_metagame_property_unit_classification_light_vehicle = 4,
    _unit_metagame_property_unit_classification_heavy_vehicle = 5,
    _unit_metagame_property_unit_classification_giant_vehicle = 6,
    _unit_metagame_property_unit_classification_standard_vehicle = 7
};

enum e_unit_unit_camera_flag_bits
{
    _unit_unit_camera_flag_bits_pitch_bounds_absolute_space_bit = 0,
    _unit_unit_camera_flag_bits_only_collides_with_environment_bit = 1,
    _unit_unit_camera_flag_bits_hides_player_unit_from_camera_bit = 2,
    _unit_unit_camera_flag_bits_use_aiming_vector_instead_of_marker_forward_bit = 3
};

enum e_unit_motion_sensor_blip_size
{
    _unit_motion_sensor_blip_size_medium = 0,
    _unit_motion_sensor_blip_size_small = 1,
    _unit_motion_sensor_blip_size_large = 2
};

enum e_unit_item_scale
{
    _unit_item_scale_small = 0,
    _unit_item_scale_medium = 1,
    _unit_item_scale_large = 2,
    _unit_item_scale_huge = 3
};

enum e_unit_grenade_type
{
    _unit_grenade_type_human_fragmentation = 0,
    _unit_grenade_type_covenant_plasma = 1,
    _unit_grenade_type_brute_spike = 2,
    _unit_grenade_type_firebomb = 3
};

enum e_unit_unit_seat_flag_bits
{
    _unit_unit_seat_flag_bits_invisible_bit = 0,
    _unit_unit_seat_flag_bits_locked_bit = 1,
    _unit_unit_seat_flag_bits_driver_bit = 2,
    _unit_unit_seat_flag_bits_gunner_bit = 3,
    _unit_unit_seat_flag_bits_third_person_camera_bit = 4,
    _unit_unit_seat_flag_bits_allows_weapons_bit = 5,
    _unit_unit_seat_flag_bits_third_person_on_enter_bit = 6,
    _unit_unit_seat_flag_bits_first_person_camera_slaved_to_gun_bit = 7,
    _unit_unit_seat_flag_bits_allow_vehicle_communication_animations_bit = 8,
    _unit_unit_seat_flag_bits_not_valid_without_driver_bit = 9,
    _unit_unit_seat_flag_bits_allow_ai_non_combatants_bit = 10,
    _unit_unit_seat_flag_bits_boarding_seat_bit = 11,
    _unit_unit_seat_flag_bits_ai_firing_disabled_by_max_acceleration_bit = 12,
    _unit_unit_seat_flag_bits_boarding_enters_seat_bit = 13,
    _unit_unit_seat_flag_bits_boarding_need_any_passenger_bit = 14,
    _unit_unit_seat_flag_bits_controls_open_and_close_bit = 15,
    _unit_unit_seat_flag_bits_invalid_for_player_bit = 16,
    _unit_unit_seat_flag_bits_invalid_for_non_player_bit = 17,
    _unit_unit_seat_flag_bits_gunner_player_only_bit = 18,
    _unit_unit_seat_flag_bits_invisible_under_major_damage_bit = 19,
    _unit_unit_seat_flag_bits_melee_instant_killable_bit = 20,
    _unit_unit_seat_flag_bits_leader_preference_bit = 21,
    _unit_unit_seat_flag_bits_allows_exit_and_detach_bit = 22
};

enum e_unit_unit_seat_ai_seat_type
{
    _unit_unit_seat_ai_seat_type_none = 0,
    _unit_unit_seat_ai_seat_type_passenger = 1,
    _unit_unit_seat_ai_seat_type_gunner = 2,
    _unit_unit_seat_ai_seat_type_small_cargo = 3,
    _unit_unit_seat_ai_seat_type_large_cargo = 4,
    _unit_unit_seat_ai_seat_type_driver = 5
};


/* ---------- structures */

struct s_unit_metagame_property;
struct s_unit_unit_camera_track;
struct s_unit_unit_camera_axis_acceleration;
struct s_unit_unit_camera_acceleration;
struct s_unit_unit_camera;
struct s_unit_unit_assassination;
struct s_unit_unit_seat_acceleration;
struct s_unit_posture;
struct s_unit_hud_interface;
struct s_unit_dialogue_variant;
struct s_unit_powered_seat;
struct s_unit_weapon;
struct s_unit_target_tracking_block_tracking_type;
struct s_unit_target_tracking_block;
struct s_unit_unit_seat_unit_hud_interface_block;
struct s_unit_unit_seat;
struct s_unit;

struct s_unit : s_game_object
{
    c_flags<e_unit_unit_flag_bits, long> unit_flags;
    c_enum<e_unit_default_team, short> default_team;
    c_enum<e_unit_constant_sound_volume, short> constant_sound_volume;
    s_tag_reference hologram_unit;
    c_tag_block<s_unit_metagame_property> metagame_properties;
    s_tag_reference integrated_light_toggle;
    real camera_field_of_view;
    float camera_stiffness;
    s_unit_unit_camera camera;
    s_unit_unit_camera sync_action_camera;
    s_unit_unit_assassination assassination;
    s_unit_unit_seat_acceleration seat_acceleration;
    float soft_ping_threshold;
    float soft_ping_interrupt_time;
    float hard_ping_threshold;
    float hard_ping_interrupt_time;
    float feign_death_threshold;
    float feign_death_time;
    float distance_of_evade_animation;
    float distance_of_dive_animation;
    float stunned_movement_threshold;
    float feign_death_chance;
    float feign_repeat_chance;
    s_tag_reference spawned_turret_character;
    short spawned_actor_count_min;
    short spawned_actor_count_max;
    float spawned_velocity;
    real aiming_velocity_maximum;
    real aiming_acceleration_maximum;
    float casual_aiming_modifier;
    real looking_velocity_maximum;
    real looking_acceleration_maximum;
    string_id right_hand_node;
    string_id left_hand_node;
    string_id preferred_gun_node;
    s_tag_reference melee_damage;
    s_tag_reference boarding_melee_damage;
    s_tag_reference boarding_melee_response;
    s_tag_reference ejection_melee_damage;
    s_tag_reference ejection_melee_response;
    s_tag_reference landing_melee_damage;
    s_tag_reference flurry_melee_damage;
    s_tag_reference obstacle_smash_melee_damage;
    s_tag_reference shield_pop_damage;
    s_tag_reference assassination_damage;
    c_enum<e_unit_motion_sensor_blip_size, short> motion_sensor_blip_size;
    c_enum<e_unit_item_scale, short> item_scale;
    c_tag_block<s_unit_posture> postures;
    c_tag_block<s_unit_hud_interface> hud_interfaces;
    c_tag_block<s_unit_dialogue_variant> dialogue_variants;
    float motion_tracker_range_modifier;
    real grenade_angle;
    real grenade_angle_max_elevation;
    real grenade_angle_min_elevation;
    float grenade_velocity;
    c_enum<e_unit_grenade_type, short> grenade_type;
    ushort grenade_count;
    c_tag_block<s_unit_powered_seat> powered_seats;
    c_tag_block<s_unit_weapon> weapons;
    c_tag_block<s_unit_target_tracking_block> target_tracking;
    c_tag_block<s_unit_unit_seat> seats;
    float emp_radius;
    s_tag_reference emp_effect;
    s_tag_reference boost_collision_damage;
    float boost_peak_power;
    float boost_rise_power;
    float boost_peak_time;
    float boost_fall_power;
    float boost_dead_time;
    float lipsync_attack_weight;
    float lipsync_decay_weight;
    s_tag_reference detach_damage;
    s_tag_reference detached_weapon;
};
static_assert(sizeof(s_unit) == 0x3E8);

struct s_unit_unit_seat
{
    c_flags<e_unit_unit_seat_flag_bits, long> flags;
    string_id seat_animation;
    string_id seat_marker_name;
    string_id entry_marker_name;
    string_id boarding_grenade_marker;
    string_id boarding_grenade_string;
    string_id boarding_melee_string;
    string_id detach_weapon_string;
    float ping_scale;
    float turnover_time;
    s_unit_unit_seat_acceleration seat_acceleration;
    float ai_scariness;
    c_enum<e_unit_unit_seat_ai_seat_type, short> ai_seat_type;
    short boarding_seat;
    float listener_interpolation_factor;
    real_bounds yaw_rate_bounds;
    real_bounds pitch_rate_bounds;
    float pitch_interpolation_time;
    real_bounds speed_reference_bounds;
    float speed_exponent;
    s_unit_unit_camera camera;
    c_tag_block<s_unit_unit_seat_unit_hud_interface_block> unit_hud_interface;
    string_id enter_seat_string;
    real_bounds yaw_range;
    s_tag_reference built_in_gunner;
    float entry_radius;
    real entry_marker_cone_angle;
    real entry_marker_facing_angle;
    float maximum_relative_velocity;
    string_id invisible_seat_region;
    long runtime_invisible_seat_region_index;
};
static_assert(sizeof(s_unit_unit_seat) == 0xE4);

struct s_unit_unit_seat_unit_hud_interface_block
{
    s_tag_reference unit_hud_interface;
};
static_assert(sizeof(s_unit_unit_seat_unit_hud_interface_block) == 0x10);

struct s_unit_target_tracking_block
{
    c_tag_block<s_unit_target_tracking_block_tracking_type> tracking_types;
    float acquire_time;
    float grace_time;
    float decay_time;
    s_tag_reference tracking_sound;
    s_tag_reference locked_sound;
};
static_assert(sizeof(s_unit_target_tracking_block) == 0x38);

struct s_unit_target_tracking_block_tracking_type
{
    string_id tracking_type2;
};
static_assert(sizeof(s_unit_target_tracking_block_tracking_type) == 0x4);

struct s_unit_weapon
{
    s_tag_reference weapon2;
};
static_assert(sizeof(s_unit_weapon) == 0x10);

struct s_unit_powered_seat
{
    float driver_powerup_time;
    float driver_powerdown_time;
};
static_assert(sizeof(s_unit_powered_seat) == 0x8);

struct s_unit_dialogue_variant
{
    short variant_number;
    short unknown;
    s_tag_reference dialogue;
};
static_assert(sizeof(s_unit_dialogue_variant) == 0x14);

struct s_unit_hud_interface
{
    s_tag_reference unit_hud_interface;
};
static_assert(sizeof(s_unit_hud_interface) == 0x10);

struct s_unit_posture
{
    string_id name;
    real_vector3d pill_offset;
};
static_assert(sizeof(s_unit_posture) == 0x10);

struct s_unit_unit_seat_acceleration
{
    real_vector3d range;
    float action_scale;
    float attach_scale;
};
static_assert(sizeof(s_unit_unit_seat_acceleration) == 0x14);

struct s_unit_unit_assassination
{
    s_tag_reference response;
    s_tag_reference weapon;
    string_id tool_stow_anchor;
    string_id tool_hand_marker;
    string_id tool_marker;
};
static_assert(sizeof(s_unit_unit_assassination) == 0x2C);

struct s_unit_unit_camera
{
    c_flags<e_unit_unit_camera_flag_bits, ushort> camera_flags;
    byte unused[2];
    string_id camera_marker_name;
    string_id camera_submerged_marker_name;
    real pitch_auto_level;
    real_bounds pitch_range;
    c_tag_block<s_unit_unit_camera_track> camera_tracks;
    real_bounds pitch_spring_bounds;
    real spring_velocity;
    c_tag_block<s_unit_unit_camera_acceleration> camera_acceleration;
};
static_assert(sizeof(s_unit_unit_camera) == 0x3C);

struct s_unit_unit_camera_acceleration
{
    float maximum_camera_velocity;
    unit_camera_axis_acceleration axes_acceleration[3];
};
static_assert(sizeof(s_unit_unit_camera_acceleration) == 0x4C);

struct s_unit_unit_camera_axis_acceleration
{
    float unknown1;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
};
static_assert(sizeof(s_unit_unit_camera_axis_acceleration) == 0x18);

struct s_unit_unit_camera_track
{
    s_tag_reference camera_track;
};
static_assert(sizeof(s_unit_unit_camera_track) == 0x10);

struct s_unit_metagame_property
{
    uchar flags;
    c_enum<e_unit_metagame_property_unit_type, char> unit;
    c_enum<e_unit_metagame_property_unit_classification, char> classification;
    char unknown;
    short points;
    short unknown2;
};
static_assert(sizeof(s_unit_metagame_property) == 0x8);

