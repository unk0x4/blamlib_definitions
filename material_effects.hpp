/* ---------- enums */

enum e_material_effects_effect_effect_reference_sweetener_mode
{
    _material_effects_effect_effect_reference_sweetener_mode_sweetener_default = 0,
    _material_effects_effect_effect_reference_sweetener_mode_sweetener_enabled = 1,
    _material_effects_effect_effect_reference_sweetener_mode_sweetener_disabled = 2
};


/* ---------- structures */

struct s_material_effects_effect_effect_reference;
struct s_material_effects_effect;
struct s_material_effects;

struct s_material_effects
{
    c_tag_block<s_material_effects_effect> effects;
};
static_assert(sizeof(s_material_effects) == 0xC);

struct s_material_effects_effect
{
    c_tag_block<s_material_effects_effect_effect_reference> old_materials;
    c_tag_block<s_material_effects_effect_effect_reference> sounds;
    c_tag_block<s_material_effects_effect_effect_reference> effects;
};
static_assert(sizeof(s_material_effects_effect) == 0x24);

struct s_material_effects_effect_effect_reference
{
    s_tag_reference effect;
    s_tag_reference sound;
    string_id material_name;
    short global_material_index;
    c_enum<e_material_effects_effect_effect_reference_sweetener_mode, char> sweetener_mode;
    byte unused[1];
    float max_visibility_distance;
};
static_assert(sizeof(s_material_effects_effect_effect_reference) == 0x2C);

