/* ---------- enums */

enum e_cloth_flags
{
    _cloth_flags_does_not_use_wind_bit = 0,
    _cloth_flags_uses_grid_attach_top_bit = 1
};

enum e_cloth_integration_type
{
    _cloth_integration_type_verlet = 0
};


/* ---------- structures */

struct s_cloth_collision_sphere;
struct s_cloth_properties;
struct s_cloth_vertex;
struct s_cloth_link;
struct s_cloth;

struct s_cloth
{
    c_flags<e_cloth_flags, long> flags;
    string_id marker_attachment_name;
    string_id second_marker_attachment_name;
    s_tag_reference shader;
    short grid_x_dimension;
    short grid_y_dimension;
    float grid_spacing_x;
    float grid_spacing_y;
    c_tag_block<s_cloth_collision_sphere> collision_spheres;
    s_cloth_properties properties;
    c_tag_block<s_cloth_vertex> vertices;
    c_tag_block<short> indices;
    c_tag_block<short> strip_indices;
    c_tag_block<s_cloth_link> links;
};
static_assert(sizeof(s_cloth) == 0x94);

struct s_cloth_link
{
    short index1;
    short index2;
    float default_distance;
};
static_assert(sizeof(s_cloth_link) == 0x8);

struct s_cloth_vertex
{
    real_point3d position;
    real_vector2d uv;
};
static_assert(sizeof(s_cloth_vertex) == 0x14);

struct s_cloth_properties
{
    c_enum<e_cloth_integration_type, short> integration_type;
    short number_iterations;
    float weight;
    float drag;
    float wind_scale;
    float wind_flappiness_scale;
    float longest_rod;
    byte unused[24];
};
static_assert(sizeof(s_cloth_properties) == 0x30);

struct s_cloth_collision_sphere
{
    string_id object_marker_name;
    float radius;
};
static_assert(sizeof(s_cloth_collision_sphere) == 0x8);

