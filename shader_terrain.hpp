/* ---------- enums */


/* ---------- structures */

struct s_shader_terrain;

struct s_shader_terrain : s_render_method
{
    string_id material_names[4];
    int16 global_material_indices[4];
    ulong unknown8;
};
static_assert(sizeof(s_shader_terrain) == 0x5C);

