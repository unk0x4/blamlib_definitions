/* ---------- enums */

enum e_sound_classes_class_internal_flag_bits
{
    _sound_classes_class_internal_flag_bits_class_is_valid_bit = 0,
    _sound_classes_class_internal_flag_bits_class_is_speech_bit = 1,
    _sound_classes_class_internal_flag_bits_class_is_scripted_bit = 2,
    _sound_classes_class_internal_flag_bits_mutes_with_object_bit = 3,
    _sound_classes_class_internal_flag_bits_bit4_bit = 4,
    _sound_classes_class_internal_flag_bits_bit5_bit = 5,
    _sound_classes_class_internal_flag_bits_bit6_bit = 6,
    _sound_classes_class_internal_flag_bits_multilingual_bit = 7,
    _sound_classes_class_internal_flag_bits_bit8_bit = 8,
    _sound_classes_class_internal_flag_bits_bit9_bit = 9,
    _sound_classes_class_internal_flag_bits_bit10_bit = 10,
    _sound_classes_class_internal_flag_bits_stops_with_dead_object_bit = 11,
    _sound_classes_class_internal_flag_bits_bit12_bit = 12,
    _sound_classes_class_internal_flag_bits_bit13_bit = 13,
    _sound_classes_class_internal_flag_bits_bit14_bit = 14,
    _sound_classes_class_internal_flag_bits_bit15_bit = 15
};

enum e_sound_classes_class_class_flag_bits
{
    _sound_classes_class_class_flag_bits_can_play_during_pause_bit = 0,
    _sound_classes_class_class_flag_bits_dry_stereo_mix_bit = 1,
    _sound_classes_class_class_flag_bits_plays_through_objects_bit = 2,
    _sound_classes_class_class_flag_bits_is_center_unspatialized_bit = 3,
    _sound_classes_class_class_flag_bits_mono_lfe_bit = 4,
    _sound_classes_class_class_flag_bits_deterministic_bit = 5,
    _sound_classes_class_class_flag_bits_use_huge_transmission_bit = 6,
    _sound_classes_class_class_flag_bits_always_use_speakers_bit = 7,
    _sound_classes_class_class_flag_bits_is_available_on_mainmenu_bit = 8,
    _sound_classes_class_class_flag_bits_ignore_stereo_headroom_bit = 9,
    _sound_classes_class_class_flag_bits_loop_fade_out_is_linear_bit = 10,
    _sound_classes_class_class_flag_bits_stop_when_object_dies_bit = 11,
    _sound_classes_class_class_flag_bits_dont_fade_on_game_over_bit = 12,
    _sound_classes_class_class_flag_bits_dont_promote_priority_by_proximity_bit = 13,
    _sound_classes_class_class_flag_bits_class_plays_on_mainmenu_bit = 14,
    _sound_classes_class_class_flag_bits_bit15_bit = 15
};

enum e_sound_classes_class_cache_miss_mode_odst
{
    _sound_classes_class_cache_miss_mode_odst_discard = 0,
    _sound_classes_class_cache_miss_mode_odst_postpone = 1
};

enum e_sound_classes_class_accoustics_flags
{
    _sound_classes_class_accoustics_flags_outside_bit = 0,
    _sound_classes_class_accoustics_flags_inside_bit = 1
};

enum e_sound_classes_class_supress_spatialization_flags
{
    _sound_classes_class_supress_spatialization_flags_first_person_bit = 0,
    _sound_classes_class_supress_spatialization_flags_third_person_bit = 1
};

enum e_sound_classes_class_stereo_playback_type
{
    _sound_classes_class_stereo_playback_type_first_person = 0,
    _sound_classes_class_stereo_playback_type_ambient = 1
};


/* ---------- structures */

struct s_sound_classes_class_sound_class_propagation;
struct s_sound_classes_class_sound_class_ducking;
struct s_sound_classes_class;
struct s_sound_classes;

struct s_sound_classes
{
    c_tag_block<s_sound_classes_class> classes;
};
static_assert(sizeof(s_sound_classes) == 0xC);

struct s_sound_classes_class
{
    short max_sounds_per_tag;
    short max_sounds_per_object;
    long preemption_time;
    c_flags<e_sound_classes_class_internal_flag_bits, ushort> internal_flags;
    c_flags<e_sound_classes_class_class_flag_bits, ushort> flags;
    short priority;
    c_enum<e_sound_classes_class_cache_miss_mode_odst, char> cache_miss_mode_odst;
    c_flags<e_sound_classes_class_accoustics_flags, char> bind_to_accoustics;
    c_flags<e_sound_classes_class_supress_spatialization_flags, uchar> supress_spatialization;
    byte padding[3];
    s_sound_classes_class_sound_class_propagation air_propagation;
    s_sound_classes_class_sound_class_propagation underwater_propagation;
    float override_speaer_gain;
    real_bounds default_distance_bounds;
    real_bounds distance_bounds;
    real_bounds gain_bounds;
    s_sound_classes_class_sound_class_ducking cutscene_ducking;
    s_sound_classes_class_sound_class_ducking scripted_dialog_ducking;
    s_sound_classes_class_sound_class_ducking cortana_effect_ducking;
    float unknown_cortana_effect;
    float doppler_factor;
    c_enum<e_sound_classes_class_stereo_playback_type, char> stereo_playback_type;
    byte padding2[3];
    float transmission_multiplier;
    float obstruction_max_bend;
    float occlusion_max_bend;
    long unknown39;
    float unknown40;
};
static_assert(sizeof(s_sound_classes_class) == 0xA0);

struct s_sound_classes_class_sound_class_ducking
{
    float gain_db;
    float fade_in_time;
    float sustain_time;
    float fade_out_time;
};
static_assert(sizeof(s_sound_classes_class_sound_class_ducking) == 0x10);

struct s_sound_classes_class_sound_class_propagation
{
    float reverb_gain;
    float direct_path_gain;
    float base_obstruction;
    float base_occlusion;
};
static_assert(sizeof(s_sound_classes_class_sound_class_propagation) == 0x10);

