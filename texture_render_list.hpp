/* ---------- enums */


/* ---------- structures */

struct s_texture_render_list_bitmap;
struct s_texture_render_list_light_unknown_block;
struct s_texture_render_list_light;
struct s_texture_render_list_bink;
struct s_texture_render_list_mannequin;
struct s_texture_render_list_weapon;
struct s_texture_render_list;

struct s_texture_render_list
{
    c_tag_block<s_texture_render_list_bitmap> bitmaps;
    c_tag_block<s_texture_render_list_light> lights;
    c_tag_block<s_texture_render_list_bink> binks;
    c_tag_block<s_texture_render_list_mannequin> mannequins;
    c_tag_block<s_texture_render_list_weapon> weapons;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
};
static_assert(sizeof(s_texture_render_list) == 0x48);

struct s_texture_render_list_weapon
{
    char name[32];
    s_tag_reference weapon2;
    float unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
};
static_assert(sizeof(s_texture_render_list_weapon) == 0x64);

struct s_texture_render_list_mannequin
{
    long unknown;
    s_tag_reference biped;
    long unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
};
static_assert(sizeof(s_texture_render_list_mannequin) == 0x4C);

struct s_texture_render_list_bink
{
    char name[32];
    s_tag_reference bink2;
};
static_assert(sizeof(s_texture_render_list_bink) == 0x30);

struct s_texture_render_list_light
{
    c_tag_block<s_texture_render_list_light_unknown_block> unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
};
static_assert(sizeof(s_texture_render_list_light) == 0x1C);

struct s_texture_render_list_light_unknown_block
{
    float unknown;
    float unknown2;
    float unknown3;
    real unknown4;
    real unknown5;
    real unknown6;
    s_tag_reference light;
};
static_assert(sizeof(s_texture_render_list_light_unknown_block) == 0x28);

struct s_texture_render_list_bitmap
{
    long index;
    char filename[256];
    long unknown;
    long width;
    long height;
};
static_assert(sizeof(s_texture_render_list_bitmap) == 0x110);

