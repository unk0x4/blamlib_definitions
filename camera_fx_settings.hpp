/* ---------- enums */

enum e_camera_fx_settings_flags
{
    _camera_fx_settings_flags_use_default_bit = 0,
    _camera_fx_settings_flags_maximum_change_is_relative_bit = 1,
    _camera_fx_settings_flags_auto_adjust_target_bit = 2,
    _camera_fx_settings_flags_unused1_bit = 3,
    _camera_fx_settings_flags_fixed1_bit = 4,
    _camera_fx_settings_flags_unused2_bit = 5,
    _camera_fx_settings_flags_fixed2_bit = 6
};


/* ---------- structures */

struct s_camera_fx_settings_camera_fx_strength;
struct s_camera_fx_settings_exposure_block;
struct s_camera_fx_settings_camera_fx_value;
struct s_camera_fx_settings_camera_fx_color;
struct s_camera_fx_settings_unknown_block;
struct s_camera_fx_settings_ssao_properties_block;
struct s_tag_function;
struct s_camera_fx_settings_unknown_block1;
struct s_camera_fx_settings_unknown_block2;
struct s_camera_fx_settings_unknown_block3;
struct s_camera_fx_settings_unknown_block4;
struct s_camera_fx_settings_unknown_block5;
struct s_camera_fx_settings_unknown_block6;
struct s_camera_fx_settings_godrays_properties_block;
struct s_camera_fx_settings;

struct s_camera_fx_settings
{
    s_camera_fx_settings_exposure_block exposure;
    s_camera_fx_settings_camera_fx_value auto_exposure_sensitivity;
    s_camera_fx_settings_camera_fx_value exposure_compensation;
    s_camera_fx_settings_camera_fx_strength highlight_bloom;
    s_camera_fx_settings_camera_fx_strength inherent_bloom;
    s_camera_fx_settings_camera_fx_strength lighting_bloom;
    s_camera_fx_settings_camera_fx_color highlight_bloom_tint;
    s_camera_fx_settings_camera_fx_color inherent_bloom_tint;
    s_camera_fx_settings_camera_fx_color lighting_bloom_tint;
    s_camera_fx_settings_camera_fx_strength unknown_strength1;
    s_camera_fx_settings_camera_fx_strength unknown_strength2;
    s_camera_fx_settings_unknown_block unknown;
    s_camera_fx_settings_camera_fx_strength constant_light;
    s_camera_fx_settings_camera_fx_strength dynamic_light;
    s_camera_fx_settings_ssao_properties_block ssao_properties;
    s_camera_fx_settings_camera_fx_value unknown_intensity1;
    c_tag_block<s_camera_fx_settings_unknown_block1> unknown33;
    c_tag_block<s_camera_fx_settings_unknown_block2> unknown34;
    c_tag_block<s_camera_fx_settings_unknown_block3> unknown35;
    c_tag_block<s_camera_fx_settings_unknown_block4> unknown36;
    c_tag_block<s_camera_fx_settings_unknown_block5> unknown37;
    c_tag_block<s_camera_fx_settings_unknown_block6> unknown38;
    s_camera_fx_settings_godrays_properties_block godrays_properties;
};
static_assert(sizeof(s_camera_fx_settings) == 0x170);

struct s_camera_fx_settings_godrays_properties_block
{
    c_flags<e_camera_fx_settings_flags, ushort> flags;
    byte unused[2];
    float radius;
    float angle_bias;
    real_rgb_color color;
    float strength;
    float power_exponent;
    float blur_sharpness;
    float decorator_darkening;
    float hemi_rejection_falloff;
};
static_assert(sizeof(s_camera_fx_settings_godrays_properties_block) == 0x24);

struct s_camera_fx_settings_unknown_block6
{
    long unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block6) == 0x28);

struct s_camera_fx_settings_unknown_block5
{
    long unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    float unknown18;
    float unknown19;
    float unknown20;
    float unknown21;
    float unknown22;
    float unknown23;
    float unknown24;
    float unknown25;
    float unknown26;
    float unknown27;
    float unknown28;
    float unknown29;
    float unknown30;
    float unknown31;
    float unknown32;
    float unknown33;
    float unknown34;
    float unknown35;
    float unknown36;
    float unknown37;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block5) == 0x94);

struct s_camera_fx_settings_unknown_block4
{
    float unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block4) == 0x14);

struct s_camera_fx_settings_unknown_block3
{
    float unknown;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block3) == 0x14);

struct s_camera_fx_settings_unknown_block2
{
    long unknown;
    float unknown2;
    float unknown3;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block2) == 0xC);

struct s_camera_fx_settings_unknown_block1
{
    float unknown;
    long unknown2;
    s_tag_function unknown3;
    s_tag_function unknown4;
    s_tag_function unknown5;
    s_tag_function unknown6;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block1) == 0x58);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_camera_fx_settings_ssao_properties_block
{
    c_flags<e_camera_fx_settings_flags, ushort> flags;
    byte unused[2];
    float ssao_radius;
    float ssao_blur_radius;
    float ssao_back_drop_strength;
};
static_assert(sizeof(s_camera_fx_settings_ssao_properties_block) == 0x10);

struct s_camera_fx_settings_unknown_block : s_camera_fx_settings_camera_fx_strength
{
    short unknown24;
    short unknown25;
};
static_assert(sizeof(s_camera_fx_settings_unknown_block) == 0x14);

struct s_camera_fx_settings_camera_fx_color
{
    c_flags<e_camera_fx_settings_flags, ushort> flags;
    byte unused[2];
    real_rgb_color color;
};
static_assert(sizeof(s_camera_fx_settings_camera_fx_color) == 0x10);

struct s_camera_fx_settings_camera_fx_value
{
    c_flags<e_camera_fx_settings_flags, ushort> flags;
    byte unused[2];
    float value;
};
static_assert(sizeof(s_camera_fx_settings_camera_fx_value) == 0x8);

struct s_camera_fx_settings_exposure_block : s_camera_fx_settings_camera_fx_strength
{
    real_bounds exposure_range;
    float auto_brightness;
    float auto_brightness_delay;
};
static_assert(sizeof(s_camera_fx_settings_exposure_block) == 0x20);

struct s_camera_fx_settings_camera_fx_strength
{
    c_flags<e_camera_fx_settings_flags, ushort> flags;
    byte unused[2];
    float strength;
    float maximum_change;
    float blend_speed;
};
static_assert(sizeof(s_camera_fx_settings_camera_fx_strength) == 0x10);

