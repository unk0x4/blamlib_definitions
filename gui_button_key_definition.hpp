/* ---------- enums */

enum e_font
{
    _font_fixedsys9 = 0,
    _font_conduit16 = 1,
    _font_conduit32 = 2,
    _font_conduit23 = 3,
    _font_conduit18 = 4,
    _font_larabie_10 = 5,
    _font_pragmata_14 = 6
};

enum e_gui_button_key_definition_bitmap_widget_blend_method
{
    _gui_button_key_definition_bitmap_widget_blend_method_standard = 0,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown = 1,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown2 = 2,
    _gui_button_key_definition_bitmap_widget_blend_method_alpha = 3,
    _gui_button_key_definition_bitmap_widget_blend_method_overlay = 4,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown3 = 5,
    _gui_button_key_definition_bitmap_widget_blend_method_lighter_color = 6,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown4 = 7,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown5 = 8,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown6 = 9,
    _gui_button_key_definition_bitmap_widget_blend_method_inverted_alpha = 10,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown7 = 11,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown8 = 12,
    _gui_button_key_definition_bitmap_widget_blend_method_unknown9 = 13
};


/* ---------- structures */

struct s_gui_definition;
struct s_gui_button_key_definition_text_widget;
struct s_gui_button_key_definition_bitmap_widget;
struct s_gui_button_key_definition;

struct s_gui_button_key_definition
{
    ulong flags;
    s_gui_definition gui_render_block;
    s_tag_reference strings;
    c_tag_block<s_gui_button_key_definition_text_widget> text_widgets;
    c_tag_block<s_gui_button_key_definition_bitmap_widget> bitmap_widgets;
};
static_assert(sizeof(s_gui_button_key_definition) == 0x54);

struct s_gui_button_key_definition_bitmap_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    s_tag_reference bitmap;
    s_tag_reference unknown2;
    c_enum<e_gui_button_key_definition_bitmap_widget_blend_method, short> blend_method;
    short unknown3;
    short sprite_index;
    short unknown4;
    string_id data_source_name;
    string_id sprite_data_source_name;
};
static_assert(sizeof(s_gui_button_key_definition_bitmap_widget) == 0x6C);

struct s_gui_button_key_definition_text_widget
{
    s_tag_reference parent;
    ulong flags;
    s_gui_definition gui_render_block;
    string_id data_source_name;
    string_id text_string;
    string_id text_color;
    c_enum<e_font, short> text_font;
    short unknown2;
};
static_assert(sizeof(s_gui_button_key_definition_text_widget) == 0x4C);

struct s_gui_definition
{
    string_id name;
    short unknown;
    short layer;
    short widescreen_y_min;
    short widescreen_x_min;
    short widescreen_y_max;
    short widescreen_x_max;
    short standard_y_min;
    short standard_x_min;
    short standard_y_max;
    short standard_x_max;
    s_tag_reference animation;
};
static_assert(sizeof(s_gui_definition) == 0x28);

