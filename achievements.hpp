/* ---------- enums */

enum e_achievements_achievement_information_block_achievements
{
    _achievements_achievement_information_block_achievements_beat_sc100 = 0,
    _achievements_achievement_information_block_achievements_beat_sc110 = 1,
    _achievements_achievement_information_block_achievements_beat_sc120 = 2,
    _achievements_achievement_information_block_achievements_beat_sc130 = 3,
    _achievements_achievement_information_block_achievements_beat_sc140 = 4,
    _achievements_achievement_information_block_achievements_beat_sc150 = 5,
    _achievements_achievement_information_block_achievements_beat_l200 = 6,
    _achievements_achievement_information_block_achievements_beat_l300 = 7,
    _achievements_achievement_information_block_achievements_beat_campaign_normal = 8,
    _achievements_achievement_information_block_achievements_beat_campaign_heroic = 9,
    _achievements_achievement_information_block_achievements_beat_campaign_legendary = 10,
    _achievements_achievement_information_block_achievements_wraith_killer = 11,
    _achievements_achievement_information_block_achievements_naughty_naughty = 12,
    _achievements_achievement_information_block_achievements_good_samaritan = 13,
    _achievements_achievement_information_block_achievements_dome_inspector = 14,
    _achievements_achievement_information_block_achievements_laser_blaster = 15,
    _achievements_achievement_information_block_achievements_both_tubes = 16,
    _achievements_achievement_information_block_achievements_i_like_fire = 17,
    _achievements_achievement_information_block_achievements_my_clothes = 18,
    _achievements_achievement_information_block_achievements_pink_and_deadly = 19,
    _achievements_achievement_information_block_achievements_dark_times = 20,
    _achievements_achievement_information_block_achievements_trading_down = 21,
    _achievements_achievement_information_block_achievements_headcase = 22,
    _achievements_achievement_information_block_achievements_boom_headshot = 23,
    _achievements_achievement_information_block_achievements_ewww_sticky = 24,
    _achievements_achievement_information_block_achievements_junior_detective = 25,
    _achievements_achievement_information_block_achievements_gumshoe = 26,
    _achievements_achievement_information_block_achievements_super_sleuth = 27,
    _achievements_achievement_information_block_achievements_metagame_points_in_sc100 = 28,
    _achievements_achievement_information_block_achievements_metagame_points_in_sc110 = 29,
    _achievements_achievement_information_block_achievements_metagame_points_in_sc120 = 30,
    _achievements_achievement_information_block_achievements_metagame_points_in_sc130_a = 31,
    _achievements_achievement_information_block_achievements_metagame_points_in_sc130_b = 32,
    _achievements_achievement_information_block_achievements_metagame_points_in_sc140 = 33,
    _achievements_achievement_information_block_achievements_metagame_points_in_l200 = 34,
    _achievements_achievement_information_block_achievements_metagame_points_in_l300 = 35,
    _achievements_achievement_information_block_achievements_be_like_marty = 36,
    _achievements_achievement_information_block_achievements_find_all_audio_logs = 37,
    _achievements_achievement_information_block_achievements_find_01_audio_logs = 38,
    _achievements_achievement_information_block_achievements_find_03_audio_logs = 39,
    _achievements_achievement_information_block_achievements_find_15_audio_logs = 40,
    _achievements_achievement_information_block_achievements_vidmaster_challenge_deja_vu = 41,
    _achievements_achievement_information_block_achievements_vidmaster_challenge_endure = 42,
    _achievements_achievement_information_block_achievements_vidmaster_challenge_classic = 43,
    _achievements_achievement_information_block_achievements_heal_up = 44,
    _achievements_achievement_information_block_achievements_stunning = 45,
    _achievements_achievement_information_block_achievements_tourist = 46
};

enum e_achievements_achievement_information_block_level_flags
{
    _achievements_achievement_information_block_level_flags_invalid_in_campaign_bit = 0,
    _achievements_achievement_information_block_level_flags_invalid_in_survival_bit = 1,
    _achievements_achievement_information_block_level_flags_resets_on_map_reload_bit = 2,
    _achievements_achievement_information_block_level_flags_uses_game_progression_bit = 3
};

enum e_achievements_achievement_information_block_chud_icon_flags
{
    _achievements_achievement_information_block_chud_icon_flags_displays_on_hud_bit = 0,
    _achievements_achievement_information_block_chud_icon_flags_bit1_bit = 1
};


/* ---------- structures */

struct s_achievements_achievement_information_block;
struct s_achievements;

struct s_achievements
{
    c_tag_block<s_achievements_achievement_information_block> achievement_information;
};
static_assert(sizeof(s_achievements) == 0xC);

struct s_achievements_achievement_information_block
{
    c_enum<e_achievements_achievement_information_block_achievements, long> achievement;
    c_flags<e_achievements_achievement_information_block_level_flags, long> flags;
    string_id level_name;
    long goal;
    c_flags<e_achievements_achievement_information_block_chud_icon_flags, long> icon_flags;
    long icon_index;
};
static_assert(sizeof(s_achievements_achievement_information_block) == 0x18);

