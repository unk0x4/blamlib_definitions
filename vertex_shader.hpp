/* ---------- enums */

enum e_shader_parameter_r_type
{
    _shader_parameter_r_type_boolean = 0,
    _shader_parameter_r_type_integer = 1,
    _shader_parameter_r_type_vector = 2,
    _shader_parameter_r_type_sampler = 3
};

enum e_shader_type
{
    _shader_type_vertex_shader = 0,
    _shader_type_pixel_shader = 1
};


/* ---------- structures */

struct s_short_offset_count_block;
struct s_vertex_shader_vertex_shader_entry_point;
struct s_shader_parameter;
struct s_vertex_shader_block;
struct s_vertex_shader;

struct s_vertex_shader
{
    ulong unknown;
    c_tag_block<s_vertex_shader_vertex_shader_entry_point> entry_points;
    ulong unknown3;
    c_tag_block<s_vertex_shader_block> shaders;
};
static_assert(sizeof(s_vertex_shader) == 0x20);

struct s_vertex_shader_block
{
    s_tag_data xbox_shader_bytecode;
    s_tag_data pc_shader_bytecode;
    c_tag_block<s_shader_parameter> xbox_parameters;
    c_enum<e_shader_type, uchar> xbox_shader_type;
    byte padding0[3];
    c_tag_block<s_shader_parameter> pc_parameters;
    c_enum<e_shader_type, uchar> pc_shader_type;
    byte padding1[3];
    ulong unknown9;
    vertex_shader_reference xbox_shader_reference;
};
static_assert(sizeof(s_vertex_shader_block) == 0x50);

struct s_shader_parameter
{
    string_id parameter_name;
    ushort register_index;
    uchar register_count;
    c_enum<e_shader_parameter_r_type, uchar> register_type;
};
static_assert(sizeof(s_shader_parameter) == 0x8);

struct s_vertex_shader_vertex_shader_entry_point
{
    c_tag_block<s_short_offset_count_block> supported_vertex_types;
};
static_assert(sizeof(s_vertex_shader_vertex_shader_entry_point) == 0xC);

struct s_short_offset_count_block
{
    uchar offset;
    uchar count;
};
static_assert(sizeof(s_short_offset_count_block) == 0x2);

