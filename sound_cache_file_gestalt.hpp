/* ---------- enums */

enum e_encoding
{
    _encoding_mono = 0,
    _encoding_stereo = 1,
    _encoding_surround = 2,
    _encoding_51_surround = 3
};

enum e_compression
{
    _compression_p_c_m_big_endian = 0,
    _compression_xbox_adpcm = 1,
    _compression_imaadpcm = 2,
    _compression_pcm = 3,
    _compression_wma = 4,
    _compression_xma = 7,
    _compression_m_p_3 = 8,
    _compression_f_s_b_4 = 9,
    _compression_ogg = 32,
    _compression_tagtool_wav = -1
};

enum e_playback_parameter_field_disable_flags
{
    _playback_parameter_field_disable_flags_distance_a_bit = 0,
    _playback_parameter_field_disable_flags_distance_b_bit = 1,
    _playback_parameter_field_disable_flags_distance_c_bit = 2,
    _playback_parameter_field_disable_flags_distance_d_bit = 3,
    _playback_parameter_field_disable_flags_bit4_bit = 4,
    _playback_parameter_field_disable_flags_bit5_bit = 5,
    _playback_parameter_field_disable_flags_bit6_bit = 6,
    _playback_parameter_field_disable_flags_bit7_bit = 7,
    _playback_parameter_field_disable_flags_bit8_bit = 8,
    _playback_parameter_field_disable_flags_bit9_bit = 9,
    _playback_parameter_field_disable_flags_bit10_bit = 10,
    _playback_parameter_field_disable_flags_bit11_bit = 11,
    _playback_parameter_field_disable_flags_bit12_bit = 12,
    _playback_parameter_field_disable_flags_bit13_bit = 13,
    _playback_parameter_field_disable_flags_bit14_bit = 14,
    _playback_parameter_field_disable_flags_bit15_bit = 15,
    _playback_parameter_field_disable_flags_bit16_bit = 16,
    _playback_parameter_field_disable_flags_bit17_bit = 17,
    _playback_parameter_field_disable_flags_bit18_bit = 18,
    _playback_parameter_field_disable_flags_bit19_bit = 19,
    _playback_parameter_field_disable_flags_bit20_bit = 20,
    _playback_parameter_field_disable_flags_bit21_bit = 21,
    _playback_parameter_field_disable_flags_bit22_bit = 22,
    _playback_parameter_field_disable_flags_bit23_bit = 23,
    _playback_parameter_field_disable_flags_bit24_bit = 24,
    _playback_parameter_field_disable_flags_bit25_bit = 25,
    _playback_parameter_field_disable_flags_bit26_bit = 26,
    _playback_parameter_field_disable_flags_bit27_bit = 27,
    _playback_parameter_field_disable_flags_bit28_bit = 28,
    _playback_parameter_field_disable_flags_bit29_bit = 29,
    _playback_parameter_field_disable_flags_bit30_bit = 30,
    _playback_parameter_field_disable_flags_bit31_bit = 31
};

enum e_playback_parameter_flags
{
    _playback_parameter_flags_override_azimuth_bit = 0,
    _playback_parameter_flags_override3_d_gain_bit = 1,
    _playback_parameter_flags_override_speaker_gain_bit = 2
};

enum e_permutation_permutation_flags
{
    _permutation_permutation_flags_sequenced_bit_bit = 0
};

enum e_custom_playback_flags
{
    _custom_playback_flags_use3_d_radio_hack_bit = 0
};

enum e_game_language
{
    _game_language_english = 0,
    _game_language_japanese = 1,
    _game_language_german = 2,
    _game_language_french = 3,
    _game_language_spanish = 4,
    _game_language_mexican = 5,
    _game_language_italian = 6,
    _game_language_korean = 7,
    _game_language_chinese_traditional = 8,
    _game_language_chinese_simplified = 9,
    _game_language_portuguese = 10,
    _game_language_russian = 11
};

enum e_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track
{
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_silence = 0,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_eat = 1,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_earth = 2,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_if = 3,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_ox = 4,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_oat = 5,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_wet = 6,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_size = 7,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_church = 8,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_fave = 9,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_though = 10,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_told = 11,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_bump = 12,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_new = 13,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_roar = 14,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_cage = 15,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_eyebrow_raise = 16,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_blink = 17,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_orientation_head_pitch = 18,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_orientation_head_roll = 19,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_orientation_head_yaw = 20,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_emphasis_head_pitch = 21,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_emphasis_head_roll = 22,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_emphasis_head_yaw = 23,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_gaze_eye_pitch = 24,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_gaze_eye_yaw = 25,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_happy = 26,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_sad = 27,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_angry = 28,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_disgusted = 29,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_scared = 30,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_surprised = 31,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_pain = 32,
    _extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track_shout = 33
};


/* ---------- structures */

struct s_platform_codec;
struct s_playback_parameter;
struct s_scale;
struct s_import_name;
struct s_unknown_block_unknown_block2;
struct s_unknown_block;
struct s_pitch_range_parameter;
struct s_permutation_chunk;
struct s_permutation;
struct s_pitch_range;
struct s_custom_playback_filter_block;
struct s_custom_playback;
struct s_language_block_permutation_duration_block;
struct s_language_block_pitch_range_duration_block;
struct s_language_block;
struct s_runtime_permutation_flag;
struct s_tag_function;
struct s_promotion_rule;
struct s_promotion_runtime_timer;
struct s_promotion;
struct s_extra_info_language_permutation_raw_info_block_seek_table_block;
struct s_extra_info_language_permutation_raw_info_block;
struct s_extra_info_language_permutation;
struct s_extra_info_facial_animation_info_block_sound_dialogue_info_block;
struct s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve;
struct s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block;
struct s_extra_info_facial_animation_info_block_facial_animation_permutation;
struct s_extra_info_facial_animation_info_block;
struct s_extra_info;
struct s_sound_cache_file_gestalt;

struct s_sound_cache_file_gestalt
{
    c_tag_block<s_platform_codec> platform_codecs;
    c_tag_block<s_playback_parameter> playback_parameters;
    c_tag_block<s_scale> scales;
    c_tag_block<s_import_name> import_names;
    c_tag_block<s_unknown_block> unknown;
    c_tag_block<s_pitch_range_parameter> pitch_range_parameters;
    c_tag_block<s_pitch_range> pitch_ranges;
    c_tag_block<s_permutation> permutations;
    c_tag_block<s_custom_playback> custom_playbacks;
    c_tag_block<s_language_block> languages;
    c_tag_block<s_runtime_permutation_flag> runtime_permutation_flags;
    s_tag_function unknown2;
    ulong unknown3;
    ulong unknown4;
    c_tag_block<s_permutation_chunk> permutation_chunks;
    c_tag_block<s_promotion> promotions;
    c_tag_block<s_extra_info> extra_info;
};
static_assert(sizeof(s_sound_cache_file_gestalt) == 0xC4);

struct s_extra_info
{
    c_tag_block<s_extra_info_language_permutation> language_permutations;
    c_tag_block<s_extra_info_facial_animation_info_block> facial_animation_info;
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
};
static_assert(sizeof(s_extra_info) == 0x28);

struct s_extra_info_facial_animation_info_block
{
    s_tag_data encoded_data;
    c_tag_block<s_extra_info_facial_animation_info_block_sound_dialogue_info_block> sound_dialogue_info;
    c_tag_block<s_extra_info_facial_animation_info_block_facial_animation_permutation> facial_animation_permutations;
};
static_assert(sizeof(s_extra_info_facial_animation_info_block) == 0x2C);

struct s_extra_info_facial_animation_info_block_facial_animation_permutation
{
    c_tag_block<s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block> facial_animation;
};
static_assert(sizeof(s_extra_info_facial_animation_info_block_facial_animation_permutation) == 0xC);

struct s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block
{
    float start_time;
    float end_time;
    float blend_in;
    float blend_out;
    byte pad[12];
    c_tag_block<s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve> facial_animation_curves;
};
static_assert(sizeof(s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block) == 0x28);

struct s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve
{
    short animation_start_time;
    c_enum<e_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track, uchar> first_track;
    c_enum<e_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track, uchar> second_track;
    c_enum<e_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve_facial_animation_track, uchar> third_track;
    char first_pose_weight;
    char second_pose_weight;
    char third_pose_weight;
};
static_assert(sizeof(s_extra_info_facial_animation_info_block_facial_animation_permutation_facial_animation_block_facial_animation_curve) == 0x8);

struct s_extra_info_facial_animation_info_block_sound_dialogue_info_block
{
    ulong mouth_data_offset;
    ulong mouth_data_length;
    ulong lipsync_data_offset;
    ulong lipsync_data_length;
};
static_assert(sizeof(s_extra_info_facial_animation_info_block_sound_dialogue_info_block) == 0x10);

struct s_extra_info_language_permutation
{
    c_tag_block<s_extra_info_language_permutation_raw_info_block> raw_info;
};
static_assert(sizeof(s_extra_info_language_permutation) == 0xC);

struct s_extra_info_language_permutation_raw_info_block
{
    string_id skip_fraction_name;
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
    ulong unknown18;
    c_tag_block<s_extra_info_language_permutation_raw_info_block_seek_table_block> seek_table;
    short compression;
    uchar language;
    uchar unknown19;
    ulong resource_sample_size;
    ulong resource_sample_offset;
    ulong sample_count;
    ulong unknown20;
    ulong unknown21;
    ulong unknown22;
    ulong unknown23;
    long unknown24;
};
static_assert(sizeof(s_extra_info_language_permutation_raw_info_block) == 0x7C);

struct s_extra_info_language_permutation_raw_info_block_seek_table_block
{
    ulong block_relative_sample_start;
    ulong block_relative_sample_end;
    ulong starting_sample_index;
    ulong ending_sample_index;
    ulong starting_offset;
    ulong ending_offset;
};
static_assert(sizeof(s_extra_info_language_permutation_raw_info_block_seek_table_block) == 0x18);

struct s_promotion
{
    c_tag_block<s_promotion_rule> rules;
    c_tag_block<s_promotion_runtime_timer> runtime_timers;
    long unknown1;
    ulong unknown2;
    ulong unknown3;
    ulong longest_permutation_duration;
    ulong total_sample_size;
    ulong unknown11;
};
static_assert(sizeof(s_promotion) == 0x30);

struct s_promotion_runtime_timer
{
    long unknown;
};
static_assert(sizeof(s_promotion_runtime_timer) == 0x4);

struct s_promotion_rule
{
    short pitch_range_index;
    short maximum_playing_count;
    float suppression_time;
    long unknown;
    long unknown2;
};
static_assert(sizeof(s_promotion_rule) == 0x10);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_runtime_permutation_flag
{
    char unknown;
};
static_assert(sizeof(s_runtime_permutation_flag) == 0x1);

struct s_language_block
{
    c_enum<e_game_language, long> language;
    c_tag_block<s_language_block_permutation_duration_block> permutation_durations;
    c_tag_block<s_language_block_pitch_range_duration_block> pitch_range_durations;
};
static_assert(sizeof(s_language_block) == 0x1C);

struct s_language_block_pitch_range_duration_block
{
    short permutation_start_index;
    short permutation_count;
};
static_assert(sizeof(s_language_block_pitch_range_duration_block) == 0x4);

struct s_language_block_permutation_duration_block
{
    short frame_count;
};
static_assert(sizeof(s_language_block_permutation_duration_block) == 0x2);

struct s_custom_playback
{
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
    c_flags<e_custom_playback_flags, long> flags;
    ulong unknown4;
    ulong unknown5;
    c_tag_block<s_custom_playback_filter_block> filter;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    ulong unknown13;
    ulong unknown14;
    ulong unknown15;
    ulong unknown16;
    ulong unknown17;
};
static_assert(sizeof(s_custom_playback) == 0x54);

struct s_custom_playback_filter_block
{
    long filter_type;
    long filter_width;
    real_bounds scale_bounds1;
    float random_base1;
    float random_variance1;
    real_bounds scale_bounds2;
    float random_base2;
    float random_variance2;
    real_bounds scale_bounds3;
    float random_base3;
    float random_variance3;
    real_bounds scale_bounds4;
    float random_base4;
    float random_variance4;
};
static_assert(sizeof(s_custom_playback_filter_block) == 0x48);

struct s_pitch_range
{
    string_id import_name;
    s_pitch_range_parameter pitch_range_parameters;
    real_bounds distance_bounds1;
    real_bounds distance_bounds2;
    long runtime_permutation_flags;
    short permutation_count;
    char runtime_discarded_permutation_index;
    char runtime_last_permutation_index;
    c_tag_block<s_permutation> permutations;
};
static_assert(sizeof(s_pitch_range) == 0x38);

struct s_permutation
{
    string_id import_name;
    float skip_fraction;
    float gain_ho;
    ulong sample_count;
    ulong permutation_number;
    ulong is_not_first_permutation;
    c_tag_block<s_permutation_chunk> permutation_chunks;
    c_flags<e_permutation_permutation_flags, short> permutation_flags_ho;
    byte padding[2];
    ulong first_sample;
};
static_assert(sizeof(s_permutation) == 0x2C);

struct s_permutation_chunk
{
    long offset;
    long encoded_size;
    long runtime_index;
    ulong first_sample;
    ulong last_sample;
};
static_assert(sizeof(s_permutation_chunk) == 0x14);

struct s_pitch_range_parameter
{
    short natural_pitch;
    short first_deterministic_flag_index;
    short_bounds bend_bounds;
    short_bounds full_volume_bounds;
    short_bounds playback_bend_bounds;
};
static_assert(sizeof(s_pitch_range_parameter) == 0x10);

struct s_unknown_block
{
    ulong unknown;
    c_tag_block<s_unknown_block_unknown_block2> unknown2;
};
static_assert(sizeof(s_unknown_block) == 0x10);

struct s_unknown_block_unknown_block2
{
    ulong unknown;
};
static_assert(sizeof(s_unknown_block_unknown_block2) == 0x4);

struct s_import_name
{
    string_id name;
};
static_assert(sizeof(s_import_name) == 0x4);

struct s_scale
{
    real_bounds gain_modifier_bounds;
    short_bounds pitch_modifier_bounds;
    real_bounds skip_fraction_modifier_bounds;
};
static_assert(sizeof(s_scale) == 0x14);

struct s_playback_parameter
{
    c_flags<e_playback_parameter_field_disable_flags, long> field_disable_flags;
    float distance_a;
    float distance_b;
    float distance_c;
    float distance_d;
    float skip_fraction;
    float maximum_bend_per_second;
    float gain_base;
    float gain_variance;
    short_bounds random_pitch_bounds;
    real_bounds cone_angle_bounds;
    float outer_cone_gain;
    c_flags<e_playback_parameter_flags, long> flags;
    real azimuth;
    float positional_gain;
    float first_person_gain;
};
static_assert(sizeof(s_playback_parameter) == 0x44);

struct s_platform_codec
{
    uchar unknown1;
    uchar load_mode;
    c_enum<e_encoding, char> encoding;
    c_enum<e_compression, char> compression;
};
static_assert(sizeof(s_platform_codec) == 0x4);

