/* ---------- enums */

enum e_creature_flags
{
    _creature_flags_immune_to_falling_damage_bit = 0,
    _creature_flags_rotate_while_airborne_bit = 1,
    _creature_flags_zapped_by_shields_bit = 2,
    _creature_flags_attach_upon_impact_bit = 3,
    _creature_flags_not_on_motion_sensor_bit = 4,
    _creature_flags_force_ground_movement_bit = 5
};

enum e_unit_default_team
{
    _unit_default_team_default = 0,
    _unit_default_team_player = 1,
    _unit_default_team_human = 2,
    _unit_default_team_covenant = 3,
    _unit_default_team_flood = 4,
    _unit_default_team_sentinel = 5,
    _unit_default_team_heretic = 6,
    _unit_default_team_prophet = 7,
    _unit_default_team_guilty = 8,
    _unit_default_team_unused9 = 9,
    _unit_default_team_unused10 = 10,
    _unit_default_team_unused11 = 11,
    _unit_default_team_unused12 = 12,
    _unit_default_team_unused13 = 13,
    _unit_default_team_unused14 = 14,
    _unit_default_team_unused15 = 15
};

enum e_unit_motion_sensor_blip_size
{
    _unit_motion_sensor_blip_size_medium = 0,
    _unit_motion_sensor_blip_size_small = 1,
    _unit_motion_sensor_blip_size_large = 2
};

enum e_blam_shape_type
{
    _blam_shape_type_sphere = 0,
    _blam_shape_type_pill = 1,
    _blam_shape_type_box = 2,
    _blam_shape_type_triangle = 3,
    _blam_shape_type_polyhedron = 4,
    _blam_shape_type_multi_sphere = 5,
    _blam_shape_type_triangle_mesh = 6,
    _blam_shape_type_compound_shape = 7,
    _blam_shape_type_unused0 = 8,
    _blam_shape_type_unused1 = 9,
    _blam_shape_type_unused2 = 10,
    _blam_shape_type_unused3 = 11,
    _blam_shape_type_unused4 = 12,
    _blam_shape_type_unused5 = 13,
    _blam_shape_type_list = 14,
    _blam_shape_type_mopp = 15
};

enum e_unit_metagame_property_unit_type
{
    _unit_metagame_property_unit_type_brute = 0,
    _unit_metagame_property_unit_type_grunt = 1,
    _unit_metagame_property_unit_type_jackal = 2,
    _unit_metagame_property_unit_type_marine = 3,
    _unit_metagame_property_unit_type_bugger = 4,
    _unit_metagame_property_unit_type_hunter = 5,
    _unit_metagame_property_unit_type_flood_infection = 6,
    _unit_metagame_property_unit_type_flood_carrier = 7,
    _unit_metagame_property_unit_type_flood_combat = 8,
    _unit_metagame_property_unit_type_flood_pureform = 9,
    _unit_metagame_property_unit_type_sentinel = 10,
    _unit_metagame_property_unit_type_elite = 11,
    _unit_metagame_property_unit_type_turret = 12,
    _unit_metagame_property_unit_type_mongoose = 13,
    _unit_metagame_property_unit_type_warthog = 14,
    _unit_metagame_property_unit_type_scorpion = 15,
    _unit_metagame_property_unit_type_hornet = 16,
    _unit_metagame_property_unit_type_pelican = 17,
    _unit_metagame_property_unit_type_shade = 18,
    _unit_metagame_property_unit_type_watchtower = 19,
    _unit_metagame_property_unit_type_ghost = 20,
    _unit_metagame_property_unit_type_chopper = 21,
    _unit_metagame_property_unit_type_mauler = 22,
    _unit_metagame_property_unit_type_wraith = 23,
    _unit_metagame_property_unit_type_banshee = 24,
    _unit_metagame_property_unit_type_phantom = 25,
    _unit_metagame_property_unit_type_scarab = 26,
    _unit_metagame_property_unit_type_guntower = 27,
    _unit_metagame_property_unit_type_engineer = 28,
    _unit_metagame_property_unit_type_engineer_recharge_station = 29
};

enum e_unit_metagame_property_unit_classification
{
    _unit_metagame_property_unit_classification_infantry = 0,
    _unit_metagame_property_unit_classification_leader = 1,
    _unit_metagame_property_unit_classification_hero = 2,
    _unit_metagame_property_unit_classification_specialist = 3,
    _unit_metagame_property_unit_classification_light_vehicle = 4,
    _unit_metagame_property_unit_classification_heavy_vehicle = 5,
    _unit_metagame_property_unit_classification_giant_vehicle = 6,
    _unit_metagame_property_unit_classification_standard_vehicle = 7
};


/* ---------- structures */

struct s_physics_model_havok_shape_base;
struct s_physics_model_shape;
struct s_physics_model_havok_shape_reference;
struct s_physics_model_sphere;
struct s_physics_model_pill;
struct s_unit_metagame_property;
struct s_creature;

struct s_creature : s_game_object
{
    c_flags<e_creature_flags, long> flags2;
    c_enum<e_unit_default_team, short> default_team;
    c_enum<e_unit_motion_sensor_blip_size, short> motion_sensor_blip_size;
    real turning_velocity_maximum;
    real turning_acceleration_maximum;
    float casual_turning_modifer;
    float autoaim_width;
    ulong flags3;
    float height_standing;
    float height_crouching;
    float radius;
    float mass;
    string_id living_material_name;
    string_id dead_material_name;
    short living_global_material_index;
    short dead_global_material_index;
    c_tag_block<s_physics_model_sphere> dead_sphere_shapes;
    c_tag_block<s_physics_model_pill> pill_shapes;
    c_tag_block<s_physics_model_sphere> sphere_shapes;
    real maximum_slope_angle;
    real downhill_falloff_angle;
    real downhill_cutoff_angle;
    real uphill_falloff_angle;
    real uphill_cutoff_angle;
    float downhill_velocity_scale;
    float uphill_velocity_scale;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
    ulong unknown9;
    ulong unknown10;
    ulong unknown11;
    ulong unknown12;
    float falling_velocity_scale;
    ulong unknown13;
    real bank_angle;
    float bank_apply_time;
    float bank_decay_time;
    float pitch_ratio;
    float maximum_velocity;
    float maximum_sidestep_velocity;
    float acceleration;
    float deceleration;
    real angular_velocity_maximum;
    real angular_acceleration_maximum;
    float crouch_velocity_modifier;
    ulong unknown14;
    s_tag_reference impact_damage;
    s_tag_reference impact_shield_damage;
    c_tag_block<s_unit_metagame_property> metagame_properties;
    real_bounds destroy_after_death_time_bounds;
};
static_assert(sizeof(s_creature) == 0x220);

struct s_unit_metagame_property
{
    uchar flags;
    c_enum<e_unit_metagame_property_unit_type, char> unit;
    c_enum<e_unit_metagame_property_unit_classification, char> classification;
    char unknown;
    short points;
    short unknown2;
};
static_assert(sizeof(s_unit_metagame_property) == 0x8);

struct s_physics_model_pill : s_physics_model_shape
{
    real_vector3d bottom;
    float bottom_radius;
    real_vector3d top;
    float top_radius;
};
static_assert(sizeof(s_physics_model_pill) == 0x60);

struct s_physics_model_sphere : s_physics_model_shape
{
    s_physics_model_havok_shape_base convex_base;
    ulong field_pointer_skip;
    s_physics_model_havok_shape_reference shape_reference;
    real_vector3d translation;
    float translation_radius;
};
static_assert(sizeof(s_physics_model_sphere) == 0x70);

struct s_physics_model_havok_shape_reference
{
    c_enum<e_blam_shape_type, short> shapetype;
    short shape_index;
    ulong child_shape_size;
};
static_assert(sizeof(s_physics_model_havok_shape_reference) == 0x8);

struct s_physics_model_shape
{
    string_id name;
    short material_index;
    short runtime_material_type;
    float relative_mass_scale;
    float friction;
    float restitution;
    float volume;
    float mass;
    short mass_distribution_index;
    char phantom_index;
    char proxy_collision_group;
    s_physics_model_havok_shape_base shape_base;
    byte pad[12];
};
static_assert(sizeof(s_physics_model_shape) == 0x40);

struct s_physics_model_havok_shape_base
{
    long field_pointer_skip;
    short size;
    short count;
    long offset;
    long user_data;
    float radius;
};
static_assert(sizeof(s_physics_model_havok_shape_base) == 0x14);

