/* ---------- enums */

enum e_collision_model_flags
{
    _collision_model_flags_contains_open_edges_bit = 0,
    _collision_model_flags_physics_built_bit = 1,
    _collision_model_flags_physics_in_use_bit = 2,
    _collision_model_flags_processed_bit = 3,
    _collision_model_flags_has_two_sided_surfaces_bit = 4
};

enum e_leaf_flags
{
    _leaf_flags_contains_double_sided_surfaces_bit = 0
};

enum e_surface_flags
{
    _surface_flags_two_sided_bit = 0,
    _surface_flags_invisible_bit = 1,
    _surface_flags_climbable_bit = 2,
    _surface_flags_breakable_bit = 3,
    _surface_flags_invalid_bit = 4,
    _surface_flags_conveyor_bit = 5,
    _surface_flags_slip_bit = 6,
    _surface_flags_plane_negated_bit = 7
};

enum e_blam_shape_type
{
    _blam_shape_type_sphere = 0,
    _blam_shape_type_pill = 1,
    _blam_shape_type_box = 2,
    _blam_shape_type_triangle = 3,
    _blam_shape_type_polyhedron = 4,
    _blam_shape_type_multi_sphere = 5,
    _blam_shape_type_triangle_mesh = 6,
    _blam_shape_type_compound_shape = 7,
    _blam_shape_type_unused0 = 8,
    _blam_shape_type_unused1 = 9,
    _blam_shape_type_unused2 = 10,
    _blam_shape_type_unused3 = 11,
    _blam_shape_type_unused4 = 12,
    _blam_shape_type_unused5 = 13,
    _blam_shape_type_list = 14,
    _blam_shape_type_mopp = 15
};

enum e_collision_model_pathfinding_sphere_flags
{
    _collision_model_pathfinding_sphere_flags_remains_when_open_bit = 0,
    _collision_model_pathfinding_sphere_flags_vehicle_only_bit = 1,
    _collision_model_pathfinding_sphere_flags_with_sectors_bit = 2
};

enum e_collision_model_node_flags
{
    _collision_model_node_flags_generate_nav_mesh_bit = 0
};


/* ---------- structures */

struct s_collision_model_material;
struct s_bsp3_d_node;
struct s_plane;
struct s_leaf;
struct s_bsp2_d_reference;
struct s_bsp2_d_node;
struct s_surface;
struct s_edge;
struct s_vertex;
struct s_collision_geometry;
struct s_collision_model_region_permutation_bsp;
struct s_hkp_referenced_object;
struct s_hkp_shape;
struct s_hkp_shape_container;
struct s_hkp_shape_collection;
struct s_collision_geometry_shape;
struct s_hkp_single_shape_container;
struct s_hkp_mopp_bv_tree_shape;
struct s_c_mopp_bv_tree_shape;
struct s_collision_bsp_physics_definition;
struct s_code_info;
struct s_hk_array_base;
struct s_hkp_mopp_code;
struct s_tag_hkp_mopp_code;
struct s_collision_model_region_permutation;
struct s_collision_model_region;
struct s_collision_model_pathfinding_sphere;
struct s_collision_model_node;
struct s_collision_model;

struct s_collision_model
{
    long collision_model_checksum;
    byte unused_errors_block[12];
    c_flags<e_collision_model_flags, long> flags;
    c_tag_block<s_collision_model_material> materials;
    c_tag_block<s_collision_model_region> regions;
    c_tag_block<s_collision_model_pathfinding_sphere> pathfinding_spheres;
    c_tag_block<s_collision_model_node> nodes;
};
static_assert(sizeof(s_collision_model) == 0x44);

struct s_collision_model_node
{
    string_id name;
    c_flags<e_collision_model_node_flags, ushort> flags;
    short parent_node;
    short next_sibling_node;
    short first_child_node;
};
static_assert(sizeof(s_collision_model_node) == 0xC);

struct s_collision_model_pathfinding_sphere
{
    short node;
    c_flags<e_collision_model_pathfinding_sphere_flags, ushort> flags;
    real_point3d center;
    float radius;
};
static_assert(sizeof(s_collision_model_pathfinding_sphere) == 0x14);

struct s_collision_model_region
{
    string_id name;
    c_tag_block<s_collision_model_region_permutation> permutations;
};
static_assert(sizeof(s_collision_model_region) == 0x10);

struct s_collision_model_region_permutation
{
    string_id name;
    c_tag_block<s_collision_model_region_permutation_bsp> bsps;
    c_tag_block<s_collision_bsp_physics_definition> bsp_physics;
    c_tag_block<s_tag_hkp_mopp_code> bsp_mopp_codes;
};
static_assert(sizeof(s_collision_model_region_permutation) == 0x28);

struct s_tag_hkp_mopp_code : s_hkp_mopp_code
{
    c_tag_block<uchar> data;
    byte padding3[4];
};
static_assert(sizeof(s_tag_hkp_mopp_code) == 0x40);

struct s_hkp_mopp_code
{
    ulong vf_table_address;
    s_hkp_referenced_object referenced_object;
    s_tag_data padding1;
    s_code_info info;
    s_hk_array_base array_base;
    s_tag_data padding2;
};
static_assert(sizeof(s_hkp_mopp_code) == 0x30);

struct s_hk_array_base
{
    ulong data_address;
    ulong size;
    ulong capacity_and_flags;
};
static_assert(sizeof(s_hk_array_base) == 0xC);

struct s_code_info
{
    real_quaternion offset;
};
static_assert(sizeof(s_code_info) == 0x10);

struct s_collision_bsp_physics_definition
{
    s_collision_geometry_shape geometry_shape;
    s_c_mopp_bv_tree_shape mopp_bv_tree_shape;
};
static_assert(sizeof(s_collision_bsp_physics_definition) == 0x80);

struct s_c_mopp_bv_tree_shape : s_hkp_mopp_bv_tree_shape
{
    float scale;
};
static_assert(sizeof(s_c_mopp_bv_tree_shape) == 0x20);

struct s_hkp_mopp_bv_tree_shape : s_hkp_shape
{
    s_hkp_single_shape_container child;
    ulong mopp_code_address;
};
static_assert(sizeof(s_hkp_mopp_bv_tree_shape) == 0x1C);

struct s_hkp_single_shape_container : s_hkp_shape_container
{
    c_enum<e_blam_shape_type, short> type;
    short index;
};
static_assert(sizeof(s_hkp_single_shape_container) == 0x8);

struct s_collision_geometry_shape : s_hkp_shape_collection
{
    byte padding1[8];
    real_quaternion a_a_b_b_center;
    real_quaternion a_a_b_b_half_extents;
    s_tag_reference_short model;
    ulong collision_bsp_address;
    ulong large_collision_bsp_address;
    char bsp_index;
    uchar collision_geometry_shape_type;
    ushort collision_geometry_shape_key;
    float scale;
    byte padding2[12];
};
static_assert(sizeof(s_collision_geometry_shape) == 0x60);

struct s_hkp_shape_collection : s_hkp_shape
{
    s_hkp_shape_container container;
    boolean disable_welding;
    byte padding[3];
};
static_assert(sizeof(s_hkp_shape_collection) == 0x18);

struct s_hkp_shape_container
{
    ulong vf_table_address;
};
static_assert(sizeof(s_hkp_shape_container) == 0x4);

struct s_hkp_shape
{
    ulong vf_table_address;
    s_hkp_referenced_object referenced_object;
    ulong user_data_address;
    ulong type;
};
static_assert(sizeof(s_hkp_shape) == 0x10);

struct s_hkp_referenced_object
{
    ushort size_and_flags;
    ushort reference_count;
};
static_assert(sizeof(s_hkp_referenced_object) == 0x4);

struct s_collision_model_region_permutation_bsp
{
    short node_index;
    byte unused[2];
    s_collision_geometry geometry;
};
static_assert(sizeof(s_collision_model_region_permutation_bsp) == 0x64);

struct s_collision_geometry
{
    c_tag_block<s_bsp3_d_node> bsp3_d_nodes;
    c_tag_block<s_plane> planes;
    c_tag_block<s_leaf> leaves;
    c_tag_block<s_bsp2_d_reference> bsp2_d_references;
    c_tag_block<s_bsp2_d_node> bsp2_d_nodes;
    c_tag_block<s_surface> surfaces;
    c_tag_block<s_edge> edges;
    c_tag_block<s_vertex> vertices;
};
static_assert(sizeof(s_collision_geometry) == 0x60);

struct s_vertex
{
    real_point3d point;
    ushort first_edge;
    short sink;
};
static_assert(sizeof(s_vertex) == 0x10);

struct s_edge
{
    ushort start_vertex;
    ushort end_vertex;
    ushort forward_edge;
    ushort reverse_edge;
    ushort left_surface;
    ushort right_surface;
};
static_assert(sizeof(s_edge) == 0xC);

struct s_surface
{
    ushort plane;
    ushort first_edge;
    short material_index;
    short breakable_surface_set;
    short breakable_surface_index;
    c_flags<e_surface_flags, uchar> flags;
    uchar best_plane_calculation_vertex;
};
static_assert(sizeof(s_surface) == 0xC);

struct s_bsp2_d_node
{
    real_plane2d plane;
    short left_child;
    short right_child;
};
static_assert(sizeof(s_bsp2_d_node) == 0x10);

struct s_bsp2_d_reference
{
    short plane_index;
    short bsp2_d_node_index;
};
static_assert(sizeof(s_bsp2_d_reference) == 0x4);

struct s_leaf
{
    c_flags<e_leaf_flags, uchar> flags;
    c_flags<e_leaf_flags, uchar> flags2;
    ushort bsp2_d_reference_count;
    ulong first_bsp2_d_reference;
};
static_assert(sizeof(s_leaf) == 0x8);

struct s_plane
{
    real_plane3d value;
};
static_assert(sizeof(s_plane) == 0x10);

struct s_bsp3_d_node
{
    u_int64 value;
};
static_assert(sizeof(s_bsp3_d_node) == 0x8);

struct s_collision_model_material
{
    string_id name;
};
static_assert(sizeof(s_collision_model_material) == 0x4);

