/* ---------- enums */


/* ---------- structures */

struct s_chocolate_mountain_new_lighting_variable;
struct s_chocolate_mountain_new;

struct s_chocolate_mountain_new
{
    c_tag_block<s_chocolate_mountain_new_lighting_variable> lighting_variables;
};
static_assert(sizeof(s_chocolate_mountain_new) == 0xC);

struct s_chocolate_mountain_new_lighting_variable
{
    float lightmap_brightness_offset;
};
static_assert(sizeof(s_chocolate_mountain_new_lighting_variable) == 0x4);

