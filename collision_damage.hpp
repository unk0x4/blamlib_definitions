/* ---------- enums */


/* ---------- structures */

struct s_collision_damage;

struct s_collision_damage
{
    float apply_damage_scale;
    float apply_recoil_damage_scale;
    real_bounds damage_acceleration_bounds;
    real_bounds damage_scale_bounds;
    real_bounds unknown;
    real_bounds recoil_damage_acceleration_bounds;
    real_bounds recoil_damage_scale_bounds;
};
static_assert(sizeof(s_collision_damage) == 0x30);

