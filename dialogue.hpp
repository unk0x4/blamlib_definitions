/* ---------- enums */


/* ---------- structures */

struct s_dialogue_vocalization;
struct s_dialogue;

struct s_dialogue
{
    s_tag_reference global_dialogue_info;
    ulong flags;
    c_tag_block<s_dialogue_vocalization> vocalizations;
    string_id mission_dialogue_designator;
};
static_assert(sizeof(s_dialogue) == 0x24);

struct s_dialogue_vocalization
{
    ushort flags;
    short unknown;
    string_id name;
    s_tag_reference sound;
};
static_assert(sizeof(s_dialogue_vocalization) == 0x18);

