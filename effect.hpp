/* ---------- enums */

enum e_effect_flags
{
    _effect_flags_deleted_when_attachment_deactivates_bit = 0,
    _effect_flags_run_events_in_parallel_bit = 1,
    _effect_flags_do_not_re_use_parts_when_looping_bit = 2,
    _effect_flags_age_creator_s_weapon_bit = 3,
    _effect_flags_use_parent_position_but_world_orientation_bit = 4,
    _effect_flags_can_penetrate_walls_bit = 5,
    _effect_flags_cannot_be_restarted_bit = 6,
    _effect_flags_force_use_own_lightprobe_bit = 7,
    _effect_flags_force_looping_bit = 8,
    _effect_flags_obsolete_effect_ordnance_is_gone_bit = 9,
    _effect_flags_render_in_hologram_pass_bit = 10,
    _effect_flags_lightprobe_only_sample_airprobes_bit = 11,
    _effect_flags_play_effect_even_outside_bsps_bit = 12,
    _effect_flags_draw_lens_flares_when_stopped_bit = 13,
    _effect_flags_kill_particles_when_stopped_bit = 14,
    _effect_flags_play_even_on_hidden_objects_bit = 15,
    _effect_flags_disable_first_person_parts_in_blind_skull_bit = 16,
    _effect_flags_hides_associated_object_on_effect_deletion_bit = 17,
    _effect_flags_bypass_mp_throttle_bit = 18,
    _effect_flags_render_in_non_first_person_pass_bit = 19,
    _effect_flags_use_averaged_locations_for_lods_bit = 20
};

enum e_effect_priority
{
    _effect_priority_low = 0,
    _effect_priority_normal = 1,
    _effect_priority_above_normal = 2,
    _effect_priority_high = 3,
    _effect_priority_very_high = 4,
    _effect_priority_essential = 5
};

enum e_effect_environment
{
    _effect_environment_any_environment = 0,
    _effect_environment_air_only = 1,
    _effect_environment_water_only = 2,
    _effect_environment_space_only = 3,
    _effect_environment_wet_only = 4,
    _effect_environment_dry_only = 5
};

enum e_effect_violence_mode
{
    _effect_violence_mode_either_mode = 0,
    _effect_violence_mode_violent_mode_only = 1,
    _effect_violence_mode_nonviolent_mode_only = 2
};

enum e_effect_event_part_flags
{
    _effect_event_part_flags_face_down_regardless_of_location_bit = 0,
    _effect_event_part_flags_offset_origin_away_from_geometry_bit = 1,
    _effect_event_part_flags_never_attached_to_object_bit = 2,
    _effect_event_part_flags_disabled_for_debugging_bit = 3,
    _effect_event_part_flags_draw_regardless_of_distance_bit = 4,
    _effect_event_part_flags_make_every_tick_bit = 5,
    _effect_event_part_flags_inherit_parent_variant_bit = 6,
    _effect_event_part_flags_batch_aoe_damage_bit = 7,
    _effect_event_part_flags_create_even_when_event_loops_back_to_self_bit = 8,
    _effect_event_part_flags_face_up_regardless_of_location_bit = 9,
    _effect_event_part_flags_sound_only_plays_in_killcam_bit = 10,
    _effect_event_part_flags_disable_in_aimed_down_sight_bit = 11,
    _effect_event_part_flags_enable_only_in_aimed_down_sight_bit = 12,
    _effect_event_part_flags_use_dynamic_direction_bit = 13
};

enum e_effect_event_priority
{
    _effect_event_priority_low = 0,
    _effect_event_priority_normal = 1,
    _effect_event_priority_above_normal = 2,
    _effect_event_priority_high = 3,
    _effect_event_priority_very_high = 4,
    _effect_event_priority_essential = 5
};

enum e_effect_event_part_camera_mode
{
    _effect_event_part_camera_mode_independent_of_camera_mode = 0,
    _effect_event_part_camera_mode_first_person_only = 1,
    _effect_event_part_camera_mode_third_person_only = 2,
    _effect_event_part_camera_mode_both_first_and_third = 3
};

enum e_effect_event_part_scales
{
    _effect_event_part_scales_velocity = 0,
    _effect_event_part_scales_velocity_delta = 1,
    _effect_event_part_scales_velocity_cone_angle = 2,
    _effect_event_part_scales_angular_velocity = 3,
    _effect_event_part_scales_angular_velocity_delta = 4,
    _effect_event_part_scales_type_specific_scale = 5
};

enum e_effect_event_acceleration_create_in_environment
{
    _effect_event_acceleration_create_in_environment_any_environment = 0,
    _effect_event_acceleration_create_in_environment_air_only = 1,
    _effect_event_acceleration_create_in_environment_water_only = 2,
    _effect_event_acceleration_create_in_environment_space_only = 3
};

enum e_effect_event_acceleration_create_in_disposition
{
    _effect_event_acceleration_create_in_disposition_either_mode = 0,
    _effect_event_acceleration_create_in_disposition_violent_mode_only = 1,
    _effect_event_acceleration_create_in_disposition_nonviolent_mode_only = 2
};

enum e_particle_coordinate_system
{
    _particle_coordinate_system_world = 0,
    _particle_coordinate_system_local = 1,
    _particle_coordinate_system_parent = 2
};

enum e_particle_camera_mode
{
    _particle_camera_mode_independent_of_camera_mode = 0,
    _particle_camera_mode_first_person_only = 1,
    _particle_camera_mode_third_person_only = 2,
    _particle_camera_mode_both_first_and_third = 3
};

enum e_effect_event_particle_system_emitter_emission_shape
{
    _effect_event_particle_system_emitter_emission_shape_sprayer = 0,
    _effect_event_particle_system_emitter_emission_shape_disc = 1,
    _effect_event_particle_system_emitter_emission_shape_globe = 2,
    _effect_event_particle_system_emitter_emission_shape_implode = 3,
    _effect_event_particle_system_emitter_emission_shape_tube = 4,
    _effect_event_particle_system_emitter_emission_shape_halo = 5,
    _effect_event_particle_system_emitter_emission_shape_impact_contour = 6,
    _effect_event_particle_system_emitter_emission_shape_impact_area = 7,
    _effect_event_particle_system_emitter_emission_shape_debris = 8,
    _effect_event_particle_system_emitter_emission_shape_line = 9,
    _effect_event_particle_system_emitter_emission_shape_custom_points = 10,
    _effect_event_particle_system_emitter_emission_shape_boat_hull_surface = 11,
    _effect_event_particle_system_emitter_emission_shape_cube = 12,
    _effect_event_particle_system_emitter_emission_shape_cylinder = 13,
    _effect_event_particle_system_emitter_emission_shape_unweighted_line = 14,
    _effect_event_particle_system_emitter_emission_shape_plane = 15,
    _effect_event_particle_system_emitter_emission_shape_jetwash = 16,
    _effect_event_particle_system_emitter_emission_shape_planar_orbit = 17,
    _effect_event_particle_system_emitter_emission_shape_sphere_orbit = 18,
    _effect_event_particle_system_emitter_emission_shape_plane_spray = 19
};

enum e_effect_event_particle_system_emitter_flags
{
    _effect_event_particle_system_emitter_flags_volume_emitter_particle_velocities_are_random_bit = 0,
    _effect_event_particle_system_emitter_flags_clamp_particle_velocities_bit = 1,
    _effect_event_particle_system_emitter_flags_particle_emitted_inside_shape_bit = 2,
    _effect_event_particle_system_emitter_flags_override_particle_direction_bit = 3
};

enum e_tag_mapping_variable_type
{
    _tag_mapping_variable_type_particle_age = 0,
    _tag_mapping_variable_type_emitter_age = 1,
    _tag_mapping_variable_type_particle_random = 2,
    _tag_mapping_variable_type_emitter_random = 3,
    _tag_mapping_variable_type_particle_random1 = 4,
    _tag_mapping_variable_type_particle_random2 = 5,
    _tag_mapping_variable_type_particle_random3 = 6,
    _tag_mapping_variable_type_particle_random4 = 7,
    _tag_mapping_variable_type_emitter_random1 = 8,
    _tag_mapping_variable_type_emitter_random2 = 9,
    _tag_mapping_variable_type_emitter_time = 10,
    _tag_mapping_variable_type_system_lod = 11,
    _tag_mapping_variable_type_game_time = 12,
    _tag_mapping_variable_type_effect_a_scale = 13,
    _tag_mapping_variable_type_effect_b_scale = 14,
    _tag_mapping_variable_type_physics_rotation = 15,
    _tag_mapping_variable_type_location_random = 16,
    _tag_mapping_variable_type_distance_from_emitter = 17,
    _tag_mapping_variable_type_particle_simulation_a = 18,
    _tag_mapping_variable_type_particle_simulation_b = 19,
    _tag_mapping_variable_type_particle_velocity = 20,
    _tag_mapping_variable_type_invalid = 21
};

enum e_tag_mapping_output_modifier
{
    _tag_mapping_output_modifier_none = 0,
    _tag_mapping_output_modifier_plus = 1,
    _tag_mapping_output_modifier_times = 2
};

enum e_effect_event_particle_system_emitter_particle_movement_data_flags
{
    _effect_event_particle_system_emitter_particle_movement_data_flags_physics_bit = 0,
    _effect_event_particle_system_emitter_particle_movement_data_flags_collide_with_structure_bit = 1,
    _effect_event_particle_system_emitter_particle_movement_data_flags_collide_with_water_bit = 2,
    _effect_event_particle_system_emitter_particle_movement_data_flags_collide_with_scenery_bit = 3,
    _effect_event_particle_system_emitter_particle_movement_data_flags_collide_with_vehicles_bit = 4,
    _effect_event_particle_system_emitter_particle_movement_data_flags_collide_with_bipeds_bit = 5,
    _effect_event_particle_system_emitter_particle_movement_data_flags_always_collide_every_frame_bit = 6,
    _effect_event_particle_system_emitter_particle_movement_data_flags_swarm_bit = 7,
    _effect_event_particle_system_emitter_particle_movement_data_flags_wind_bit = 8,
    _effect_event_particle_system_emitter_particle_movement_data_flags_turbulence_bit = 9,
    _effect_event_particle_system_emitter_particle_movement_data_flags_global_force_bit = 10,
    _effect_event_particle_system_emitter_particle_movement_data_flags_disable_swarm_collision_bit = 11
};

enum e_effect_event_particle_system_emitter_particle_movement_data_movement_type
{
    _effect_event_particle_system_emitter_particle_movement_data_movement_type_physics = 0,
    _effect_event_particle_system_emitter_particle_movement_data_movement_type_collider = 1,
    _effect_event_particle_system_emitter_particle_movement_data_movement_type_swarm = 2,
    _effect_event_particle_system_emitter_particle_movement_data_movement_type_wind = 3,
    _effect_event_particle_system_emitter_particle_movement_data_movement_type_turbulence = 4,
    _effect_event_particle_system_emitter_particle_movement_data_movement_type_global_force = 5
};

enum e_effect_event_particle_system_emitter_particle_movement_data_movement_flags
{
    _effect_event_particle_system_emitter_particle_movement_data_movement_flags_cylinder_bit = 0,
    _effect_event_particle_system_emitter_particle_movement_data_movement_flags_plane_bit = 1
};


/* ---------- structures */

struct s_effect_location;
struct s_effect_event_part;
struct s_effect_event_acceleration;
struct s_tag_function;
struct s_tag_mapping;
struct s_effect_event_particle_system_emitter_translational_offset_data;
struct s_effect_event_particle_system_emitter_relative_direction_data;
struct s_effect_event_particle_system_emitter_particle_movement_data_movement_parameter;
struct s_effect_event_particle_system_emitter_particle_movement_data_movement;
struct s_effect_event_particle_system_emitter_particle_movement_data;
struct s_effect_event_particle_system_emitter_particle_self_acceleration_data;
struct s_effect_event_particle_system_emitter_runtime_m_gpu_data_property;
struct s_effect_event_particle_system_emitter_runtime_m_gpu_data_function;
struct s_effect_event_particle_system_emitter_runtime_m_gpu_data_color;
struct s_effect_event_particle_system_emitter_runtime_m_gpu_data;
struct s_effect_event_particle_system_emitter;
struct s_effect_event_particle_system;
struct s_effect_event;
struct s_effect_conical_distribution;
struct s_effect;

struct s_effect
{
    c_flags<e_effect_flags, long> flags;
    ulong fixed_random_seed;
    float overlap_threshold;
    float continue_if_within;
    float death_delay;
    char unknown1;
    char unknown2;
    char unknown3;
    char unknown4;
    short loop_start_event;
    c_enum<e_effect_priority, short> priority;
    ulong unknown5;
    c_tag_block<s_effect_location> locations;
    c_tag_block<s_effect_event> events;
    s_tag_reference looping_sound;
    char location_index;
    char event_index;
    short unknown11;
    float always_play_distance;
    float never_play_distance;
    float runtime_lightprobe_death_delay;
    float runtime_local_space_death_delay;
    c_tag_block<s_effect_conical_distribution> conical_distributions;
};
static_assert(sizeof(s_effect) == 0x68);

struct s_effect_conical_distribution
{
    short projection_yaw_count;
    short projection_pitch_count;
    float distribution_exponent;
    real spread;
};
static_assert(sizeof(s_effect_conical_distribution) == 0xC);

struct s_effect_event
{
    string_id name;
    long unknown;
    char unknown2;
    char unknown3;
    char unknown4;
    char unknown5;
    float skip_fraction;
    float delay_bounds_min;
    float delay_bounds_max;
    float duration_bounds_min;
    float duration_bounds_max;
    c_tag_block<s_effect_event_part> parts;
    c_tag_block<s_effect_event_acceleration> accelerations;
    c_tag_block<s_effect_event_particle_system> particle_systems;
};
static_assert(sizeof(s_effect_event) == 0x44);

struct s_effect_event_particle_system
{
    c_enum<e_effect_event_priority, char> priority;
    char game_mode;
    char unknown3;
    char unknown4;
    s_tag_reference particle;
    ulong location_index;
    c_enum<e_particle_coordinate_system, short> coordinate_system;
    c_enum<e_effect_environment, short> environment;
    c_enum<e_effect_violence_mode, short> disposition;
    c_enum<e_particle_camera_mode, short> camera_mode;
    short sort_bias;
    ushort flags;
    float pixel_budget;
    float near_range;
    float near_cutoff;
    float near_fade_override;
    float lod_in_distance;
    float lod_feather_in_delta;
    float inverse_lod_feather_in;
    float lod_out_distance;
    float lod_feather_out_delta;
    float inverse_lod_feather_out;
    c_tag_block<s_effect_event_particle_system_emitter> emitters;
    float unknown16;
};
static_assert(sizeof(s_effect_event_particle_system) == 0x5C);

struct s_effect_event_particle_system_emitter
{
    string_id name;
    c_enum<e_effect_event_particle_system_emitter_emission_shape, char> emission_shape;
    c_flags<e_effect_event_particle_system_emitter_flags, uchar> emitter_flags;
    short unknown1;
    s_tag_reference custom_shape;
    float bounding_radius_estimate;
    float bound_radius_override;
    real_point2d uv_scroll_rate;
    s_effect_event_particle_system_emitter_translational_offset_data translational_offset;
    s_effect_event_particle_system_emitter_relative_direction_data relative_direction;
    s_tag_mapping emission_radius;
    s_tag_mapping emission_angle;
    s_tag_mapping particle_starting_count;
    s_tag_mapping particle_max_count;
    s_tag_mapping particle_emission_rate;
    s_tag_mapping particle_emission_per_distance;
    s_tag_mapping particle_lifespan;
    s_effect_event_particle_system_emitter_particle_movement_data particle_movement;
    s_effect_event_particle_system_emitter_particle_self_acceleration_data particle_self_acceleration;
    s_tag_mapping particle_velocity;
    s_tag_mapping particle_angular_velocity;
    s_tag_mapping particle_mass;
    s_tag_mapping particle_drag_coefficient;
    s_tag_mapping particle_size;
    s_tag_mapping particle_tint;
    s_tag_mapping particle_alpha;
    s_tag_mapping particle_alpha_black_point;
    s_effect_event_particle_system_emitter_runtime_m_gpu_data runtime_m_gpu;
};
static_assert(sizeof(s_effect_event_particle_system_emitter) == 0x300);

struct s_effect_event_particle_system_emitter_runtime_m_gpu_data
{
    long constant_per_particle_properties;
    long constant_over_time_properties;
    long used_particle_states;
    c_tag_block<s_effect_event_particle_system_emitter_runtime_m_gpu_data_property> properties;
    c_tag_block<s_effect_event_particle_system_emitter_runtime_m_gpu_data_function> functions;
    c_tag_block<s_effect_event_particle_system_emitter_runtime_m_gpu_data_color> colors;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_runtime_m_gpu_data) == 0x30);

struct s_effect_event_particle_system_emitter_runtime_m_gpu_data_color
{
    single data[4];
};
static_assert(sizeof(s_effect_event_particle_system_emitter_runtime_m_gpu_data_color) == 0x10);

struct s_effect_event_particle_system_emitter_runtime_m_gpu_data_function
{
    single data[16];
};
static_assert(sizeof(s_effect_event_particle_system_emitter_runtime_m_gpu_data_function) == 0x40);

struct s_effect_event_particle_system_emitter_runtime_m_gpu_data_property
{
    single data[4];
};
static_assert(sizeof(s_effect_event_particle_system_emitter_runtime_m_gpu_data_property) == 0x10);

struct s_effect_event_particle_system_emitter_particle_self_acceleration_data
{
    s_tag_mapping mapping;
    real_vector3d starting_interpolant;
    real_vector3d ending_interpolant;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_particle_self_acceleration_data) == 0x38);

struct s_effect_event_particle_system_emitter_particle_movement_data
{
    s_tag_reference template;
    c_flags<e_effect_event_particle_system_emitter_particle_movement_data_flags, long> flags;
    c_tag_block<s_effect_event_particle_system_emitter_particle_movement_data_movement> movements;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_particle_movement_data) == 0x20);

struct s_effect_event_particle_system_emitter_particle_movement_data_movement
{
    c_enum<e_effect_event_particle_system_emitter_particle_movement_data_movement_type, short> tyoe;
    c_flags<e_effect_event_particle_system_emitter_particle_movement_data_movement_flags, uchar> flags;
    byte unused[1];
    c_tag_block<s_effect_event_particle_system_emitter_particle_movement_data_movement_parameter> parameters;
    long runtime_m_constant_parameters;
    long runtime_m_used_particle_states;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_particle_movement_data_movement) == 0x18);

struct s_effect_event_particle_system_emitter_particle_movement_data_movement_parameter
{
    long parameter_id;
    s_tag_mapping property;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_particle_movement_data_movement_parameter) == 0x24);

struct s_effect_event_particle_system_emitter_relative_direction_data
{
    s_tag_mapping mapping;
    real_euler_angles3d direction_at0;
    real_euler_angles3d direction_at1;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_relative_direction_data) == 0x38);

struct s_effect_event_particle_system_emitter_translational_offset_data
{
    s_tag_mapping mapping;
    real_point3d starting_interpolant;
    real_point3d ending_interpolant;
};
static_assert(sizeof(s_effect_event_particle_system_emitter_translational_offset_data) == 0x38);

struct s_tag_mapping
{
    c_enum<e_tag_mapping_variable_type, char> input_variable;
    c_enum<e_tag_mapping_variable_type, char> range_variable;
    c_enum<e_tag_mapping_output_modifier, char> output_modifier;
    c_enum<e_tag_mapping_variable_type, char> output_modifier_input;
    s_tag_function function;
    float runtime_m_constant_value;
    uchar runtime_m_flags;
    byte unused[3];
};
static_assert(sizeof(s_tag_mapping) == 0x20);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_effect_event_acceleration
{
    c_enum<e_effect_event_acceleration_create_in_environment, short> create_in_environment;
    c_enum<e_effect_event_acceleration_create_in_disposition, short> create_in_disposition;
    short location_index;
    byte unused[2];
    float acceleration_amount;
    float inner_cone_angle;
    float outer_cone_angle;
};
static_assert(sizeof(s_effect_event_acceleration) == 0x14);

struct s_effect_event_part
{
    c_enum<e_effect_environment, short> create_in_environment;
    c_enum<e_effect_violence_mode, short> create_in_disposition;
    short primary_location;
    short secondary_location;
    c_flags<e_effect_event_part_flags, ushort> flags;
    c_enum<e_effect_event_priority, char> priority;
    c_enum<e_effect_event_part_camera_mode, char> camera_mode;
    tag runtime_base_group_tag;
    s_tag_reference type;
    real_bounds velocity_bounds;
    real_euler_angles2d velocity_orientation;
    real velocity_cone_angle;
    real_bounds angular_velocity_bounds;
    real_bounds radius_modifier_bounds;
    real_point3d relative_offset;
    real_euler_angles2d relative_orientation;
    c_enum<e_effect_event_part_scales, long> a_scales_values;
    c_enum<e_effect_event_part_scales, long> b_scales_values;
};
static_assert(sizeof(s_effect_event_part) == 0x60);

struct s_effect_location
{
    string_id marker_name;
    long unknown1;
    char unknown2;
    char unknown3;
    char unknown4;
    char unknown5;
};
static_assert(sizeof(s_effect_location) == 0xC);

