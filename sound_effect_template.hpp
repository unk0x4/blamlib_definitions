/* ---------- enums */


/* ---------- structures */

struct s_tag_function;
struct s_sound_effect_template_additional_sound_input;
struct s_sound_effect_template;

struct s_sound_effect_template
{
    float template_collection_block;
    float template_collection_block2;
    float template_collection_block3;
    long input_effect_name;
    c_tag_block<s_sound_effect_template_additional_sound_input> additional_sound_inputs;
};
static_assert(sizeof(s_sound_effect_template) == 0x1C);

struct s_sound_effect_template_additional_sound_input
{
    string_id dsp_effect;
    s_tag_function low_frequency_sound_function;
    float time_period;
};
static_assert(sizeof(s_sound_effect_template_additional_sound_input) == 0x1C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

