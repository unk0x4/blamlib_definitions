/* ---------- enums */


/* ---------- structures */

struct s_sound_effect_collection_sound_effect_filter_block;
struct s_sound_effect_collection_sound_effect_pitch_lfo_block;
struct s_sound_effect_collection_sound_effect_filter_lfo_block;
struct s_sound_effect_collection_sound_effect_sound_effect_block_component;
struct s_tag_function;
struct s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block_parameter;
struct s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block;
struct s_sound_effect_collection_sound_effect_sound_effect_block;
struct s_sound_effect_collection_sound_effect;
struct s_sound_effect_collection;

struct s_sound_effect_collection
{
    c_tag_block<s_sound_effect_collection_sound_effect> sound_effects;
};
static_assert(sizeof(s_sound_effect_collection) == 0xC);

struct s_sound_effect_collection_sound_effect
{
    string_id name;
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    ulong flags;
    ulong unknown4;
    ulong unknown5;
    c_tag_block<s_sound_effect_collection_sound_effect_filter_block> filter;
    c_tag_block<s_sound_effect_collection_sound_effect_pitch_lfo_block> pitch_lfo;
    c_tag_block<s_sound_effect_collection_sound_effect_filter_lfo_block> filter_lfo;
    c_tag_block<s_sound_effect_collection_sound_effect_sound_effect_block> sound_effect2;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect) == 0x4C);

struct s_sound_effect_collection_sound_effect_sound_effect_block
{
    s_tag_reference sound_effect_template;
    c_tag_block<s_sound_effect_collection_sound_effect_sound_effect_block_component> components;
    c_tag_block<s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block> template_collection;
    ulong unknown1;
    ulong unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_sound_effect_block) == 0x48);

struct s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block
{
    string_id dsp_effect;
    c_tag_block<s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block_parameter> parameters;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block) == 0x10);

struct s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block_parameter
{
    string_id name;
    float unknown;
    float unknown2;
    float hardware_offset;
    float unknown3;
    float default_scalar_value;
    s_tag_function function;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_sound_effect_block_template_collection_block_parameter) == 0x2C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

struct s_sound_effect_collection_sound_effect_sound_effect_block_component
{
    s_tag_reference sound;
    ulong gain;
    long flags;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_sound_effect_block_component) == 0x18);

struct s_sound_effect_collection_sound_effect_filter_lfo_block
{
    real_bounds delay_scale;
    real_bounds delay_random_base_variance;
    real_bounds frequency_scale;
    real_bounds frequency_random_base_variance;
    real_bounds cutoff_modulation_scale;
    real_bounds cutoff_modulation_random_base_variance;
    real_bounds gain_modulation_scale;
    real_bounds gain_modulation_random_base_variance;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_filter_lfo_block) == 0x40);

struct s_sound_effect_collection_sound_effect_pitch_lfo_block
{
    real_bounds delay_scale;
    real_bounds delay_random_base_variance;
    real_bounds frequency_scale;
    real_bounds frequency_random_base_variance;
    real_bounds pitch_modulation_scale;
    real_bounds pitch_modulation_random_base_variance;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_pitch_lfo_block) == 0x30);

struct s_sound_effect_collection_sound_effect_filter_block
{
    long filter_type;
    long filter_width;
    real_bounds left_frequency_scale;
    real_bounds left_frequency_random_base_variance;
    real_bounds left_gain_scale;
    real_bounds left_gain_random_base_variance;
    real_bounds right_frequency_scale;
    real_bounds right_frequency_random_base_variance;
    real_bounds right_gain_scale;
    real_bounds right_gain_random_base_variance;
};
static_assert(sizeof(s_sound_effect_collection_sound_effect_filter_block) == 0x48);

