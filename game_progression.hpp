/* ---------- enums */

enum e_game_progression_map_progression_data_block_map_progression_flags
{
    _game_progression_map_progression_data_block_map_progression_flags_bit0_bit = 0,
    _game_progression_map_progression_data_block_map_progression_flags_bit1_bit = 1
};

enum e_game_progression_map_progression_data_block_map_progression
{
    _game_progression_map_progression_data_block_map_progression_default = 0,
    _game_progression_map_progression_data_block_map_progression_is_hub = 1,
    _game_progression_map_progression_data_block_map_progression_returns_to_hub = 2
};


/* ---------- structures */

struct s_game_progression_progression_variable;
struct s_game_progression_boolean_progression_region;
struct s_game_progression_integer_progression_unknown_block;
struct s_game_progression_map_progression_data_block;
struct s_game_progression;

struct s_game_progression
{
    c_tag_block<s_game_progression_progression_variable> integer_progression_value;
    c_tag_block<s_game_progression_progression_variable> boolean_progression_value;
    c_tag_block<s_game_progression_boolean_progression_region> boolean_progression_regions;
    c_tag_block<s_game_progression_integer_progression_unknown_block> integer_progression_unknown;
    c_tag_block<s_game_progression_map_progression_data_block> map_progression_data;
    byte unused[8];
};
static_assert(sizeof(s_game_progression) == 0x44);

struct s_game_progression_map_progression_data_block
{
    string_id internal_map_name;
    c_flags<e_game_progression_map_progression_data_block_map_progression_flags, long> flags;
    c_enum<e_game_progression_map_progression_data_block_map_progression, long> map_progression_type;
    s_tag_reference scenario_unused;
    long map_id;
    long campaign_id;
    char scenario_path[256];
};
static_assert(sizeof(s_game_progression_map_progression_data_block) == 0x124);

struct s_game_progression_integer_progression_unknown_block
{
    short integer_progression_index;
    byte unused[2];
};
static_assert(sizeof(s_game_progression_integer_progression_unknown_block) == 0x4);

struct s_game_progression_boolean_progression_region
{
    short start_index;
    short end_index;
};
static_assert(sizeof(s_game_progression_boolean_progression_region) == 0x4);

struct s_game_progression_progression_variable
{
    string_id variable;
};
static_assert(sizeof(s_game_progression_progression_variable) == 0x4);

