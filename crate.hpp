/* ---------- enums */

enum e_crate_crate_flags
{
    _crate_crate_flags_does_not_block_area_of_effect_bit = 0,
    _crate_crate_flags_attach_texture_camera_hack_bit = 1,
    _crate_crate_flags_targetable_bit = 2,
    _crate_crate_flags_crate_walls_block_area_of_effect_bit = 3,
    _crate_crate_flags_crate_blocks_damage_flash_damage_response_bit = 4,
    _crate_crate_flags_crate_blocks_rumble_damage_response_bit = 5,
    _crate_crate_flags_crate_takes_top_level_area_of_effect_damage_bit = 6,
    _crate_crate_flags_crate_blocks_forced_projectile_overpenetration_bit = 7,
    _crate_crate_flags_unimportant_bit = 8,
    _crate_crate_flags_always_check_children_collision_bit = 9,
    _crate_crate_flags_allow_friendly_team_to_pass_through_inside_bit = 10,
    _crate_crate_flags_allow_ally_team_to_pass_through_inside_bit = 11,
    _crate_crate_flags_allow_friendly_team_to_pass_through_outside_bit = 12,
    _crate_crate_flags_allow_ally_team_to_pass_through_outside_bit = 13,
    _crate_crate_flags_reject_all_contact_points_inside_bit = 14,
    _crate_crate_flags_reject_all_contact_points_outside_bit = 15
};

enum e_character_metagame_flags
{
    _character_metagame_flags_must_have_active_seats_bit = 0
};

enum e_character_metagame_unit
{
    _character_metagame_unit_brute = 0,
    _character_metagame_unit_grunt = 1,
    _character_metagame_unit_jackal = 2,
    _character_metagame_unit_marine = 3,
    _character_metagame_unit_bugger = 4,
    _character_metagame_unit_hunter = 5,
    _character_metagame_unit_flood_infection = 6,
    _character_metagame_unit_flood_carrier = 7,
    _character_metagame_unit_flood_combat = 8,
    _character_metagame_unit_flood_pureform = 9,
    _character_metagame_unit_sentinel = 10,
    _character_metagame_unit_elite = 11,
    _character_metagame_unit_turret = 12,
    _character_metagame_unit_mongoose = 13,
    _character_metagame_unit_warthog = 14,
    _character_metagame_unit_scorpion = 15,
    _character_metagame_unit_hornet = 16,
    _character_metagame_unit_pelican = 17,
    _character_metagame_unit_shade = 18,
    _character_metagame_unit_watchtower = 19,
    _character_metagame_unit_ghost = 20,
    _character_metagame_unit_chopper = 21,
    _character_metagame_unit_mauler = 22,
    _character_metagame_unit_wraith = 23,
    _character_metagame_unit_banshee = 24,
    _character_metagame_unit_phantom = 25,
    _character_metagame_unit_scarab = 26,
    _character_metagame_unit_guntower = 27,
    _character_metagame_unit_engineer = 28,
    _character_metagame_unit_engineer_recharge_station = 29
};

enum e_character_metagame_classification
{
    _character_metagame_classification_infantry = 0,
    _character_metagame_classification_leader = 1,
    _character_metagame_classification_hero = 2,
    _character_metagame_classification_specialist = 3,
    _character_metagame_classification_light_vehicle = 4,
    _character_metagame_classification_heavy_vehicle = 5,
    _character_metagame_classification_giant_vehicle = 6,
    _character_metagame_classification_standard_vehicle = 7
};


/* ---------- structures */

struct s_character_metagame_properties;
struct s_crate;

struct s_crate : s_game_object
{
    c_flags<e_crate_crate_flags, ushort> flags2;
    byte pad[2];
    c_tag_block<s_character_metagame_properties> metagame_properties;
    char unknown7;
    char unknown8;
    char unknown9;
    char unknown10;
};
static_assert(sizeof(s_crate) == 0x134);

struct s_character_metagame_properties
{
    c_flags<e_character_metagame_flags, uchar> flags;
    c_enum<e_character_metagame_unit, char> unit;
    c_enum<e_character_metagame_classification, char> classification;
    byte pad0[1];
    short points;
    byte pad1[2];
};
static_assert(sizeof(s_character_metagame_properties) == 0x8);

