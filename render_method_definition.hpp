/* ---------- enums */


/* ---------- structures */

struct s_render_method_definition_method_shader_option;
struct s_render_method_definition_method;
struct s_render_method_definition_draw_mode_unknown_block2_unknown_block;
struct s_render_method_definition_draw_mode_unknown_block2;
struct s_render_method_definition_draw_mode;
struct s_render_method_definition_vertex_block;
struct s_render_method_definition;

struct s_render_method_definition
{
    s_tag_reference render_method_options;
    c_tag_block<s_render_method_definition_method> methods;
    c_tag_block<s_render_method_definition_draw_mode> draw_modes;
    c_tag_block<s_render_method_definition_vertex_block> vertices;
    s_tag_reference global_pixel_shader;
    s_tag_reference global_vertex_shader;
    long unknown2;
    long unknown3;
};
static_assert(sizeof(s_render_method_definition) == 0x5C);

struct s_render_method_definition_vertex_block
{
    short vertex_type;
    short unknown2;
    ulong unknown3;
    ulong unknown4;
    ulong unknown5;
};
static_assert(sizeof(s_render_method_definition_vertex_block) == 0x10);

struct s_render_method_definition_draw_mode
{
    long mode;
    c_tag_block<s_render_method_definition_draw_mode_unknown_block2> unknown2;
};
static_assert(sizeof(s_render_method_definition_draw_mode) == 0x10);

struct s_render_method_definition_draw_mode_unknown_block2
{
    ulong unknown;
    c_tag_block<s_render_method_definition_draw_mode_unknown_block2_unknown_block> unknown2;
};
static_assert(sizeof(s_render_method_definition_draw_mode_unknown_block2) == 0x10);

struct s_render_method_definition_draw_mode_unknown_block2_unknown_block
{
    ulong unknown;
};
static_assert(sizeof(s_render_method_definition_draw_mode_unknown_block2_unknown_block) == 0x4);

struct s_render_method_definition_method
{
    string_id type;
    c_tag_block<s_render_method_definition_method_shader_option> shader_options;
    string_id vertex_shader_method_macro_name;
    string_id pixel_shader_method_macro_name;
};
static_assert(sizeof(s_render_method_definition_method) == 0x18);

struct s_render_method_definition_method_shader_option
{
    string_id type;
    s_tag_reference option;
    string_id vertex_shader_option_macro_name;
    string_id pixel_shader_option_macro_name;
};
static_assert(sizeof(s_render_method_definition_method_shader_option) == 0x1C);

