/* ---------- enums */


/* ---------- structures */

struct s_sound_scenery;

struct s_sound_scenery : s_game_object
{
    real_bounds distance;
    real_bounds cone_angle;
    byte unused2[12];
};
static_assert(sizeof(s_sound_scenery) == 0x13C);

