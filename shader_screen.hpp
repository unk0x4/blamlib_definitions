/* ---------- enums */


/* ---------- structures */

struct s_shader_screen;

struct s_shader_screen : s_render_method
{
    string_id material;
    uchar layer;
    uchar sorting_order;
    uchar flags;
    byte padding[1];
};
static_assert(sizeof(s_shader_screen) == 0x48);

