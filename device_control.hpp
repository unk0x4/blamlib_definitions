/* ---------- enums */

enum e_device_control_type
{
    _device_control_type_toggle = 0,
    _device_control_type_on = 1,
    _device_control_type_off = 2,
    _device_control_type_call = 3,
    _device_control_type_generator = 4
};

enum e_device_control_triggers_when
{
    _device_control_triggers_when_touched_by_player = 0,
    _device_control_triggers_when_destroyed = 1
};


/* ---------- structures */

struct s_device_control;

struct s_device_control : s_device
{
    c_enum<e_device_control_type, short> type;
    c_enum<e_device_control_triggers_when, short> triggers_when;
    float call_value;
    string_id action_string;
    s_tag_reference on;
    s_tag_reference off;
    s_tag_reference deny;
};
static_assert(sizeof(s_device_control) == 0x1F4);

