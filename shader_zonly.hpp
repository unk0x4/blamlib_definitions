/* ---------- enums */


/* ---------- structures */

struct s_shader_zonly;

struct s_shader_zonly : s_render_method
{
    string_id material;
    ulong unknown9;
};
static_assert(sizeof(s_shader_zonly) == 0x48);

