/* ---------- enums */


/* ---------- structures */

struct s_podium_settings_unknown_block;
struct s_podium_settings;

struct s_podium_settings
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    real unknown4;
    real unknown5;
    ulong unknown6;
    real unknown7;
    long unknown8;
    s_tag_reference unknown9;
    c_tag_block<s_podium_settings_unknown_block> unknown10;
};
static_assert(sizeof(s_podium_settings) == 0x3C);

struct s_podium_settings_unknown_block
{
    ulong unknown;
    ulong unknown2;
    ulong unknown3;
    real unknown4;
    ulong unknown5;
    ulong unknown6;
    long unknown7;
    s_tag_reference effect;
};
static_assert(sizeof(s_podium_settings_unknown_block) == 0x2C);

