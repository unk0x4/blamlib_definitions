/* ---------- enums */

enum e_lens_flare_unknown_flags
{
    _lens_flare_unknown_flags_bit0_bit = 0,
    _lens_flare_unknown_flags_bit1_bit = 1
};

enum e_lens_flare_occlusion_offset_direction
{
    _lens_flare_occlusion_offset_direction_toward_viewer = 0,
    _lens_flare_occlusion_offset_direction_marker_forward = 1,
    _lens_flare_occlusion_offset_direction_none = 2
};

enum e_lens_flare_flags
{
    _lens_flare_flags_rotate_occlusion_testing_box_with_lens_flare_bit = 0,
    _lens_flare_flags_no_occlusion_test_bit = 1,
    _lens_flare_flags_only_render_in_first_person_bit = 2,
    _lens_flare_flags_only_render_in_third_person_bit = 3,
    _lens_flare_flags_no_reflection_opacity_feedback_bit = 4,
    _lens_flare_flags_scale_by_marker_bit = 5,
    _lens_flare_flags_do_not_autofade_bit = 6,
    _lens_flare_flags_do_not_render_while_zoomed_bit = 7,
    _lens_flare_flags_use_separate_x_and_y_falloff_angles_bit = 8
};

enum e_lens_flare_rotation_function
{
    _lens_flare_rotation_function_none = 0,
    _lens_flare_rotation_function_rotation_a = 1,
    _lens_flare_rotation_function_rotation_b = 2,
    _lens_flare_rotation_function_rotation_translation = 3,
    _lens_flare_rotation_function_translation = 4
};

enum e_lens_flare_falloff_function
{
    _lens_flare_falloff_function_linear = 0,
    _lens_flare_falloff_function_late = 1,
    _lens_flare_falloff_function_very_late = 2,
    _lens_flare_falloff_function_early = 3,
    _lens_flare_falloff_function_very_early = 4,
    _lens_flare_falloff_function_cosine = 5,
    _lens_flare_falloff_function_zero = 6,
    _lens_flare_falloff_function_one = 7
};

enum e_lens_flare_reflection_flags
{
    _lens_flare_reflection_flags_align_rotation_with_screen_center_bit = 0,
    _lens_flare_reflection_flags_radius_not_scaled_by_distance_bit = 1,
    _lens_flare_reflection_flags_radius_scaled_by_occlusion_factor_bit = 2,
    _lens_flare_reflection_flags_occluded_by_solid_objects_bit = 3,
    _lens_flare_reflection_flags_ignore_light_color_bit = 4,
    _lens_flare_reflection_flags_not_affected_by_inner_occlusion_bit = 5
};

enum e_lens_flare_animation_flags
{
    _lens_flare_animation_flags_synchronized_bit = 0
};

enum e_lens_flare_color_block_output_modifier
{
    _lens_flare_color_block_output_modifier_none = 0,
    _lens_flare_color_block_output_modifier_add = 1,
    _lens_flare_color_block_output_modifier_multiply = 2
};


/* ---------- structures */

struct s_tag_function;
struct s_lens_flare_reflection;
struct s_lens_flare_brightness_block;
struct s_lens_flare_color_block;
struct s_lens_flare_rotation_block;
struct s_lens_flare;

struct s_lens_flare
{
    real falloff_angle;
    real cutoff_angle;
    long occlusion_reflection_index;
    float occlusion_radius;
    c_flags<e_lens_flare_unknown_flags, short> unknown_flags;
    c_enum<e_lens_flare_occlusion_offset_direction, short> occlusion_offset_direction;
    ulong unknown1;
    ulong unknown2;
    float near_fade_distance;
    float far_fade_distance;
    s_tag_reference bitmap;
    c_flags<e_lens_flare_flags, ushort> flags;
    short runtime_flags;
    c_enum<e_lens_flare_rotation_function, short> rotation_function;
    byte unused2[2];
    real rotation_function_scale;
    c_enum<e_lens_flare_falloff_function, short> falloff_function;
    byte unused3[2];
    c_tag_block<s_lens_flare_reflection> reflections;
    c_flags<e_lens_flare_animation_flags, ushort> animation_flags;
    byte unused4[2];
    c_tag_block<s_lens_flare_brightness_block> time_brightness;
    c_tag_block<s_lens_flare_brightness_block> age_brightness;
    c_tag_block<s_lens_flare_color_block> time_color;
    c_tag_block<s_lens_flare_color_block> age_color;
    c_tag_block<s_lens_flare_rotation_block> time_rotation;
    c_tag_block<s_lens_flare_rotation_block> age_rotation;
};
static_assert(sizeof(s_lens_flare) == 0x9C);

struct s_lens_flare_rotation_block
{
    s_tag_function function;
};
static_assert(sizeof(s_lens_flare_rotation_block) == 0x14);

struct s_lens_flare_color_block
{
    string_id input_variable;
    string_id range_variable;
    c_enum<e_lens_flare_color_block_output_modifier, short> output_modifier;
    byte unused[2];
    string_id output_modifier_input;
    s_tag_function function;
};
static_assert(sizeof(s_lens_flare_color_block) == 0x24);

struct s_lens_flare_brightness_block
{
    s_tag_function function;
};
static_assert(sizeof(s_lens_flare_brightness_block) == 0x14);

struct s_lens_flare_reflection
{
    c_flags<e_lens_flare_reflection_flags, ushort> flags;
    short bitmap_index;
    ulong unknown2;
    s_tag_reference bitmap_override;
    float rotation_offset_ho;
    float position_flare_axis;
    real_bounds offset_bounds;
    s_tag_function radius_curve_function;
    s_tag_function scale_curve_x_function;
    s_tag_function scale_curve_y_function;
    s_tag_function brightness_curve_function;
    real_rgb_color tint_color;
    float tint_modulation_factor_ho;
    float tint_power;
};
static_assert(sizeof(s_lens_flare_reflection) == 0x8C);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

