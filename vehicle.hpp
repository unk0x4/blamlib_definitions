/* ---------- enums */

enum e_vehicle_vehicle_flag_bits_gen3_bits
{
    _vehicle_vehicle_flag_bits_gen3_bits_no_friction_with_driver_bit = 0,
    _vehicle_vehicle_flag_bits_gen3_bits_can_trigger_automatic_opening_doors_bit = 1,
    _vehicle_vehicle_flag_bits_gen3_bits_autoaim_when_teamless_bit = 2,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_weapon_cannot_rotate_bit = 3,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_does_not_require_driver_bit = 4,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_driver_enable_bit = 5,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_driver_flying_bit = 6,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_driver_can_sidestep_bit = 7,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_driver_hovering_bit = 8,
    _vehicle_vehicle_flag_bits_gen3_bits_noncombat_vehicle_bit = 9,
    _vehicle_vehicle_flag_bits_gen3_bits_does_not_cause_collision_damage_bit = 10,
    _vehicle_vehicle_flag_bits_gen3_bits_ai_auto_turret_bit = 11,
    _vehicle_vehicle_flag_bits_gen3_bits_hydraulics_bit = 12,
    _vehicle_vehicle_flag_bits_gen3_bits_ignore_kill_volumes_bit = 13,
    _vehicle_vehicle_flag_bits_gen3_bits_targetable_when_open_bit = 14,
    _vehicle_vehicle_flag_bits_gen3_bits_reduce_weapon_accel_when_on_ground_bit = 15,
    _vehicle_vehicle_flag_bits_gen3_bits_reduce_weapon_accel_when_airborne_bit = 16,
    _vehicle_vehicle_flag_bits_gen3_bits_do_not_force_units_to_exit_when_upside_down_bit = 17,
    _vehicle_vehicle_flag_bits_gen3_bits_creates_enemy_spawn_influencers_bit = 18,
    _vehicle_vehicle_flag_bits_gen3_bits_driver_cannot_take_damage_bit = 19,
    _vehicle_vehicle_flag_bits_gen3_bits_player_cannot_flip_vehicle_bit = 20,
    _vehicle_vehicle_flag_bits_gen3_bits_do_not_kill_riders_at_terminal_velocity_bit = 21,
    _vehicle_vehicle_flag_bits_gen3_bits_riders_use_radio_only_bit = 22,
    _vehicle_vehicle_flag_bits_gen3_bits_treat_dual_wielded_weapon_as_secondary_weapon_bit = 23
};

enum e_vehicle_vehicle_scout_physics_flags
{
    _vehicle_vehicle_scout_physics_flags_hovercraft_bit = 0,
    _vehicle_vehicle_scout_physics_flags_slope_scales_speed_bit = 1
};

enum e_vehicle_walker_physics_leg_leg_flags
{
    _vehicle_walker_physics_leg_leg_flags_contrained_plant_bit = 0
};

enum e_vehicle_havok_vehicle_physics_flags_gen3_bits
{
    _vehicle_havok_vehicle_physics_flags_gen3_bits_has_suspension_bit = 0,
    _vehicle_havok_vehicle_physics_flags_gen3_bits_friction_points_test_only_environments_bit = 1
};

enum e_vehicle_anti_gravity_point_flags
{
    _vehicle_anti_gravity_point_flags_gets_damage_from_region_bit = 0,
    _vehicle_anti_gravity_point_flags_only_active_on_water_bit = 1
};

enum e_vehicle_friction_point_flags
{
    _vehicle_friction_point_flags_gets_damage_from_region_bit = 0,
    _vehicle_friction_point_flags_powered_bit = 1,
    _vehicle_friction_point_flags_front_turning_bit = 2,
    _vehicle_friction_point_flags_rear_turning_bit = 3,
    _vehicle_friction_point_flags_attached_to_e_brake_bit = 4,
    _vehicle_friction_point_flags_can_be_destroyed_bit = 5
};

enum e_vehicle_friction_point_friction_type
{
    _vehicle_friction_point_friction_type_point = 0,
    _vehicle_friction_point_friction_type_forward = 1
};

enum e_vehicle_friction_point_model_state_destroyed
{
    _vehicle_friction_point_model_state_destroyed_default = 0,
    _vehicle_friction_point_model_state_destroyed_minor_damage = 1,
    _vehicle_friction_point_model_state_destroyed_medium_damage = 2,
    _vehicle_friction_point_model_state_destroyed_major_damage = 3,
    _vehicle_friction_point_model_state_destroyed_destroyed = 4
};

enum e_vehicle_player_training_vehicle_type
{
    _vehicle_player_training_vehicle_type_none = 0,
    _vehicle_player_training_vehicle_type_warthog = 1,
    _vehicle_player_training_vehicle_type_warthog_turret = 2,
    _vehicle_player_training_vehicle_type_ghost = 3,
    _vehicle_player_training_vehicle_type_banshee = 4,
    _vehicle_player_training_vehicle_type_tank = 5,
    _vehicle_player_training_vehicle_type_wraith = 6
};

enum e_vehicle_vehicle_size
{
    _vehicle_vehicle_size_small = 0,
    _vehicle_vehicle_size_large = 1
};


/* ---------- structures */

struct s_vehicle_vehicle_flag_bits;
struct s_vehicle_gear;
struct s_vehicle_engine_physics;
struct s_vehicle_human_tank_physics;
struct s_vehicle_vehicle_steering_control;
struct s_vehicle_vehicle_turning_control;
struct s_vehicle_human_jeep_physics;
struct s_vehicle_vehicle_velocity_control;
struct s_vehicle_vehicle_steering_animation;
struct s_vehicle_human_plane_physics;
struct s_vehicle_alien_scout_gravity_function;
struct s_vehicle_alien_scout_physics;
struct s_vehicle_alien_fighter_physics;
struct s_vehicle_turret_physics;
struct s_vehicle_walker_physics_leg;
struct s_vehicle_walker_physics_definition;
struct s_vehicle_vehicle_mantis;
struct s_vehicle_vtol_physics;
struct s_vehicle_chopper_physics;
struct s_vehicle_guardian_physics;
struct s_vehicle_vehicle_physics_types;
struct s_vehicle_havok_vehicle_physics_flags;
struct s_vehicle_anti_gravity_point;
struct s_vehicle_friction_point;
struct s_vehicle_phantom_shape;
struct s_vehicle_havok_vehicle_physics;
struct s_vehicle;

struct s_vehicle : s_unit
{
    s_vehicle_vehicle_flag_bits vehicle_flags;
    s_vehicle_vehicle_physics_types physics_types;
    s_vehicle_havok_vehicle_physics havok_physics_new;
    c_enum<e_vehicle_player_training_vehicle_type, char> player_training_vehicle_type;
    c_enum<e_vehicle_vehicle_size, char> vehicle_size;
    char complex_suspension_sample_count;
    byte unused4[1];
    real_bounds flipping_angular_velocity_range_new;
    float crouch_transition_time;
    float hoojytsu;
    float seat_entrance_acceleration_scale;
    float seat_exit_acceleration_scale;
    float flip_time_new;
    string_id flip_over_message_new;
    s_tag_reference suspension_sound;
    s_tag_reference special_effect;
    s_tag_reference driver_boost_damage_effect_or_response;
    s_tag_reference rider_boost_damage_effect_or_response;
    float unknown31;
    float unknown32;
};
static_assert(sizeof(s_vehicle) == 0x530);

struct s_vehicle_havok_vehicle_physics
{
    s_vehicle_havok_vehicle_physics_flags flags;
    float ground_friction;
    float ground_depth;
    float ground_damp_factor;
    float ground_moving_friction;
    float ground_slope_to_stop_all_traction;
    float ground_slope_to_start_traction_loss;
    float maximum_normal_force_contribution;
    float anti_gravity_bank_lift;
    float steering_bank_reaction_scale;
    float gravity_scale;
    float radius;
    float unknown5;
    float unknown6;
    float unknown7;
    c_tag_block<s_vehicle_anti_gravity_point> anti_gravity_points;
    c_tag_block<s_vehicle_friction_point> friction_points;
    c_tag_block<s_vehicle_phantom_shape> phantom_shapes;
};
static_assert(sizeof(s_vehicle_havok_vehicle_physics) == 0x60);

struct s_vehicle_phantom_shape
{
    long unknown;
    short size;
    short count;
    long overall_shape_index;
    long offset;
    float unknown2;
    float unknown3;
    float unknown4;
    long unknown5;
    float unknown6;
    float unknown7;
    float unknown8;
    float unknown9;
    float unknown10;
    float unknown11;
    float unknown12;
    float unknown13;
    float unknown14;
    float unknown15;
    float unknown16;
    float unknown17;
    long multisphere_count;
    ulong flags;
    float x_0;
    float x_1;
    float y_0;
    float y_1;
    float z_0;
    float z_1;
    long unknown18;
    short size2;
    short count2;
    long overall_shape_index2;
    long offset2;
    long number_of_spheres;
    float unknown19;
    float unknown20;
    float unknown21;
    float sphere0_x;
    float sphere0_y;
    float sphere0_z;
    float sphere0_radius;
    float sphere1_x;
    float sphere1_y;
    float sphere1_z;
    float sphere1_radius;
    float sphere2_x;
    float sphere2_y;
    float sphere2_z;
    float sphere2_radius;
    float sphere3_x;
    float sphere3_y;
    float sphere3_z;
    float sphere3_radius;
    float sphere4_x;
    float sphere4_y;
    float sphere4_z;
    float sphere4_radius;
    float sphere5_x;
    float sphere5_y;
    float sphere5_z;
    float sphere5_radius;
    float sphere6_x;
    float sphere6_y;
    float sphere6_z;
    float sphere6_radius;
    float sphere7_x;
    float sphere7_y;
    float sphere7_z;
    float sphere7_radius;
    long unknown22;
    short size3;
    short count3;
    long overall_shape_index3;
    long offset3;
    long number_of_spheres2;
    float unknown23;
    float unknown24;
    float unknown25;
    float sphere0_x_2;
    float sphere0_y_2;
    float sphere0_z_2;
    float sphere0_radius2;
    float sphere1_x_2;
    float sphere1_y_2;
    float sphere1_z_2;
    float sphere1_radius2;
    float sphere2_x_2;
    float sphere2_y_2;
    float sphere2_z_2;
    float sphere2_radius2;
    float sphere3_x_2;
    float sphere3_y_2;
    float sphere3_z_2;
    float sphere3_radius2;
    float sphere4_x_2;
    float sphere4_y_2;
    float sphere4_z_2;
    float sphere4_radius2;
    float sphere5_x_2;
    float sphere5_y_2;
    float sphere5_z_2;
    float sphere5_radius2;
    float sphere6_x_2;
    float sphere6_y_2;
    float sphere6_z_2;
    float sphere6_radius2;
    float sphere7_x_2;
    float sphere7_y_2;
    float sphere7_z_2;
    float sphere7_radius2;
    long unknown26;
    short size4;
    short count4;
    long overall_shape_index4;
    long offset4;
    long number_of_spheres3;
    float unknown27;
    float unknown28;
    float unknown29;
    float sphere0_x_3;
    float sphere0_y_3;
    float sphere0_z_3;
    float sphere0_radius3;
    float sphere1_x_3;
    float sphere1_y_3;
    float sphere1_z_3;
    float sphere1_radius3;
    float sphere2_x_3;
    float sphere2_y_3;
    float sphere2_z_3;
    float sphere2_radius3;
    float sphere3_x_3;
    float sphere3_y_3;
    float sphere3_z_3;
    float sphere3_radius3;
    float sphere4_x_3;
    float sphere4_y_3;
    float sphere4_z_3;
    float sphere4_radius3;
    float sphere5_x_3;
    float sphere5_y_3;
    float sphere5_z_3;
    float sphere5_radius3;
    float sphere6_x_3;
    float sphere6_y_3;
    float sphere6_z_3;
    float sphere6_radius3;
    float sphere7_x_3;
    float sphere7_y_3;
    float sphere7_z_3;
    float sphere7_radius3;
    long unknown30;
    short size5;
    short count5;
    long overall_shape_index5;
    long offset5;
    long number_of_spheres4;
    float unknown31;
    float unknown32;
    float unknown33;
    float sphere0_x_4;
    float sphere0_y_4;
    float sphere0_z_4;
    float sphere0_radius4;
    float sphere1_x_4;
    float sphere1_y_4;
    float sphere1_z_4;
    float sphere1_radius4;
    float sphere2_x_4;
    float sphere2_y_4;
    float sphere2_z_4;
    float sphere2_radius4;
    float sphere3_x_4;
    float sphere3_y_4;
    float sphere3_z_4;
    float sphere3_radius4;
    float sphere4_x_4;
    float sphere4_y_4;
    float sphere4_z_4;
    float sphere4_radius4;
    float sphere5_x_4;
    float sphere5_y_4;
    float sphere5_z_4;
    float sphere5_radius4;
    float sphere6_x_4;
    float sphere6_y_4;
    float sphere6_z_4;
    float sphere6_radius4;
    float sphere7_x_4;
    float sphere7_y_4;
    float sphere7_z_4;
    float sphere7_radius4;
    float unknown34;
    float unknown35;
    float unknown36;
    float unknown37;
    float unknown38;
    float unknown39;
    float unknown40;
    float unknown41;
    float unknown42;
    float unknown43;
    float unknown44;
    float unknown45;
    float unknown46;
    float unknown47;
    float unknown48;
    float unknown49;
};
static_assert(sizeof(s_vehicle_phantom_shape) == 0x330);

struct s_vehicle_friction_point
{
    string_id marker_name;
    c_flags<e_vehicle_friction_point_flags, long> flags;
    float fraction_of_total_mass;
    float radius;
    float damaged_radius;
    c_enum<e_vehicle_friction_point_friction_type, short> friction_type;
    short unknown;
    float moving_friction_velocity_diff;
    float e_brake_moving_friction;
    float e_brake_friction;
    float e_brake_moving_friction_velocity_diff;
    float unknown2;
    float unknown3;
    float unknown4;
    float unknown5;
    float unknown6;
    string_id collision_material_name;
    short collision_global_material_index;
    c_enum<e_vehicle_friction_point_model_state_destroyed, short> model_state_destroyed;
    string_id region_name;
    long region_index;
};
static_assert(sizeof(s_vehicle_friction_point) == 0x4C);

struct s_vehicle_anti_gravity_point
{
    string_id marker_name;
    c_flags<e_vehicle_anti_gravity_point_flags, long> flags;
    float antigrav_strength;
    float antigrav_height;
    float antigrav_damp_factor;
    float antigrav_extension_damping;
    float antigrav_normal_k_1;
    float antigrav_normal_k_0;
    float radius;
    byte unused1[12];
    byte unused2[2];
    short damage_source_region_index;
    string_id damage_source_region_name;
    float default_state_error;
    float minor_damage_error;
    float medium_damage_error;
    float major_damage_error;
    float destroyed_state_error;
};
static_assert(sizeof(s_vehicle_anti_gravity_point) == 0x4C);

struct s_vehicle_havok_vehicle_physics_flags
{
    c_flags<e_vehicle_havok_vehicle_physics_flags_gen3_bits, long> gen3;
};
static_assert(sizeof(s_vehicle_havok_vehicle_physics_flags) == 0x4);

struct s_vehicle_vehicle_physics_types
{
    c_tag_block<s_vehicle_human_tank_physics> human_tank;
    c_tag_block<s_vehicle_human_jeep_physics> human_jeep;
    c_tag_block<s_vehicle_human_plane_physics> human_plane;
    c_tag_block<s_vehicle_alien_scout_physics> alien_scout;
    c_tag_block<s_vehicle_alien_fighter_physics> alien_fighter;
    c_tag_block<s_vehicle_turret_physics> turret;
    c_tag_block<s_vehicle_vehicle_mantis> mantis;
    c_tag_block<s_vehicle_vtol_physics> vtol;
    c_tag_block<s_vehicle_chopper_physics> chopper;
    c_tag_block<s_vehicle_guardian_physics> guardian;
};
static_assert(sizeof(s_vehicle_vehicle_physics_types) == 0x78);

struct s_vehicle_guardian_physics
{
    s_vehicle_vehicle_steering_control steering_control;
    s_vehicle_vehicle_velocity_control velocity_control;
    float torque_scale;
    float anti_gravity_force_z_offset;
};
static_assert(sizeof(s_vehicle_guardian_physics) == 0x30);

struct s_vehicle_chopper_physics
{
    s_vehicle_vehicle_steering_control steering_control;
    s_vehicle_vehicle_turning_control turning_control;
    s_vehicle_engine_physics engine;
    float wheel_circumference;
    string_id rotation_marker;
    float magic_turning_scale;
    float magic_turning_acceleration;
    float magic_turning_maximum_velocity;
    float magic_turning_exponent;
    float bank_to_slide_ratio;
    float bank_slide_exponent;
    float bank_to_turn_ratio;
    float bank_turn_exponent;
    float bank_fraction;
    float bank_rate;
    float wheel_acceleration;
    float gyroscopic_damping;
};
static_assert(sizeof(s_vehicle_chopper_physics) == 0x70);

struct s_vehicle_vtol_physics
{
    s_vehicle_vehicle_turning_control turning;
    string_id left_lift_marker;
    string_id right_lift_marker;
    string_id thrust_marker;
    real pitch_up_range_min;
    real pitch_up_range_max;
    real pitch_down_range_min;
    real pitch_down_range_max;
    float lift_distance;
    float maximum_upward_speed;
    float maximum_down_acceleration;
    float maximum_up_acceleration;
    float maximum_turn_acceleration;
    float turn_acceleration_gain;
    float rotor_dampening;
    float maximum_left_acceleration;
    float maximum_forward_acceleration;
    float lift_arm_pivot_length;
    float drag_coefficient;
    float constant_deceleration;
    float magic_angular_acceleration_exponent;
    float magic_angular_acceleration_scale;
    float magic_angular_acceleration_k;
    real lift_angles_acceleration;
    real render_lift_angles_acceleration;
    float propeller_rotation_speed_min;
    float propeller_rotation_speed_max;
};
static_assert(sizeof(s_vehicle_vtol_physics) == 0x74);

struct s_vehicle_vehicle_mantis
{
    s_vehicle_vehicle_steering_control steering_contol;
    s_vehicle_vehicle_turning_control turning_control;
    s_vehicle_vehicle_velocity_control velocity_control;
    s_vehicle_walker_physics_definition walker_physics;
};
static_assert(sizeof(s_vehicle_vehicle_mantis) == 0xAC);

struct s_vehicle_walker_physics_definition
{
    real_vector3d maximum_leg_motion;
    float maximum_turn;
    c_tag_block<s_vehicle_walker_physics_leg> legs;
    float leg_apex_draction;
    float lift_exponent;
    float drop_exponent;
    real_vector3d object_space_pivot_position;
    float walk_cycle_pause;
    short stable_planted_legs;
    uchar unused_padding;
    float time_without_plant_buffer;
    float not_along_up_gravity_scale;
    float speed_acceleration_limit;
    float speed_acceleration_match_scale;
    float slide_acceleration_limit;
    float slide_acceleration_match_scale;
    float turn_acceleration_limit;
    float turn_acceleration_match_scale;
    float jump_set_time;
    float jump_set_interpolation_fraction;
    float jump_leap_time;
    float jump_recovery_time;
    float jump_recovery_fraction;
    float jump_leg_set_distance;
    float jump_leg_distance;
};
static_assert(sizeof(s_vehicle_walker_physics_definition) == 0x78);

struct s_vehicle_walker_physics_leg
{
    uchar leg_group;
    uchar leg_side;
    uchar leg_side_order;
    uchar valid;
    string_id hip_node_a_name;
    string_id hip_node_b_name;
    string_id knee_node_a_name;
    string_id knee_node_b_name;
    string_id foot_marker_name;
    byte unused_padding[60];
    c_flags<e_vehicle_walker_physics_leg_leg_flags, long> flags;
    real_vector3d runtime_initial_origin_to_hip_offset;
    real_vector3d runtime_pivot_center_to_hip_offset;
    float runtime_upper_leg_length;
    float runtime_lower_leg_length;
    short runtime_hip_node_a_index;
    short runtime_hip_node_b_index;
    short runtime_knee_node_a_index;
    short runtime_knee_node_b_index;
    short runtime_foot_marker_group_index;
    short runtime_foot_node_index;
    short runtime_hip_node_index;
    short runtime_knee_node_index;
    real_vector3d plant_constraint_position;
    byte unused_padding1[12];
};
static_assert(sizeof(s_vehicle_walker_physics_leg) == 0xA0);

struct s_vehicle_turret_physics
{
    float unknown1;
    float unknown2;
};
static_assert(sizeof(s_vehicle_turret_physics) == 0x8);

struct s_vehicle_alien_fighter_physics
{
    s_vehicle_vehicle_steering_control steering;
    s_vehicle_vehicle_turning_control turning;
    s_vehicle_vehicle_velocity_control velocity_control;
    float slide_accel_against_direction;
    float flying_torque_scale;
    real_euler_angles2d fixed_gun_offset;
    float loop_trick_duration;
    float roll_trick_duration;
    float zero_gravity_speed;
    float full_gravity_speed;
    float strafe_boost_scale;
    float off_stick_decel_scale;
    float cruising_throttle;
    float dive_speed_scale;
    byte unused[4];
};
static_assert(sizeof(s_vehicle_alien_fighter_physics) == 0x68);

struct s_vehicle_alien_scout_physics
{
    s_vehicle_vehicle_steering_control steering;
    s_vehicle_vehicle_velocity_control velocity_control;
    c_flags<e_vehicle_vehicle_scout_physics_flags, uchar> flags;
    byte unused[3];
    float drag_coefficient;
    float constant_deceleration;
    float torque_scale;
    s_vehicle_alien_scout_gravity_function engine_gravity_function;
    s_vehicle_alien_scout_gravity_function contrail_object_function;
    real_bounds gear_rotation_speed;
    s_vehicle_vehicle_steering_animation steering_animation;
};
static_assert(sizeof(s_vehicle_alien_scout_physics) == 0x70);

struct s_vehicle_alien_scout_gravity_function
{
    string_id object_function_damage_region;
    real_bounds anti_gravity_engine_speed_range;
    float engine_speed_acceleration;
    float maximum_vehicle_speed;
};
static_assert(sizeof(s_vehicle_alien_scout_gravity_function) == 0x14);

struct s_vehicle_human_plane_physics
{
    s_vehicle_vehicle_velocity_control velocity_control;
    float maximum_up_rise;
    float maximum_down_rise;
    float rise_acceleration;
    float rise_deceleration;
    float flying_torque_scale;
    float air_friction_deceleration;
    float thrust_scale;
    float turn_rate_scale_when_boosting;
    real maximum_roll;
    s_vehicle_vehicle_steering_animation steering_animation;
};
static_assert(sizeof(s_vehicle_human_plane_physics) == 0x4C);

struct s_vehicle_vehicle_steering_animation
{
    float interpolation_scale;
    real maximum_angle;
};
static_assert(sizeof(s_vehicle_vehicle_steering_animation) == 0x8);

struct s_vehicle_vehicle_velocity_control
{
    float maximum_forward_speed;
    float maximum_reverse_speed;
    float speed_acceleration;
    float speed_deceleration;
    float maximum_left_slide;
    float maximum_right_slide;
    float slide_acceleration;
    float slide_deceleration;
};
static_assert(sizeof(s_vehicle_vehicle_velocity_control) == 0x20);

struct s_vehicle_human_jeep_physics
{
    s_vehicle_vehicle_steering_control steering;
    s_vehicle_vehicle_turning_control turning;
    s_vehicle_engine_physics engine;
    float wheel_circumference;
    float gravity_adjust;
};
static_assert(sizeof(s_vehicle_human_jeep_physics) == 0x40);

struct s_vehicle_vehicle_turning_control
{
    float maximum_left_turn;
    float maximum_right_turn;
    float turn_rate;
};
static_assert(sizeof(s_vehicle_vehicle_turning_control) == 0xC);

struct s_vehicle_vehicle_steering_control
{
    real overdampen_cusp_angle_new;
    float overdampen_exponent;
};
static_assert(sizeof(s_vehicle_vehicle_steering_control) == 0x8);

struct s_vehicle_human_tank_physics
{
    real forward_arc;
    float flip_window;
    float pegged_fraction;
    float maximum_left_differential;
    float maximum_right_differential;
    float differential_acceleration;
    float differential_deceleration;
    float maximum_left_reverse_differential;
    float maximum_right_reverse_differential;
    float differential_reverse_acceleration;
    float differential_reverse_deceleration;
    s_vehicle_engine_physics engine;
    float wheel_circumference;
    float gravity_adjust;
};
static_assert(sizeof(s_vehicle_human_tank_physics) == 0x58);

struct s_vehicle_engine_physics
{
    float engine_momentum;
    float engine_maximum_angular_velocity;
    c_tag_block<s_vehicle_gear> gears;
    s_tag_reference gear_shift_sound;
};
static_assert(sizeof(s_vehicle_engine_physics) == 0x24);

struct s_vehicle_gear
{
    float min_loaded_torque;
    float max_loaded_torque;
    float peak_loaded_torque_scale;
    float past_peak_loaded_torque_exponent;
    float loaded_torque_at_max_angular_velocity;
    float loaded_torque_at2_x_max_angular_velocity;
    float min_cruising_torque;
    float max_cruising_torque;
    float peak_cruising_torque_scale;
    float past_peak_cruising_torque_exponent;
    float cruising_torque_at_max_angular_velocity;
    float cruising_torque_at2_x_max_angular_velocity;
    float min_time_to_upshift;
    float engine_upshift_scale;
    float gear_ratio;
    float min_time_to_downshift;
    float engine_downshift_scale;
};
static_assert(sizeof(s_vehicle_gear) == 0x44);

struct s_vehicle_vehicle_flag_bits
{
    c_flags<e_vehicle_vehicle_flag_bits_gen3_bits, long> gen3;
};
static_assert(sizeof(s_vehicle_vehicle_flag_bits) == 0x4);

