/* ---------- enums */


/* ---------- structures */

struct s_tag_function;
struct s_wind;

struct s_wind
{
    s_tag_function direction_mapping;
    s_tag_function speed_mapping;
    s_tag_function bend_mapping;
    s_tag_function oscillation_mapping;
    s_tag_function frequency_mapping;
    float gust_size;
    s_tag_reference gust_noise_bitmap;
    byte unused[12];
};
static_assert(sizeof(s_wind) == 0x84);

struct s_tag_function
{
    s_tag_data data;
};
static_assert(sizeof(s_tag_function) == 0x14);

