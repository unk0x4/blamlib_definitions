/* ---------- enums */

enum e_survival_mode_globals_survival_event_type
{
    _survival_mode_globals_survival_event_type_general = 0,
    _survival_mode_globals_survival_event_type_flavor = 1,
    _survival_mode_globals_survival_event_type_slayer = 2,
    _survival_mode_globals_survival_event_type_capture_the_flag = 3,
    _survival_mode_globals_survival_event_type_oddball = 4,
    _survival_mode_globals_survival_event_type_unused = 5,
    _survival_mode_globals_survival_event_type_king_of_the_hill = 6,
    _survival_mode_globals_survival_event_type_vip = 7,
    _survival_mode_globals_survival_event_type_juggernaut = 8,
    _survival_mode_globals_survival_event_type_territories = 9,
    _survival_mode_globals_survival_event_type_assault = 10,
    _survival_mode_globals_survival_event_type_infection = 11,
    _survival_mode_globals_survival_event_type_survival = 12,
    _survival_mode_globals_survival_event_type_unknown = 13
};

enum e_survival_mode_globals_survival_event_audience
{
    _survival_mode_globals_survival_event_audience_cause_player = 0,
    _survival_mode_globals_survival_event_audience_cause_team = 1,
    _survival_mode_globals_survival_event_audience_effect_player = 2,
    _survival_mode_globals_survival_event_audience_effect_team = 3,
    _survival_mode_globals_survival_event_audience_all = 4
};

enum e_survival_mode_globals_survival_event_team
{
    _survival_mode_globals_survival_event_team_none_player_only = 0,
    _survival_mode_globals_survival_event_team_cause = 1,
    _survival_mode_globals_survival_event_team_effect = 2,
    _survival_mode_globals_survival_event_team_all = 3
};

enum e_survival_mode_globals_survival_event_required_field
{
    _survival_mode_globals_survival_event_required_field_none = 0,
    _survival_mode_globals_survival_event_required_field_cause_player = 1,
    _survival_mode_globals_survival_event_required_field_cause_team = 2,
    _survival_mode_globals_survival_event_required_field_effect_player = 3,
    _survival_mode_globals_survival_event_required_field_effect_team = 4
};


/* ---------- structures */

struct s_survival_mode_globals_survival_event;
struct s_survival_mode_globals;

struct s_survival_mode_globals
{
    ulong unknown;
    s_tag_reference in_game_strings;
    s_tag_reference timer_sound;
    s_tag_reference timer_sound_zero;
    c_tag_block<s_survival_mode_globals_survival_event> survival_events;
    ulong unknown2;
    ulong unknown3;
};
static_assert(sizeof(s_survival_mode_globals) == 0x48);

struct s_survival_mode_globals_survival_event
{
    ushort flags;
    c_enum<e_survival_mode_globals_survival_event_type, short> type;
    string_id event;
    c_enum<e_survival_mode_globals_survival_event_audience, short> audience;
    short unknown;
    short unknown2;
    c_enum<e_survival_mode_globals_survival_event_team, short> team;
    string_id display_string;
    string_id display_medal;
    ulong unknown3;
    float display_duration;
    c_enum<e_survival_mode_globals_survival_event_required_field, short> required_field;
    c_enum<e_survival_mode_globals_survival_event_required_field, short> excluded_audience;
    c_enum<e_survival_mode_globals_survival_event_required_field, short> required_field2;
    c_enum<e_survival_mode_globals_survival_event_required_field, short> excluded_audience2;
    string_id primary_string;
    long primary_string_duration;
    string_id plural_display_string;
    float sound_delay_announcer_only;
    ushort sound_flags;
    short unknown4;
    s_tag_reference english_sound;
    s_tag_reference japanese_sound;
    s_tag_reference german_sound;
    s_tag_reference french_sound;
    s_tag_reference spanish_sound;
    s_tag_reference latin_american_spanish_sound;
    s_tag_reference italian_sound;
    s_tag_reference korean_sound;
    s_tag_reference chinese_traditional_sound;
    s_tag_reference chinese_simplified_sound;
    s_tag_reference portuguese_sound;
    s_tag_reference polish_sound;
    ulong unknown5;
    ulong unknown6;
    ulong unknown7;
    ulong unknown8;
};
static_assert(sizeof(s_survival_mode_globals_survival_event) == 0x10C);

