/* ---------- enums */

enum e_model_animation_graph_animation_inheritance_flags
{
    _model_animation_graph_animation_inheritance_flags_inherit_root_trans_scale_only_bit = 0,
    _model_animation_graph_animation_inheritance_flags_inherit_for_use_on_player_bit = 1
};

enum e_model_animation_graph_animation_private_flags
{
    _model_animation_graph_animation_private_flags_prepared_for_cache_bit = 0,
    _model_animation_graph_animation_private_flags_bit1_bit = 1,
    _model_animation_graph_animation_private_flags_imported_with_codec_compressors_bit = 2,
    _model_animation_graph_animation_private_flags_bit3_bit = 3,
    _model_animation_graph_animation_private_flags_written_to_cache_bit = 4,
    _model_animation_graph_animation_private_flags_animation_data_reordered_bit = 5,
    _model_animation_graph_animation_private_flags_ready_for_use_bit = 6
};

enum e_model_animation_graph_skeleton_node_skeleton_model_flags
{
    _model_animation_graph_skeleton_node_skeleton_model_flags_primary_model_bit = 0,
    _model_animation_graph_skeleton_node_skeleton_model_flags_secondary_model_bit = 1,
    _model_animation_graph_skeleton_node_skeleton_model_flags_local_root_bit = 2,
    _model_animation_graph_skeleton_node_skeleton_model_flags_left_hand_bit = 3,
    _model_animation_graph_skeleton_node_skeleton_model_flags_right_hand_bit = 4,
    _model_animation_graph_skeleton_node_skeleton_model_flags_left_arm_member_bit = 5
};

enum e_model_animation_graph_skeleton_node_skeleton_node_joint_flags
{
    _model_animation_graph_skeleton_node_skeleton_node_joint_flags_ball_socket_bit = 0,
    _model_animation_graph_skeleton_node_skeleton_node_joint_flags_hinge_bit = 1,
    _model_animation_graph_skeleton_node_skeleton_node_joint_flags_no_movement_bit = 2
};

enum e_model_animation_graph_animation_tag_reference_flags
{
    _model_animation_graph_animation_tag_reference_flags_allow_on_player_bit = 0,
    _model_animation_graph_animation_tag_reference_flags_left_arm_only_bit = 1,
    _model_animation_graph_animation_tag_reference_flags_right_arm_only_bit = 2,
    _model_animation_graph_animation_tag_reference_flags_first_person_only_bit = 3,
    _model_animation_graph_animation_tag_reference_flags_forward_only_bit = 4,
    _model_animation_graph_animation_tag_reference_flags_reverse_only_bit = 5,
    _model_animation_graph_animation_tag_reference_flags_bit6_bit = 6,
    _model_animation_graph_animation_tag_reference_flags_bit7_bit = 7,
    _model_animation_graph_animation_tag_reference_flags_bit8_bit = 8,
    _model_animation_graph_animation_tag_reference_flags_bit9_bit = 9,
    _model_animation_graph_animation_tag_reference_flags_bit10_bit = 10,
    _model_animation_graph_animation_tag_reference_flags_bit11_bit = 11,
    _model_animation_graph_animation_tag_reference_flags_bit12_bit = 12,
    _model_animation_graph_animation_tag_reference_flags_bit13_bit = 13,
    _model_animation_graph_animation_tag_reference_flags_bit14_bit = 14,
    _model_animation_graph_animation_tag_reference_flags_bit15_bit = 15
};

enum e_model_animation_graph_foot_markers_block_anchors
{
    _model_animation_graph_foot_markers_block_anchors_false = 0,
    _model_animation_graph_foot_markers_block_anchors_true = 1
};

enum e_model_animation_graph_animation_playback_flags
{
    _model_animation_graph_animation_playback_flags_disable_interpolation_in_bit = 0,
    _model_animation_graph_animation_playback_flags_disable_interpolation_out_bit = 1,
    _model_animation_graph_animation_playback_flags_disable_mode_ik_bit = 2,
    _model_animation_graph_animation_playback_flags_disable_weapon_ik_bit = 3,
    _model_animation_graph_animation_playback_flags_disable_weapon_aim1_st_person_bit = 4,
    _model_animation_graph_animation_playback_flags_disable_look_screen_bit = 5,
    _model_animation_graph_animation_playback_flags_disable_transition_adjustment_bit = 6
};

enum e_model_animation_graph_animation_compression
{
    _model_animation_graph_animation_compression_best_score = 0,
    _model_animation_graph_animation_compression_best_compression = 1,
    _model_animation_graph_animation_compression_best_accuracy = 2,
    _model_animation_graph_animation_compression_best_fullframe = 3,
    _model_animation_graph_animation_compression_best_small_keyframe = 4,
    _model_animation_graph_animation_compression_best_large_keyframe = 5
};

enum e_model_animation_graph_frame_type
{
    _model_animation_graph_frame_type_base = 0,
    _model_animation_graph_frame_type_overlay = 1,
    _model_animation_graph_frame_type_replacement = 2
};

enum e_animation_movement_data_type
{
    _animation_movement_data_type_none = 0,
    _animation_movement_data_type_dx_dy = 1,
    _animation_movement_data_type_dx_dy_dyaw = 2,
    _animation_movement_data_type_dx_dy_dz_dyaw = 3
};

enum e_model_animation_graph_animation_production_flags
{
    _model_animation_graph_animation_production_flags_do_not_monitor_changes_bit = 0,
    _model_animation_graph_animation_production_flags_verify_sound_events_bit = 1,
    _model_animation_graph_animation_production_flags_do_not_inherit_for_player_graphs_bit = 2
};

enum e_model_animation_graph_animation_internal_flags
{
    _model_animation_graph_animation_internal_flags_unused1_bit = 0,
    _model_animation_graph_animation_internal_flags_world_relative_bit = 1,
    _model_animation_graph_animation_internal_flags_unused2_bit = 2,
    _model_animation_graph_animation_internal_flags_unused3_bit = 3,
    _model_animation_graph_animation_internal_flags_unused4_bit = 4,
    _model_animation_graph_animation_internal_flags_compression_disabled_bit = 5,
    _model_animation_graph_animation_internal_flags_old_production_checksum_bit = 6,
    _model_animation_graph_animation_internal_flags_valid_production_checksum_bit = 7
};

enum e_model_animation_graph_animation_frame_event_type
{
    _model_animation_graph_animation_frame_event_type_primary_keyframe = 0,
    _model_animation_graph_animation_frame_event_type_secondary_keyframe = 1,
    _model_animation_graph_animation_frame_event_type_left_foot = 2,
    _model_animation_graph_animation_frame_event_type_right_foot = 3,
    _model_animation_graph_animation_frame_event_type_allow_interruption = 4,
    _model_animation_graph_animation_frame_event_type_transition_a = 5,
    _model_animation_graph_animation_frame_event_type_transition_b = 6,
    _model_animation_graph_animation_frame_event_type_transition_c = 7,
    _model_animation_graph_animation_frame_event_type_transition_d = 8,
    _model_animation_graph_animation_frame_event_type_both_feet_shuffle = 9,
    _model_animation_graph_animation_frame_event_type_body_impact = 10,
    _model_animation_graph_animation_frame_event_type_left_foot_lock = 11,
    _model_animation_graph_animation_frame_event_type_left_foot_unlock = 12,
    _model_animation_graph_animation_frame_event_type_right_foot_lock = 13,
    _model_animation_graph_animation_frame_event_type_right_foot_unlock = 14,
    _model_animation_graph_animation_frame_event_type_blend_range_marker = 15,
    _model_animation_graph_animation_frame_event_type_stride_expansion = 16,
    _model_animation_graph_animation_frame_event_type_stride_contraction = 17,
    _model_animation_graph_animation_frame_event_type_ragdoll_keyframe = 18,
    _model_animation_graph_animation_frame_event_type_drop_weapon_keyframe = 19,
    _model_animation_graph_animation_frame_event_type_match_a = 20,
    _model_animation_graph_animation_frame_event_type_match_b = 21,
    _model_animation_graph_animation_frame_event_type_match_c = 22,
    _model_animation_graph_animation_frame_event_type_match_d = 23,
    _model_animation_graph_animation_frame_event_type_jetpack_closed = 24,
    _model_animation_graph_animation_frame_event_type_jetpack_open = 25,
    _model_animation_graph_animation_frame_event_type_sound_event = 26,
    _model_animation_graph_animation_frame_event_type_effect_event = 27,
    _model_animation_graph_animation_frame_event_type_left_hand = 28,
    _model_animation_graph_animation_frame_event_type_right_hand = 29,
    _model_animation_graph_animation_frame_event_type_start_bamf = 30,
    _model_animation_graph_animation_frame_event_type_end_bamf = 31,
    _model_animation_graph_animation_frame_event_type_hide = 32
};

enum e_model_animation_graph_animation_dialogue_event_type
{
    _model_animation_graph_animation_dialogue_event_type_bump = 0,
    _model_animation_graph_animation_dialogue_event_type_dive = 1,
    _model_animation_graph_animation_dialogue_event_type_evade = 2,
    _model_animation_graph_animation_dialogue_event_type_lift = 3,
    _model_animation_graph_animation_dialogue_event_type_sigh = 4,
    _model_animation_graph_animation_dialogue_event_type_contempt = 5,
    _model_animation_graph_animation_dialogue_event_type_anger = 6,
    _model_animation_graph_animation_dialogue_event_type_fear = 7,
    _model_animation_graph_animation_dialogue_event_type_relief = 8,
    _model_animation_graph_animation_dialogue_event_type_sprint = 9,
    _model_animation_graph_animation_dialogue_event_type_sprint_end = 10,
    _model_animation_graph_animation_dialogue_event_type_ass_grabber = 11,
    _model_animation_graph_animation_dialogue_event_type_kill_ass = 12,
    _model_animation_graph_animation_dialogue_event_type_ass_grabbed = 13,
    _model_animation_graph_animation_dialogue_event_type_die_ass = 14
};

enum e_model_animation_graph_animation_object_space_parent_node_flags
{
    _model_animation_graph_animation_object_space_parent_node_flags_rotation_bit = 0,
    _model_animation_graph_animation_object_space_parent_node_flags_translation_bit = 1,
    _model_animation_graph_animation_object_space_parent_node_flags_scale_bit = 2,
    _model_animation_graph_animation_object_space_parent_node_flags_motion_root_bit = 3
};

enum e_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link
{
    _model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link_no_keyframe = 0,
    _model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link_keyframe_type_a = 1,
    _model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link_keyframe_type_b = 2,
    _model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link_keyframe_type_c = 3,
    _model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link_keyframe_type_d = 4
};

enum e_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags
{
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags_initiator_bit = 0,
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags_critical_participant_bit = 1,
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags_disabled_bit = 2,
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags_airborne_bit = 3,
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags_partner_bit = 4
};

enum e_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant_participant_flags
{
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant_participant_flags_none = 0,
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant_participant_flags_supports_any_type = 1,
    _model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant_participant_flags_disabled = 2
};

enum e_model_animation_graph_object_overlay_function_controls
{
    _model_animation_graph_object_overlay_function_controls_frame = 0,
    _model_animation_graph_object_overlay_function_controls_scale = 1
};

enum e_model_animation_graph_inheritance_list_flags
{
    _model_animation_graph_inheritance_list_flags_tighten_nodes_bit = 0
};

enum e_old_raw_page_flags
{
    _old_raw_page_flags_use_checksum_bit = 0,
    _old_raw_page_flags_in_resources_bit = 1,
    _old_raw_page_flags_in_textures_bit = 2,
    _old_raw_page_flags_in_textures_b_bit = 3,
    _old_raw_page_flags_in_audio_bit = 4,
    _old_raw_page_flags_in_resources_b_bit = 5,
    _old_raw_page_flags_in_mods_bit = 6,
    _old_raw_page_flags_use_checksum2_bit = 7,
    _old_raw_page_flags_location_mask_bit = 1
};

enum e_tag_resource_type_gen3
{
    _tag_resource_type_gen3_none = -1,
    _tag_resource_type_gen3_collision = 0,
    _tag_resource_type_gen3_bitmap = 1,
    _tag_resource_type_gen3_bitmap_interleaved = 2,
    _tag_resource_type_gen3_sound = 3,
    _tag_resource_type_gen3_animation = 4,
    _tag_resource_type_gen3_render_geometry = 5,
    _tag_resource_type_gen3_bink = 6,
    _tag_resource_type_gen3_pathfinding = 7
};


/* ---------- structures */

struct s_model_animation_graph_skeleton_node;
struct s_model_animation_graph_animation_tag_reference;
struct s_model_animation_graph_blend_screen;
struct s_model_animation_graph_foot_markers_block;
struct s_model_animation_graph_animation_frame_event;
struct s_model_animation_graph_animation_sound_event;
struct s_model_animation_graph_animation_effect_event;
struct s_model_animation_graph_animation_dialogue_event;
struct s_model_animation_graph_animation_object_space_parent_node;
struct s_model_animation_graph_animation_foot_tracking_block_foot_lock_cycle_block;
struct s_model_animation_graph_animation_foot_tracking_block;
struct s_model_animation_graph_animation_shared_animation_data;
struct s_model_animation_graph_animation;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_entry;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction_region;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_animation_set;
struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block;
struct s_model_animation_graph_mode_mode_ik_block;
struct s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant;
struct s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant;
struct s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action;
struct s_model_animation_graph_mode_weapon_class_block_sync_action_group;
struct s_model_animation_graph_mode_weapon_class_block;
struct s_model_animation_graph_mode_foot_tracking_defaults_block;
struct s_model_animation_graph_mode;
struct s_model_animation_graph_vehicle_suspension_block;
struct s_model_animation_graph_object_overlay;
struct s_model_animation_graph_inheritance_node_map_block;
struct s_model_animation_graph_inheritance_node_map_flag;
struct s_model_animation_graph_inheritance;
struct s_model_animation_graph_weapon_list_block;
struct s_model_animation_graph_additional_node_data_block;
struct s_resource_page;
struct s_resource_fixup_location;
struct s_resource_interop_location;
struct s_resource_data;
struct s_pageable_resource;
struct s_tag_resource_reference;
struct s_model_animation_graph_resource_group;
struct s_model_animation_graph;

struct s_model_animation_graph
{
    s_tag_reference parent_animation_graph;
    c_flags<e_model_animation_graph_animation_inheritance_flags, uchar> inheritance_flags;
    c_flags<e_model_animation_graph_animation_private_flags, uchar> private_flags;
    short animation_codec_pack;
    c_tag_block<s_model_animation_graph_skeleton_node> skeleton_nodes;
    c_tag_block<s_model_animation_graph_animation_tag_reference> sound_references;
    c_tag_block<s_model_animation_graph_animation_tag_reference> effect_references;
    c_tag_block<s_model_animation_graph_blend_screen> blend_screens;
    c_tag_block<s_model_animation_graph_foot_markers_block> foot_markers;
    c_tag_block<s_model_animation_graph_animation> animations;
    c_tag_block<s_model_animation_graph_mode> modes;
    c_tag_block<s_model_animation_graph_vehicle_suspension_block> vehicle_suspension;
    c_tag_block<s_model_animation_graph_object_overlay> object_overlays;
    c_tag_block<s_model_animation_graph_inheritance> inheritance_list;
    c_tag_block<s_model_animation_graph_weapon_list_block> weapon_list;
    u_int32 left_arm_nodes[8];
    u_int32 right_arm_nodes[8];
    s_tag_data last_import_results;
    c_tag_block<s_model_animation_graph_additional_node_data_block> additional_node_data;
    c_tag_block<s_model_animation_graph_resource_group> resource_groups;
};
static_assert(sizeof(s_model_animation_graph) == 0x104);

struct s_model_animation_graph_resource_group
{
    long member_count;
    s_tag_resource_reference resource_reference;
};
static_assert(sizeof(s_model_animation_graph_resource_group) == 0xC);

struct s_tag_resource_reference
{
    s_pageable_resource pageable_resource;
    long unused;
};
static_assert(sizeof(s_tag_resource_reference) == 0x8);

struct s_pageable_resource
{
    s_resource_page page;
    s_resource_data resource;
};
static_assert(sizeof(s_pageable_resource) == 0x6C);

struct s_resource_data
{
    s_tag_reference parent_tag;
    ushort salt;
    c_enum<e_tag_resource_type_gen3, char> resource_type;
    uchar flags;
    s_tag_data definition_data;
    cache_address definition_address;
    c_tag_block<s_resource_fixup_location> fixup_locations;
    c_tag_block<s_resource_interop_location> interop_locations;
    long unknown2;
};
static_assert(sizeof(s_resource_data) == 0x48);

struct s_resource_interop_location
{
    cache_address address;
    long resource_structure_type_index;
};
static_assert(sizeof(s_resource_interop_location) == 0x8);

struct s_resource_fixup_location
{
    ulong block_offset;
    cache_address address;
    long type;
    long offset;
    long raw_address;
};
static_assert(sizeof(s_resource_fixup_location) == 0x8);

struct s_resource_page
{
    short salt;
    c_flags<e_old_raw_page_flags, uchar> old_flags;
    char compression_codec_index;
    long index;
    ulong compressed_block_size;
    ulong uncompressed_block_size;
    long crc_checksum;
    ulong unknown_size;
    ulong unknown4;
    ulong unknown5;
    ulong unknown6;
};
static_assert(sizeof(s_resource_page) == 0x24);

struct s_model_animation_graph_additional_node_data_block
{
    string_id node_name;
    real_quaternion default_rotation;
    real_point3d default_translation;
    float default_scale;
    real_point3d minimum_bounds;
    real_point3d maximum_bounds;
};
static_assert(sizeof(s_model_animation_graph_additional_node_data_block) == 0x3C);

struct s_model_animation_graph_weapon_list_block
{
    string_id weapon_name;
    string_id weapon_class;
};
static_assert(sizeof(s_model_animation_graph_weapon_list_block) == 0x8);

struct s_model_animation_graph_inheritance
{
    s_tag_reference inherited_graph;
    c_tag_block<s_model_animation_graph_inheritance_node_map_block> node_map;
    c_tag_block<s_model_animation_graph_inheritance_node_map_flag> node_map_flags;
    float root_z_offset;
    c_flags<e_model_animation_graph_inheritance_list_flags, long> flags;
};
static_assert(sizeof(s_model_animation_graph_inheritance) == 0x30);

struct s_model_animation_graph_inheritance_node_map_flag
{
    long local_node_flags;
};
static_assert(sizeof(s_model_animation_graph_inheritance_node_map_flag) == 0x4);

struct s_model_animation_graph_inheritance_node_map_block
{
    short local_node;
};
static_assert(sizeof(s_model_animation_graph_inheritance_node_map_block) == 0x2);

struct s_model_animation_graph_object_overlay
{
    string_id label;
    short graph_index;
    short animation;
    byte unused1[2];
    c_enum<e_model_animation_graph_object_overlay_function_controls, short> function_controls;
    string_id function;
    byte unused2[4];
};
static_assert(sizeof(s_model_animation_graph_object_overlay) == 0x14);

struct s_model_animation_graph_vehicle_suspension_block
{
    string_id label;
    short graph_index;
    short animation;
    string_id marker_name;
    float mass_point_offset;
    float full_extension_ground_depth;
    float full_compression_ground_depth;
    string_id region_name;
    float destroyed_mass_point_offset;
    float destroyed_full_extension_ground_depth;
    float destroyed_full_compression_ground_depth;
};
static_assert(sizeof(s_model_animation_graph_vehicle_suspension_block) == 0x28);

struct s_model_animation_graph_mode
{
    string_id name;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block> weapon_class;
    c_tag_block<s_model_animation_graph_mode_mode_ik_block> mode_ik;
    c_tag_block<s_model_animation_graph_mode_foot_tracking_defaults_block> foot_defaults;
};
static_assert(sizeof(s_model_animation_graph_mode) == 0x28);

struct s_model_animation_graph_mode_foot_tracking_defaults_block
{
    short foot;
    short default_state;
};
static_assert(sizeof(s_model_animation_graph_mode_foot_tracking_defaults_block) == 0x4);

struct s_model_animation_graph_mode_weapon_class_block
{
    string_id label;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block> weapon_type;
    c_tag_block<s_model_animation_graph_mode_mode_ik_block> weapon_ik;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_sync_action_group> sync_action_groups;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block) == 0x28);

struct s_model_animation_graph_mode_weapon_class_block_sync_action_group
{
    string_id name;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action> sync_actions;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_sync_action_group) == 0x10);

struct s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action
{
    string_id name;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant> same_type_participants;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant> other_participants;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action) == 0x1C);

struct s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant
{
    c_enum<e_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant_participant_flags, long> flags;
    s_tag_reference object_type;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_other_participant) == 0x14);

struct s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant
{
    c_flags<e_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant_participant_flags, long> flags;
    short graph_index;
    short animation;
    real_point3d start_offset;
    real_vector3d start_facing;
    real_vector3d end_offset;
    float time_until_hurt;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_sync_action_group_sync_action_same_type_participant) == 0x30);

struct s_model_animation_graph_mode_mode_ik_block
{
    string_id marker;
    string_id attach_to_marker;
};
static_assert(sizeof(s_model_animation_graph_mode_mode_ik_block) == 0x8);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block
{
    string_id label;
    s_model_animation_graph_mode_weapon_class_block_weapon_type_block_animation_set set;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block) == 0x34);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_animation_set
{
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_entry> actions;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_entry> overlays;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block> death_and_damage;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition> transitions;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_animation_set) == 0x30);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition
{
    string_id full_name;
    string_id state_name;
    short unknown;
    char index_a;
    char index_b;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination> destinations;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition) == 0x18);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination
{
    string_id full_name;
    string_id mode_name;
    string_id state_name;
    c_enum<e_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination_frame_event_link, char> frame_event_link;
    char unknown;
    char index_a;
    char index_b;
    short graph_index;
    short animation;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_transition_destination) == 0x14);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block
{
    string_id label;
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction> directions;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block) == 0x10);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction
{
    c_tag_block<s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction_region> regions;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction) == 0xC);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction_region
{
    short graph_index;
    short animation;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_death_and_damage_block_direction_region) == 0x4);

struct s_model_animation_graph_mode_weapon_class_block_weapon_type_block_entry
{
    string_id label;
    short graph_index;
    short animation;
};
static_assert(sizeof(s_model_animation_graph_mode_weapon_class_block_weapon_type_block_entry) == 0x8);

struct s_model_animation_graph_animation
{
    string_id name;
    float weight;
    short loop_frame_index;
    c_flags<e_model_animation_graph_animation_playback_flags, ushort> playback_flags;
    s_model_animation_graph_animation_shared_animation_data animation_data;
};
static_assert(sizeof(s_model_animation_graph_animation) == 0x88);

struct s_model_animation_graph_animation_shared_animation_data
{
    char blend_screen;
    c_enum<e_model_animation_graph_animation_compression, char> desired_compression;
    c_enum<e_model_animation_graph_animation_compression, char> current_compression;
    char node_count;
    short frame_count;
    c_enum<e_model_animation_graph_frame_type, char> animation_type;
    c_enum<e_animation_movement_data_type, char> frame_info_type;
    c_flags<e_model_animation_graph_animation_production_flags, ushort> production_flags;
    c_flags<e_model_animation_graph_animation_internal_flags, ushort> internal_flags;
    long node_list_checksum;
    long production_checksum;
    short unknown2;
    short unknown3;
    short previous_variant_sibling;
    short next_variant_sibling;
    short resource_group_index;
    short resource_group_member_index;
    c_tag_block<s_model_animation_graph_animation_frame_event> frame_events;
    c_tag_block<s_model_animation_graph_animation_sound_event> sound_events;
    c_tag_block<s_model_animation_graph_animation_effect_event> effect_events;
    c_tag_block<s_model_animation_graph_animation_dialogue_event> dialogue_events;
    c_tag_block<s_model_animation_graph_animation_object_space_parent_node> object_space_parent_nodes;
    c_tag_block<s_model_animation_graph_animation_foot_tracking_block> foot_tracking;
    real_vector3d heading;
    float heading_angle;
    float translation_magnitude;
};
static_assert(sizeof(s_model_animation_graph_animation_shared_animation_data) == 0x7C);

struct s_model_animation_graph_animation_foot_tracking_block
{
    short foot;
    byte pad[2];
    c_tag_block<s_model_animation_graph_animation_foot_tracking_block_foot_lock_cycle_block> foot_lock_cycles;
};
static_assert(sizeof(s_model_animation_graph_animation_foot_tracking_block) == 0x10);

struct s_model_animation_graph_animation_foot_tracking_block_foot_lock_cycle_block
{
    short started_locking;
    short locked;
    short started_unlocking;
    short unlocked;
    real_point3d lock_point;
};
static_assert(sizeof(s_model_animation_graph_animation_foot_tracking_block_foot_lock_cycle_block) == 0x14);

struct s_model_animation_graph_animation_object_space_parent_node
{
    short node_index;
    c_flags<e_model_animation_graph_animation_object_space_parent_node_flags, ushort> flags;
    short rotation_x;
    short rotation_y;
    short rotation_z;
    short rotation_w;
    real_point3d default_translation;
    float default_scale;
};
static_assert(sizeof(s_model_animation_graph_animation_object_space_parent_node) == 0x1C);

struct s_model_animation_graph_animation_dialogue_event
{
    c_enum<e_model_animation_graph_animation_dialogue_event_type, short> event_type;
    short frame;
};
static_assert(sizeof(s_model_animation_graph_animation_dialogue_event) == 0x4);

struct s_model_animation_graph_animation_effect_event
{
    short effect;
    short frame;
    string_id marker_name;
    uchar damage_effect_reporting_type;
    byte pad[3];
};
static_assert(sizeof(s_model_animation_graph_animation_effect_event) == 0xC);

struct s_model_animation_graph_animation_sound_event
{
    short sound;
    short frame;
    string_id marker_name;
};
static_assert(sizeof(s_model_animation_graph_animation_sound_event) == 0x8);

struct s_model_animation_graph_animation_frame_event
{
    c_enum<e_model_animation_graph_animation_frame_event_type, short> type;
    short frame;
};
static_assert(sizeof(s_model_animation_graph_animation_frame_event) == 0x4);

struct s_model_animation_graph_foot_markers_block
{
    string_id foot_marker;
    real_bounds foot_bounds;
    string_id ankle_marker;
    real_bounds ankle_bounds;
    c_enum<e_model_animation_graph_foot_markers_block_anchors, short> anchors;
    byte pad[2];
};
static_assert(sizeof(s_model_animation_graph_foot_markers_block) == 0x1C);

struct s_model_animation_graph_blend_screen
{
    string_id label;
    real right_yaw_per_frame;
    real left_yaw_per_frame;
    short right_frame_count;
    short left_frame_count;
    real down_pitch_per_frame;
    real up_pitch_per_frame;
    short down_pitch_frame_count;
    short up_pitch_frame_count;
};
static_assert(sizeof(s_model_animation_graph_blend_screen) == 0x1C);

struct s_model_animation_graph_animation_tag_reference
{
    s_tag_reference reference;
    c_flags<e_model_animation_graph_animation_tag_reference_flags, ushort> flags;
    short internal_flags;
};
static_assert(sizeof(s_model_animation_graph_animation_tag_reference) == 0x14);

struct s_model_animation_graph_skeleton_node
{
    string_id name;
    short next_sibling_node_index;
    short first_child_node_index;
    short parent_node_index;
    c_flags<e_model_animation_graph_skeleton_node_skeleton_model_flags, uchar> model_flags;
    c_flags<e_model_animation_graph_skeleton_node_skeleton_node_joint_flags, uchar> node_joint_flags;
    real_vector3d base_vector;
    float vector_range;
    float z_position;
};
static_assert(sizeof(s_model_animation_graph_skeleton_node) == 0x20);

