/* ---------- enums */

enum e_custom_object_custom_object_type
{
    _custom_object_custom_object_type_cube = 0
};


/* ---------- structures */

struct s_custom_object;

struct s_custom_object : s_game_object
{
    c_enum<e_custom_object_custom_object_type, long> custom_object_type;
};
static_assert(sizeof(s_custom_object) == 0x124);

