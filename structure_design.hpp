/* ---------- enums */

enum e_structure_design_soft_ceiling_type
{
    _structure_design_soft_ceiling_type_acceleration = 0,
    _structure_design_soft_ceiling_type_soft_kill = 1,
    _structure_design_soft_ceiling_type_slip_surface = 2
};


/* ---------- structures */

struct s_hkp_referenced_object;
struct s_code_info;
struct s_hk_array_base;
struct s_hkp_mopp_code;
struct s_tag_hkp_mopp_code;
struct s_structure_design_soft_ceiling_triangle;
struct s_structure_design_soft_ceiling;
struct s_structure_design_water_group;
struct s_structure_design_water_plane;
struct s_structure_design_water_debug_triangle;
struct s_structure_design_water_instance;
struct s_structure_design;

struct s_structure_design
{
    long version;
    c_tag_block<s_tag_hkp_mopp_code> soft_ceiling_mopp_codes;
    c_tag_block<s_structure_design_soft_ceiling> soft_ceilings;
    c_tag_block<s_tag_hkp_mopp_code> water_mopp_codes;
    c_tag_block<s_structure_design_water_group> water_groups;
    c_tag_block<s_structure_design_water_instance> water_instances;
    ulong unknown2;
};
static_assert(sizeof(s_structure_design) == 0x44);

struct s_structure_design_water_instance
{
    short water_name_index;
    byte unused[2];
    real_vector3d flow_velocity;
    float flow_force;
    c_tag_block<s_structure_design_water_plane> water_planes;
    c_tag_block<s_structure_design_water_debug_triangle> water_debug_triangles;
};
static_assert(sizeof(s_structure_design_water_instance) == 0x2C);

struct s_structure_design_water_debug_triangle
{
    real_point3d point1;
    real_point3d point2;
    real_point3d point3;
};
static_assert(sizeof(s_structure_design_water_debug_triangle) == 0x24);

struct s_structure_design_water_plane
{
    real_plane3d plane;
};
static_assert(sizeof(s_structure_design_water_plane) == 0x10);

struct s_structure_design_water_group
{
    string_id name;
};
static_assert(sizeof(s_structure_design_water_group) == 0x4);

struct s_structure_design_soft_ceiling
{
    string_id name;
    c_enum<e_structure_design_soft_ceiling_type, short> type;
    byte unused[2];
    c_tag_block<s_structure_design_soft_ceiling_triangle> soft_ceiling_triangles;
};
static_assert(sizeof(s_structure_design_soft_ceiling) == 0x14);

struct s_structure_design_soft_ceiling_triangle
{
    real_plane3d plane;
    real_point3d bounding_sphere_center;
    float bounding_sphere_radius;
    real_point3d point1;
    real_point3d point2;
    real_point3d point3;
};
static_assert(sizeof(s_structure_design_soft_ceiling_triangle) == 0x44);

struct s_tag_hkp_mopp_code : s_hkp_mopp_code
{
    c_tag_block<uchar> data;
    byte padding3[4];
};
static_assert(sizeof(s_tag_hkp_mopp_code) == 0x40);

struct s_hkp_mopp_code
{
    ulong vf_table_address;
    s_hkp_referenced_object referenced_object;
    s_tag_data padding1;
    s_code_info info;
    s_hk_array_base array_base;
    s_tag_data padding2;
};
static_assert(sizeof(s_hkp_mopp_code) == 0x30);

struct s_hk_array_base
{
    ulong data_address;
    ulong size;
    ulong capacity_and_flags;
};
static_assert(sizeof(s_hk_array_base) == 0xC);

struct s_code_info
{
    real_quaternion offset;
};
static_assert(sizeof(s_code_info) == 0x10);

struct s_hkp_referenced_object
{
    ushort size_and_flags;
    ushort reference_count;
};
static_assert(sizeof(s_hkp_referenced_object) == 0x4);

