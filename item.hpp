/* ---------- enums */

enum e_item_item_flag_bits
{
    _item_item_flag_bits_always_maintains_z_up_bit = 0,
    _item_item_flag_bits_destroyed_by_explosions_bit = 1,
    _item_item_flag_bits_unaffected_by_gravity_bit = 2
};


/* ---------- structures */

struct s_item;

struct s_item : s_game_object
{
    c_flags<e_item_item_flag_bits, long> item_flags;
    short old_message_index;
    short sort_order;
    float old_multiplayer_on_ground_scale;
    float old_campaign_on_ground_scale;
    string_id pickup_message;
    string_id swap_message;
    string_id pickup_or_dual_wield_message;
    string_id swap_or_dual_wield_message;
    string_id picked_up_message;
    string_id switch_to_message;
    string_id switch_to_from_ai_message;
    string_id all_weapons_empty_message;
    s_tag_reference collision_sound;
    c_tag_block<s_tag_reference_block> predicted_bitmaps;
    s_tag_reference detonation_damage_effect;
    float detonation_delay_min;
    float detonation_delay_max;
    s_tag_reference detonating_effect;
    s_tag_reference detonation_effect;
    float campaign_ground_scale;
    float multiplayer_ground_scale;
    float small_unit_hold_scale;
    float small_unit_holster_scale;
    float medium_unit_hold_scale;
    float medium_unit_holster_scale;
    float large_unit_hold_scale;
    float large_unit_holster_scale;
    float huge_unit_hold_scale;
    float huge_unit_holster_scale;
    float grounded_friction_length;
    float grounded_friction_unknown;
};
static_assert(sizeof(s_item) == 0x1D4);

